#include "test_string.h"

#include <std/string.h>

TEST_FUNCTION(StringLengthTest)
{
    u32 NullLength = StringLength((char*)NULL);

    if (NullLength != 0)
    {
        TEST_RETURN_FAILURE("Null string does not have a length of 0");
    }

    char* String =
        "This is a test to figure out if this test function can actually get the length of this string";
    u32 KnownStringLength      = 93;
    u32 CalculatedStringLength = StringLength(String);

    if (KnownStringLength != CalculatedStringLength)
    {
        TEST_RETURN_FAILURE("The calculated length is %u32, but it should be %u32", CalculatedStringLength,
                            KnownStringLength);
    }

    char* LongerAllocationLength = (char*)Allocator->Allocate(sizeof(char) * 1024);

    engine_memset(LongerAllocationLength, 'a', 10);
    LongerAllocationLength[10] = 0;

    u32 Length = StringLength(LongerAllocationLength);

    if (Length != 10)
    {
        TEST_RETURN_FAILURE(
            "Failed to get the correct length of a string that is zeroed before the end of the allocated block");
    }

    Allocator->Free(LongerAllocationLength, sizeof(char) * 1024);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StringsEqualTest)
{
    array<string> Names;
    CreateArray(&Names, Allocator);

    Add(&Names, CreateString("David"));
    Add(&Names, CreateString("Greg"));
    Add(&Names, CreateString("Gordon"));
    Add(&Names, CreateString("Thomas"));
    Add(&Names, CreateString("Daniel"));
    Add(&Names, CreateString("Anthony"));
    Add(&Names, CreateString("Nick"));
    Add(&Names, CreateString("John"));
    Add(&Names, CreateString("Jordon"));
    Add(&Names, CreateString("David"));

    b32 SameNameList[10][10] = {
        { true, false, false, false, false, false, false, false, false, true },
        { false, true, false, false, false, false, false, false, false, false },
        { false, false, true, false, false, false, false, false, false, false },
        { false, false, false, true, false, false, false, false, false, false },
        { false, false, false, false, true, false, false, false, false, false },
        { false, false, false, false, false, true, false, false, false, false },
        { false, false, false, false, false, false, true, false, false, false },
        { false, false, false, false, false, false, false, true, false, false },
        { false, false, false, false, false, false, false, false, true, false },
        { true, false, false, false, false, false, false, false, false, true },
    };

    u32 NamesLength = Names.Size;

    for (u32 NamesIndex = 0; NamesIndex < NamesLength; ++NamesIndex)
    {
        b32*   List        = SameNameList[NamesIndex];
        string CurrentName = Names[NamesIndex];

        for (u32 CheckIndex = 0; CheckIndex < NamesLength; ++CheckIndex)
        {
            string Check            = Names[CheckIndex];
            b32    KnownResult      = List[CheckIndex];
            b32    CalculatedResult = StringsEqual(CurrentName, Check);

            if (CalculatedResult != KnownResult)
            {
                TEST_RETURN_FAILURE(
                    "The calculated result, %b32, is not the same as the known result, %b32, for the strings: \"%str\" and \"%str\"",
                    CalculatedResult, KnownResult, CurrentName, Check);
            }
        }
    }

    for (u64 NameIndex = 0; NameIndex < Names.Size; ++NameIndex)
    {
        FreeString(Names + NameIndex, Allocator);
    }

    FreeArray(&Names);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(CopyStringTest)
{
    string Value1 = CreateString("This is a test");
    string CopyValue1;

    CopyString(&CopyValue1, Value1, Allocator);

    if (!StringsEqual(Value1, CopyValue1))
    {
        TEST_RETURN_FAILURE("Value1('%str') does not equal CopyValue1('%str')", Value1, CopyValue1);
    }

    string Value2 = CreateString("This is another test that should work");
    string CopyValue2;

    CopyString(&CopyValue2, Value2, Allocator);

    if (!StringsEqual(Value2, CopyValue2))
    {
        TEST_RETURN_FAILURE("Value1('%str') does not equal CopyValue1('%str')", Value2, CopyValue2);
    }

    FreeString(&CopyValue1, Allocator);
    FreeString(&CopyValue2, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(EndsWithTest)
{
    string TestShortString = CreateString("test");
    string TestLongString  = CreateString("test this is a longer string test");

    if (EndsWith(TestShortString, TestLongString))
    {
        TEST_RETURN_FAILURE("Failed to return false when the first string was shorter than the second");
    }

    string String = CreateString("This is a test");

    for (u64 Index = 1; Index < (String.Length + 1); ++Index)
    {
        string TestString;
        string StringAtIndex;

        StringAt(&StringAtIndex, String, Index);
        CopyString(&TestString, StringAtIndex, Allocator);

        if (!EndsWith(String, TestString))
        {
            TEST_RETURN_FAILURE("The String: %str does not end with: %str", String, TestString);
        }

        FreeString(&TestString, Allocator);
    }

    string WrongString = CreateString("This");
    if (EndsWith(String, WrongString))
    {
        TEST_RETURN_FAILURE("Returned true on a known false value");
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StartsWithTest)
{
    string String = CreateString("This is a test");
    string TestString;
    TestString.Buffer   = String.Buffer;
    TestString.Capacity = String.Capacity;

    for (u64 Index = 1; Index < (String.Length + 1); ++Index)
    {
        TestString.Length = Index;

        if (!StartsWith(String, TestString))
        {
            TEST_RETURN_FAILURE("The String: %str does not start with: %str", String, TestString);
        }
    }

    if (StartsWith(String, CreateString("test")))
    {
        TEST_RETURN_FAILURE("Returned true on a known false value");
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(UnsignedIntegerToStringTest)
{
    string ZeroString;
    UnsignedIntegerToString(&ZeroString, 0, Allocator);

    if (!StringsEqual(ZeroString, CreateString("0")))
    {
        TEST_RETURN_FAILURE("Failed to return zero string");
    }

    FreeString(&ZeroString, Allocator);

    u64    Values[]       = { 100, 123, 5454, 786454, 34556 };
    string ValueStrings[] = { CreateString("100"), CreateString("123"), CreateString("5454"),
                              CreateString("786454"), CreateString("34556") };
    u32    ValuesLength   = 5;

    for (u32 ValueIndex = 0; ValueIndex < ValuesLength; ++ValueIndex)
    {
        string ValueToString;
        UnsignedIntegerToString(&ValueToString, Values[ValueIndex], Allocator);

        if (!StringsEqual(ValueToString, ValueStrings[ValueIndex]))
        {
            TEST_RETURN_FAILURE("The value %u64 did not convert to string correctly, result: %str",
                                Values[ValueIndex], ValueToString);
        }

        FreeString(&ValueToString, Allocator);
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(SignedIntegerToStringTest)
{
    string ZeroString;
    SignedIntegerToString(&ZeroString, 0, Allocator);

    if (!StringsEqual(ZeroString, CreateString("0")))
    {
        TEST_RETURN_FAILURE("Failed to return zero string");
    }

    FreeString(&ZeroString, Allocator);

    s64    Values[]       = { 100, -123, 5454, -786454, 34556 };
    string ValueStrings[] = { CreateString("100"), CreateString("-123"), CreateString("5454"),
                              CreateString("-786454"), CreateString("34556") };
    u32    ValuesLength   = 5;

    for (u32 ValueIndex = 0; ValueIndex < ValuesLength; ++ValueIndex)
    {
        string ValueToString;
        SignedIntegerToString(&ValueToString, Values[ValueIndex], Allocator);

        if (!StringsEqual(ValueToString, ValueStrings[ValueIndex]))
        {
            TEST_RETURN_FAILURE("The value %s64 did not convert to string correctly, result: %str",
                                Values[ValueIndex], ValueToString);
        }

        FreeString(&ValueToString, Allocator);
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(FloatToStringTest)
{
    string ZeroString;
    FloatToString(&ZeroString, 0, Allocator);

    if (!StringsEqual(ZeroString, CreateString("0")))
    {
        TEST_RETURN_FAILURE("Failed to return zero string");
    }

    FreeString(&ZeroString, Allocator);

    f64 Values[]     = { .09528005, -.34550005, 34.05003400, -9600.05200000345, 74.05840000345, 100.0 };
    u32 ValuesLength = 6;

    for (u32 ValueIndex = 0; ValueIndex < ValuesLength; ++ValueIndex)
    {
        string ValueToString;
        FloatToString(&ValueToString, Values[ValueIndex], Allocator);

        FreeString(&ValueToString, Allocator);
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StringToUnsignedIntegerTest)
{
    char* Strings[] = { "0", "300000", "573432685", "2054000", "12700340" };
    u64   Values[]  = { 0, 300000, 573432685, 2054000, 12700340 };
    u32   Length    = 5;

    for (u32 SubTestIndex = 0; SubTestIndex < Length; ++SubTestIndex)
    {
        string TestString = CreateString(Strings[SubTestIndex], StringLength(Strings[SubTestIndex]));
        u64    CalculatedValue;
        StringToUnsignedInteger(&CalculatedValue, TestString);
        u64 KnownValue = Values[SubTestIndex];

        if (CalculatedValue != KnownValue)
        {
            TEST_RETURN_FAILURE("The known value, %u64. and calculated value, %u64, are different",
                                KnownValue, CalculatedValue);
        }
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StringToSignedIntegerTest)
{
    char* Strings[] = { "0", "-34000", "5700", "-10700", "12700" };
    s64   Values[]  = { 0, -34000, 5700, -10700, 12700 };
    u32   Length    = 5;

    for (u32 SubTestIndex = 0; SubTestIndex < Length; ++SubTestIndex)
    {
        string TestString = CreateString(Strings[SubTestIndex], StringLength(Strings[SubTestIndex]));
        s64    CalculatedValue;
        StringToSignedInteger(&CalculatedValue, TestString);
        s64 KnownValue = Values[SubTestIndex];

        if (CalculatedValue != KnownValue)
        {
            TEST_RETURN_FAILURE("The known value, %s64. and calculated value, %s64, are different",
                                KnownValue, CalculatedValue);
        }
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StringToFloatTest)
{
    char* Strings[] = { "0", "-34.0004356", "5700.6846322", "-10.700546", "1270.76543" };
    f64   Values[]  = { 0, -34.0004356, 5700.6846322, -10.700546, 1270.76543 };
    u32   Length    = 5;

    for (u32 SubTestIndex = 0; SubTestIndex < Length; ++SubTestIndex)
    {
        string TestString = CreateString(Strings[SubTestIndex], StringLength(Strings[SubTestIndex]));
        f64    CalculatedValue;
        StringToFloat(&CalculatedValue, TestString);
        f64 KnownValue = Values[SubTestIndex];

        if (!CompareFloat(CalculatedValue, KnownValue, .001))
        {
            TEST_RETURN_FAILURE("The known value, %f64. and calculated value, %f64, are different",
                                KnownValue, CalculatedValue);
        }
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(FormatStringTest)
{
    string TestStringValue = CreateString("This is a test");

    s8  S8TestValue1  = 120;
    s8  S8TestValue2  = -120;
    s16 S16TestValue1 = 1200;
    s16 S16TestValue2 = -1200;
    s32 S32TestValue1 = 12000;
    s32 S32TestValue2 = -12000;
    s64 S64TestValue1 = 120000;
    s64 S64TestValue2 = -120000;

    string S8KnownValue1  = CreateString("120");
    string S8KnownValue2  = CreateString("-120");
    string S16KnownValue1 = CreateString("1200");
    string S16KnownValue2 = CreateString("-1200");
    string S32KnownValue1 = CreateString("12000");
    string S32KnownValue2 = CreateString("-12000");
    string S64KnownValue1 = CreateString("120000");
    string S64KnownValue2 = CreateString("-120000");

    u8  U8TestValue  = 120;
    u16 U16TestValue = 1200;
    u32 U32TestValue = 12000;
    u64 U64TestValue = 120000;

    string U8KnownValue  = CreateString("120");
    string U16KnownValue = CreateString("1200");
    string U32KnownValue = CreateString("12000");
    string U64KnownValue = CreateString("120000");

    f32 F32TestValue = -10.0f;
    f64 F64TestValue = -1000.0;

    string F32StartValue = CreateString("-10.0");
    string F64StartValue = CreateString("-1000.0");

    string FormattedStringCalculatedValue;
    FormatString(&FormattedStringCalculatedValue, "%str", Allocator, TestStringValue);

    if (!StringsEqual(FormattedStringCalculatedValue, TestStringValue))
    {
        TEST_RETURN_FAILURE("'This is a test' could not be formatted into another string");
    }

    FreeString(&FormattedStringCalculatedValue, Allocator);

    string FormattedS8CalculatedValue1;
    FormatString(&FormattedS8CalculatedValue1, "%s8", Allocator, S8TestValue1);

    if (!StringsEqual(FormattedS8CalculatedValue1, S8KnownValue1))
    {
        TEST_RETURN_FAILURE("signed 120 could not be formatted into a string");
    }

    FreeString(&FormattedS8CalculatedValue1, Allocator);

    string FormattedS8CalculatedValue2;
    FormatString(&FormattedS8CalculatedValue2, "%s8", Allocator, S8TestValue2);

    if (!StringsEqual(FormattedS8CalculatedValue2, S8KnownValue2))
    {
        TEST_RETURN_FAILURE("signed -120 could not be formatted into a string");
    }

    FreeString(&FormattedS8CalculatedValue2, Allocator);

    string FormattedS16CalculatedValue1;
    FormatString(&FormattedS16CalculatedValue1, "%s16", Allocator, S16TestValue1);

    if (!StringsEqual(FormattedS16CalculatedValue1, S16KnownValue1))
    {
        TEST_RETURN_FAILURE("signed 1200 could not be formatted into a string");
    }

    FreeString(&FormattedS16CalculatedValue1, Allocator);

    string FormattedS16CalculatedValue2;
    FormatString(&FormattedS16CalculatedValue2, "%s16", Allocator, S16TestValue2);

    if (!StringsEqual(FormattedS16CalculatedValue2, S16KnownValue2))
    {
        TEST_RETURN_FAILURE("signed -1200 could not be formatted into a string");
    }

    FreeString(&FormattedS16CalculatedValue2, Allocator);

    string FormattedS32CalculatedValue1;
    FormatString(&FormattedS32CalculatedValue1, "%s32", Allocator, S32TestValue1);

    if (!StringsEqual(FormattedS32CalculatedValue1, S32KnownValue1))
    {
        TEST_RETURN_FAILURE("signed 12000 could not be formatted into a string");
    }

    FreeString(&FormattedS32CalculatedValue1, Allocator);

    string FormattedS32CalculatedValue2;
    FormatString(&FormattedS32CalculatedValue2, "%s32", Allocator, S32TestValue2);

    if (!StringsEqual(FormattedS32CalculatedValue2, S32KnownValue2))
    {
        TEST_RETURN_FAILURE("signed -12000 could not be formatted into a string");
    }

    FreeString(&FormattedS32CalculatedValue2, Allocator);

    string FormattedS64CalculatedValue1;
    FormatString(&FormattedS64CalculatedValue1, "%s64", Allocator, S64TestValue1);

    if (!StringsEqual(FormattedS64CalculatedValue1, S64KnownValue1))
    {
        TEST_RETURN_FAILURE("signed 120000 could not be formatted into a string");
    }

    FreeString(&FormattedS64CalculatedValue1, Allocator);

    string FormattedS64CalculatedValue2;
    FormatString(&FormattedS64CalculatedValue2, "%s64", Allocator, S64TestValue2);

    if (!StringsEqual(FormattedS64CalculatedValue2, S64KnownValue2))
    {
        TEST_RETURN_FAILURE("signed -120000 could not be formatted into a string");
    }

    FreeString(&FormattedS64CalculatedValue2, Allocator);

    u64    Capacity = 2048;
    string FormattedU8CalculatedValue;
    FormattedU8CalculatedValue.Buffer   = (char*)Allocator->Allocate(Capacity);
    FormattedU8CalculatedValue.Length   = 0;
    FormattedU8CalculatedValue.Capacity = Capacity;
    FormatStringToBuffer(&FormattedU8CalculatedValue, "%u8", Allocator, U8TestValue);

    if (!StringsEqual(FormattedU8CalculatedValue, U8KnownValue))
    {
        TEST_RETURN_FAILURE("unsigned 120 could not be formatted into a string");
    }

    FreeString(&FormattedU8CalculatedValue, Allocator);

    string FormattedU16CalculatedValue;
    FormattedU16CalculatedValue.Buffer   = (char*)Allocator->Allocate(Capacity);
    FormattedU16CalculatedValue.Length   = 0;
    FormattedU16CalculatedValue.Capacity = Capacity;
    FormatStringToBuffer(&FormattedU16CalculatedValue, "%u16", Allocator, U16TestValue);

    if (!StringsEqual(FormattedU16CalculatedValue, U16KnownValue))
    {
        TEST_RETURN_FAILURE("unsigned 1200 could not be formatted into a string");
    }

    FreeString(&FormattedU16CalculatedValue, Allocator);

    string FormattedU32CalculatedValue;
    FormattedU32CalculatedValue.Buffer   = (char*)Allocator->Allocate(Capacity);
    FormattedU32CalculatedValue.Length   = 0;
    FormattedU32CalculatedValue.Capacity = Capacity;
    FormatStringToBuffer(&FormattedU32CalculatedValue, "%u32", Allocator, U32TestValue);

    if (!StringsEqual(FormattedU32CalculatedValue, U32KnownValue))
    {
        TEST_RETURN_FAILURE("unsigned 12000 could not be formatted into a string");
    }

    FreeString(&FormattedU32CalculatedValue, Allocator);

    string FormattedU64CalculatedValue;
    FormattedU64CalculatedValue.Buffer   = (char*)Allocator->Allocate(Capacity);
    FormattedU64CalculatedValue.Length   = 0;
    FormattedU64CalculatedValue.Capacity = Capacity;
    FormatStringToBuffer(&FormattedU64CalculatedValue, "%u64", Allocator, U64TestValue);

    if (!StringsEqual(FormattedU64CalculatedValue, U64KnownValue))
    {
        TEST_RETURN_FAILURE("unsigned 120000 could not be formatted into a string");
    }

    FreeString(&FormattedU64CalculatedValue, Allocator);

    string FormattedTrueCalculatedValue;
    FormattedTrueCalculatedValue.Buffer   = (char*)Allocator->Allocate(Capacity);
    FormattedTrueCalculatedValue.Length   = 0;
    FormattedTrueCalculatedValue.Capacity = Capacity;
    FormatStringToBuffer(&FormattedTrueCalculatedValue, "%b32", Allocator, true);

    if (!StringsEqual(FormattedTrueCalculatedValue, CreateString("true")))
    {
        TEST_RETURN_FAILURE("true could not be formatted into a string");
    }

    FreeString(&FormattedTrueCalculatedValue, Allocator);

    string FormattedFalseCalculatedValue;
    FormattedFalseCalculatedValue.Buffer   = (char*)Allocator->Allocate(Capacity);
    FormattedFalseCalculatedValue.Length   = 0;
    FormattedFalseCalculatedValue.Capacity = Capacity;
    FormatStringToBuffer(&FormattedFalseCalculatedValue, "%b32", Allocator, false);

    if (!StringsEqual(FormattedFalseCalculatedValue, CreateString("false")))
    {
        TEST_RETURN_FAILURE("false could not be formatted into a string");
    }

    FreeString(&FormattedFalseCalculatedValue, Allocator);

    string FormattedCharCalculatedValue;
    FormattedCharCalculatedValue.Buffer   = (char*)Allocator->Allocate(Capacity);
    FormattedCharCalculatedValue.Length   = 0;
    FormattedCharCalculatedValue.Capacity = Capacity;
    FormatStringToBuffer(&FormattedCharCalculatedValue, "%char", Allocator, 'c');

    if (!StringsEqual(FormattedCharCalculatedValue, CreateString("c")))
    {
        TEST_RETURN_FAILURE("'c' could not be formatted into a string");
    }

    FreeString(&FormattedCharCalculatedValue, Allocator);

    string FormattedFloatValue;
    FormattedFloatValue.Buffer   = (char*)Allocator->Allocate(Capacity);
    FormattedFloatValue.Length   = 0;
    FormattedFloatValue.Capacity = Capacity;
    FormatStringToBuffer(&FormattedFloatValue, "%f32", Allocator, F32TestValue);

    if (!StartsWith(FormattedFloatValue, F32StartValue))
    {
        TEST_RETURN_FAILURE(
            "The formatted float value did not start with the intended value: %str, given the formatted one: %str",
            string(F32StartValue), FormattedFloatValue);
    }

    FreeString(&FormattedFloatValue, Allocator);

    string FormattedDoubleValue;
    FormattedDoubleValue.Buffer   = (char*)Allocator->Allocate(Capacity);
    FormattedDoubleValue.Length   = 0;
    FormattedDoubleValue.Capacity = Capacity;
    FormatStringToBuffer(&FormattedDoubleValue, "%f64", Allocator, F64TestValue);

    if (!StartsWith(FormattedDoubleValue, F64StartValue))
    {
        TEST_RETURN_FAILURE(
            "The formatted double value did not start with the intended value: %str, given the formatted one: %str",
            string(F64StartValue), FormattedDoubleValue);
    }

    FreeString(&FormattedDoubleValue, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(RemoveWhitespaceTest)
{
    string Value      = CreateString("This is a test which should remove all of the whitespace");
    string KnownValue = CreateString("Thisisatestwhichshouldremoveallofthewhitespace");

    string CalculatedValue;
    RemoveWhitespace(&CalculatedValue, Value, Allocator);

    if (!StringsEqual(KnownValue, CalculatedValue))
    {
        TEST_RETURN_FAILURE("The known value('%str') and the calculated value('%str') are different",
                            KnownValue, CalculatedValue);
    }

    FreeString(&CalculatedValue, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(SplitStringTest)
{
    string Value = CreateString(
        "This is going to be a test,where everything is cut off by commas,and hopefully everything is going to work,we are going to give this a shot,and see if this is going to work,which i really hope it does,but if it does not work,then there is a problem");

    split_string Nothing = SplitString(Value, '.', Allocator);

    if (Nothing.Count != 0)
    {
        TEST_RETURN_FAILURE("Found regexes when none were supposed to be found, regex count: %u32",
                            Nothing.Count);
    }

    if (Nothing.Buffer != NULL)
    {
        TEST_RETURN_FAILURE("Failed to initialize split_string::Buffer to NULL");
    }

    string       KnownBuffer[]     = { CreateString("This is going to be a test"),
                             CreateString("where everything is cut off by commas"),
                             CreateString("and hopefully everything is going to work"),
                             CreateString("we are going to give this a shot"),
                             CreateString("and see if this is going to work"),
                             CreateString("which i really hope it does"),
                             CreateString("but if it does not work"),
                             CreateString("then there is a problem") };
    u32          KnownBufferLength = 8;
    split_string Result            = SplitString(Value, ',', Allocator);

    if (Result.Count != KnownBufferLength)
    {
        TEST_RETURN_FAILURE(
            "The knwon buffer length(%u32) is not the same as the calculated split string buffer count(%u32)",
            KnownBufferLength, Result.Count);
    }

    for (u32 StringIndex = 0; StringIndex < KnownBufferLength; ++StringIndex)
    {
        string ResultString = Result.Buffer[StringIndex];
        if (!StringsEqual(KnownBuffer[StringIndex], ResultString))
        {
            TEST_RETURN_FAILURE(
                "The known string('%str') is not the same as the calculated string('%str') at the index of %u32",
                KnownBuffer[StringIndex], Result.Buffer[StringIndex], StringIndex);
        }

        FreeString(&ResultString, Allocator);
    }

    Allocator->Free(Result.Buffer, Result.Count * sizeof(string));

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(ContainsCharTest)
{
    string Value = CreateString("This is going to be a test to see if all of these words are in this string");
    char   ComparisonValues[]     = { 'T', 'h', 'i', 's', 'i', 'a', 'g', 'o', 'r',
                                'l', 'a', 'o', 'h', 'w', 'a', 'i', 't', 's' };
    u32    ComparisonValuesLength = 18;

    for (u32 ComparisonIndex = 0; ComparisonIndex < ComparisonValuesLength; ++ComparisonIndex)
    {
        char CurrentWord = ComparisonValues[ComparisonIndex];

        if (!Contains(Value, CurrentWord))
        {
            TEST_RETURN_FAILURE("'%char' was supposed to be inside the Value string: '%str'", CurrentWord,
                                Value);
        }
    }

    if (Contains(Value, 'z'))
    {
        TEST_RETURN_FAILURE("Returned true for a known value of false");
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(ContainsStringTest)
{
    string Value = CreateString(
        "This is a value that is a long string becauase this is going to get tested in a loop over and over again and just needs to be large for enough testing");
    string NotInValueValue = CreateString("This is a value that is not in value");
    string OtherValue      = CreateString("this is not in value");

    if (Contains(Value, NotInValueValue))
    {
        TEST_RETURN_FAILURE("Found a value when no value should have been found");
    }

    if (Contains(OtherValue, NotInValueValue))
    {
        TEST_RETURN_FAILURE("Larger Part than string returned true somehow");
    }

    for (u64 Index = 0; Index + 1 < Value.Length; ++Index)
    {
        string TestValue = Value[Index];

        if (!Contains(Value, TestValue))
        {
            TEST_RETURN_FAILURE("For some reason \"%str\" is not in \"%str\"", TestValue, Value);
        }
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(SubStringTest)
{
    string FullValue = CreateString("This string is going to be used as a test");
    string Value;

    Value.Buffer   = FullValue.Buffer;
    Value.Length   = FullValue.Length;
    Value.Capacity = FullValue.Capacity;

    for (u64 ValueIndex = 0; ValueIndex < (Value.Length - 5); ++ValueIndex)
    {
        string CalculatedString;

        // End = 0 means the end of the string or FullValue.Length
        if (!SubString(&CalculatedString, FullValue, ValueIndex /*Start*/, 0 /*End*/, Allocator))
        {
            TEST_RETURN_FAILURE("SubString failed FullValue smaller than End - Start");
        }

        if (!StringsEqual(Value, CalculatedString))
        {
            string ValueAt;
            StringAt(&ValueAt, Value, ValueIndex);

            TEST_RETURN_FAILURE("SubString did not work, value: '%str', calculated string: '%str'", ValueAt,
                                CalculatedString);
        }

        --Value.Length;
        Value.Buffer = Value.Buffer + 1;

        FreeString(&CalculatedString, Allocator);
    }

    Value = CreateEmptyString();

    if (SubString(&Value, FullValue, 0, FullValue.Length + 1, Allocator))
    {
        TEST_RETURN_FAILURE("Created a substring that reached out of bounds");
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(IndexOfTest)
{
    string EmptyString = CreateEmptyString();

    if (IndexOf(EmptyString, '9') != -1 && IndexOf(EmptyString, CreateString("9")) != -1)
    {
        TEST_RETURN_FAILURE("Returned incorrect value for a null string");
    }

    string Value = CreateString("abcdefghijklmnopqrstuvwxyz");

    for (u32 ValueIndex = 0; ValueIndex < Value.Length; ++ValueIndex)
    {
        u32 CalculatedCharIndex   = IndexOf(Value, Value.Buffer[ValueIndex]);
        u32 CalculatedStringIndex = IndexOf(Value, Value[ValueIndex]);

        if (CalculatedCharIndex != ValueIndex)
        {
            TEST_RETURN_FAILURE("The known index(%u32) is not the same as the calculated char index(%u32)",
                                ValueIndex, CalculatedCharIndex);
        }

        if (CalculatedStringIndex != ValueIndex)
        {
            TEST_RETURN_FAILURE("The known index(%u32) is not the same as the calculated string index(%u32)",
                                ValueIndex, CalculatedStringIndex);
        }
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(WhiteSpaceTest)
{
    if (IsWhiteSpace(' ') && IsWhiteSpace('\n') && IsWhiteSpace('\r') && IsWhiteSpace('\0'))
    {
        TEST_RETURN_SUCCESS();
    }

    TEST_RETURN_FAILURE("WhiteSpace detection failed");
}

TEST_FUNCTION(AlphaTest)
{
    for (char LowercaseAlpaIndex = 'a'; LowercaseAlpaIndex < ('z' + 1); ++LowercaseAlpaIndex)
    {
        if (!IsAlpha(LowercaseAlpaIndex))
        {
            TEST_RETURN_FAILURE("%char should have been properly recognized as an alpha character",
                                LowercaseAlpaIndex);
        }
    }

    for (char LowercaseAlpaIndex = 'A'; LowercaseAlpaIndex < ('Z' + 1); ++LowercaseAlpaIndex)
    {
        if (!IsAlpha(LowercaseAlpaIndex))
        {
            TEST_RETURN_FAILURE("%char should have been properly recognized as an alpha character",
                                LowercaseAlpaIndex);
        }
    }

    for (char LowercaseAlpaIndex = '!'; LowercaseAlpaIndex < ('-' + 1); ++LowercaseAlpaIndex)
    {
        if (IsAlpha(LowercaseAlpaIndex))
        {
            TEST_RETURN_FAILURE("%char should not have been recognized as an alpha character",
                                LowercaseAlpaIndex);
        }
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(DigitTest)
{
    for (char LowercaseAlpaIndex = '0'; LowercaseAlpaIndex < ('9' + 1); ++LowercaseAlpaIndex)
    {
        if (!IsDigit(LowercaseAlpaIndex))
        {
            TEST_RETURN_FAILURE("%char should have been properly recognized as a digit", LowercaseAlpaIndex);
        }
    }

    for (char LowercaseAlpaIndex = '!'; LowercaseAlpaIndex < ('-' + 1); ++LowercaseAlpaIndex)
    {
        if (IsDigit(LowercaseAlpaIndex))
        {
            TEST_RETURN_FAILURE("%char should not have been recognized as a digit", LowercaseAlpaIndex);
        }
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(SymbolStringTest)
{
    for (char LowercaseAlpaIndex = '!'; LowercaseAlpaIndex < ('/' + 1); ++LowercaseAlpaIndex)
    {
        if (!IsSymbol(LowercaseAlpaIndex))
        {
            TEST_RETURN_FAILURE("%char should have been properly recognized as a symbol", LowercaseAlpaIndex);
        }
    }

    for (char LowercaseAlpaIndex = '0'; LowercaseAlpaIndex < ('9' + 1); ++LowercaseAlpaIndex)
    {
        if (IsSymbol(LowercaseAlpaIndex))
        {
            TEST_RETURN_FAILURE("%char should not have been recognized as a symbol", LowercaseAlpaIndex);
        }
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AdditonalIndexOfStringTest)
{
    string String         = CreateString("This is a test");
    string Test1          = CreateString("This");
    string Test2          = CreateString(" is");
    string Test3          = CreateString("a");
    string NotFoundString = CreateString("bob");

    // Testing to make sure if the test string is larger than the first string it returns -1
    if (IndexOf(Test1, String) != -1)
    {
        TEST_RETURN_FAILURE("Failed to get -1 from invalid arguments");
    }

    if (IndexOf(String, NotFoundString) != -1)
    {
        TEST_RETURN_FAILURE("The string apparently contains the second, but it should not");
    }

    if (IndexOf(String, Test1) != 0)
    {
        TEST_RETURN_FAILURE("Failed to get the correct index of Test1");
    }

    if (IndexOf(String, Test2) != 4)
    {
        TEST_RETURN_FAILURE("Failed to get the correct index of Test2");
    }

    if (IndexOf(String, Test3) != 8)
    {
        TEST_RETURN_FAILURE("Failed to get the correct index of Test3");
    }

    TEST_RETURN_SUCCESS();
}

void
AddStringTests(array<test>* Tests)
{
    AddTest(StringLengthTest, "String Length Test");
    AddTest(StringsEqualTest, "Strings Equal Test");

    AddTest(EndsWithTest, "Ends With Test");
    AddTest(StartsWithTest, "Starts With Test");

    AddTest(UnsignedIntegerToStringTest, "U64ToString Test");
    AddTest(SignedIntegerToStringTest, "S64ToString Test");
    AddTest(FloatToStringTest, "F64ToString Test");

    AddTest(StringToUnsignedIntegerTest, "StringToU64 Test");
    AddTest(StringToSignedIntegerTest, "StringToS64 Test");
    AddTest(StringToFloatTest, "StringToF64 Test");

    AddTest(FormatStringTest, "FormatString Test");

    AddTest(CopyStringTest, "CopyString Test");
    AddTest(SplitStringTest, "SplitString Test");
    AddTest(ContainsCharTest, "Contains Character Test");
    AddTest(ContainsStringTest, "Contains String Test");
    AddTest(SubStringTest, "SubString Test");
    AddTest(RemoveWhitespaceTest, "RemoveWhitespace Test");
    AddTest(IndexOfTest, "IndexOf(char) Test");
    AddTest(AdditonalIndexOfStringTest, "IndexOf(string) Test");

    AddTest(WhiteSpaceTest, "IsWhiteSpace Test");
    AddTest(AlphaTest, "IsAlpha Test");
    AddTest(DigitTest, "IsDigit Test");
    AddTest(SymbolStringTest, "IsSymbol Test");
}
