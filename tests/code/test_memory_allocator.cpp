#include "test_memory_allocator.h"

#include <std/memory_allocator.h>
#include <std/timer.h>

TEST_FUNCTION(CreateAndDestroyTest)
{
    u64               AllocationLength = 256;
    memory_allocator* MemoryAllocator  = CreateMemoryAllocator(AllocationLength);

    if (MemoryAllocator == NULL)
    {
        TEST_RETURN_FAILURE("Failed to allocate the memory allocator.");
    }

    if (MemoryAllocator->Memory == NULL)
    {
        TEST_RETURN_FAILURE("Failed to allocate the memory.");
    }

    if (MemoryAllocator->Pointer != 0)
    {
        TEST_RETURN_FAILURE("Failed to allocate the memory allocator base pointer.");
    }

    if (MemoryAllocator->AllocationLength != AllocationLength)
    {
        TEST_RETURN_FAILURE("The AllocationLength is %u64 when it should be %u64.",
                            MemoryAllocator->AllocationLength, AllocationLength);
    }

    if (!MemoryAllocator->Allocator)
    {
        TEST_RETURN_FAILURE("Failed to create the allocator");
    }

    if (MemoryAllocator->AllocationRegions.Buffer == NULL)
    {
        TEST_RETURN_FAILURE("Failed to create the allocation regions");
    }

    DestroyMemoryAllocator(&MemoryAllocator);

    if (MemoryAllocator != NULL)
    {
        TEST_RETURN_FAILURE("Failed to free the memory allocator");
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(MemoryAllocatorAllocate)
{
    memory_allocator* MemoryAllocator = CreateMemoryAllocator(sizeof(s32) * 256);

    if (MemoryAllocator->AllocationLength != (sizeof(s32) * 256))
    {
        TEST_RETURN_FAILURE("The AllocationLength is %s32 when it should be %s32",
                            MemoryAllocator->AllocationLength, sizeof(s32) * 256);
    }

    for (s32 IntIndex = 0; IntIndex < 256; ++IntIndex)
    {
        s32* Int = (s32*)PushMemory(MemoryAllocator, sizeof(s32));
        *Int     = (IntIndex + 1) * 100;

        if (((IntIndex + 1) * sizeof(s32)) != MemoryAllocator->Pointer)
        {
            TEST_RETURN_FAILURE(
                "The pointer pointing to the next available memory is %s32 when it should be %s32",
                MemoryAllocator->Pointer, ((IntIndex + 1) * sizeof(s32)));
        }
    }

    if (MemoryAllocator->AllocationLength != MemoryAllocator->Pointer)
    {
        TEST_RETURN_FAILURE(
            "MemoryAllocator Pointer(%u32) is not equal to MemoryAllocator AllocationLength(%u32)",
            MemoryAllocator->Pointer, MemoryAllocator->AllocationLength);
    }

    if (PushMemory(MemoryAllocator, sizeof(s32)) != NULL)
    {
        TEST_RETURN_FAILURE("Failed to handle allocating too much memory");
    }

    DestroyMemoryAllocator(&MemoryAllocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(MemoryAllocatorClear)
{
    memory_allocator* MemoryAllocator = CreateMemoryAllocator(sizeof(s32) * 256);

    for (u32 SubTestIndex = 0; SubTestIndex < 1024; ++SubTestIndex)
    {
        ClearMemoryAllocator(MemoryAllocator);

        if (MemoryAllocator->Pointer != 0)
        {
            TEST_RETURN_FAILURE("The temp memory pointer was not reset to 0 it was %u32",
                                MemoryAllocator->Pointer);
        }

        for (s32 IntIndex = 0; IntIndex < 256; ++IntIndex)
        {
            s32* Int = (s32*)PushMemory(MemoryAllocator, sizeof(s32));
            *Int     = (IntIndex + 1) * 100;

            if (((IntIndex + 1) * sizeof(s32)) != MemoryAllocator->Pointer)
            {
                TEST_RETURN_FAILURE(
                    "The pointer pointing to the next available memory is %s32 when it should be %s32",
                    MemoryAllocator->Pointer, ((IntIndex + 1) * sizeof(s32)));
            }
        }
    }

    if (MemoryAllocator->AllocationLength != MemoryAllocator->Pointer)
    {
        TEST_RETURN_FAILURE(
            "MemoryAllocator Pointer(%u32) is not equal to MemoryAllocator AllocationLength(%u32)",
            MemoryAllocator->Pointer, MemoryAllocator->AllocationLength);
    }

    DestroyMemoryAllocator(&MemoryAllocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(MemoryAllocatorAddSorted)
{
    array<allocation_region> Regions;
    CreateArray(&Regions, Allocator);

    allocation_region Region1 = { 0, 100 };
    allocation_region Region2 = { 150, 200 };
    allocation_region Region3 = { 350, 499 };
    allocation_region Region4 = { 500, 1000 };
    allocation_region Region5 = { 125, 149 };
    allocation_region Region6 = { 110, 120 };
    allocation_region Region7 = { 101, 108 };

    AddSorted(&Regions, Region1);
    AddSorted(&Regions, Region4);
    AddSorted(&Regions, Region2);
    AddSorted(&Regions, Region3);
    AddSorted(&Regions, Region5);
    AddSorted(&Regions, Region6);
    AddSorted(&Regions, Region7);

    u32 RegionCount = Regions.Size;

    if (RegionCount != 4)
    {
        TEST_RETURN_FAILURE("Failed to add all of the 3 regions, region count: %u32", RegionCount);
    }

    allocation_region First  = Regions[0];
    allocation_region Second = Regions[1];
    allocation_region Third  = Regions[2];
    allocation_region Fourth = Regions[3];

    if (First.StartIndex != Region1.StartIndex || First.EndIndex != Region7.EndIndex)
    {
        TEST_RETURN_FAILURE("First region is incorrect, StartIndex: %u32 EndIndex: %u32", First.StartIndex,
                            First.EndIndex);
    }

    if (Second.StartIndex != Region6.StartIndex || Second.EndIndex != Region6.EndIndex)
    {
        TEST_RETURN_FAILURE("Second region is incorrect, StartIndex: %u32 EndIndex: %u32", Second.StartIndex,
                            Second.EndIndex);
    }

    if (Third.StartIndex != Region5.StartIndex || Third.EndIndex != Region2.EndIndex)
    {
        TEST_RETURN_FAILURE("Third region is incorrect, StartIndex: %u32 EndIndex: %u32", Third.StartIndex,
                            Third.EndIndex);
    }

    if (Fourth.StartIndex != Region3.StartIndex || Fourth.EndIndex != Region4.EndIndex)
    {
        TEST_RETURN_FAILURE("Fourth region is incorrect, StartIndex: %u32 EndIndex: %u32", Fourth.StartIndex,
                            Fourth.EndIndex);
    }

    FreeArray(&Regions);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(MemoryAllocatorAddSortedMemory)
{
    memory_allocator* MemoryAllocator = CreateMemoryAllocator(1000);

    void* Region1 = PushMemory(MemoryAllocator, 200);
    void* Region2 = PushMemory(MemoryAllocator, 200);
    void* Region3 = PushMemory(MemoryAllocator, 200);
    void* Region4 = PushMemory(MemoryAllocator, 200);
    void* Region5 = PushMemory(MemoryAllocator, 200);

    FreeMemory(MemoryAllocator, Region1, 200);
    FreeMemory(MemoryAllocator, Region2, 200);
    FreeMemory(MemoryAllocator, Region3, 200);
    FreeMemory(MemoryAllocator, Region4, 200);
    FreeMemory(MemoryAllocator, Region5, 200);

    u32 RegionCount = MemoryAllocator->AllocationRegions.Size;

    if (RegionCount != 1)
    {
        TEST_RETURN_FAILURE("Wrong Region count: %u32", RegionCount);
    }

    allocation_region Region = MemoryAllocator->AllocationRegions[0];

    if (Region.StartIndex != 0 || Region.EndIndex != 999)
    {
        TEST_RETURN_FAILURE("Incorrect region, StartIndex: %u32 EndIndex: %u32", Region.StartIndex,
                            Region.EndIndex);
    }

    DestroyMemoryAllocator(&MemoryAllocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(MemoryAllocatorCombineAllocationRegions)
{
    memory_allocator* MemoryAllocator = CreateMemoryAllocator(1000);

    void* Region1 = PushMemory(MemoryAllocator, 200);
    void* Region2 = PushMemory(MemoryAllocator, 200);
    void* Region3 = PushMemory(MemoryAllocator, 200);
    void* Region4 = PushMemory(MemoryAllocator, 200);
    void* Region5 = PushMemory(MemoryAllocator, 200);

    CombineAllocationRegions(MemoryAllocator);

    if (MemoryAllocator->Pointer != 1000)
    {
        TEST_RETURN_FAILURE("Incorrect MemoryAllocator pointer, Pointer: %u32", MemoryAllocator->Pointer);
    }

    FreeMemory(MemoryAllocator, Region1, 200);
    FreeMemory(MemoryAllocator, Region3, 200);
    FreeMemory(MemoryAllocator, Region2, 200);
    FreeMemory(MemoryAllocator, Region5, 200);
    FreeMemory(MemoryAllocator, Region4, 200);

    u32 RegionCount = MemoryAllocator->AllocationRegions.Size;

    if (RegionCount != 3)
    {
        TEST_RETURN_FAILURE("Wrong Region count: %u32", RegionCount);
    }

    CombineAllocationRegions(MemoryAllocator);

    RegionCount = MemoryAllocator->AllocationRegions.Size;

    if (RegionCount != 0)
    {
        TEST_RETURN_FAILURE("Wrong Region count after combining: %u32", RegionCount);
    }

    Region1 = PushMemory(MemoryAllocator, 200);
    Region2 = PushMemory(MemoryAllocator, 200);
    Region3 = PushMemory(MemoryAllocator, 200);

    FreeMemory(MemoryAllocator, Region1, 200);
    FreeMemory(MemoryAllocator, Region3, 200);

    CombineAllocationRegions(MemoryAllocator);

    RegionCount = MemoryAllocator->AllocationRegions.Size;

    if (RegionCount != 1)
    {
        TEST_RETURN_FAILURE("Wrong Region count after combining with 3 regions: %u32", RegionCount);
    }

    allocation_region AllocationRegion1 = MemoryAllocator->AllocationRegions[0];

    if (AllocationRegion1.StartIndex != 0 || AllocationRegion1.EndIndex != 199)
    {
        TEST_RETURN_FAILURE("Incorrect AllocationRegion1, StartIndex: %u32 EndIndex: %u32",
                            AllocationRegion1.StartIndex, AllocationRegion1.EndIndex);
    }

    if (MemoryAllocator->Pointer != 400)
    {
        TEST_RETURN_FAILURE("Incorrect MemoryAllocator pointer, Pointer: %u32", MemoryAllocator->Pointer);
    }

    CombineAllocationRegions(MemoryAllocator);

    RegionCount = MemoryAllocator->AllocationRegions.Size;
    if (RegionCount != 1)
    {
        TEST_RETURN_FAILURE("Wrong Region count after combining with 1 region: %u32", RegionCount);
    }

    AllocationRegion1 = MemoryAllocator->AllocationRegions[0];

    if (AllocationRegion1.StartIndex != 0 || AllocationRegion1.EndIndex != 199)
    {
        TEST_RETURN_FAILURE("Incorrect AllocationRegion1, StartIndex: %u32 EndIndex: %u32",
                            AllocationRegion1.StartIndex, AllocationRegion1.EndIndex);
    }

    if (MemoryAllocator->Pointer != 400)
    {
        TEST_RETURN_FAILURE(
            "Incorrect MemoryAllocator pointer after a pass that shouldnt do anything the poiner changed from 400 to %u32",
            MemoryAllocator->Pointer);
    }

    DestroyMemoryAllocator(&MemoryAllocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(MemoryAllocatorFindSuitable)
{
    memory_allocator* MemoryAllocator = CreateMemoryAllocator(1000);

    if (FindSuitableMemoryLocation(MemoryAllocator, 10) != -1)
    {
        TEST_RETURN_FAILURE("Returned a valid start index from no regions");
    }

    FreeMemory(MemoryAllocator, PushMemory(MemoryAllocator, 50), 50);
    PushMemory(MemoryAllocator, 75);

    FreeMemory(MemoryAllocator, PushMemory(MemoryAllocator, 200), 200);

    u32 Pointer = FindSuitableMemoryLocation(MemoryAllocator, 100);
    if (Pointer != 125)
    {
        TEST_RETURN_FAILURE("Failed to get the first available allocation region, pointer: %u32", Pointer);
    }

    if (MemoryAllocator->AllocationRegions.Size != 2)
    {
        TEST_RETURN_FAILURE("Wrong region count should be 2, but was %u32",
                            MemoryAllocator->AllocationRegions.Size);
    }

    allocation_region Region1 = MemoryAllocator->AllocationRegions[1];

    if (Region1.StartIndex != 225 || Region1.EndIndex != 324)
    {
        TEST_RETURN_FAILURE("Failed to change the allocation region, StartIndex: %u32 EndIndex: %u32",
                            Region1.StartIndex, Region1.EndIndex);
    }

    if (FindSuitableMemoryLocation(MemoryAllocator, 100) != 225)
    {
        TEST_RETURN_FAILURE("Failed to get the first available allocation region");
    }

    if (MemoryAllocator->AllocationRegions.Size != 1)
    {
        TEST_RETURN_FAILURE("Wrong region count should be 1, but was %u32",
                            MemoryAllocator->AllocationRegions.Size);
    }

    DestroyMemoryAllocator(&MemoryAllocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(MemoryAllocatorCallbackTest)
{
    memory_allocator* MemoryAllocator = CreateMemoryAllocator(Megabytes(1));
    u64               Length          = 64;
    array<u32>        TestArray;
    
    CreateArray(&TestArray, MemoryAllocator->Allocator, Length);

    for (u32 AddIndex = 0; AddIndex < Length; ++AddIndex)
    {
        Add(&TestArray, AddIndex);
    }

    for (u32 CheckIndex = 0; CheckIndex < Length; ++CheckIndex)
    {
        u32 CurrentValue = TestArray[CheckIndex];

        if (CurrentValue != CheckIndex)
        {
            TEST_RETURN_FAILURE("Incorrect value found");
        }
    }
    
    DestroyMemoryAllocator(&MemoryAllocator);

    TEST_RETURN_SUCCESS();
}

void
AddMemoryAllocatorTests(array<test>* Tests)
{
    AddTest(CreateAndDestroyTest, "Memory Allocator Create and Destroy Test");
    AddTest(MemoryAllocatorAllocate, "Memory Allocator Allocate");
    AddTest(MemoryAllocatorClear, "Memory Allocator Clear");
    AddTest(MemoryAllocatorAddSorted, "Memory Allocator AddSorted");
    AddTest(MemoryAllocatorAddSortedMemory, "Memory Allocator AddSorted while allocating");
    AddTest(MemoryAllocatorCombineAllocationRegions, "Memory Allocator CombineAllocationRegions");
    AddTest(MemoryAllocatorFindSuitable, "Memory Allocator FindSuitableMemoryLocation");
    AddTest(MemoryAllocatorCallbackTest, "MemoryAllocator Callback");
}
