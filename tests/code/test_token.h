#if !defined(TEST_TOKEN_H)

#include <std/array.h>
#include <std/test_framework.h>

void AddTokenTests(array<test>* Tests);

#define TEST_TOKEN_H
#endif
