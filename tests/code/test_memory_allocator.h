#if !defined(TEST_TEMP_MEMORY_H)

#include <std/array.h>
#include <std/test_framework.h>

void AddMemoryAllocatorTests(array<test>* Tests);

#define TEST_TEMP_MEMORY_H
#endif
