#if !defined(TEST_STREAM_H)

#include <std/test_framework.h>
#include <std/array.h>

void AddStreamTests(array<test>* Tests);

#define TEST_STREAM_H
#endif
