#include "test_stream.h"

#include <std/stream.h>

TEST_FUNCTION(StreamCreateTest)
{
    stream Stream;
    CreateStream(&Stream, Allocator, Kilobytes(2));

    if (!Stream.Buffer ||
	Stream.WritePointer != 0 ||
	Stream.ReadPointer != 0 ||
	Stream.Length != Kilobytes(2))
    {
	TEST_RETURN_FAILURE("TestStream was not initialized correctly");
    }

    DestroyStream(&Stream);
    
    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StreamSetBufferTest)
{
    stream TestStream;
    char* Pointer = (char*) Allocator->Allocate(sizeof(char) * 10);

    SetBuffer(&TestStream, Pointer, 10);
    
    if (TestStream.Buffer != Pointer ||
	TestStream.Length != 10)
    {
	TEST_RETURN_FAILURE("SetBuffer did not set the correct pointer or length value");
    }

    Allocator->Free(Pointer, sizeof(char) * 10);
    
    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StreamResetTest)
{
    stream Stream;
    CreateStream(&Stream, Allocator, Kilobytes(2));
    Stream.Buffer[0] = 'q';
    Stream.WritePointer = 10;
    Stream.ReadPointer = 10;

    ResetStream(&Stream);

    if (Stream.WritePointer != 0 ||
	Stream.ReadPointer != 0 ||
	Stream.Buffer[0] != 0)
    {
	TEST_RETURN_FAILURE("ResetStream failed to reset the values in the stream");
    }

    DestroyStream(&Stream);
    
    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StreamCanFitTest)
{
    stream Stream;
    Stream.WritePointer = 0;
    Stream.Length = 10;

    if (CanFit(&Stream, 11))
    {
	TEST_RETURN_FAILURE("CanFit should have been false but it is true");
    }

    if (!CanFit(&Stream, 10))
    {
	TEST_RETURN_FAILURE("CanFit should have been true but it is false");
    }
    
    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StreamWriteTemplateTest)
{
    stream Stream;
    CreateStream(&Stream, Allocator, 8);

    u8  TestValue8  = 10;
    u16 TestValue16 = 100;
    u32 TestValue32 = 1000;
    u64 TestValue64 = 10000;

    Write(&Stream, TestValue8 );
    Write(&Stream, TestValue16);
    Write(&Stream, TestValue32);
    Write(&Stream, TestValue64);

    u8*  TestValue8Calc  = (u8* ) Stream.Buffer;
    u16* TestValue16Calc = (u16*) (Stream.Buffer + 1);
    u32* TestValue32Calc = (u32*) (Stream.Buffer + 3);
    u64* TestValue64Calc = (u64*) (Stream.Buffer + 7);

    if (*TestValue8Calc  != TestValue8  ||
	*TestValue16Calc != TestValue16 ||
	*TestValue32Calc != TestValue32 ||
	*TestValue64Calc != TestValue64)
    {
	TEST_RETURN_FAILURE("There was an error when trying to write to integers into a stream");
    }
    
    if (Stream.Length != 16)
    {
	    TEST_RETURN_FAILURE("The stream failed to resize correctly, currently allocated length: %u64", Stream.Length);
    }

    DestroyStream(&Stream);
    
    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StreamReadTemplateTest)
{
    stream Stream;
    CreateStream(&Stream, Allocator, 8);

    u8  TestValue8  = 10;
    u16 TestValue16 = 100;
    u32 TestValue32 = 1000;
    u64 TestValue64 = 10000;

    Write(&Stream, TestValue8 );
    Write(&Stream, TestValue16);
    Write(&Stream, TestValue32);
    Write(&Stream, TestValue64);

    u8  TestValue8Calc  = Read<u8>(&Stream);
    u16 TestValue16Calc = Read<u16>(&Stream);
    u32 TestValue32Calc = Read<u32>(&Stream);
    u64 TestValue64Calc = Read<u64>(&Stream);

    if (TestValue8Calc  != TestValue8  ||
	TestValue16Calc != TestValue16 ||
	TestValue32Calc != TestValue32 ||
	TestValue64Calc != TestValue64)
    {
	TEST_RETURN_FAILURE("There was an error when trying to read integers from a stream");
    }

    if (Stream.Length != 16)
    {
	    TEST_RETURN_FAILURE("The stream failed to resize correctly, currently allocated length: %u64", Stream.Length);
    }

    DestroyStream(&Stream);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StreamReadTest)
{
    stream Stream;
    CreateStream(&Stream, Allocator, 8);

    u8  TestValue8  = 10;
    u16 TestValue16 = 100;
    u32 TestValue32 = 1000;
    u64 TestValue64 = 10000;

    Write(&Stream, TestValue8 );
    Write(&Stream, TestValue16);
    Write(&Stream, TestValue32);
    Write(&Stream, TestValue64);

    u8*  TestValue8Calc  = (u8*)  Read(&Stream, sizeof(u8 ));
    u16* TestValue16Calc = (u16*) Read(&Stream, sizeof(u16));
    u32* TestValue32Calc = (u32*) Read(&Stream, sizeof(u32));
    u64* TestValue64Calc = (u64*) Read(&Stream, sizeof(u64));

    if (*TestValue8Calc  != TestValue8  ||
	*TestValue16Calc != TestValue16 ||
	*TestValue32Calc != TestValue32 ||
	*TestValue64Calc != TestValue64)
    {
	TEST_RETURN_FAILURE("There was an error when trying to read integers from a stream");
    }

    if (Stream.Length != 16)
    {
	    TEST_RETURN_FAILURE("The stream failed to resize correctly, currently allocated length: %u64", Stream.Length);
    }

    DestroyStream(&Stream);
    
    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StreamWriteTest)
{
    stream Stream;
    CreateStream(&Stream, Allocator, 8);

    u8  TestValue8  = 10;
    u16 TestValue16 = 100;
    u32 TestValue32 = 1000;
    u64 TestValue64 = 10000;

    Write(&Stream, &TestValue8 , sizeof(u8 ));
    Write(&Stream, &TestValue16, sizeof(u16));
    Write(&Stream, &TestValue32, sizeof(u32));
    Write(&Stream, &TestValue64, sizeof(u64));

    u8*  TestValue8Calc  = (u8* ) Stream.Buffer;
    u16* TestValue16Calc = (u16*) (Stream.Buffer + 1);
    u32* TestValue32Calc = (u32*) (Stream.Buffer + 3);
    u64* TestValue64Calc = (u64*) (Stream.Buffer + 7);

    if (*TestValue8Calc  != TestValue8  ||
	*TestValue16Calc != TestValue16 ||
	*TestValue32Calc != TestValue32 ||
	*TestValue64Calc != TestValue64)
    {
	TEST_RETURN_FAILURE("There was an error when trying to write to integers into a stream");
    }

    if (Stream.Length != 16)
    {
	    TEST_RETURN_FAILURE("The stream failed to resize correctly, currently allocated length: %u64", Stream.Length);
    }

    DestroyStream(&Stream);
    
    TEST_RETURN_SUCCESS();
}

void
AddStreamTests(array<test>* Tests)
{
    AddTest(StreamCreateTest, "CreateStream Test");
    AddTest(StreamSetBufferTest, "SetBuffer Test");
    AddTest(StreamResetTest, "ResetStream Test");
    AddTest(StreamCanFitTest, "CanFit Test");
    AddTest(StreamWriteTemplateTest, "Write<T> Test");
    AddTest(StreamReadTemplateTest, "Read<T> Test");
    AddTest(StreamWriteTest, "Write<T> Test");
    AddTest(StreamReadTest, "Read<T> Test");
}

