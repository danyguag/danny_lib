#if !defined(TEST_HASHMAP_H)

#include <std/test_framework.h>
#include <std/array.h>

void AddHashmapTests(array<test>* Tests);

#define TEST_HASHMAP_H
#endif
