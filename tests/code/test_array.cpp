#include "test_array.h"

TEST_FUNCTION(AddWithParam)
{
    array<s32> Numbers;
    CreateArray<s32>(&Numbers, Allocator, (u32)128);

    for (s32 ValueIndex = 0; ValueIndex < 256; ++ValueIndex)
    {
        Add(&Numbers, ValueIndex);

        if (Numbers.Size != ((u32)ValueIndex + 1))
        {
            TEST_RETURN_FAILURE(
                "The array pointer was not incremented correcty, should have been %s32 but it was %u32",
                ValueIndex, Numbers.Size);
        }
    }

    for (s32 ValueIndex = 0; ValueIndex < 256; ++ValueIndex)
    {
        s32 CurrentNumber = Numbers[ValueIndex];

        if (CurrentNumber != ValueIndex)
        {
            TEST_RETURN_FAILURE("The value is %u32 when it is supposed to be %u32\n", CurrentNumber,
                                ValueIndex);
        }
    }

    if (Numbers.AllocatedLength != 256)
    {
        TEST_RETURN_FAILURE("The allocation length is incorrect: %u32\n", Numbers.AllocatedLength);
    }

    FreeArray(&Numbers);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AddAnotherArray)
{
    array<u32> Numbers1, Numbers2;
    u64        ExpectedLength;

    CreateArray(&Numbers1, Allocator);
    CreateArray(&Numbers2, Allocator);

    ExpectedLength = Numbers1.AllocatedLength * 2;

    for (u32 Index = 0; Index < Numbers1.AllocatedLength; ++Index)
    {
        Add(&Numbers1, Index);
        Add(&Numbers2, Index + (u32)Numbers1.AllocatedLength);
    }

    // Last param is start offset to copy from source
    Add(&Numbers1, &Numbers2, 0);

    if (Numbers1.AllocatedLength != ExpectedLength)
    {
        TEST_RETURN_FAILURE("Failed to allocate correct size, expected length: %u64 current length: %u64", ExpectedLength,
                            Numbers1.AllocatedLength);
    }

    if (Numbers1.Size != ExpectedLength)
    {
        TEST_RETURN_FAILURE("Failed to add the other array, expected size: %u64 current size: %u64", ExpectedLength,
                            Numbers1.Size);
    }

    for (u32 Index = 0; Index < Numbers1.AllocatedLength; ++Index)
    {
	if (Numbers1[Index] != Index)
	{
	    TEST_RETURN_FAILURE("Failed to get correct value at index: %u32", Index);
	}
    }

    FreeArray(&Numbers2);
    FreeArray(&Numbers1);
    
    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AddWithReturn)
{
    array<s32> Numbers;
    CreateArray<s32>(&Numbers, Allocator, (u32)128);

    for (s32 ValueIndex = 0; ValueIndex < 256; ++ValueIndex)
    {
        s32* CurrentNumber = Add(&Numbers);
        *CurrentNumber     = ValueIndex;

        if (Numbers.Size != ((u32)ValueIndex + 1))
        {
            TEST_RETURN_FAILURE(
                "The array pointer was not incremented correcty, should have been %s32 but it was %u32",
                ValueIndex, Numbers.Size);
        }
    }

    for (s32 ValueIndex = 0; ValueIndex < 256; ++ValueIndex)
    {
        s32 CurrentNumber = Numbers[ValueIndex];
        if (CurrentNumber != ValueIndex)
        {
            TEST_RETURN_FAILURE("The value is %u32 when it is supposed to be %u32\n", CurrentNumber,
                                ValueIndex);
        }
    }

    if (Numbers.AllocatedLength != 256)
    {
        TEST_RETURN_FAILURE(
            "The array should have allocated when the 257th element was added but resized before that\n");
    }

    FreeArray(&Numbers);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AddCharPointer)
{
    array<string> Numbers;
    CreateArray(&Numbers, Allocator, (u32)128);

    for (s32 ValueIndex = 0; ValueIndex < 256; ++ValueIndex)
    {
        string CurrentNumber;
        FormatString(&CurrentNumber, "%s32", Allocator, ValueIndex);
        Add(&Numbers, CurrentNumber);

        if (Numbers.Size != ((u32)ValueIndex + 1))
        {
            TEST_RETURN_FAILURE(
                "The array pointer was not incremented correcty, should have been %s32 but it was %u32",
                ValueIndex, Numbers.Size);
        }

        FreeString(&CurrentNumber, Allocator);
    }

    for (s32 ValueIndex = 0; ValueIndex < 256; ++ValueIndex)
    {
        string CurrentNumber = Numbers[ValueIndex];
        string CorrectValue;
        FormatString(&CorrectValue, "%s32", Allocator, ValueIndex);

        if (!StringsEqual(CurrentNumber, CorrectValue))
        {
            TEST_RETURN_FAILURE("The value is %u32 when it is supposed to be %u32\n", CurrentNumber,
                                CorrectValue);
        }
        FreeString(&CorrectValue, Allocator);
        FreeString(Numbers + ValueIndex, Allocator);
    }

    if (Numbers.AllocatedLength != 256)
    {
        TEST_RETURN_FAILURE(
            "The array should have allocated when the 257th element was added but resized before that\n");
    }

    FreeArray(&Numbers);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(ClearArrayTest)
{
    for (s32 SubTestIndex = 1; SubTestIndex < 11; ++SubTestIndex)
    {
        array<s32> Numbers;
        CreateArray<s32>(&Numbers, Allocator);

        for (s32 ValueIndex = 0; ValueIndex < 256; ++ValueIndex)
        {
            s32* CurrentNumber = Add(&Numbers);
            *CurrentNumber     = ValueIndex * SubTestIndex;
        }

        ClearArray(&Numbers);

        if (Numbers.Size > 0 || Numbers[0] != 0)
        {
            TEST_RETURN_FAILURE("The array was not cleared correctly on Sub test number %u32\n",
                                SubTestIndex);
        }

        FreeArray(&Numbers);
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(FreeArrayTest)
{
    array<s32> Numbers;
    CreateArray<s32>(&Numbers, Allocator);

    for (s32 ValueIndex = 0; ValueIndex < 256; ++ValueIndex)
    {
        s32* CurrentNumber = Add(&Numbers);
        *CurrentNumber     = ValueIndex;
    }

    FreeArray(&Numbers);

    if (Numbers.AllocatedLength != 0)
    {
        TEST_RETURN_FAILURE("The array was not free correctly\n");
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(CopyArrayTest)
{
    array<int> Source;
    array<int> Destination;
    CreateArray(&Source, Allocator);

    Add(&Source, 10);
    Add(&Source, 11);
    Add(&Source, 12);
    Add(&Source, 13);
    Add(&Source, 14);
    Add(&Source, 15);

    CopyArray(&Destination, &Source, Allocator);

    if (Destination.AllocatedLength != Source.AllocatedLength)
    {
        TEST_RETURN_FAILURE("Failed to set the new arrays allocated length");
    }

    if (Destination.Size != Source.Size)
    {
        TEST_RETURN_FAILURE("Failed to set the new arrays pointer");
    }

    if (Destination.Allocator != Allocator)
    {
        TEST_RETURN_FAILURE("Failed to set the new arrays memory callback");
    }

    for (u32 Index = 0; Index < Destination.Size; ++Index)
    {
        if (Destination[Index] != Source[Index])
        {
            TEST_RETURN_FAILURE("Failed to set the new arrays entries");
        }
    }

    FreeArray(&Destination);
    FreeArray(&Source);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(CompareArrayTest)
{
    array<u32> Array;
    array<u32> Other;
    CreateArray(&Array, Allocator);

    for (u32 i = 0; i < 10; ++i)
    {
        Add(&Array, i);
    }

    if (CompareArray(Array, Other))
    {
        TEST_RETURN_FAILURE("Should have returned false because the second array is NULL");
    }

    if (CompareArray(Other, Array))
    {
        TEST_RETURN_FAILURE("Should have returned false because the second array is NULL");
    }

    if (CompareArray(Array, Other))
    {
        TEST_RETURN_FAILURE("Should have returned false because the size of the array differs");
    }

    CreateArray(&Other, Allocator);
    for (u32 i = 0; i < 9; ++i)
    {
        Add(&Other, i);
    }

    if (CompareArray(Array, Other))
    {
        TEST_RETURN_FAILURE("Should have returned false because the arrays have different entries");
    }

    Add(&Other, (u32)9);

    if (!CompareArray(Array, Other))
    {
        TEST_RETURN_FAILURE("The arrays are equal but returned false");
    }

    FreeArray(&Array);
    FreeArray(&Other);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(ResizableTest)
{
    array<u32> Array;
    CreateArray(&Array, Allocator, (u32)10);

    if (!IsEmpty(&Array))
    {
        TEST_RETURN_FAILURE("IsEmpty failed to report an empty array");
    }

    for (u32 Index = 0; Index < 11; ++Index)
    {
        Add(&Array, Index);

        if (Index == 9)
        {
            if (!IsFull(&Array))
            {
                TEST_RETURN_FAILURE("IsFull failed to report a full array");
            }
        }
    }

    if (Array.Size != 11)
    {
        TEST_RETURN_FAILURE("Failed to resize and add the last entry");
    }

    if (Array.AllocatedLength != 20)
    {
        TEST_RETURN_FAILURE("Failed to resize to the correct length");
    }

    FreeArray(&Array);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(ArrayContainsTest)
{
    u32           Count = 7;
    array<string> StoredValues;
    string        KnownValues[] = { CreateString("test1"), CreateString("test2"), CreateString("test3"),
                             CreateString("test4"), CreateString("test5"), CreateString("test6"),
                             CreateString("test7") };

    CreateArray(&StoredValues, Allocator);

    for (u32 Index = 0; Index < Count; ++Index)
    {
        Add(&StoredValues, KnownValues[Index]);
    }

    for (u32 Index = 0; Index < Count; ++Index)
    {
        if (!Contains(&StoredValues, KnownValues[Index]))
        {
            TEST_RETURN_FAILURE("Failed to return true for known stored value at index: %u32", Index);
        }

        if (!StoredValues[Index])
        {
            TEST_RETURN_FAILURE("Expected string is invalid, known value: \"%str\"", KnownValues[Index]);
        }

        FreeString(StoredValues + Index, Allocator);
    }

    FreeArray(&StoredValues);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StringSpecializationTest)
{
    array<string> EntryNames;
    array<string> Copy;
    u32           StartLength = 20;
    string Start = CreateString((char*)Allocator->Allocate((sizeof(char) * StartLength) + 1), StartLength);
    u64    DigitPointer = 0;

    CreateArray(&EntryNames, Allocator);

    {
        char* StartCopy = "aaaaaaaaaaaaaaaaaaaa";

        engine_memcpy(Start.Buffer, StartCopy, StartLength);
        Start.Buffer[StartLength] = 0;
        Start.Capacity            = StartLength + 1;
    }

    while (DigitPointer < StartLength)
    {
        if (DoesFileExist(Start.Buffer))
        {
            DeleteFile(Start);
        }

        if (!Add(&EntryNames, Start))
        {
            TEST_RETURN_FAILURE("Failed to add name into array, current: \"%str\"", Start);
        }

        char CurrentStart = Start.Buffer[DigitPointer];

        if (CurrentStart == 'z')
        {
            ++DigitPointer;
        }

        Start.Buffer[DigitPointer] = Start.Buffer[DigitPointer] + 1;
    }

    u32 NameCount = EntryNames.Size;

    CopyArray(&Copy, &EntryNames, Allocator);

    if (!CompareArray(Copy, EntryNames))
    {
        TEST_RETURN_FAILURE("CopyArray string specialization failed");
    }

    for (u32 NameIndex = NameCount - 1; NameIndex < NameCount; --NameIndex)
    {
        string& CurrentString = EntryNames[NameIndex];

        if (!CurrentString)
        {
            TEST_RETURN_FAILURE("Failed to initialize string correctly, failed name index: %u32", NameIndex);
        }

        Remove(&EntryNames, NameIndex);
        FreeString(Copy + NameIndex, Allocator);
    }

    FreeArray(&Copy);
    FreeArray(&EntryNames);
    FreeString(&Start, Allocator);

    TEST_RETURN_SUCCESS();
}

void
AddArrayTests(array<test>* Tests)
{
    AddTest(AddWithParam, "Array Add with param Test");
    AddTest(AddAnotherArray, "Array Add another array Test");
    AddTest(AddWithReturn, "Array Add with return Test");
    AddTest(AddCharPointer, "Array Add Char Pointer Test");
    AddTest(ClearArrayTest, "Array Clear Test");
    AddTest(FreeArrayTest, "Array Free Test");
    AddTest(CopyArrayTest, "Array Copy Test");
    AddTest(CompareArrayTest, "Array Compare Test");
    AddTest(ResizableTest, "ResizableTest Test");
    AddTest(ArrayContainsTest, "Contains Test");
    AddTest(StringSpecializationTest, "String specialization Test");
}
