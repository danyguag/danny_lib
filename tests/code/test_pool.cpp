#include "test_pool.h"

#include <std/pool.h>

TEST_FUNCTION(CreateAndDestroyPoolTest)
{
    u32 MaxEntryCount = 256;
    pool<u32> Pool;
    CreatePool(&Pool, MaxEntryCount, Allocator);

    if (Pool.Entries.AllocatedLength == 0)
    {
	TEST_RETURN_FAILURE("Failed to initialize pool entries array");
    }

    if (!Pool.RemovedIndices)
    {
	TEST_RETURN_FAILURE("Failed to initialize pool removed indices queue");
    }

    DestroyPool(&Pool);
    
    if (Pool.Entries.AllocatedLength > 0)
    {
	TEST_RETURN_FAILURE("Failed to destroy pool entries array");
    }

    if (Pool.RemovedIndices)
    {
	TEST_RETURN_FAILURE("Failed to destroy pool removed indices queue");
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(GetNextEntryTest)
{
    u32 MaxEntryCount = 256;
    pool<u32> Pool;
    CreatePool(&Pool, MaxEntryCount, Allocator);

    for (u32 Index = 0; Index < MaxEntryCount + 1; ++Index)
    {
	u32* Number = GetNextEntry(&Pool);

	if (Number == NULL && Index != MaxEntryCount)
	{
	    TEST_RETURN_FAILURE("Failed to add correct amount of entries");
	}
    }
    
    DestroyPool(&Pool);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(RemoveEntryTest)
{
    u32 MaxEntryCount = 256;
    pool<u32> Pool;
    CreatePool(&Pool, MaxEntryCount, Allocator);
    array<u32*> NumberPointers;
    CreateArray(&NumberPointers, Allocator);

    for (u32 Index = 0; Index < MaxEntryCount; ++Index)
    {
	u32* Number = GetNextEntry(&Pool);

	Add(&NumberPointers, Number);
    }

    for (u32 Index = 0; Index < MaxEntryCount; ++Index)
    {
	u32* Number = NumberPointers[Index];
	
	RemoveEntry(&Pool, Number);
    }

    for (u64 Index = 0; Index < Pool.RemovedIndices.Size(); ++Index)
    {
	u64 RemovedIndex;
	
	if (!Dequeue(&RemovedIndex, &Pool.RemovedIndices))
	{
	    TEST_RETURN_FAILURE("Failed to peek front entry");
	}

	if (RemovedIndex != Index)
	{
	    TEST_RETURN_FAILURE("Failed to remove entries in the correct order");
	}
    }    

    FreeArray(&NumberPointers);
    DestroyPool(&Pool);

    TEST_RETURN_SUCCESS();
}

void
AddPoolTests(array<test>* Tests)
{
    AddTest(CreateAndDestroyPoolTest, "Create and Destroy Pool Test");
    AddTest(GetNextEntryTest, "GetNextEntry Test");
    AddTest(RemoveEntryTest, "RemoveEntry Test");
}
