#include "tests.h"

#include <std/base.h>
#include <std/hash_map.h>
#include <std/json.h>
#include <std/memory_allocator.h>
#include <std/string.h>
#include <std/test_framework.h>
#include <std/timer.h>

#include "test_array.h"
#include "test_circular_queue.h"
#include "test_commandline_parser.h"
#include "test_hashmap.h"
#include "test_json.h"
#include "test_memory.h"
#include "test_memory_allocator.h"
#include "test_packet_handler.h"
#include "test_pool.h"
#include "test_server_client.h"
#include "test_stream.h"
#include "test_string.h"
#include "test_token.h"

#include <vector>

static void TimeAllocationAndMalloc();
static void TimeArrayAndVector();
static void TimeJsonRead();
static void TimeAsyncIo();
static void CollisionTest();

static u64 CheckCollisions(u64 Length, u64* NumberOfInsertions);

/*
 */

int
main(int Args, char** Argv)
{
    UNUSED_VARIABLE(Args);
    UNUSED_VARIABLE(Argv);

    allocator Allocator;
    InitHeapAllocator(&Allocator);
    
    test_framework* TestFramework = CreateTestFrameWork(&Allocator);
    array<test> Tests;

    CreateArray(&Tests, &Allocator);

    {// Memory Tests
	AddMemoryTests(&Tests);
	AddTestCollection(TestFramework, "memory", &Tests, &Allocator);

	AddMemoryAllocatorTests(&Tests);
	AddTestCollection(TestFramework, "memory_allocator", &Tests, &Allocator);
    }
    
    memory_allocator** MemoryAllocators = (memory_allocator**)Allocator.Allocate(sizeof(memory_allocator*) * 13);
    allocator* Allocators  = (allocator*)Allocator.Allocate(sizeof(allocator) * 13);

    for (u32 AllocatorIndex = 0; AllocatorIndex < 13; ++AllocatorIndex)
    {
        //MemoryAllocators[AllocatorIndex] = CreateMemoryAllocator(Megabytes(256));
        // Allocators[AllocatorIndex] = MemoryAllocators[AllocatorIndex]->Allocator;
        InitHeapAllocator(Allocators + AllocatorIndex);
    }

    {// Container Tests
	AddArrayTests(&Tests);
	AddTestCollection(TestFramework, "array", &Tests, Allocators + 0);

	AddHashmapTests(&Tests);
	AddTestCollection(TestFramework, "hash_map", &Tests, Allocators + 1);

	AddCircularQueueTests(&Tests);
	AddTestCollection(TestFramework, "circular_queue", &Tests, Allocators + 2);

	AddPoolTests(&Tests);
	AddTestCollection(TestFramework, "pool", &Tests, Allocators + 3);
    }
    
    {// Buffer manipulation Tests

	AddStringTests(&Tests);
	AddTestCollection(TestFramework, "string", &Tests, Allocators + 4);

	AddTokenTests(&Tests);
	AddTestCollection(TestFramework, "token", &Tests, Allocators + 5);

	AddStreamTests(&Tests);
	AddTestCollection(TestFramework, "stream", &Tests, Allocators + 6);

	AddJsonTests(&Tests);
	AddTestCollection(TestFramework, "json", &Tests, Allocators + 7);

	AddCommandlineParserTests(&Tests);
	AddTestCollection(TestFramework, "commandline_parser", &Tests, Allocators + 8);
    }
    
    {// Networking Tests
	AddPacketHandlerTests(&Tests);
	AddTestCollection(TestFramework, "packet_handler", &Tests, Allocators + 9);

	AddServerAndClientTests(&Tests);
	AddTestCollection(TestFramework, "server_client", &Tests, Allocators + 10);
    }
    
    WaitAndDestroyTestFramework(TestFramework);

    /* for (u32 AllocatorIndex = 0; AllocatorIndex < 13; ++AllocatorIndex)
    {
        DestroyMemoryAllocator(MemoryAllocators + AllocatorIndex);
	}*/

    Allocator.Free(Allocators, sizeof(allocator*) * 13);
    Allocator.Free(MemoryAllocators, sizeof(memory_allocator*) * 13);

    //
    /*    TimeJsonRead();
        TimeAllocationAndMalloc();
        TimeArrayAndVector();
    TimeAsyncIo();

    CollisionTest();

    "build_hooks":
        [
            "./run_tests"
        ]

*/

    FreeArray(&Tests);

    return (0);
}

static void
PrintTime(const char* Name, s64 CurrentTime, s64 EndTime)
{
    string Message;
    allocator Allocator;

    InitHeapAllocator(&Allocator);
    FormatString(&Message, "%str: %s64ns\n", &Allocator, CreateString(Name), EndTime - CurrentTime);

    StandardPrint(Message);
    FreeString(&Message, &Allocator);
}

static s64
do_something(s64 Something)
{
    return Something + Something;
}

static void
TimeAllocationAndMalloc()
{
    char** Buffers = (char**)malloc(sizeof(char*) * 10);
    { // Timing malloc
        u64 CurrentTime = GetTime();

        for (u32 Index = 0; Index < 10; ++Index)
        {
            Buffers[Index] = (char*)malloc(10000);
        }
        u64 EndTime = GetTime();
        PrintTime("malloc", CurrentTime, EndTime);

        CurrentTime = GetTime();
        for (u32 Index = 0; Index < 10; ++Index)
        {
            free(Buffers[Index]);
        }
        EndTime = GetTime();
        PrintTime("free", CurrentTime, EndTime);
    }
    { // Timing memory_callback malloc heap allocation
        u64 CurrentTime = GetTime();
	allocator Allocator;

	InitHeapAllocator(&Allocator);

        for (u32 Index = 0; Index < 10; ++Index)
        {
            Buffers[Index] = (char*)Allocator.Allocate(10000);
        }
        u64 EndTime = GetTime();
        PrintTime("AllocateMemory(HEAP)", CurrentTime, EndTime);

        CurrentTime = GetTime();
        for (u32 Index = 0; Index < 10; ++Index)
        {
            Allocator.Free(Buffers[Index], 10000);
        }
        EndTime = GetTime();
        PrintTime("FreeMemory(HEAP)", CurrentTime, EndTime);
    }

    { // Timing memory_allocator
        memory_allocator* MemoryAllocator = CreateMemoryAllocator(10000 * 10);
        u64               CurrentTime     = GetTime();

        for (u32 Index = 0; Index < 10; ++Index)
        {
            Buffers[Index] = (char*)MemoryAllocator->Allocator->Allocate(10000 * sizeof(char));
        }
        u64 EndTime = GetTime();
        PrintTime("AllocateMemory(MemoryAllocator->MemoryCallback)", CurrentTime, EndTime);

        CurrentTime = GetTime();
        for (u32 Index = 0; Index < 10; ++Index)
        {
            MemoryAllocator->Allocator->Free(Buffers[Index], 10000 * sizeof(char));
        }

        EndTime = GetTime();
        PrintTime("FreeMemory(MemoryAllocator->MemoryCallback)", CurrentTime, EndTime);
        DestroyMemoryAllocator(&MemoryAllocator);
    }
}

static void
TimeArrayAndVector()
{ // Test the speed of vector
    allocator        Allocator;
    u64              CurrentTime = GetTime();
    std::vector<u32> Vector      = std::vector<u32>();
    u64              EndTime     = GetTime();
    PrintTime("Vector Creation", CurrentTime, EndTime);

    InitHeapAllocator(&Allocator);
    CurrentTime = GetTime();
    array<u32> Array;
    CreateArray(&Array, &Allocator);
    EndTime = GetTime();

    PrintTime("Array Creation", CurrentTime, EndTime);

    CurrentTime = GetTime();
    Vector.push_back((u32)10);
    EndTime = GetTime();

    PrintTime("Vector add", CurrentTime, EndTime);

    CurrentTime = GetTime();
    Add(&Array, (u32)10);
    EndTime = GetTime();

    PrintTime("Array add", CurrentTime, EndTime);

    CurrentTime = GetTime();
    u32* Next   = Add(&Array);
    *Next       = 10;
    EndTime     = GetTime();

    PrintTime("Array add pointer", CurrentTime, EndTime);

    CurrentTime     = GetTime();
    u32 VectorValue = Vector[0];
    EndTime         = GetTime();

    PrintTime("Vector []", CurrentTime, EndTime);

    CurrentTime    = GetTime();
    u32 ArrayValue = Array[0];
    EndTime        = GetTime();

    PrintTime("Array []", CurrentTime, EndTime);

    (void)VectorValue;
    (void)ArrayValue;

    array<u64> Test;
    CreateArray(&Test, &Allocator);

    u64 Count = 5000;

    for (u64 AdditionIndex = 0; AdditionIndex < Count; ++AdditionIndex)
    {
        Add(&Test, AdditionIndex);
    }

    stream Stream;
    CreateStream(&Stream, &Allocator);

    CurrentTime = GetTime();
    for (u64 TestIndex = 0; TestIndex < Test.Size; ++TestIndex)
    {
        u64 Data = Test[TestIndex];

        Write(&Stream, Data);
    }
    EndTime = GetTime();

    PrintTime("Array Foreach", CurrentTime, EndTime);

    stream NextStream;
    CreateStream(&NextStream, &Allocator);

    CurrentTime   = GetTime();
    u32 TestCount = Test.Size;
    for (u32 Index = 0; Index < TestCount; ++Index)
    {
        u64 Data = Test[Index] + Index;

        Write(&NextStream, Data);
    }
    EndTime = GetTime();

    PrintTime("Array raw for", CurrentTime, EndTime);

    FreeArray(&Array);
    FreeArray(&Test);

    DestroyStream(&Stream);
    DestroyStream(&NextStream);
}

static void
TimeJsonRead()
{
    // NOTE: each line takes roughly 1000 nanoseconds to parse
    /*
     * Use the time of array and the different things inside of json parser to see about some speed ups
     */

    file File = OpenFile("test_data.json", FILE_READ, false);

    if (File.Handle < 0)
    {
        StandardPrint(CreateString("Failed to open test_data.json\n"));

        return;
    }

    allocator Allocator;
    InitHeapAllocator(&Allocator);
    
    char* FileContents = ReadFile(File, &Allocator);

    if (!FileContents)
    {
        StandardPrint(CreateString("Failed to read test_data.json\n"));

        return;
    }

    
    json Object;
    u64  CurrentTime = GetTime();
    u64  EndTime;

    BufferToJSON(&Object, FileContents, File.Length, &Allocator);
    EndTime = GetTime();

    PrintTime("Large Json Read", CurrentTime, EndTime);

    if (!Object.Name)
    {
        StandardPrint(CreateString("Failed to parse test_data.json\n"));

        return;
    }

    StandardPrint(CreateString("Read JSON\n"));

    //    json* Metadata = GetJsonObject(Object, "Meta Data");
    json*  SeriesData;
    string SeriesDataName = CreateString("Time Series (Daily)");
    GetJsonObject(&SeriesData, &Object, SeriesDataName);
    string Message;
    FormatString(&Message, "Series Object Count: %u32\n", &Allocator, SeriesData->Objects.Size);

    StandardPrint(Message);
    FreeString(&Message, &Allocator);
}

static u64
CheckCollisions(u64 Length, u64* NumberOfInsertions)
{
    allocator  Allocator;
    array<u64> Hits;

    InitHeapAllocator(&Allocator);
    CreateArray(&Hits, &Allocator, Length);

    u32    StartLength = 50;
    string Start       = CreateString((char*)Allocator.Allocate(sizeof(char) * StartLength), StartLength);
    u64    Pointer     = 0;

    char*  StartCopy    = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";
    string End          = CreateString("zzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
    u64    DigitPointer = 0;

    engine_memcpy(Start.Buffer, StartCopy, StartLength);

    while (!StringsEqual(Start, End))
    {
        u64  Index        = HashFunction(Start) % Length;
        char CurrentStart = Start.Buffer[DigitPointer];

        Hits[Index] += 1;
        ++Pointer;

        if (CurrentStart == 'z')
        {
            ++DigitPointer;
        }

        Start.Buffer[DigitPointer] = Start.Buffer[DigitPointer] + 1;
    }

    FreeString(&Start, &Allocator);

    u64 Sum = 0;

    for (u32 Index = 0; Index < Length; ++Index)
    {
        u64 CurrentHit = Hits[Index];
        if (CurrentHit == 0)
        {
            continue;
        }

        Sum += CurrentHit;
        Sum -= 1;
    }

    if (NumberOfInsertions)
    {
        *NumberOfInsertions = Pointer;
    }

    FreeArray(&Hits);

    return (Sum);
}

void
CollisionTest()
{
    u64 FullLength = Gigabytes(1);
    u64 Start      = FullLength - Megabytes(1);

    u64 Min           = 0;
    u64 MinInsertions = 0;
    u64 MinLength     = 0;
    u64 Max           = 0;
    u64 MaxInsertions = 0;
    u64 MaxLength     = 0;
    allocator Allocator;

    InitHeapAllocator(&Allocator);

    {
        u64 Iterations = FullLength - Start;

        string Message;

        FormatString(&Message, "-------- FullLength: %u64 Iteration Count: %u64 Start Index: %u64\n",
                     &Allocator, FullLength, Iterations, Start);
        StandardPrint(Message);
        FreeString(&Message, &Allocator);
    }

    for (u64 Length = Start; Length < FullLength; ++Length)
    {
        u64 NumberOfInsertions = 0;
        u64 CollisionCount     = CheckCollisions(Length, &NumberOfInsertions);

        if (Min == 0 && Max == 0)
        {
            Min           = CollisionCount;
            MinInsertions = NumberOfInsertions;
            MinLength     = Length;

            Max           = CollisionCount;
            MaxInsertions = NumberOfInsertions;
            MaxLength     = Length;
        }

        if (Min > CollisionCount)
        {
            Min           = CollisionCount;
            MinInsertions = NumberOfInsertions;
            MinLength     = Length;
        }

        if (Max < CollisionCount)
        {
            Max           = CollisionCount;
            MaxInsertions = NumberOfInsertions;
            MaxLength     = Length;
        }

        if (CollisionCount <= 45)
        {
            string Message;

            FormatString(&Message, "-------- Length: %u64 Insertions: %u64 Collision Count: %u64\n", &Allocator, Length,
                         NumberOfInsertions, CollisionCount);
            StandardPrint(Message);
            FreeString(&Message, &Allocator);
        }
    }

    { // Min
        string MinMessage;

        FormatString(&MinMessage, "-------- Length: %u64 Insertions: %u64 Collision Count: %u64\n", &Allocator, MinLength,
                     MinInsertions, Min);
        StandardPrint(MinMessage);
        FreeString(&MinMessage, &Allocator);
    }

    { // Max
        string MaxMessage;

        FormatString(&MaxMessage, "-------- Length: %u64 Insertions: %u64 Collision Count: %u64\n", &Allocator, MaxLength,
                     MaxInsertions, Max);
        StandardPrint(MaxMessage);
        FreeString(&MaxMessage, &Allocator);
    }
}
