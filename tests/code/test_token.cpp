#include "test_token.h"

#include <std/token.h>

char* TokenTypeToString(token_type Type);

/**
 * Token type to string
 *
 * @param Type The type to convert to a string
 * @return char* The resulting type as a string
 */
char*
TokenTypeToString(token_type Type)
{
    switch (Type)
    {
        case TOKEN_SYMBOL:
        {

            return ("TOKEN_SYMBOL");
        }
        break;
        case TOKEN_NUMBER:
        {

            return ("TOKEN_NUMBER");
        }
        break;
        case TOKEN_TEXT:
        {

            return ("TOKEN_TEXT");
        }
        break;
        case TOKEN_STRING:
        {

            return ("TOKEN_STRING");
        }
        break;
        case TOKEN_BOOLEAN:
        {

            return ("TOKEN_BOOLEAN");
        }
        break;
        case TOKEN_FLAG:
        {

            return ("TOKEN_FLAG");
        }
        break;
        default:
        {

            return ("TOKEN_NONE");
        }
    }
}

/*
  ResetToken
  TOKEN_SYMBOL
  TOKEN_NUMBER
  TOKEN_TEXT
  TOKEN_STRING
  TOKEN_BOOLEAN
  TOKEN_FLAG
*/

TEST_FUNCTION(ResetTokenTest)
{
    token Token {};
    Token.Type = TOKEN_FLAG;
    Token.Flag = 'g';

    ResetToken(&Token, Allocator);

    if (Token.Type != TOKEN_NONE)
    {
        TEST_RETURN_FAILURE("The token type was not reset to TOKEN_NONE");
    }

    if (Token.Flag == 'g')
    {
        TEST_RETURN_FAILURE("The Token was not reset correctly, Token.Flag == 'g'");
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(SymbolTest)
{
    char*     Buffer = "!@#%^&";
    tokenizer Tokenizer {};
    CreateTokenizer(&Tokenizer, Buffer, StringLength(Buffer));

    token Next {};
    u32   TokenCount = 0;

    while (HasNextToken(Tokenizer))
    {
        ++TokenCount;

        if (!GetNextToken(&Tokenizer, &Next, Allocator))
	{
	    TEST_RETURN_FAILURE("Failed to get next token");
	}

        if (Next.Type != TOKEN_SYMBOL)
        {
            TEST_RETURN_FAILURE("The token type(%str) should be TOKEN_SYMBOL", TokenTypeToString(Next.Type));
        }
    }

    if (TokenCount != 6)
    {
        TEST_RETURN_FAILURE(
            "The tokenizer did not go over the entire string and only found %u32 out of 6 tokens",
            TokenCount);
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(NumberTest)
{
    char*     Buffer   = "-10 10 1000 -1000";
    s32       Values[] = { -10, 10, 1000, -1000 };
    tokenizer Tokenizer {};
    CreateTokenizer(&Tokenizer, Buffer, StringLength(Buffer));

    token Next {};
    u32   TokenCount = 0;

    while (HasNextToken(Tokenizer))
    {
        if (!GetNextToken(&Tokenizer, &Next, Allocator))
	{
	    TEST_RETURN_FAILURE("Failed to get next token");
	}

        if (Next.Type != TOKEN_NUMBER)
        {
            TEST_RETURN_FAILURE("The token type(%str) should be TOKEN_SYMBOL", TokenTypeToString(Next.Type));
        }

        s32 CalculatedValue = (s32)Next.Number;

        if (Values[TokenCount] != CalculatedValue)
        {
            TEST_RETURN_FAILURE("The known value(%s32) is not equal to the calculated value(%s32)",
                                Values[TokenCount], CalculatedValue);
        }
        ++TokenCount;
    }

    if (TokenCount != 4)
    {
        TEST_RETURN_FAILURE(
            "The tokenizer did not go over the entire string and only found %u32 out of 4 tokens",
            TokenCount);
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(TextTest)
{
    char*     Buffer   = "ThisisaTest anotherone testtest bob";
    string    Values[] = { CreateString("ThisisaTest"), CreateString("anotherone"), CreateString("testtest"),
                        CreateString("bob") };
    tokenizer Tokenizer {};
    CreateTokenizer(&Tokenizer, Buffer, StringLength(Buffer));

    token Next {};
    u32   TokenCount = 0;

    while (HasNextToken(Tokenizer))
    {
        if (!GetNextToken(&Tokenizer, &Next, Allocator))
	{
	    TEST_RETURN_FAILURE("Failed to get next token");
	}

        if (Next.Type != TOKEN_TEXT)
        {
            TEST_RETURN_FAILURE("The token type(%str) should be TOKEN_TEXT", TokenTypeToString(Next.Type));
        }

        string CalculatedText = Next.String;

        if (!StringsEqual(Values[TokenCount], CalculatedText))
        {
            TEST_RETURN_FAILURE("The known value: \'%str\' is not equal to the calculated value: \'%str\'",
                                Values[TokenCount], CalculatedText);
        }
        ++TokenCount;
    }

    ResetToken(&Next, Allocator);

    if (TokenCount != 4)
    {
        TEST_RETURN_FAILURE(
            "The tokenizer did not go over the entire string and only found %u32 out of 4 tokens",
            TokenCount);
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(StringTest)
{
    char*     Buffer   = "\"ThisisaTest\" \"anotherone\" \'testtest\' \'bob\'";
    string    Values[] = { CreateString("ThisisaTest"), CreateString("anotherone"), CreateString("testtest"),
                        CreateString("bob") };
    tokenizer Tokenizer {};
    CreateTokenizer(&Tokenizer, Buffer, StringLength(Buffer));

    token Next {};
    u32   TokenCount = 0;

    while (HasNextToken(Tokenizer))
    {
        if (!GetNextToken(&Tokenizer, &Next, Allocator))
	{
	    TEST_RETURN_FAILURE("Failed to get next token");
	}

        if (Next.Type != TOKEN_STRING)
        {
            TEST_RETURN_FAILURE("The token type(%str) should be TOKEN_STRING", TokenTypeToString(Next.Type));
        }

        string CalculatedString = Next.String;

        if (!StringsEqual(Values[TokenCount], CalculatedString))
        {
            TEST_RETURN_FAILURE("The known value: \'%str\' is not equal to the calculated value: \'%str\'",
                                Values[TokenCount], CalculatedString);
        }
        ++TokenCount;
    }

    ResetToken(&Next, Allocator);

    if (TokenCount != 4)
    {
        TEST_RETURN_FAILURE(
            "The tokenizer did not go over the entire string and only found %u32 out of 4 tokens",
            TokenCount);
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(BooleanTest)
{
    char*     Buffer   = "false true true false";
    b32       Values[] = { false, true, true, false };
    tokenizer Tokenizer {};
    CreateTokenizer(&Tokenizer, Buffer, StringLength(Buffer));

    token Next {};
    u32   TokenCount = 0;

    while (HasNextToken(Tokenizer))
    {
        if (!GetNextToken(&Tokenizer, &Next, Allocator))
	{
	    TEST_RETURN_FAILURE("Failed to get next token");
	}

        if (Next.Type != TOKEN_BOOLEAN)
        {
            TEST_RETURN_FAILURE("The token type(%str) should be TOKEN_BOOLEAN", TokenTypeToString(Next.Type));
        }

        b32 CalculatedBoolean = Next.Boolean;

        if (Values[TokenCount] != CalculatedBoolean)
        {
            TEST_RETURN_FAILURE("The known value(%b32) is not equal to the calculated value(%b32)",
                                Values[TokenCount], CalculatedBoolean);
        }
        ++TokenCount;
    }

    if (TokenCount != 4)
    {
        TEST_RETURN_FAILURE(
            "The tokenizer did not go over the entire string and only found %u32 out of 4 tokens",
            TokenCount);
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(FlagTest)
{
    char*     Buffer   = "-r -t -g -k";
    char      Values[] = { 'r', 't', 'g', 'k' };
    tokenizer Tokenizer {};
    CreateTokenizer(&Tokenizer, Buffer, StringLength(Buffer));

    token Next {};
    u32   TokenCount = 0;

    while (HasNextToken(Tokenizer))
    {
        if (!GetNextToken(&Tokenizer, &Next, Allocator))
	{
	    TEST_RETURN_FAILURE("Failed to get next token");
	}

        if (Next.Type != TOKEN_FLAG)
        {
            TEST_RETURN_FAILURE("The token type(%str) should be TOKEN_FLAG", TokenTypeToString(Next.Type));
        }

        char CalculatedFlag = Next.Flag;

        if (Values[TokenCount] != CalculatedFlag)
        {
            TEST_RETURN_FAILURE("The known value(%char) is not equal to the calculated value(%char)",
                                Values[TokenCount], CalculatedFlag);
        }
        ++TokenCount;
    }

    if (TokenCount != 4)
    {
        TEST_RETURN_FAILURE(
            "The tokenizer did not go over the entire string and only found %u32 out of 4 tokens",
            TokenCount);
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(GetNextTokenFailuresTest)
{
    char* TestStrings[] = { "\'Testing a failure", "  ", NULL};
    u8 Index = 0;
    tokenizer Tokenizer;
    token UnusedToken;

    while (TestStrings[Index] != NULL)
    {
	char* TestString = TestStrings[Index++];
	u64 TestStringLength = StringLength(TestString);

	CreateTokenizer(&Tokenizer, TestString, TestStringLength);

	if (GetNextToken(&Tokenizer, &UnusedToken, Allocator))
	{
	    TEST_RETURN_FAILURE("Got a token when there was supposed to be an error");
	}
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(RandomTokenTest)
{
    char* Buffer = "! 1000 Testtest \'What is going on?\' false -r";

    char   KnownSymbol  = '!';
    s32    KnownNumber  = 1000;
    string KnownText    = CreateString("Testtest");
    string KnownString  = CreateString("What is going on?");
    b32    KnownBoolean = false;
    char   KnownFlag    = 'r';

    tokenizer Tokenizer {};
    CreateTokenizer(&Tokenizer, Buffer, StringLength(Buffer));

    token Next {};
    u32   TokenCount = 0;

    while (HasNextToken(Tokenizer))
    {
	b32 Return = GetNextToken(&Tokenizer, &Next, Allocator);
        if (!Return)
	{
	    TEST_RETURN_FAILURE("Failed to get next token");
	}

        if (TokenCount == 0)
        {
            if (Next.Type != TOKEN_SYMBOL)
            {
                TEST_RETURN_FAILURE("The token type(%str) should be TOKEN_SYMBOL",
                                    TokenTypeToString(Next.Type));
            }

            char CalculatedSymbol = Next.Symbol;

            if (KnownSymbol != CalculatedSymbol)
            {
                TEST_RETURN_FAILURE("The known value(%char) is not equal to the calculated value(%char)",
                                    KnownSymbol, CalculatedSymbol);
            }
        }
        else if (TokenCount == 1)
        {
            if (Next.Type != TOKEN_NUMBER)
            {
                TEST_RETURN_FAILURE("The token type(%str) should be TOKEN_NUMBER",
                                    TokenTypeToString(Next.Type));
            }

            s32 CalculatedNumber = (s32)Next.Number;

            if (KnownNumber != CalculatedNumber)
            {
                TEST_RETURN_FAILURE("The known value(%s32) is not equal to the calculated value(%s32)",
                                    KnownNumber, CalculatedNumber);
            }
        }
        else if (TokenCount == 2)
        {
            if (Next.Type != TOKEN_TEXT)
            {
                TEST_RETURN_FAILURE("The token type(%str) should be TOKEN_TEXT",
                                    TokenTypeToString(Next.Type));
            }

            string CalculatedText = Next.String;

            if (!StringsEqual(CalculatedText, KnownText))
            {
                TEST_RETURN_FAILURE(
                    "The known value: \'%str\' is not equal to the calculated value: \'%str\'", KnownText,
                    CalculatedText);
            }
        }
        else if (TokenCount == 3)
        {
            if (Next.Type != TOKEN_STRING)
            {
                TEST_RETURN_FAILURE("The token type(%str) should be TOKEN_STRING",
                                    TokenTypeToString(Next.Type));
            }

            string CalculatedString = Next.String;

            if (!StringsEqual(CalculatedString, KnownString))
            {
                TEST_RETURN_FAILURE(
                    "The known value: \'%str\' is not equal to the calculated value: \'%str\'", KnownString,
                    CalculatedString);
            }
        }
        else if (TokenCount == 4)
        {
            if (Next.Type != TOKEN_BOOLEAN)
            {
                TEST_RETURN_FAILURE("The token type(%str) should be TOKEN_BOOLEAN",
                                    TokenTypeToString(Next.Type));
            }

            char CalculatedBoolean = Next.Boolean;

            if (KnownBoolean != CalculatedBoolean)
            {
                TEST_RETURN_FAILURE("The known value(%b32) is not equal to the calculated value(%b32)",
                                    KnownBoolean, CalculatedBoolean);
            }
        }
        else if (TokenCount == 5)
        {
            if (Next.Type != TOKEN_FLAG)
            {
                TEST_RETURN_FAILURE("The token type(%str) should be TOKEN_FLAG",
                                    TokenTypeToString(Next.Type));
            }

            char CalculatedFlag = Next.Flag;

            if (KnownFlag != CalculatedFlag)
            {
                TEST_RETURN_FAILURE("The known value(%char) is not equal to the calculated value(%char)",
                                    KnownFlag, CalculatedFlag);
            }
        }

        ++TokenCount;
    }

    if (GetNextToken(&Tokenizer, &Next, Allocator))
    {
	TEST_RETURN_FAILURE("Got a token when there was none left");
    }
    
    if (TokenCount != 6)
    {
        TEST_RETURN_FAILURE(
            "The tokenizer did not go over the entire string and only found %u32 out of 6 tokens",
            TokenCount);
    }

    TEST_RETURN_SUCCESS();
}

void
AddTokenTests(array<test>* Tests)
{
    AddTest(ResetTokenTest, "ResetToken Test");
    AddTest(SymbolTest, "Symbol Test");
    AddTest(NumberTest, "Number Test");
    AddTest(TextTest, "Text Test");
    AddTest(StringTest, "String Test");
    AddTest(BooleanTest, "Boolean Test");
    AddTest(FlagTest, "Flag Test");

    AddTest(RandomTokenTest, "GetNextToken Test");
    AddTest(GetNextTokenFailuresTest, "GetNextTokenFailures Test");
}
