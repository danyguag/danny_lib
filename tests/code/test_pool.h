#if !defined(TEST_POOL_H)

#include <std/test_framework.h>
#include <std/array.h>

void AddPoolTests(array<test>* Tests);

#define TEST_POOL_H
#endif
