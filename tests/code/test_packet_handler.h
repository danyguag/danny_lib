#if !defined(TEST_PACKET_HANDLER_H)

#include <std/array.h>
#include <std/test_framework.h>

void AddPacketHandlerTests(array<test>* Tests);

#define TEST_PACKET_HANDLER_H
#endif
