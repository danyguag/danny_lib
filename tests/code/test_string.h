#if !defined(TEST_STRING_H)

#include <std/test_framework.h>
#include <std/array.h>

void AddStringTests(array<test>* Tests);

#define TEST_STRING_H
#endif
