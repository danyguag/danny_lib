#if !defined(TEST_JSON_H)

#include <std/array.h>
#include <std/test_framework.h>

void AddJsonTests(array<test>* Tests);

#define TEST_JSON_H
#endif
