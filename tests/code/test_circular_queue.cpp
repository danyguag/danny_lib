#include "test_circular_queue.h"

#include <std/circular_queue.h>

TEST_FUNCTION(CreateAndDestroyQueueTest)
{
    circular_queue<u32> TestQueue;
    CreateCircularQueue(&TestQueue, Allocator, DEFAULT_QUEUE_SIZE);

    if (!TestQueue)
    {
	TEST_RETURN_FAILURE("Failed to create circular queue");
    }
    
    if (!IsEmpty(&TestQueue))
    {
	TEST_RETURN_FAILURE("The queue is not empty after creation");
    }

    if (TestQueue.EnqueueIndex != (TestQueue.AllocatedLength - 1))
    {
	TEST_RETURN_FAILURE("Failed to initialize EnqueueIndex");
    }

    if (TestQueue.DequeueIndex != 0)
    {
	TEST_RETURN_FAILURE("Failed to initialize DequeueIndex");
    }

    if (TestQueue.AllocatedLength != 257)
    {
	TEST_RETURN_FAILURE("Failed to allocate to the correct default size");
    }

    if (!TestQueue.Buffer)
    {
	TEST_RETURN_FAILURE("Failed to allocate the queue buffer");
    }
    
    DestroyQueue(&TestQueue);

    if (TestQueue)
    {
	TEST_RETURN_FAILURE("Failed to destroy the queue");
    }
    
    if (TestQueue.EnqueueIndex != 0)
    {
	TEST_RETURN_FAILURE("Failed to clean up EnqueueIndex");
    }

    if (TestQueue.DequeueIndex != 0)
    {
	TEST_RETURN_FAILURE("Failed to clean up DequeueIndex");
    }

    if (TestQueue.AllocatedLength != 0)
    {
	TEST_RETURN_FAILURE("AllocatedLength is not 0");
    }

    if (TestQueue.Buffer)
    {
	TEST_RETURN_FAILURE("Failed to free the queue");
    }
    
    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(EnqueueAndDequeueTest)
{
    circular_queue<u32> TestQueue;
    CreateCircularQueue(&TestQueue, Allocator, DEFAULT_QUEUE_SIZE);

    u32 FirstEntry = 10;
    u32 SecondEntry = 20;
    Enqueue(&TestQueue, FirstEntry);

    {
	u32* NextEntry = Enqueue(&TestQueue);
	*NextEntry = SecondEntry;
    }

    if (TestQueue.Size() != 2)
    {
	TEST_RETURN_FAILURE("The two entries were not added");
    }

    if (TestQueue.EnqueueIndex != 1)
    {
	TEST_RETURN_FAILURE("The enqueue index was not incremented correctly");
    }

    if (TestQueue.DequeueIndex != 0)
    {
	TEST_RETURN_FAILURE("The DequeueIndex was changed");
    }

    u32 FirstStoredEntry;

    if (!Dequeue(&FirstStoredEntry, &TestQueue) || FirstStoredEntry != 10)
    {
	TEST_RETURN_FAILURE("The first entry is not correct");
    }

    if (TestQueue.DequeueIndex != 1)
    {
	TEST_RETURN_FAILURE("The DequeueIndex was not changed");
    }

    if (TestQueue.Size() != 1)
    {
	TEST_RETURN_FAILURE("The entry count was not decremented to 1");
    }

    u32 SecondStoredEntry;

    if (!Dequeue(&SecondStoredEntry, &TestQueue) || SecondStoredEntry != 20)
    {
	TEST_RETURN_FAILURE("The second entry is not correct");
    }

    if (TestQueue.DequeueIndex != 2)
    {
	TEST_RETURN_FAILURE("The DequeueIndex was not changed");
    }

    if (TestQueue.Size() != 0)
    {
	TEST_RETURN_FAILURE("The entry count was not decremented to 0");
    }
    
    DestroyQueue(&TestQueue);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(ClearCircularQueueTest)
{
    circular_queue<u32> TestQueue;
    CreateCircularQueue(&TestQueue, Allocator, DEFAULT_QUEUE_SIZE);

    Enqueue(&TestQueue, (u32) 10);
    Enqueue(&TestQueue, (u32) 10);
    Enqueue(&TestQueue, (u32) 10);
    Enqueue(&TestQueue, (u32) 10);
    Enqueue(&TestQueue, (u32) 10);
    Enqueue(&TestQueue, (u32) 10);
    Enqueue(&TestQueue, (u32) 10);
    Enqueue(&TestQueue, (u32) 10);
    Enqueue(&TestQueue, (u32) 10);

    Clear(&TestQueue);
    
    if (!IsEmpty(&TestQueue))
    {
	TEST_RETURN_FAILURE("The queue is not empty after creation");
    }

    if (TestQueue.EnqueueIndex != (TestQueue.AllocatedLength - 1))
    {
	TEST_RETURN_FAILURE("Failed to reset EnqueueIndex");
    }

    if (TestQueue.DequeueIndex != 0)
    {
	TEST_RETURN_FAILURE("Failed to reset DequeueIndex");
    }

    DestroyQueue(&TestQueue);
    
    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(ResizeCircularQueueTest)
{
    circular_queue<u32> TestQueue;
    CreateCircularQueue(&TestQueue, Allocator, DEFAULT_QUEUE_SIZE);

    TestQueue.EnqueueIndex = 19;
    TestQueue.DequeueIndex = 10;
    TestQueue.EnqueuedCount = 10;

    circular_queue_internal::ResizeQueue(&TestQueue);

    if (TestQueue.DequeueIndex != 0)
    {
	TEST_RETURN_FAILURE("Failed to reset DequeueIndex to the 0");
    }

    if (TestQueue.EnqueueIndex != TestQueue.EnqueuedCount - 1)
    {
	TEST_RETURN_FAILURE("Failed to change EnqueueIndex to the correct value");
    }

    if (TestQueue.AllocatedLength != 513)
    {
	TEST_RETURN_FAILURE("Failed to change AllocatedLength to the correct value");
    }

    DestroyQueue(&TestQueue);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(IsFullTest)
{
    circular_queue<u32> TestQueue;
    CreateCircularQueue(&TestQueue, Allocator, DEFAULT_QUEUE_SIZE);

    for (u32 Index = 0; Index < 256; ++Index)
    {
	Enqueue(&TestQueue, Index);
    }

    if (!IsFull(&TestQueue))
    {
	TEST_RETURN_FAILURE("IsFull failed to confirm the test queue is full");
    }

    DestroyQueue(&TestQueue);
    
    TEST_RETURN_SUCCESS();
}

void
AddCircularQueueTests(array<test>* Tests)
{
    AddTest(CreateAndDestroyQueueTest, "Create and DestroyQueue Test");
    AddTest(EnqueueAndDequeueTest, "Enqueue and DequeueTest Test");
    AddTest(ClearCircularQueueTest, "Clear Test");
    AddTest(ResizeCircularQueueTest, "ResizeCircularQueue Test");
    AddTest(IsFullTest, "IsFull Test");
}

