#include "test_json.h"

#include <std/json.h>

TEST_FUNCTION(CreateEmptyJsonTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);

    if (!StringsEqual(JsonObject.Name, CreateString("Root")))
    {
        TEST_RETURN_FAILURE("Empty root json object did not initialize correctly");
    }

    string JsonName = CreateString("Test");
    json   JsonObjectName;
    CreateEmptyJson(&JsonObjectName, JsonName, Allocator);

    if (!StringsEqual(JsonObjectName.Name, JsonName))
    {
        TEST_RETURN_FAILURE("Empty \"Test\" json object did not initialize correctly");
    }

    DestroyJson(&JsonObject, Allocator);
    DestroyJson(&JsonObjectName, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AddJsonObjectTest)
{
    json Parent;
    CreateEmptyJson(&Parent, CreateString("Parent"), Allocator);
    json Child;
    CreateEmptyJson(&Child, CreateString("Child"), Allocator);

    AddJsonObject(&Parent, Child, Allocator);

    if (Parent.Objects.Buffer == NULL)
    {
        TEST_RETURN_FAILURE("Failed to initialize json object array");
    }

    if (Parent.Objects.Size != 1 || !StringsEqual(Parent.Objects[0].Name, Child.Name))
    {
        TEST_RETURN_FAILURE("Failed to add child into the parent object");
    }

    DestroyJson(&Parent, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AddJsonNumberTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);
    f32    Value     = 100.5;
    string ValueName = CreateString("Value");

    AddJsonNumber(&JsonObject, ValueName, Value, Allocator);

    if (JsonObject.Numbers.Buffer == NULL)
    {
        TEST_RETURN_FAILURE("Failed to initialize json number array");
    }

    if (JsonObject.Numbers.Size != 1 || !CompareFloat(JsonObject.Numbers[0].Value, Value, .00001) ||
        !StringsEqual(JsonObject.Numbers[0].Name, ValueName))
    {
        TEST_RETURN_FAILURE("Failed to add a number into the json object");
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AddJsonBooleanTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);
    b32    BooleanFalse     = false;
    string BooleanNameFalse = CreateString("Value");

    AddJsonBoolean(&JsonObject, BooleanNameFalse, BooleanFalse, Allocator);

    if (JsonObject.Booleans.Buffer == NULL)
    {
        TEST_RETURN_FAILURE("Failed to initialize json boolean array");
    }

    if (JsonObject.Booleans.Size != 1 || JsonObject.Booleans[0].Value != BooleanFalse ||
        !StringsEqual(JsonObject.Booleans[0].Name, BooleanNameFalse))
    {
        TEST_RETURN_FAILURE("Failed to add a boolean into the json object");
    }

    b32    BooleanTrue     = true;
    string BooleanNameTrue = CreateString("Value1");

    AddJsonBoolean(&JsonObject, BooleanNameTrue, BooleanTrue, Allocator);

    if (JsonObject.Booleans.Buffer == NULL)
    {
        TEST_RETURN_FAILURE("Failed to initialize json boolean array");
    }

    if (JsonObject.Booleans.Size != 2 || JsonObject.Booleans[1].Value != BooleanTrue ||
        !StringsEqual(JsonObject.Booleans[1].Name, BooleanNameTrue))
    {
        TEST_RETURN_FAILURE("Failed to add child into the parent object");
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AddJsonStringTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);
    string Value     = CreateString("test");
    string ValueName = CreateString("Value");

    AddJsonString(&JsonObject, ValueName, Value, Allocator);

    if (JsonObject.Strings.Buffer == NULL)
    {
        TEST_RETURN_FAILURE("Failed to initialize json string array");
    }

    if (JsonObject.Strings.Size != 1 || !StringsEqual(JsonObject.Strings[0].Value, Value) ||
        !StringsEqual(JsonObject.Strings[0].Name, ValueName))
    {
        TEST_RETURN_FAILURE("Failed to add child into the parent object");
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AddJsonStringArrayTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);

    array<string> StringArray;
    CreateArray(&StringArray, Allocator);

    for (u32 Index = 1; Index < 7; ++Index)
    {
        string AddString;

        FormatString(&AddString, "value%u32", Allocator, Index);
        Add(&StringArray, AddString);
        FreeString(&AddString, Allocator);
    }

    string StringArrayName = CreateString("Value");

    AddJsonStringArray(&JsonObject, StringArrayName, StringArray, Allocator);

    if (JsonObject.Arrays.Buffer == NULL)
    {
        TEST_RETURN_FAILURE("Failed to initialize json string array");
    }

    if (JsonObject.Arrays.Size != 1 || !CompareArray(JsonObject.Arrays[0].StringValues, StringArray) ||
        !StringsEqual(JsonObject.Arrays[0].Name, StringArrayName))
    {
        TEST_RETURN_FAILURE("Failed to add string array into the json object");
    }

    for (u64 StringIndex = 0; StringIndex < StringArray.Size; ++StringIndex)
    {
        FreeString(StringArray + StringIndex, Allocator);
    }

    FreeArray(&StringArray);
    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AddJsonBooleanArrayTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);

    array<b32> BooleanArray;
    CreateArray(&BooleanArray, Allocator);
    Add(&BooleanArray, (b32) true);
    Add(&BooleanArray, (b32) false);
    Add(&BooleanArray, (b32) true);
    Add(&BooleanArray, (b32) false);
    Add(&BooleanArray, (b32) true);
    Add(&BooleanArray, (b32) false);
    string BooleanArrayName = CreateString("Value");

    AddJsonBooleanArray(&JsonObject, BooleanArrayName, BooleanArray, Allocator);

    if (JsonObject.Arrays.Buffer == NULL)
    {
        TEST_RETURN_FAILURE("Failed to initialize json boolean array");
    }

    if (JsonObject.Arrays.Size != 1 || !CompareArray(JsonObject.Arrays[0].BooleanValues, BooleanArray) ||
        !StringsEqual(JsonObject.Arrays[0].Name, BooleanArrayName))
    {
        TEST_RETURN_FAILURE("Failed to add boolean array into the json object");
    }

    FreeArray(&BooleanArray);
    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AddJsonNumberArrayTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);

    array<f64> NumberArray;
    CreateArray(&NumberArray, Allocator);
    Add(&NumberArray, 1.0);
    Add(&NumberArray, 1.0);
    Add(&NumberArray, 1.0);
    Add(&NumberArray, 1.0);
    Add(&NumberArray, 1.0);
    Add(&NumberArray, 1.0);
    string NumberArrayName = CreateString("Value");

    AddJsonNumberArray(&JsonObject, NumberArrayName, NumberArray, Allocator);

    if (JsonObject.Arrays.Buffer == NULL)
    {
        TEST_RETURN_FAILURE("Failed to initialize json number array");
    }

    if (JsonObject.Arrays.Size != 1 || !CompareArray(JsonObject.Arrays[0].NumberValues, NumberArray) ||
        !StringsEqual(JsonObject.Arrays[0].Name, NumberArrayName))
    {
        TEST_RETURN_FAILURE("Failed to add number array into the json object");
    }

    FreeArray(&NumberArray);
    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(GetJsonObjectTest)
{
    json Parent;
    CreateEmptyJson(&Parent, CreateString("Parent"), Allocator);
    json   Child;
    string Name = CreateString("Child");
    CreateEmptyJson(&Child, Name, Allocator);

    AddJsonObject(&Parent, Child, Allocator);

    json* Object;

    if (!GetJsonObject(&Object, &Parent, Name))
    {
        TEST_RETURN_FAILURE("Found no object with the name %str", Name);
    }

    if (Object == NULL)
    {
        TEST_RETURN_FAILURE("Failed to get a json object");
    }

    if (!StringsEqual(Name, Object->Name))
    {
        TEST_RETURN_FAILURE("The name is not correct from the given json object");
    }

    json* JsonObject;

    if (GetJsonObject(&JsonObject, &Parent, "Name that does not exit"))
    {
        TEST_RETURN_FAILURE("Found object when searching for non existent object");
    }

    DestroyJson(&Parent, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(GetJsonNumberTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);
    f64    Value     = 100.5;
    string ValueName = CreateString("Value");

    AddJsonNumber(&JsonObject, ValueName, Value, Allocator);

    f64 Number;
    if (!GetJsonNumber(&Number, &JsonObject, ValueName))
    {
        TEST_RETURN_FAILURE("Failed to find and object with the name \"%str\"", ValueName);
    }

    if (!CompareFloat(Number, Value, .000001f))
    {
        TEST_RETURN_FAILURE("The number was incorrect");
    }

    f64 FloatValue;

    if (GetJsonNumber(&FloatValue, &JsonObject, "Name that does not exit"))
    {
        TEST_RETURN_FAILURE("Found number when searching for non existent number");
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(GetJsonBooleanTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);
    string BooleanNameFalse = CreateString("Value");
    string BooleanNameTrue  = CreateString("Value1");
    b32    BooleanFalse     = false;
    b32    BooleanTrue      = true;

    AddJsonBoolean(&JsonObject, BooleanNameFalse, BooleanFalse, Allocator);
    AddJsonBoolean(&JsonObject, BooleanNameTrue, BooleanTrue, Allocator);

    b32 CalculatedBooleanFalse;
    if (!GetJsonBoolean(&CalculatedBooleanFalse, &JsonObject, BooleanNameFalse))
    {
        TEST_RETURN_FAILURE("Failed to find the json object for the false boolean");
    }

    b32 CalculatedBooleanTrue;
    if (!GetJsonBoolean(&CalculatedBooleanTrue, &JsonObject, BooleanNameTrue))
    {
        TEST_RETURN_FAILURE("Failed to find the json object for the true boolean");
    }

    if (CalculatedBooleanFalse)
    {
        TEST_RETURN_FAILURE("Failed to get the correct boolean value of false");
    }

    if (!CalculatedBooleanTrue)
    {
        TEST_RETURN_FAILURE("Failed to get the correct boolean value of true");
    }

    b32 BooleanValue;

    if (GetJsonBoolean(&BooleanValue, &JsonObject, "Name that does not exit"))
    {
        TEST_RETURN_FAILURE("Found boolean when searching for non existent boolean");
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(GetJsonStringTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);
    string Value     = CreateString("test");
    string ValueName = CreateString("Value");

    AddJsonString(&JsonObject, ValueName, Value, Allocator);

    string String;
    GetJsonString(&String, &JsonObject, ValueName);

    if (!StringsEqual(String, Value))
    {
        TEST_RETURN_FAILURE("Failed to get the correct string value");
    }

    string StringValue;

    if (GetJsonString(&StringValue, &JsonObject, "Name that does not exit"))
    {
        TEST_RETURN_FAILURE("Found string when searching for non existent string");
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(GetJsonStringArrayTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);

    array<string> StringArray;
    CreateArray(&StringArray, Allocator);

    for (u32 Index = 1; Index < 7; ++Index)
    {
        string AddString;

        FormatString(&AddString, "value%u32", Allocator, Index);
        Add(&StringArray, AddString);
        FreeString(&AddString, Allocator);
    }

    string StringArrayName = CreateString("Value");

    AddJsonStringArray(&JsonObject, StringArrayName, StringArray, Allocator);

    array<string> CalculatedStringArray;
    GetJsonStringArray(&CalculatedStringArray, &JsonObject, StringArrayName);

    if (!CompareArray(CalculatedStringArray, StringArray))
    {
        TEST_RETURN_FAILURE("Failed to get the correct string array");
    }

    array<string> StringArrayValue;

    if (GetJsonStringArray(&StringArrayValue, &JsonObject, "Name that does not exit"))
    {
        TEST_RETURN_FAILURE("Found string array when searching for non existent string array");
    }

    for (u64 StringIndex = 0; StringIndex < StringArray.Size; ++StringIndex)
    {
        FreeString(StringArray + StringIndex, Allocator);
    }

    FreeArray(&StringArray);
    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(GetJsonBooleanArrayTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);

    array<b32> BooleanArray;
    CreateArray(&BooleanArray, Allocator);
    Add(&BooleanArray, (b32) true);
    Add(&BooleanArray, (b32) false);
    Add(&BooleanArray, (b32) true);
    Add(&BooleanArray, (b32) false);
    Add(&BooleanArray, (b32) true);
    Add(&BooleanArray, (b32) false);
    string BooleanArrayName = CreateString("Value");

    AddJsonBooleanArray(&JsonObject, BooleanArrayName, BooleanArray, Allocator);

    array<b32> CalculatedBooleanArray;
    GetJsonBooleanArray(&CalculatedBooleanArray, &JsonObject, BooleanArrayName);

    if (!CompareArray(CalculatedBooleanArray, BooleanArray))
    {
        TEST_RETURN_FAILURE("Failed to get the correct boolean array");
    }

    array<b32> BooleanArrayValue;

    if (GetJsonBooleanArray(&BooleanArrayValue, &JsonObject, "Name that does not exit"))
    {
        TEST_RETURN_FAILURE("Found b32 array when searching for non existent b32 array");
    }

    FreeArray(&BooleanArray);
    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(GetJsonNumberArrayTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);

    array<f64> NumberArray;
    CreateArray(&NumberArray, Allocator);
    Add(&NumberArray, 1.0);
    Add(&NumberArray, 1.0);
    Add(&NumberArray, 1.0);
    Add(&NumberArray, 1.0);
    Add(&NumberArray, 1.0);
    Add(&NumberArray, 1.0);
    string NumberArrayName = CreateString("Value");

    AddJsonNumberArray(&JsonObject, NumberArrayName, NumberArray, Allocator);

    array<f64> CalculatedNumberArray;
    GetJsonNumberArray(&CalculatedNumberArray, &JsonObject, NumberArrayName);

    if (!CompareArray(CalculatedNumberArray, NumberArray))
    {
        TEST_RETURN_FAILURE("Failed to get the correct boolean array");
    }

    array<f64> NumberArrayValue;

    if (GetJsonNumberArray(&NumberArrayValue, &JsonObject, "Name that does not exit"))
    {
        TEST_RETURN_FAILURE("Found number array when searching for non existent number array");
    }

    FreeArray(&NumberArray);
    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(GetJsonObjectArrayTest)
{
    json JsonObject;
    CreateEmptyJson(&JsonObject, CreateEmptyString(), Allocator);

    array<json> ObjectArray;
    CreateArray(&ObjectArray, Allocator);

    Add(&ObjectArray);
    Add(&ObjectArray);
    Add(&ObjectArray);
    Add(&ObjectArray);
    Add(&ObjectArray);
    Add(&ObjectArray);

    string ObjectArrayName = CreateString("Test_Array");
    AddJsonObjectArray(&JsonObject, ObjectArrayName, ObjectArray, Allocator);

    array<json> CalculatedObjectArray;
    GetJsonObjectArray(&CalculatedObjectArray, &JsonObject, ObjectArrayName);

    if (CalculatedObjectArray.Size != 6)
    {
        TEST_RETURN_FAILURE("Failed to get the correct object array");
    }

    array<json> JsonArrayValue;

    if (GetJsonObjectArray(&JsonArrayValue, &JsonObject, "Name that does not exit"))
    {
        TEST_RETURN_FAILURE("Found object array when searching for non existent object array");
    }
    FreeArray(&ObjectArray);
    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AddJsonObjectArrayTest)
{
    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(JsonParseNumbersTest)
{
    string JsonData =
        CreateString("{ \"Value1\": 1, \"Value2\": 2, \"Value3\": 3, \"Value4\": 4, \"Value5\": 5 }");
    json JsonObject;

    engine_memset((char*)&JsonObject, 0, sizeof(json));
    BufferToJSON(&JsonObject, JsonData.Buffer, JsonData.Length, Allocator);

    u32 NumberCount = JsonObject.Numbers.Size;

    if (NumberCount != 5)
    {
        TEST_RETURN_FAILURE("Failed to parse all 5 of the values, only %u32 were parsed", NumberCount);
    }

    for (u32 Index = 1; Index <= NumberCount; ++Index)
    {
        string ValueName;
        f64    Valuef;
        u32    Value;

        FormatString(&ValueName, "Value%u32", Allocator, Index);

        if (!GetJsonNumber(&Valuef, &JsonObject, ValueName))
        {
            TEST_RETURN_FAILURE("Failed to get number value for %str", ValueName);
        }

        Value = (u32)Valuef;

        if (Value != Index)
        {
            TEST_RETURN_FAILURE("Failed to get correct value for %str, got %u32 expected %u32", ValueName,
                                Value, Index);
        }

        FreeString(&ValueName, Allocator);
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(JsonParseBooleansTest)
{
    string JsonData = CreateString(
        "{ \"Value1\": false, \"Value2\": true, \"Value3\": false, \"Value4\": true, \"Value5\": false }");
    json JsonObject;
    b32  CorrectValue = false;

    engine_memset((char*)&JsonObject, 0, sizeof(json));
    BufferToJSON(&JsonObject, JsonData.Buffer, JsonData.Length, Allocator);

    u32 BooleanCount = JsonObject.Booleans.Size;

    if (BooleanCount != 5)
    {
        TEST_RETURN_FAILURE("Failed to parse all 5 of the values, only %u32 were parsed", BooleanCount);
    }

    for (u32 Index = 1; Index <= BooleanCount; ++Index)
    {
        string ValueName;
        b32    Value;

        FormatString(&ValueName, "Value%u32", Allocator, Index);

        if (!GetJsonBoolean(&Value, &JsonObject, ValueName))
        {
            TEST_RETURN_FAILURE("Failed to get boolean value for %str", ValueName);
        }

        if (Value != CorrectValue)
        {
            TEST_RETURN_FAILURE("Failed to get correct value for %str, got %b32 expected %b32", ValueName,
                                Value, CorrectValue);
        }

        CorrectValue = !CorrectValue;
        FreeString(&ValueName, Allocator);
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(JsonParseStringsTest)
{
    string JsonData = CreateString(
        "{ \"Value1\": \"Value1\", \"Value2\": \"Value2\", \"Value3\": \"Value3\", \"Value4\": \"Value4\", \"Value5\": \"Value5\" }");
    json JsonObject;

    engine_memset((char*)&JsonObject, 0, sizeof(json));
    BufferToJSON(&JsonObject, JsonData.Buffer, JsonData.Length, Allocator);

    u32 StringCount = JsonObject.Strings.Size;

    if (StringCount != 5)
    {
        TEST_RETURN_FAILURE("Failed to parse all 5 of the values, only %u32 were parsed", StringCount);
    }

    for (u32 Index = 1; Index <= StringCount; ++Index)
    {
        string ValueName;
        string Value;

        FormatString(&ValueName, "Value%u32", Allocator, Index);

        if (!GetJsonString(&Value, &JsonObject, ValueName))
        {
            TEST_RETURN_FAILURE("Failed to get boolean value for %str", ValueName);
        }

        if (!Value)
        {
            TEST_RETURN_FAILURE("Invalid string value from json string: %str", ValueName);
        }

        if (Value != ValueName)
        {
            TEST_RETURN_FAILURE("Incorrect string value(\"%str\") from json string: %str", Value, ValueName);
        }

        FreeString(&ValueName, Allocator);
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(JsonParseObjectsTest)
{
    string JsonData = CreateString(
        "{ \"Value1\":{\"Test\":100},\"Value2\":{\"Test\":false},\"Value3\":{\"Test\":\"Value3\"},\"Value4\": {\"Value4\":{\"Test\":\"Test\"}},\"Value5\":{\"Value5\":{\"Value5\":{\"Test1\":\"Test1\",\"Test2\":false}}} }");
    json JsonObject;

    engine_memset((char*)&JsonObject, 0, sizeof(json));
    BufferToJSON(&JsonObject, JsonData.Buffer, JsonData.Length, Allocator);

    u32 ObjectCount = JsonObject.Objects.Size;

    if (ObjectCount != 5)
    {
        TEST_RETURN_FAILURE("Failed to parse all 5 of the values, only %u32 were parsed", ObjectCount);
    }

    {
        json* Value1;

        if (!GetJsonObject(&Value1, &JsonObject, "Value1"))
        {
            TEST_RETURN_FAILURE("Failed to get boolean value for Value1");
        }

        if (Value1 == NULL)
        {
            TEST_RETURN_FAILURE("Invalid object value from json buffer");
        }

        if (Value1->Numbers.Size != 1)
        {
            TEST_RETURN_FAILURE("Incorrect numbers length for the object Value1");
        }

        f64 Value1f;
        u32 Value1u;

        if (!GetJsonNumber(&Value1f, Value1, "Test"))
        {
            TEST_RETURN_FAILURE("Failed to get value Test from the Value1 json object");
        }

        Value1u = RoundFloat(Value1f);

        if (Value1u != 100)
        {
            TEST_RETURN_FAILURE("Failed to get the correct value from Test from the Value1 json object");
        }
    }

    {
        json* Value2;

        if (!GetJsonObject(&Value2, &JsonObject, "Value2"))
        {
            TEST_RETURN_FAILURE("Failed to get boolean value for Value2");
        }

        if (Value2 == NULL)
        {
            TEST_RETURN_FAILURE("Invalid object value from json buffer");
        }

        if (Value2->Booleans.Size != 1)
        {
            TEST_RETURN_FAILURE("Incorrect numbers length for the object Value2");
        }

        b32 Value2b;

        if (!GetJsonBoolean(&Value2b, Value2, "Test"))
        {
            TEST_RETURN_FAILURE("Failed to get value Test from the Value2 json object");
        }

        if (Value2b)
        {
            TEST_RETURN_FAILURE("Failed to get the correct value from Test from the Value2 json object");
        }
    }

    {
        json* Value3;

        if (!GetJsonObject(&Value3, &JsonObject, "Value3"))
        {
            TEST_RETURN_FAILURE("Failed to get boolean value for Value3");
        }

        if (Value3 == NULL)
        {
            TEST_RETURN_FAILURE("Invalid object value from json buffer");
        }

        if (Value3->Strings.Size != 1)
        {
            TEST_RETURN_FAILURE("Incorrect numbers length for the object Value3");
        }

        string Value3s;

        if (!GetJsonString(&Value3s, Value3, "Test"))
        {
            TEST_RETURN_FAILURE("Failed to get value Test from the Value3 json object");
        }

        if (Value3s != "Value3")
        {
            TEST_RETURN_FAILURE("Failed to get the correct value from Test from the Value3 json object");
        }
    }

    {
        json* Value4;

        if (!GetJsonObject(&Value4, &JsonObject, "Value4"))
        {
            TEST_RETURN_FAILURE("Failed to get boolean value for Value4");
        }

        if (Value4 == NULL)
        {
            TEST_RETURN_FAILURE("Invalid object value from json buffer");
        }

        if (Value4->Objects.Size != 1)
        {
            TEST_RETURN_FAILURE("Incorrect numbers length for the object Value4");
        }

        json* Value4Child;

        if (!GetJsonObject(&Value4Child, Value4, "Value4"))
        {
            TEST_RETURN_FAILURE("Failed to get value Test from the Value3 json object");
        }

        if (Value4Child == NULL)
        {
            TEST_RETURN_FAILURE("Failed to get child object from Value3 json object");
        }

        if (Value4Child->Strings.Size != 1)
        {
            TEST_RETURN_FAILURE("Failed to parse string value from child object of Value4");
        }

        string TestValue4;

        if (!GetJsonString(&TestValue4, Value4Child, "Test"))
        {
            TEST_RETURN_FAILURE("Failed to get string value from child object of Value4");
        }

        if (TestValue4 != "Test")
        {
            TEST_RETURN_FAILURE("Incorrect value for json string Test inside Value4 child object");
        }
    }

    { //\"Value5\":{\"Value5\":{\"Value5\":{\"Test1\":\"Test\",\"Test2\":false}}}
        json* Value5;

        if (!GetJsonObject(&Value5, &JsonObject, "Value5"))
        {
            TEST_RETURN_FAILURE("Failed to get boolean value for Value5");
        }

        if (Value5 == NULL)
        {
            TEST_RETURN_FAILURE("Invalid object value from json buffer");
        }

        if (Value5->Objects.Size != 1)
        {
            TEST_RETURN_FAILURE("Incorrect numbers length for the object Value5");
        }

        json* Value5Child;

        if (!GetJsonObject(&Value5Child, Value5, "Value5"))
        {
            TEST_RETURN_FAILURE("Failed to get child object from Value5");
        }

        if (Value5Child == NULL)
        {
            TEST_RETURN_FAILURE("Failed to get child object from Value3 json object");
        }

        if (Value5Child->Objects.Size != 1)
        {
            TEST_RETURN_FAILURE("Failed to parse object value from child object of Value5");
        }

        json* Value5GrandChild;

        if (!GetJsonObject(&Value5GrandChild, Value5Child, "Value5"))
        {
            TEST_RETURN_FAILURE("Failed to get grandchild object from Value5");
        }

        if (Value5GrandChild == NULL)
        {
            TEST_RETURN_FAILURE("Failed to get valid grandchild object from Value5");
        }

        if (Value5GrandChild->Strings.Size != 1)
        {
            TEST_RETURN_FAILURE("Failed to parse string value from great grandchild object of Value5");
        }

        if (Value5GrandChild->Booleans.Size != 1)
        {
            TEST_RETURN_FAILURE("Failed to parse boolean value from great grandchild object of Value5");
        }

        string Value5s;
        b32    Value5b;

        if (!GetJsonString(&Value5s, Value5GrandChild, "Test1"))
        {
            TEST_RETURN_FAILURE("Failed to get Test1 from great grandchild object from Value5");
        }

        if (!GetJsonBoolean(&Value5b, Value5GrandChild, "Test2"))
        {
            TEST_RETURN_FAILURE("Failed to get Test2 from great grandchild object from Value5");
        }

        if (Value5s != "Test1")
        {
            TEST_RETURN_FAILURE("Incorrect value for Test1 in great grandchild of Value5");
        }

        if (Value5b)
        {
            TEST_RETURN_FAILURE("Incorrect value for Test2 in great grandchild of Value5");
        }
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(JsonParseNumberArraysTest)
{
    string JsonData = CreateString(
        "{\"Value1\":[0,1,2,3,4],\"Value2\":[4,3,2,1,0],\"Value3\":[5,6,7,8,9],\"Value4\":[9,8,7,6,5],\"Value5\":[10,11,12,13,14]}");
    json JsonObject;
    
    engine_memset((char*)&JsonObject, 0, sizeof(json));
    BufferToJSON(&JsonObject, JsonData.Buffer, JsonData.Length, Allocator);

    array<f64> Value1Array;
    array<f64> Value2Array;
    array<f64> Value3Array;
    array<f64> Value4Array;
    array<f64> Value5Array;

    if (!GetJsonNumberArray(&Value1Array, &JsonObject, "Value1"))
    {
        TEST_RETURN_FAILURE("Failed to get Value1 Array");
    }

    if (!GetJsonNumberArray(&Value2Array, &JsonObject, "Value2"))
    {
        TEST_RETURN_FAILURE("Failed to get Value2 Array");
    }

    if (!GetJsonNumberArray(&Value3Array, &JsonObject, "Value3"))
    {
        TEST_RETURN_FAILURE("Failed to get Value3 Array");
    }

    if (!GetJsonNumberArray(&Value4Array, &JsonObject, "Value4"))
    {
        TEST_RETURN_FAILURE("Failed to get Value4 Array");
    }

    if (!GetJsonNumberArray(&Value5Array, &JsonObject, "Value5"))
    {
        TEST_RETURN_FAILURE("Failed to get Value5 Array");
    }

    for (u32 Index = 0; Index < 5; ++Index)
    {
        u32 Value1ArrayValue = (u32)Value1Array[Index];
        u32 Value2ArrayValue = (u32)Value2Array[Index];
        u32 Value3ArrayValue = (u32)Value3Array[Index];
        u32 Value4ArrayValue = (u32)Value4Array[Index];
        u32 Value5ArrayValue = (u32)Value5Array[Index];

        if (Value1ArrayValue != Index)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value1 Array");
        }

        if (Value2ArrayValue != 4 - Index)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value2 Array");
        }

        if (Value3ArrayValue != Index + 5)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value3 Array");
        }

        if (Value4ArrayValue != 9 - Index)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value4 Array");
        }

        if (Value5ArrayValue != Index + 10)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value5 Array");
        }
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(JsonParseBooleanArraysTest)
{
    string JsonData = CreateString(
        "{\"Value1\":[false,true,false,true,false],\"Value2\":[true,false,true,false,true],\"Value3\":[false,true,false,true,false],\"Value4\":[true,false,true,false,true]}");
    json JsonObject;

    engine_memset((char*)&JsonObject, 0, sizeof(json));
    BufferToJSON(&JsonObject, JsonData.Buffer, JsonData.Length, Allocator);

    array<b32> Value1Array;
    array<b32> Value2Array;
    array<b32> Value3Array;
    array<b32> Value4Array;

    if (!GetJsonBooleanArray(&Value1Array, &JsonObject, "Value1"))
    {
        TEST_RETURN_FAILURE("Failed to get Value1 Array");
    }

    if (!GetJsonBooleanArray(&Value2Array, &JsonObject, "Value2"))
    {
        TEST_RETURN_FAILURE("Failed to get Value2 Array");
    }

    if (!GetJsonBooleanArray(&Value3Array, &JsonObject, "Value3"))
    {
        TEST_RETURN_FAILURE("Failed to get Value3 Array");
    }

    if (!GetJsonBooleanArray(&Value4Array, &JsonObject, "Value4"))
    {
        TEST_RETURN_FAILURE("Failed to get Value4 Array");
    }

    b32 TestValue = false;

    for (u32 Index = 0; Index < 5; ++Index)
    {
        b32 Value1ArrayValue = Value1Array[Index];
        b32 Value2ArrayValue = Value2Array[Index];
        b32 Value3ArrayValue = Value3Array[Index];
        b32 Value4ArrayValue = Value4Array[Index];

        if (Value1ArrayValue != TestValue)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value1 Array, got %b32 expected %b32",
                                Value1ArrayValue, TestValue);
        }

        if (Value2ArrayValue == TestValue)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value2 Array, got %b32 expected %b32",
                                Value2ArrayValue, !TestValue);
        }

        if (Value3ArrayValue != TestValue)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value3 Array, got %b32 expected %b32",
                                Value3ArrayValue, TestValue);
        }

        if (Value4ArrayValue == TestValue)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value4 Array, got %b32 expected %b32",
                                Value4ArrayValue, !TestValue);
        }

        TestValue = !TestValue;
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(JsonParseStringArraysTest)
{
    string JsonData = CreateString(
        "{\"Value1\":[\"Value11\",\"Value12\",\"Value13\",\"Value14\",\"Value15\"],\"Value2\":[\"Value21\",\"Value22\",\"Value23\",\"Value24\",\"Value25\"],\"Value3\":[\"Value31\",\"Value32\",\"Value33\",\"Value34\",\"Value35\"],\"Value4\":[\"Value41\",\"Value42\",\"Value43\",\"Value44\",\"Value45\"]}");
    json JsonObject;

    engine_memset((char*)&JsonObject, 0, sizeof(json));
    BufferToJSON(&JsonObject, JsonData.Buffer, JsonData.Length, Allocator);

    array<string> Value1Array;
    array<string> Value2Array;
    array<string> Value3Array;
    array<string> Value4Array;

    if (!GetJsonStringArray(&Value1Array, &JsonObject, "Value1"))
    {
        TEST_RETURN_FAILURE("Failed to get Value1 Array");
    }

    if (!GetJsonStringArray(&Value2Array, &JsonObject, "Value2"))
    {
        TEST_RETURN_FAILURE("Failed to get Value2 Array");
    }

    if (!GetJsonStringArray(&Value3Array, &JsonObject, "Value3"))
    {
        TEST_RETURN_FAILURE("Failed to get Value3 Array");
    }

    if (!GetJsonStringArray(&Value4Array, &JsonObject, "Value4"))
    {
        TEST_RETURN_FAILURE("Failed to get Value4 Array");
    }

    for (u32 Index = 1; Index <= 5; ++Index)
    {
        string Value1ArrayValue = Value1Array[Index - 1];
        string Value2ArrayValue = Value2Array[Index - 1];
        string Value3ArrayValue = Value3Array[Index - 1];
        string Value4ArrayValue = Value4Array[Index - 1];

        string ExpectedValue1;
        string ExpectedValue2;
        string ExpectedValue3;
        string ExpectedValue4;

        FormatString(&ExpectedValue1, "Value1%u32", Allocator, Index);
        FormatString(&ExpectedValue2, "Value2%u32", Allocator, Index);
        FormatString(&ExpectedValue3, "Value3%u32", Allocator, Index);
        FormatString(&ExpectedValue4, "Value4%u32", Allocator, Index);

        if (Value1ArrayValue != ExpectedValue1)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value1 Array, got \"%str\" expected \"%str\"",
                                Value1ArrayValue, ExpectedValue1);
        }

        if (Value2ArrayValue != ExpectedValue2)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value2 Array, got \"%str\" expected \"%str\"",
                                Value2ArrayValue, ExpectedValue2);
        }

        if (Value3ArrayValue != ExpectedValue3)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value3 Array, got \"%str\" expected \"%str\"",
                                Value3ArrayValue, ExpectedValue3);
        }

        if (Value4ArrayValue != ExpectedValue4)
        {
            TEST_RETURN_FAILURE("Incorrect array value in Value4 Array, got \"%str\" expected \"%str\"",
                                Value4ArrayValue, ExpectedValue4);
        }

        FreeString(&ExpectedValue1, Allocator);
        FreeString(&ExpectedValue2, Allocator);
        FreeString(&ExpectedValue3, Allocator);
        FreeString(&ExpectedValue4, Allocator);
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(JsonParseObjectArraysTest)
{
    string JsonData = CreateString(
        "{\"TestArray\":[{\"Value1\":[\"Value11\",\"Value12\",\"Value13\",\"Value14\",\"Value15\"],\"Value2\":[\"Value21\",\"Value22\",\"Value23\",\"Value24\",\"Value25\"],\"Value3\":[\"Value31\",\"Value32\",\"Value33\",\"Value34\",\"Value35\"],\"Value4\":[\"Value41\",\"Value42\",\"Value43\",\"Value44\",\"Value45\"]},{\"Value1\":[\"Value11\",\"Value12\",\"Value13\",\"Value14\",\"Value15\"],\"Value2\":[\"Value21\",\"Value22\",\"Value23\",\"Value24\",\"Value25\"],\"Value3\":[\"Value31\",\"Value32\",\"Value33\",\"Value34\",\"Value35\"],\"Value4\":[\"Value41\",\"Value42\",\"Value43\",\"Value44\",\"Value45\"]},{\"Value1\":[\"Value11\",\"Value12\",\"Value13\",\"Value14\",\"Value15\"],\"Value2\":[\"Value21\",\"Value22\",\"Value23\",\"Value24\",\"Value25\"],\"Value3\":[\"Value31\",\"Value32\",\"Value33\",\"Value34\",\"Value35\"],\"Value4\":[\"Value41\",\"Value42\",\"Value43\",\"Value44\",\"Value45\"]},{\"Value1\":[\"Value11\",\"Value12\",\"Value13\",\"Value14\",\"Value15\"],\"Value2\":[\"Value21\",\"Value22\",\"Value23\",\"Value24\",\"Value25\"],\"Value3\":[\"Value31\",\"Value32\",\"Value33\",\"Value34\",\"Value35\"],\"Value4\":[\"Value41\",\"Value42\",\"Value43\",\"Value44\",\"Value45\"]}]}");
    json JsonObject;
    
    engine_memset((char*)&JsonObject, 0, sizeof(json));
    BufferToJSON(&JsonObject, JsonData.Buffer, JsonData.Length, Allocator);

    array<json> Objects;

    if (!GetJsonObjectArray(&Objects, &JsonObject, "TestArray"))
    {
        TEST_RETURN_FAILURE("Failed to get TestArray object array");
    }

    u32 ObjectCount = Objects.Size;

    if (ObjectCount != 4)
    {
        TEST_RETURN_FAILURE("Incorrect number of objects in TestArray");
    }

    for (u32 ObjectIndex = 0; ObjectIndex < ObjectCount; ++ObjectIndex)
    {
        json* Object = Objects + ObjectIndex;

        array<string> Value1Array;
        array<string> Value2Array;
        array<string> Value3Array;
        array<string> Value4Array;

        if (!GetJsonStringArray(&Value1Array, Object, "Value1"))
        {
            TEST_RETURN_FAILURE("Failed to get Value1 Array");
        }

        if (!GetJsonStringArray(&Value2Array, Object, "Value2"))
        {
            TEST_RETURN_FAILURE("Failed to get Value2 Array");
        }

        if (!GetJsonStringArray(&Value3Array, Object, "Value3"))
        {
            TEST_RETURN_FAILURE("Failed to get Value3 Array");
        }

        if (!GetJsonStringArray(&Value4Array, Object, "Value4"))
        {
            TEST_RETURN_FAILURE("Failed to get Value4 Array");
        }

        for (u32 Index = 1; Index <= 5; ++Index)
        {
            string Value1ArrayValue = Value1Array[Index - 1];
            string Value2ArrayValue = Value2Array[Index - 1];
            string Value3ArrayValue = Value3Array[Index - 1];
            string Value4ArrayValue = Value4Array[Index - 1];

            string ExpectedValue1;
            string ExpectedValue2;
            string ExpectedValue3;
            string ExpectedValue4;

            FormatString(&ExpectedValue1, "Value1%u32", Allocator, Index);
            FormatString(&ExpectedValue2, "Value2%u32", Allocator, Index);
            FormatString(&ExpectedValue3, "Value3%u32", Allocator, Index);
            FormatString(&ExpectedValue4, "Value4%u32", Allocator, Index);

            if (Value1ArrayValue != ExpectedValue1)
            {
                TEST_RETURN_FAILURE("Incorrect array value in Value1 Array, got \"%str\" expected \"%str\"",
                                    Value1ArrayValue, ExpectedValue1);
            }

            if (Value2ArrayValue != ExpectedValue2)
            {
                TEST_RETURN_FAILURE("Incorrect array value in Value2 Array, got \"%str\" expected \"%str\"",
                                    Value2ArrayValue, ExpectedValue2);
            }

            if (Value3ArrayValue != ExpectedValue3)
            {
                TEST_RETURN_FAILURE("Incorrect array value in Value3 Array, got \"%str\" expected \"%str\"",
                                    Value3ArrayValue, ExpectedValue3);
            }

            if (Value4ArrayValue != ExpectedValue4)
            {
                TEST_RETURN_FAILURE("Incorrect array value in Value4 Array, got \"%str\" expected \"%str\"",
                                    Value4ArrayValue, ExpectedValue4);
            }

            FreeString(&ExpectedValue1, Allocator);
            FreeString(&ExpectedValue2, Allocator);
            FreeString(&ExpectedValue3, Allocator);
            FreeString(&ExpectedValue4, Allocator);
        }
    }

    DestroyJson(&JsonObject, Allocator);

    TEST_RETURN_SUCCESS();
}

void
AddJsonTests(array<test>* Tests)
{
    AddTest(CreateEmptyJsonTest, "CreateEmptyJson Test");

    AddTest(AddJsonObjectTest, "AddJsonObject Test");
    AddTest(AddJsonNumberTest, "AddJsonNumber Test");
    AddTest(AddJsonBooleanTest, "AddJsonBoolean Test");
    AddTest(AddJsonStringTest, "AddJsonString Test");
    AddTest(AddJsonObjectArrayTest, "AddJsonObjectArray Test");
    AddTest(AddJsonStringArrayTest, "AddJsonStringArray Test");
    AddTest(AddJsonBooleanArrayTest, "AddJsonBooleanArray Test");
    AddTest(AddJsonNumberArrayTest, "AddJsonNumberArray Test");

    AddTest(GetJsonObjectTest, "GetJsonObject Test");
    AddTest(GetJsonNumberTest, "GetJsonNumber Test");
    AddTest(GetJsonBooleanTest, "GetJsonBoolean Test");
    AddTest(GetJsonStringTest, "GetJsonString Test");
    AddTest(GetJsonObjectArrayTest, "GetJsonObjectArray Test");
    AddTest(GetJsonStringArrayTest, "GetJsonStringArray Test");
    AddTest(GetJsonBooleanArrayTest, "GetJsonBooleanArray Test");
    AddTest(GetJsonNumberArrayTest, "GetJsonNumberArray Test");

    AddTest(JsonParseNumbersTest, "Json Parse Numbers Test");
    AddTest(JsonParseBooleansTest, "Json Parse Booleans Test");
    AddTest(JsonParseStringsTest, "Json Parse Strings Test");
    AddTest(JsonParseObjectsTest, "Json Parse Objects Test");
    AddTest(JsonParseNumberArraysTest, "Json Parse Number Arrays Test");
    AddTest(JsonParseBooleanArraysTest, "Json Parse Boolean Arrays Test");
    AddTest(JsonParseStringArraysTest, "Json Parse String Arrays Test");
    AddTest(JsonParseObjectArraysTest, "Json Parse Object Arrays Test");
}
