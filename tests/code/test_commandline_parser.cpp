#include "test_commandline_parser.h"

#include <std/commandline_parser.h>

/*

struct split_string
{
    string* Buffer;
    u32     Count;
};
 */

TEST_FUNCTION(AddKnownFlagsTest)
{
    string CommandlineCommand = CreateString(
        "./test_application -f --flag -o option --option=option_equal -s \"string\" --string=\"string_equal\" -e \"string with spaces\" --word=\"equal string with spaces\"");
    split_string SplitCommand = SplitString(CommandlineCommand, ' ', Allocator);
    char**       Args         = (char**)Allocator->Allocate(sizeof(char**) * SplitCommand.Count);

    for (u32 Index = 0; Index < SplitCommand.Count; ++Index)
    {
        Args[Index] = SplitCommand.Buffer[Index].Buffer;
    }

    commandline_parser Parser;
    CreateCommandlineParser(&Parser, CreateString("This is the general help message\n"), Allocator);

    flag* FlagShortFlag = AddFlag(&Parser, CreateString("f"), CreateEmptyString(), ARGUMENT_TYPE_FLAG,
                                  CreateString("Flag for flag"));
    flag* FlagLongFlag  = AddFlag(&Parser, CreateEmptyString(), CreateString("flag"), ARGUMENT_TYPE_FLAG,
                                 CreateString("Equal Flag for flag"));

    flag* FlagShortOption = AddFlag(&Parser, CreateString("o"), CreateEmptyString(), ARGUMENT_TYPE_WORD,
                                    CreateString("Flag for option word"));
    flag* FlagLongOption  = AddFlag(&Parser, CreateEmptyString(), CreateString("option"), ARGUMENT_TYPE_WORD,
                                   CreateString("Flag for option equal word"));

    flag* FlagShortString = AddFlag(&Parser, CreateString("s"), CreateEmptyString(), ARGUMENT_TYPE_STRING,
                                    CreateString("Flag for string word"));
    flag* FlagLongString = AddFlag(&Parser, CreateEmptyString(), CreateString("string"), ARGUMENT_TYPE_STRING,
                                   CreateString("Flag for string equal word"));

    flag* FlagShortMultiString = AddFlag(&Parser, CreateString("e"), CreateEmptyString(),
                                         ARGUMENT_TYPE_STRING, CreateString("Flag for multi word string"));
    flag* FlagLongMultiString =
        AddFlag(&Parser, CreateEmptyString(), CreateString("word"), ARGUMENT_TYPE_STRING,
                CreateString("Flag for string multi word equal"));

    ParseCommandlineArgs(&Parser, (int)SplitCommand.Count, Args);

    if (!FlagShortFlag->Found)
    {
        TEST_RETURN_FAILURE("Flag -f failed to be parsed");
    }

    if (!FlagLongFlag->Found)
    {
        TEST_RETURN_FAILURE("Flag --flag failed to be parsed");
    }

    if (!FlagShortOption->Found)
    {
        TEST_RETURN_FAILURE("Failed to get flag -o");
    }

    if (FlagShortOption->Value != "option")
    {
        TEST_RETURN_FAILURE("Failed to value from flag -o");
    }

    if (!FlagLongOption->Found)
    {
        TEST_RETURN_FAILURE("Failed to get flag --option");
    }

    if (FlagLongOption->Value != "option_equal")
    {
        TEST_RETURN_FAILURE("Failed to value from flag --option");
    }

    if (!FlagShortString->Found)
    {
        TEST_RETURN_FAILURE("Failed to get flag -s");
    }

    if (FlagShortString->Value != "string")
    {
        TEST_RETURN_FAILURE("Failed to value from flag -s");
    }

    if (!FlagLongString->Found)
    {
        TEST_RETURN_FAILURE("Failed to get flag --string");
    }

    if (FlagLongString->Value != "string_equal")
    {
        TEST_RETURN_FAILURE("Failed to value from flag --string");
    }

    if (!FlagShortMultiString->Found)
    {
        TEST_RETURN_FAILURE("Failed to get flag -e");
    }

    if (FlagShortMultiString->Value != "string with spaces")
    {
        TEST_RETURN_FAILURE("Failed to value from flag -e");
    }

    if (!FlagLongMultiString->Found)
    {
        TEST_RETURN_FAILURE("Failed to get flag --word");
    }

    if (FlagLongMultiString->Value != "equal string with spaces")
    {
        TEST_RETURN_FAILURE("Failed to value from flag --word");
    }

    Allocator->Free(Args, sizeof(char**) * SplitCommand.Count);

    for (u32 Index = 0; Index < SplitCommand.Count; ++Index)
    {
        FreeString(SplitCommand.Buffer + Index, Allocator);
    }

    Allocator->Free(SplitCommand.Buffer, sizeof(string) * SplitCommand.Count);

    DestroyCommandlineParser(&Parser, Allocator);

    TEST_RETURN_SUCCESS();
}

void
AddCommandlineParserTests(array<test>* Tests)
{
    AddTest(AddKnownFlagsTest, "AddKnownFlagsTest Test");
}
