#if !defined(TESTS_H)

#define Print(FormattedString, ...)					\
    {                                                                                                        \
        string Result;                                                                                       \
        FormatString(&Result, FormattedString, HEAP_ALLOCATOR_MEMORY_CALLBACK(), __VA_ARGS__);               \
        StandardPrint(Result.Buffer);                                                                        \
        FreeString(&Result);                                                                                  \
    }


#define TESTS_H
#endif
