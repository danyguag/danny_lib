#include "test_packet_handler.h"

#include <std/packet_handler.h>

TEST_FUNCTION(CreateAndDestroyPacketHandlerTest)
{
    packet_handler PacketHandler;
    CreatePacketHandler(&PacketHandler, Allocator);

    if (PacketHandler.Callbacks.AllocatedLength < 1)
    {
	TEST_RETURN_FAILURE("Failed to create the callbacks array");
    }
    
    DestroyPacketHandler(&PacketHandler);

    if (PacketHandler.Callbacks.AllocatedLength > 0)
    {
	TEST_RETURN_FAILURE("Failed to create the callbacks array");
    }
    
    TEST_RETURN_SUCCESS();
}

void
TestFunctionCallback(system_handle Socket, stream* Stream, void* UserData)
{
    UNUSED_VARIABLE(Socket);
    UNUSED_VARIABLE(Stream);
    
    u32* Test = (u32*) UserData;

    if (Stream && Stream->WritePointer > 0)
    {
	u32 TestValue = Read<u32>(Stream);

	if (TestValue != 1)
	{
	    StandardPrint(CreateString("Failed to read valid value from test write stream\n"));
	}
    }
    
    *Test = *Test + 1;
}

TEST_FUNCTION(AddCallbackTest)
{
    packet_handler PacketHandler;
    CreatePacketHandler(&PacketHandler, Allocator);

    //Something non 0 will do for this test
    u32 PacketId = 10;
    u32 Test = 0;

    AddCallback(&PacketHandler, PacketId, TestFunctionCallback, &Test);
    packet_handler_callback* Callback = GetPacketFunctionById(PacketHandler.Callbacks, 100);

    if (Callback != NULL)
    {
        TEST_RETURN_FAILURE("Found a callback with an invalid packet id");
    }

    Callback = GetPacketFunctionById(PacketHandler.Callbacks, PacketId);

    if (!Callback)
    {
	TEST_RETURN_FAILURE("The function was either not added or added into the incorrect location");
    }

    if (!Callback->Function)
    {
	TEST_RETURN_FAILURE("The function pointer is not valid");
    }

    if (!Callback->UserData)
    {
	TEST_RETURN_FAILURE("The user data is not value");
    }
    
    Callback->Function(0, NULL, Callback->UserData);

    if (Test != 1)
    {
	TEST_RETURN_FAILURE("The expected value for test was not 1");
    }
    
    DestroyPacketHandler(&PacketHandler);
    
    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(RemoveCallbackTest)
{
    packet_handler PacketHandler;
    CreatePacketHandler(&PacketHandler, Allocator);

    //Something non 0 will do for this test
    u32 PacketId = 10;
    u32 Test = 0;

    AddCallback(&PacketHandler, PacketId, TestFunctionCallback, &Test);
    RemoveCallback(&PacketHandler, PacketId);
    packet_handler_callback* Callback = GetPacketFunctionById(PacketHandler.Callbacks, PacketId);

    if (Callback)
    {
	TEST_RETURN_FAILURE("The callback was not removed");
    }
    
    DestroyPacketHandler(&PacketHandler);
    
    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(PacketWriteTest)
{
    u32 PacketId = 100;
    u32 IntegerCount = 1000;
    stream PacketStream;

    CreateStream(&PacketStream, Allocator, DEFAULT_STREAM_ALLOCATION_SIZE);
    StartPacketWrite(&PacketStream, PacketId);

    for (u32 Index = 0; Index < IntegerCount; ++Index)
    {
        Write<u32>(&PacketStream, Index);
    }

    EndPacketWrite(&PacketStream);

    u64 ExpectedPacketLength = sizeof(u32) * IntegerCount;
    u64 ExpectedPacketStreamLength = sizeof(u32) + sizeof(u64) + ExpectedPacketLength;

    if (PacketStream.WritePointer != ExpectedPacketStreamLength)
    {
        TEST_RETURN_FAILURE("Invalid write stream length: %u64 expected: %u64", PacketStream.Length, ExpectedPacketStreamLength);
    }

    u32 ReadPacketId = Read<u32>(&PacketStream);

    if (ReadPacketId != PacketId)
    {
        TEST_RETURN_FAILURE("Packet Id is in the incorrect place, read: %u32 expected %u32", ReadPacketId, PacketId);
    }

    u64 ReadPacketLength = Read<u64>(&PacketStream);

    if (ReadPacketLength != ExpectedPacketLength)
    {
        TEST_RETURN_FAILURE("Packet length is in the incorrect place, read: %u64 expected: %u64", ReadPacketLength, ExpectedPacketLength);
    }

    for (u32 Index = 0; Index < IntegerCount; ++Index)
    {
        u32 PacketValue = Read<u32>(&PacketStream);

	if (PacketValue != Index)
	{
            TEST_RETURN_FAILURE("Packet value is the invalid: %u32 expected: %u32", PacketValue, Index);
	}
    }

    DestroyStream(&PacketStream);

    TEST_RETURN_SUCCESS();
}

void
AddPacketHandlerTests(array<test>* Tests)
{
    AddTest(CreateAndDestroyPacketHandlerTest, "Create and Destroy PacketHandler Test");
    AddTest(AddCallbackTest, "AddCallback Test");
    AddTest(RemoveCallbackTest, "RemoveCallback Test");
    AddTest(PacketWriteTest, "Packet Write Test");
}
