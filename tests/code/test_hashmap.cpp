#include "test_hashmap.h"

#include <std/hash_map.h>

TEST_FUNCTION(CreateAndFreeHashmap)
{
    hash_map<u32> Map;

    if (Map)
    {
        TEST_RETURN_FAILURE("Map is not false before creation");
    }

    u64 Capacity = 512;

    CreateHashmap(&Map, Allocator, Capacity);

    if (!Map)
    {
        TEST_RETURN_FAILURE("Failed to create hashmap");
    }

    if (!Map.Buffer)
    {
        TEST_RETURN_FAILURE("Failed to create hashmap buffer");
    }

    if (Map.Size != 0)
    {
        TEST_RETURN_FAILURE("Failed to initialize hashmap size: %u64", Map.Size);
    }

    if (Map.Capacity != Capacity)
    {
        TEST_RETURN_FAILURE("Failed to initialize hashmap Capacity: %u64", Map.Capacity);
    }

    DestroyHashmap(&Map);

    if (Map)
    {
        TEST_RETURN_FAILURE("Failed to destroy hashmap");
    }

    if (Map.Buffer)
    {
        TEST_RETURN_FAILURE("Failed to destroy hashmap buffer");
    }

    if (Map.Size != 0 || Map.Capacity != 0)
    {
        TEST_RETURN_FAILURE("Failed to deinitialize hashmap size and capacity");
    }

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AddAndGetTest)
{
    hash_map<u32> Map;
    CreateHashmap(&Map, Allocator);

    string Key1 = CreateString("test1");
    string Key2 = CreateString("test2");
    string Key3 = CreateString("test3");
    string Key4 = CreateString("test4");
    string Key5 = CreateString("test5");

    u32 Value1 = 101;
    u32 Value2 = 102;
    u32 Value3 = 103;
    u32 Value4 = 104;
    u32 Value5 = 105;

    u32* pValue1 = Add(&Map, Key1);
    *pValue1     = Value1;

    if (Map.Size != 1)
    {
        TEST_RETURN_FAILURE("Failed to add 1st value pair");
    }

    u32* pValue2 = Add(&Map, Key2);
    *pValue2     = Value2;

    if (Map.Size != 2)
    {
        TEST_RETURN_FAILURE("Failed to add 2nd value pair");
    }

    u32* pValue3 = Add(&Map, Key3);
    *pValue3     = Value3;

    if (Map.Size != 3)
    {
        TEST_RETURN_FAILURE("Failed to add 2nd value pair");
    }

    u32* pValue4 = Add(&Map, Key4);
    *pValue4     = Value4;

    if (Map.Size != 4)
    {
        TEST_RETURN_FAILURE("Failed to add 2nd value pair");
    }

    u32* pValue5 = Add(&Map, Key5);
    *pValue5     = Value5;

    if (Map.Size != 5)
    {
        TEST_RETURN_FAILURE("Failed to add 2nd value pair");
    }

    u32* StoredValue1 = Get(&Map, Key1);
    u32* StoredValue2 = Get(&Map, Key2);
    u32* StoredValue3 = Get(&Map, Key3);
    u32* StoredValue4 = Get(&Map, Key4);
    u32* StoredValue5 = Get(&Map, Key5);

    if (!StoredValue1)
    {
        TEST_RETURN_FAILURE("Failed to load value for Key: %str", Key1);
    }

    if (*StoredValue1 != Value1)
    {
        TEST_RETURN_FAILURE("Failed to load the right value for Key: %str, value: %u32", Key1, StoredValue1);
    }

    if (!StoredValue2)
    {
        TEST_RETURN_FAILURE("Failed to load value for Key: %str", Key2);
    }

    if (*StoredValue2 != Value2)
    {
        TEST_RETURN_FAILURE("Failed to load the right value for Key: %str, value: %u32", Key2, StoredValue2);
    }

    if (!StoredValue3)
    {
        TEST_RETURN_FAILURE("Failed to load value for Key: %str", Key3);
    }

    if (*StoredValue3 != Value3)
    {
        TEST_RETURN_FAILURE("Failed to load the right value for Key: %str, value: %u32", Key3, StoredValue3);
    }

    if (!StoredValue4)
    {
        TEST_RETURN_FAILURE("Failed to load value for Key: %str", Key4);
    }

    if (*StoredValue4 != Value4)
    {
        TEST_RETURN_FAILURE("Failed to load the right value for Key: %str, value: %u32", Key4, StoredValue4);
    }

    if (!StoredValue5)
    {
        TEST_RETURN_FAILURE("Failed to load value for Key: %str", Key5);
    }

    if (*StoredValue5 != Value5)
    {
        TEST_RETURN_FAILURE("Failed to load the right value for Key: %str, value: %u32", Key5, StoredValue5);
    }

    DestroyHashmap(&Map);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(RemoveTest)
{
    hash_map<u32> Map;
    CreateHashmap(&Map, Allocator);

    string Key1 = CreateString("test1");
    string Key2 = CreateString("test2");
    string Key3 = CreateString("test3");
    string Key4 = CreateString("test4");
    string Key5 = CreateString("test5");

    u32* pValue1 = Add(&Map, Key1);
    u32* pValue2 = Add(&Map, Key2);
    u32* pValue3 = Add(&Map, Key3);
    u32* pValue4 = Add(&Map, Key4);
    u32* pValue5 = Add(&Map, Key5);

    *pValue1 = 101;
    *pValue2 = 102;
    *pValue3 = 103;
    *pValue4 = 104;
    *pValue5 = 105;

    if (!Remove(&Map, Key1))
    {
        TEST_RETURN_FAILURE("Failed to remove the first entry");
    }

    { // NOTE: Check to make sure the pair was removed
        u64                        OldIndex   = GetIndexByKey(Map.Capacity, Key1);
        array<hash_map_pair<u32>>* OldEntries = Map.Buffer + OldIndex;

        if (OldEntries->AllocatedLength == 0)
        {
            TEST_RETURN_FAILURE("Got the wrong old entry array");
        }

        if (OldEntries->Size > 0)
        {
            TEST_RETURN_FAILURE("Failed to remove pair from the array");
        }
    }

    if (Map.Size != 4)
    {
        TEST_RETURN_FAILURE("Failed to remove the first entry from the hashmap");
    }

    if (!Remove(&Map, Key2))
    {
        TEST_RETURN_FAILURE("Failed to remove the second entry");
    }

    { // NOTE: Check to make sure the pair was removed
        u64                        OldIndex   = GetIndexByKey(Map.Capacity, Key2);
        array<hash_map_pair<u32>>* OldEntries = Map.Buffer + OldIndex;

        if (OldEntries->AllocatedLength == 0)
        {
            TEST_RETURN_FAILURE("Got the wrong old entry array");
        }

        if (OldEntries->Size > 0)
        {
            TEST_RETURN_FAILURE("Failed to remove pair from the array");
        }
    }

    if (Map.Size != 3)
    {
        TEST_RETURN_FAILURE("Failed to remove the second entry from the hashmap");
    }

    if (!Remove(&Map, Key3))
    {
        TEST_RETURN_FAILURE("Failed to remove the third entry");
    }

    { // NOTE: Check to make sure the pair was removed
        u64                        OldIndex   = GetIndexByKey(Map.Capacity, Key3);
        array<hash_map_pair<u32>>* OldEntries = Map.Buffer + OldIndex;

        if (OldEntries->AllocatedLength == 0)
        {
            TEST_RETURN_FAILURE("Got the wrong old entry array");
        }

        if (OldEntries->Size > 0)
        {
            TEST_RETURN_FAILURE("Failed to remove pair from the array");
        }
    }

    if (Map.Size != 2)
    {
        TEST_RETURN_FAILURE("Failed to remove the third entry from the hashmap");
    }

    if (!Remove(&Map, Key4))
    {
        TEST_RETURN_FAILURE("Failed to remove the fourth entry");
    }

    { // NOTE: Check to make sure the pair was removed
        u64                        OldIndex   = GetIndexByKey(Map.Capacity, Key4);
        array<hash_map_pair<u32>>* OldEntries = Map.Buffer + OldIndex;

        if (OldEntries->AllocatedLength == 0)
        {
            TEST_RETURN_FAILURE("Got the wrong old entry array");
        }

        if (OldEntries->Size > 0)
        {
            TEST_RETURN_FAILURE("Failed to remove pair from the array");
        }
    }

    if (Map.Size != 1)
    {
        TEST_RETURN_FAILURE("Failed to remove the fourth entry from the hashmap");
    }

    if (!Remove(&Map, Key5))
    {
        TEST_RETURN_FAILURE("Failed to remove the fifth entry");
    }

    { // NOTE: Check to make sure the pair was removed
        u64                        OldIndex   = GetIndexByKey(Map.Capacity, Key5);
        array<hash_map_pair<u32>>* OldEntries = Map.Buffer + OldIndex;

        if (OldEntries->AllocatedLength == 0)
        {
            TEST_RETURN_FAILURE("Got the wrong old entry array");
        }

        if (OldEntries->Size > 0)
        {
            TEST_RETURN_FAILURE("Failed to remove pair from the array");
        }
    }

    if (Map.Size != 0)
    {
        TEST_RETURN_FAILURE("Failed to remove the fifth entry from the hashmap");
    }

    DestroyHashmap(&Map);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(ResizeAndOverlapTest)
{
    hash_map<u64> Map;
    CreateHashmap(&Map, Allocator);

    u32    StartLength = 10;
    string Start       = CreateString((char*)Allocator->Allocate(sizeof(char) * StartLength), StartLength);
    string Previous    = CreateString((char*)Allocator->Allocate(sizeof(char) * StartLength), StartLength);
    u64    Pointer     = 0;

    string StartCopy    = CreateString("aaaaaaaaaa");
    string End          = CreateString("zzzzzzzzzz");
    u64    DigitPointer = 0;

    engine_memcpy(Start.Buffer, StartCopy, StartLength);
    engine_memset(Previous.Buffer, '0', StartLength);

    while (!StringsEqual(Start, End))
    {
        u64                        MapIndex       = GetIndexByKey(Map.Capacity, Start);
        array<hash_map_pair<u64>>* Entries        = Map.Buffer + MapIndex;
        u32                        PreviousLength = Entries->Size;

        if (Previous.Buffer[0] != '0' && StringsEqual(Previous, Start))
        {
            TEST_RETURN_FAILURE("Two keys that are identical\n");
        }

        engine_memcpy(Previous.Buffer, Start.Buffer, Previous.Length);

        u64* Next         = Add(&Map, Start);
        *Next             = Pointer;
        char CurrentStart = Start.Buffer[DigitPointer];

        Entries = Map.Buffer + MapIndex;

        if (Entries->AllocatedLength == 0 || Entries->Size == 0 || PreviousLength + 1 != Entries->Size)
        {
            TEST_RETURN_FAILURE("Failed to add entry");
        }

        ++Pointer;

        if (CurrentStart == 'z')
        {
            ++DigitPointer;
        }

        Start.Buffer[DigitPointer] = Start.Buffer[DigitPointer] + 1;
    }

    if (Map.Size != Pointer)
    {
        TEST_RETURN_FAILURE("Failed to add all of the entries, Map.Size: %u64, Pointer: %u64", Map.Size,
                            Pointer);
    }

    Pointer      = 0;
    DigitPointer = 0;
    engine_memcpy(Start.Buffer, StartCopy, StartLength);

    while (!StringsEqual(Start, End))
    {
        b32 Test = StringsEqual(Start, CreateString("zraaaaaaaa"));

        if (Test)
        {
            Test = true;
        }

        u64* Current = Get(&Map, Start);

        if (!Current)
        {
            TEST_RETURN_FAILURE("Failed to get value for key: %str", Start);
        }

        if (*Current != Pointer)
        {
            u64                        CurrentIndex = GetIndexByKey(Map.Capacity, Start);
            array<hash_map_pair<u64>>* Entries      = Map.Buffer + CurrentIndex;

            if (Entries)
            {
                CurrentIndex = 0;
            }

            TEST_RETURN_FAILURE(
                "Failed to get correct value for the key: %str, Pointer: %u64, CurrentValue: %u64", Start,
                Pointer, *Current);
        }

        ++Pointer;
        char CurrentStart = Start.Buffer[DigitPointer];

        if (CurrentStart == 'z')
        {
            ++DigitPointer;
        }

        Start.Buffer[DigitPointer] = Start.Buffer[DigitPointer] + 1;
    }

    // The last cases for both types of while loops disregard the last element,
    // zzzzzzzzzz isnt added and aaaaaaaaaa is not removed
    Start.Buffer[DigitPointer] = Start.Buffer[DigitPointer] - 1;

    while (!StringsEqual(Start, StartCopy))
    {
        u64* Current = Get(&Map, Start);

        if (!Current)
        {
            TEST_RETURN_FAILURE("Failed to get value for key on second try: %str", Start);
        }

        if (!Remove(&Map, Start))
        {
            TEST_RETURN_FAILURE("Failed to remove key: %str", Start);
        }

        char CurrentStart = Start.Buffer[DigitPointer];

        if (CurrentStart == 'a')
        {
            --DigitPointer;
        }

        Start.Buffer[DigitPointer] = Start.Buffer[DigitPointer] - 1;
    }

    FreeString(&Previous, Allocator);
    FreeString(&Start, Allocator);
    DestroyHashmap(&Map);

    TEST_RETURN_SUCCESS();
}

void
AddHashmapTests(array<test>* Tests)
{
    AddTest(CreateAndFreeHashmap, "Create and Free Test");
    AddTest(AddAndGetTest, "Add and Get Test");
    AddTest(RemoveTest, "Remove Test");
    AddTest(ResizeAndOverlapTest, "ResizeAndOverlap Test");
}
