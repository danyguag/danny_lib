#include "test_memory.h"

#include <std/memory.h>

TEST_FUNCTION(AllocateMemoryTest)
{
    char* Memory = (char*)Allocator->Allocate(28);

    if (Memory)
    {
        Allocator->Free(Memory, 28);
        TEST_RETURN_SUCCESS();
    }

    TEST_RETURN_FAILURE("Could not allocate memory\n");
}

TEST_FUNCTION(MemoryCopyReturnTest)
{
    for (u32 SubTestIndex = 0; SubTestIndex < 10; ++SubTestIndex)
    {
        u32* Destination = (u32*)engine_memcpy((char*)&SubTestIndex, sizeof(u32), Allocator);

        if (*Destination != SubTestIndex)
        {
            Allocator->Free(Destination, sizeof(u32));
            TEST_RETURN_FAILURE(
                "The memory copied is the incorrect value at SubTestIndex(%u32) with a value of %s32\n",
                SubTestIndex, Destination);
        }
        Allocator->Free(Destination, sizeof(u32));
    }

    TEST_RETURN_SUCCESS();
}

void
AddMemoryTests(array<test>* Tests)
{
    AddTest(AllocateMemoryTest, "Heap Memory Allocation Test");
    AddTest(MemoryCopyReturnTest, "Heap Memory Copy Return Test");
}
