#include "test_server_client.h"

#include <std/client.h>
#include <std/server.h>

void
PacketCallbackFunction(system_handle Socket, stream* ReadStream, void* UserData)
{
    UNUSED_VARIABLE(Socket);
    string* ErrorMessage = (string*)UserData;

    u32 TestNumber1 = Read<u32>(ReadStream);
    u32 TestNumber2 = Read<u32>(ReadStream);
    u32 TestNumber3 = Read<u32>(ReadStream);
    u32 TestNumber4 = Read<u32>(ReadStream);

    if (TestNumber1 != 10)
    {
        *ErrorMessage = CreateString("TestNumber1: Failed to receive the correct value");

        return;
    }

    if (TestNumber2 != 20)
    {
        *ErrorMessage = CreateString("TestNumber2: Failed to receive the correct value");

        return;
    }

    if (TestNumber3 != 30)
    {
        *ErrorMessage = CreateString("TestNumber3: Failed to receive the correct value");

        return;
    }

    if (TestNumber4 != 40)
    {
        *ErrorMessage = CreateString("TestNumber4: Failed to receive the correct value");

        return;
    }
}

TEST_FUNCTION(ServerAndClientTest)
{
    u32 Port     = 3000;
    u32 PacketId = 10;

    tcp_server Server;
    tcp_client Client;
    stream     Packet;
    string     ErrorMessage = CreateEmptyString();

    if (!CreateTcpServer(&Server, Port, 10, Allocator))
    {
        TEST_RETURN_FAILURE("Failed to start server");
    }

    CreateTcpClient(&Client, Allocator);
    SetSocketNonBlocking(&Client);
    CreateStream(&Packet, Allocator, DEFAULT_STREAM_ALLOCATION_SIZE);

    if (!Connect(&Client, CreateString("127.0.0.1"), Port) && GetPlatformError() != 115)
    {
        TEST_RETURN_FAILURE("Failed to start connected");
    }

    StartPacketWrite(&Packet, PacketId);
    Write<u32>(&Packet, (u32)10);
    Write<u32>(&Packet, (u32)20);
    Write<u32>(&Packet, (u32)30);
    Write<u32>(&Packet, (u32)40);
    EndPacketWrite(&Packet);

    AddPacketCallback(&Server, PacketId, PacketCallbackFunction, &ErrorMessage);
    AddPacketCallback(&Client, PacketId, PacketCallbackFunction, &ErrorMessage);

    connected_tcp_client* ServerClient = AcceptTcpConnection(&Server);

    if (!ServerClient)
    {
        TEST_RETURN_FAILURE("Server: Failed to connect to the client");
    }

    SendData(ServerClient->s, Packet.Buffer, Packet.WritePointer);

    Sleep(100);

    ProcessAllIncoming(&Client);

    if (ErrorMessage)
    {
        TEST_RETURN_FAILURE(ErrorMessage.Buffer);
    }

    SendData(Client.s, Packet.Buffer, Packet.WritePointer);
    Sleep(100);
    ProcessAllIncoming(&Server);

    if (ErrorMessage)
    {
        TEST_RETURN_FAILURE(ErrorMessage.Buffer);
    }

    DestroyStream(&Packet);
    DestroyTcpClient(&Client);
    DestroyTcpServer(&Server);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(GetClientBySocketTest)
{
    tcp_server Server;
    CreateArray(&Server.Clients, Allocator);

    connected_tcp_client* Client1       = Add(&Server.Clients);
    connected_tcp_client* Client2       = Add(&Server.Clients);
    system_handle         Client1Socket = 100;
    system_handle         Client2Socket = 50;

    Client1->s = Client1Socket;
    Client2->s = Client2Socket;

    connected_tcp_client* FoundClient1 = GetClientBySocket(&Server, Client1Socket);

    if (FoundClient1 == NULL)
    {
        TEST_RETURN_FAILURE("Failed to find client 1");
    }

    if (FoundClient1 != Client1)
    {
        TEST_RETURN_FAILURE("Found incorrect client when looking for client 1");
    }

    connected_tcp_client* FoundClient2 = GetClientBySocket(&Server, Client2Socket);

    if (FoundClient2 == NULL)
    {
        TEST_RETURN_FAILURE("Failed to find client 2");
    }

    if (FoundClient2 != Client2)
    {
        TEST_RETURN_FAILURE("Found incorrect client when looking for client 2");
    }

    connected_tcp_client* InvalidClient = GetClientBySocket(&Server, 25);

    if (InvalidClient != NULL)
    {
        TEST_RETURN_FAILURE("Found a client when searching for a nonexistent client");
    }

    FreeArray(&Server.Clients);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(RemoveClientBySocketTest)
{
    tcp_server Server;
    CreateArray(&Server.Clients, Allocator);

    connected_tcp_client* Client1       = Add(&Server.Clients);
    connected_tcp_client* Client2       = Add(&Server.Clients);
    system_handle         Client1Socket = OpenSocket(SOCKET_TYPE_TCP);
    system_handle         Client2Socket = OpenSocket(SOCKET_TYPE_TCP);
    system_handle         InvalidSocket = -1;

    Client1->s = Client1Socket;
    Client2->s = Client2Socket;

    u32 ClientCountBefore = Server.Clients.Size;
    RemoveClientBySocket(&Server, InvalidSocket);

    if (ClientCountBefore != Server.Clients.Size)
    {
        TEST_RETURN_FAILURE("A client was removed despite passing in an invalid socket");
    }

    RemoveClientBySocket(&Server, Client2Socket);

    if (ClientCountBefore == Server.Clients.Size)
    {
        TEST_RETURN_FAILURE("Failed to remove client 2 from the server client list");
    }

    RemoveClientBySocket(&Server, Client1Socket);

    if (Server.Clients.Size != 0)
    {
        TEST_RETURN_FAILURE("Failed to remove client 1 from the server client list");
    }

    FreeArray(&Server.Clients);

    TEST_RETURN_SUCCESS();
}

// NOTE: if this test fails and it is for the client 1 ack count not being
//      decremented then increase the SleepTime
TEST_FUNCTION(KeepAliveTest)
{
    /*

*/

    u32 Port              = 4004;
    u32 KeepAlivePacketId = 100;
    u32 SleepTime         = 500;
    s64 TimerTime         = Miliseconds(SleepTime);

    tcp_server Server;
    tcp_client Client;
    tcp_client Client2;

    if (!CreateTcpServer(&Server, Port, 10, Allocator))
    {
        TEST_RETURN_FAILURE("Failed to start server");
    }

    CreateTcpClient(&Client, Allocator);
    SetSocketNonBlocking(&Client);
    SetClientKeepAlive(&Client, KeepAlivePacketId, Allocator);

    CreateTcpClient(&Client2, Allocator);
    SetSocketNonBlocking(&Client2);
    SetClientKeepAlive(&Client2, KeepAlivePacketId, Allocator);

    if (!Connect(&Client, CreateString("127.0.0.1"), Port) && GetPlatformError() != 115)
    {
        TEST_RETURN_FAILURE("Client1 Failed to connect to test server");
    }

    connected_tcp_client* ServerClient = AcceptTcpConnection(&Server);

    if (!ServerClient)
    {
        TEST_RETURN_FAILURE("Server failed to accept client 1");
    }

    if (!Connect(&Client2, CreateString("127.0.0.1"), Port) && GetPlatformError() != 115)
    {
        TEST_RETURN_FAILURE("Client2 Failed to connect to test server");
    }

    connected_tcp_client* ServerClient2 = AcceptTcpConnection(&Server);

    if (!ServerClient2)
    {
        TEST_RETURN_FAILURE("Server failed to accept client 2");
    }

    timer ServerDeadClientTimer;
    SetServerKeepAlive(&Server, KeepAlivePacketId, TimerTime, 2, Allocator);
    StartTimer(&ServerDeadClientTimer, TimerTime);
    Sleep(SleepTime);

    ProcessAllIncoming(&Server);

    if (ServerClient->MissedAckCount != 1)
    {
        TEST_RETURN_FAILURE("Failed to increment server client 1 acknowledge count, expected 1 got %u32",
                            ServerClient->MissedAckCount);
    }

    if (ServerClient2->MissedAckCount != 1)
    {
        TEST_RETURN_FAILURE("Failed to increment server client 2 acknowledge count, expected 1 got %u32",
                            ServerClient2->MissedAckCount);
    }

    ProcessAllIncoming(&Client);
    ProcessAllIncoming(&Client2);
    ProcessAllIncoming(&Server);

    if (ServerClient->MissedAckCount != 0)
    {
        TEST_RETURN_FAILURE("Failed to decrement server client 1 acknowledge count, expected 0 got %u32",
                            ServerClient->MissedAckCount);
    }

    if (ServerClient2->MissedAckCount != 0)
    {
        TEST_RETURN_FAILURE("Failed to decrement server client 2 acknowledge count, expected 0 got %u32",
                            ServerClient2->MissedAckCount);
    }

    Sleep(SleepTime);
    ProcessAllIncoming(&Client2);
    ProcessAllIncoming(&Server);
    Sleep(SleepTime);
    ProcessAllIncoming(&Client2);
    ProcessAllIncoming(&Server);
    Sleep(SleepTime);
    ProcessAllIncoming(&Client2);
    ProcessAllIncoming(&Server);
    Sleep(SleepTime);
    ProcessAllIncoming(&Client2);
    ProcessAllIncoming(&Server);

    if (ServerClient->IsAlive)
    {
        TEST_RETURN_FAILURE("Failed to set server client to dead");
    }

    if (!ServerClient2->IsAlive)
    {
        TEST_RETURN_FAILURE("Failed to keep client 2 alive");
    }

    ClearDeadClients<void>(&Server,
                           [](void* UserData)
                           {
                               UNUSED_VARIABLE(UserData);
                           });

    if (Server.Clients.Size != 1)
    {
        TEST_RETURN_FAILURE("Failed to clear dead client");
    }

    DestroyTcpClient(&Client);
    DestroyTcpClient(&Client2);
    DestroyTcpServer(&Server);

    TEST_RETURN_SUCCESS();
}

TEST_FUNCTION(AcceptTcpConnectionFailureTest)
{
    u32        Port = 5000;
    tcp_server Server;

    if (!CreateTcpServer(&Server, Port, 10, Allocator))
    {
        TEST_RETURN_FAILURE("Failed to start server");
    }

    SetNonblocking(Server.s);

    if (AcceptTcpConnection(&Server) != NULL)
    {
        TEST_RETURN_FAILURE("Found a client when there is no client to accpet");
    }

    DestroyTcpServer(&Server);

    TEST_RETURN_SUCCESS();
}

void
AddServerAndClientTests(array<test>* Tests)
{
    AddTest(GetClientBySocketTest, "GetClientBySocket Test");
    AddTest(RemoveClientBySocketTest, "GetRemoveClientBySocket Test");
    AddTest(AcceptTcpConnectionFailureTest, "AcceptTcpConnection non blocking Test");
    AddTest(ServerAndClientTest, "Server and Client Packet Test");
    AddTest(KeepAliveTest, "Server and Client Keep Alive Test");
}
