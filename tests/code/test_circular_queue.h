#if !defined(TEST_CIRCULAR_QUEUE_H)

#include <std/array.h>
#include <std/test_framework.h>

void AddCircularQueueTests(array<test>* Tests);

#define TEST_CIRCULAR_QUEUE_H
#endif
