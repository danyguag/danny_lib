#if !defined(TEST_COMMANDLINE_PARSER_H)

#include <std/array.h>
#include <std/test_framework.h>

void AddCommandlineParserTests(array<test>* Tests);

#define TEST_COMMANDLINE_PARSER_H
#endif


