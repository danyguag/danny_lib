#if !defined(TEST_MEMORY_H)

#include <std/test_framework.h>
#include <std/array.h>

void AddMemoryTests(array<test>* Tests);

#define TEST_MEMORY_H
#endif
