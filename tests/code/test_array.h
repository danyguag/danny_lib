#if !defined(TEST_ARRAY_H)

#include <std/test_framework.h>
#include <std/array.h>

void AddArrayTests(array<test>* Tests);

#define TEST_ARRAY_H
#endif
