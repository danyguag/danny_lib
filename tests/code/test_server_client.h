#if !defined(TEST_SERVER_CLIENT_H)

#include <std/test_framework.h>
#include <std/array.h>

void AddServerAndClientTests(array<test>* Tests);

#define TEST_SERVER_CLIENT_H
#endif
