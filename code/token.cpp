#include "std/token.h"

#include "std/memory.h"
#include "std/string.h"

#include "std/base.h"

void
CreateTokenizer(tokenizer* Tokenizer, char* Buffer, u64 BufferLength)
{
    Tokenizer->Buffer.Buffer   = Buffer;
    Tokenizer->Buffer.Length   = BufferLength;
    Tokenizer->Buffer.Capacity = BufferLength;
    Tokenizer->Pointer         = 0;
}

void
ResetToken(token* Token, allocator* Allocator)
{
    if (Token->Type == TOKEN_STRING || Token->Type == TOKEN_TEXT)
    {
        FreeString(&Token->String, Allocator);
    }

    engine_memset((char*)Token, 0, sizeof(token));
    Token->Type = TOKEN_NONE;
}

b32
GetNextToken(tokenizer* Tokenizer, token* Token, allocator* Allocator)
{
    string Current = Tokenizer->Buffer[Tokenizer->Pointer];

    while (true)
    {
        if (Tokenizer->Pointer >= Tokenizer->Buffer.Length)
        {
            return (false);
        }

        if (!IsWhiteSpace(Current.Buffer[0]))
        {
            break;
        }

        ++Tokenizer->Pointer;
        Current += 1;
    }

    if (Token->Type != TOKEN_NONE)
    {
        ResetToken(Token, Allocator);
    }

    Token->Type = TOKEN_NONE;

    char CurrentCharacter  = Current.Buffer[0];
    char CurrentCharacter2 = Current.Buffer[1];

    if (IsDigit(CurrentCharacter) ||
        (Current.Length > 1 && (CurrentCharacter == '-' && IsDigit(CurrentCharacter2))))
    {
        u64 Start = Tokenizer->Pointer;
        u64 End   = 0;

        Token->Type = TOKEN_NUMBER;

        if (CurrentCharacter == '-')
        {
            Current += 1;
            CurrentCharacter = Current.Buffer[0];
            ++End;
        }

        while (true)
        {
            if (IsDigit(CurrentCharacter) || CurrentCharacter == '.')
            {
                ++End;
            }
            else
            {
                break;
            }
            Current += 1;
            CurrentCharacter = Current.Buffer[0];
        }

        string NumberString = Tokenizer->Buffer[Start];
        NumberString.Length = End;

        StringToFloat(&Token->Number, NumberString);
        Tokenizer->Pointer += NumberString.Length;
    }
    else if (StartsWith(Current, CreateString("false")) ||
             StartsWith(Current, CreateString("true")))
    {
        b32 Boolean = (CurrentCharacter == 't') ? true : false;

        Token->Type    = TOKEN_BOOLEAN;
        Token->Boolean = Boolean;
        Tokenizer->Pointer += (Boolean) ? 4 : 5;
    }
    else if (IsAlpha(CurrentCharacter))
    {
        u64 Start = Tokenizer->Pointer;
        u64 End   = Start;

        Token->Type = TOKEN_TEXT;

        while (true)
        {
            if (IsAlpha(CurrentCharacter))
            {
                ++End;
            }
            else
            {
                break;
            }
            Current += 1;
            CurrentCharacter = Current.Buffer[0];
        }

        Tokenizer->Pointer += End - Start;

        SubString(&Token->String, Tokenizer->Buffer, Start, End, Allocator);
    }
    else if (CurrentCharacter == '"' || CurrentCharacter == '\'')
    {
        u64    BufferPointer = Tokenizer->Pointer + 1;
        s64    EndOffset;
        string Buffer;

        StringAt(&Buffer, Tokenizer->Buffer, BufferPointer);
        Token->Type = TOKEN_STRING;
        EndOffset   = IndexOf(Buffer, '"');

        if (CurrentCharacter == '\'')
        {
            EndOffset = IndexOf(Buffer, '\'');
        }

        if (EndOffset == -1)
        {
            return (false);
        }

        Tokenizer->Pointer += EndOffset + 1;
	
        SubString(&Token->String, Tokenizer->Buffer, BufferPointer, Tokenizer->Pointer++, Allocator);
    }
    else if (CurrentCharacter == '-' && !IsDigit(Current[1]))
    {
        char Flag = Current.Buffer[1];

        Token->Type = TOKEN_FLAG;
        Token->Flag = Flag;
        Tokenizer->Pointer += 2;
    }
    else
    {
        Token->Type   = TOKEN_SYMBOL;
        Token->Symbol = CurrentCharacter;
        ++Tokenizer->Pointer;
    }

    return (true);
}
