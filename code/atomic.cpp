#include "std/atomic.h"

s64
AtomicAdd(volatile s64* Value, s64 Addend)
{
    __sync_synchronize();
    __sync_fetch_and_add(Value, Addend);
    return (*Value);
}
