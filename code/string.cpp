#include "std/string.h"

#include "std/memory.h"

#define ABS(Value) ((Value < 0) ? (Value * -1) : Value)

string
CreateEmptyString()
{
    string String;

    String.Buffer   = NULL;
    String.Length   = 0;
    String.Capacity = 0;

    return (String);
}

string
CreateString(char* Buffer, u64 Length)
{
    string String;

    String.Buffer   = Buffer;
    String.Length   = Length;
    String.Capacity = Length;

    return (String);
}

string
CreateString(const char* Buffer)
{
    string String;

    String.Buffer   = (char*)Buffer;
    String.Length   = StringLength((char*)Buffer);
    String.Capacity = String.Length;

    return (String);
}

b32
string::operator==(string& String)
{
    return (StringsEqual(*this, String));
}

b32
string::operator==(const char* String)
{
    return (StringsEqual(*this, CreateString(String)));
}

b32
string::operator!=(string& String)
{
    return (!StringsEqual(*this, String));
}

b32
string::operator!=(const char* String)
{
    return (!StringsEqual(*this, CreateString(String)));
}

u32
StringLength(char* String)
{
    if (!String)
    {
        return (0);
    }

    uint32_t Count = 0;
    while ((*String) != 0 && (*String) != '\0')
    {
        Count++;
        String++;
    }
    return Count;
}

b32
StringsEqual(string& String1, string String2)
{
    if (String1.Length == String2.Length)
    {
        for (u32 i = 0; i < String1.Length; ++i)
        {
            if (String1.Buffer[i] != String2.Buffer[i])
            {
                return (false);
            }
        }
        return (true);
    }

    return (false);
}

b32
EndsWith(string& String, string Test)
{
    if (String.Length < Test.Length)
    {
        return (false);
    }

    for (u32 TestIndex = 0; TestIndex < Test.Length; ++TestIndex)
    {
        if (String.Buffer[(String.Length - Test.Length) + TestIndex] !=
            Test.Buffer[TestIndex])
        {
            return (false);
        }
    }

    return (true);
}

b32
StartsWith(string& String, string Test)
{
    if (Test.Length > String.Length)
    {
        return (false);
    }

    for (u32 i = 0; i < Test.Length; ++i)
    {
        if (String.Buffer[i] != Test.Buffer[i])
        {

            return (false);
        }
    }

    return (true);
}

static void
CreateZeroString(string* String, allocator* Allocator)
{
    char* Buffer = (char*)Allocator->Allocate(2 * sizeof(char));
    Buffer[0]    = '0';
    Buffer[1]    = 0;

    String->Buffer   = Buffer;
    String->Length   = 1;
    String->Capacity = 2;
}

void
UnsignedIntegerToString(string* Out, u64 Value, allocator* Allocator)
{
    if (Value == 0)
    {
        CreateZeroString(Out, Allocator);
        return;
    }

    u32 Length = 0;
    u64 tValue = Value;

    while (tValue != 0)
    {
        tValue /= 10;
        ++Length;
    }

    char* String  = (char*)Allocator->Allocate(Length + 1);
    u32   Index   = Length - 1;
    u8    Current = 0;

    tValue = Value;

    while (tValue >= 10)
    {
        Current = tValue % 10;
        tValue /= 10;
        String[Index] = (char)('0' + Current);
        --Index;
    }

    String[Index] = (char)('0' + tValue);
    --Index;
    String[Length] = 0;

    Out->Buffer   = String;
    Out->Length   = Length;
    Out->Capacity = Length + 1;
}

void
SignedIntegerToString(string* Out, s64 Value, allocator* Allocator)
{
    if (Value == 0)
    {
        CreateZeroString(Out, Allocator);
        return;
    }

    u32 Length = 0;

    s64 tValue = Value;

    while (tValue != 0)
    {
        tValue /= 10;
        ++Length;
    }

    b32 Negative       = (Value < 0);
    u32 ResultLength   = Length + 1;
    u32 NegativeOffset = 0;

    if (Negative)
    {
        ++NegativeOffset;
    }

    char* String = (char*)Allocator->Allocate(ResultLength + NegativeOffset);
    u32   Index  = Length - 1 + NegativeOffset;

    if (Value < 0)
    {
        tValue = Value * -1;
    }
    else
    {
        tValue = Value;
    }

    u8 Current = 0;

    while (tValue >= 10)
    {
        Current = tValue % 10;
        tValue /= 10;
        String[Index] = (char)('0' + Current);
        --Index;
    }

    String[Index] = (char)('0' + tValue);

    if (Negative)
    {
        String[Index - 1] = '-';
    }

    String[Length + NegativeOffset] = 0;

    Out->Buffer   = String;
    Out->Length   = Length + NegativeOffset;
    Out->Capacity = Length + NegativeOffset + 1;
}

void
FloatToString(string* Out, f64 Value, allocator* Allocator)
{
    if (ABS(Value) < .000000001f)
    {
        CreateZeroString(Out, Allocator);
        return;
    }

    b32    Negative       = Value < 0.0f;
    u32    NegativeOffset = (Negative) ? 1 : 0;
    string WholeNum;

    if (Negative)
    {
        Value *= -1;
    }

    u32 WholeNumber = (u32)Value;

    if (WholeNumber > 0)
    {
        UnsignedIntegerToString(&WholeNum, WholeNumber, Allocator);
    }
    else
    {
        WholeNum = CreateEmptyString();
    }

    f64 DecimalValue         = Value - (f64)WholeNumber;
    u32 DecimalLength        = 15;
    u64 NumDecimalPlacesZero = 0;

    for (u32 DecimalIndex = 0; DecimalIndex < DecimalLength; ++DecimalIndex)
    {
        DecimalValue *= 10;
        if (((s32)DecimalValue) == 0)
        {
            ++NumDecimalPlacesZero;
        }
    }

    string DecimalNum;
    string DecimalNumWithoutZero;

    UnsignedIntegerToString(&DecimalNumWithoutZero, (u64)DecimalValue, Allocator);

    if (NumDecimalPlacesZero > 0)
    {
        DecimalNum.Length = DecimalNumWithoutZero.Length + NumDecimalPlacesZero;
        DecimalNum.Buffer = (char*)Allocator->Allocate(DecimalNum.Length);

        engine_memset(DecimalNum.Buffer, '0', NumDecimalPlacesZero);
        engine_memcpy(DecimalNum[NumDecimalPlacesZero].Buffer,
                      DecimalNumWithoutZero.Buffer, DecimalNumWithoutZero.Length);
        FreeString(&DecimalNumWithoutZero, Allocator);
    }
    else
    {
        DecimalNum = DecimalNumWithoutZero;
    }

    char* Result = (char*)Allocator->Allocate(DecimalNum.Length + WholeNum.Length + 2 +
                                              NegativeOffset);
    u32   Offset = 1;
    u64   WholeNumLength = WholeNum.Length;

    if (WholeNum.Length > 0)
    {
        engine_memcpy(Result + NegativeOffset, WholeNum.Buffer, WholeNum.Length);
        Result[WholeNum.Length + NegativeOffset] = '.';
        Offset += WholeNum.Length;
    }
    else
    {
        Result[0 + NegativeOffset] = '.';
    }

    engine_memcpy(&Result[WholeNumLength + 1 + NegativeOffset], DecimalNum.Buffer,
                  DecimalNum.Length);

    if (Negative)
    {
        Result[0] = '-';
    }

    u32 ResultLength     = DecimalNum.Length + WholeNumLength + 1 + NegativeOffset;
    Result[ResultLength] = 0;

    Out->Buffer   = Result;
    Out->Length   = ResultLength;
    Out->Capacity = ResultLength + 1;

    if (WholeNum.Length > 0)
    {
        FreeString(&WholeNum, Allocator);
    }

    // Since this may or may not be null terminated both cases have to be
    // handled
    if (NumDecimalPlacesZero > 0)
    {
        Allocator->Free(DecimalNum.Buffer, DecimalNum.Length);
    }
    else
    {
        // Same as the other case but the length is +1 because of the null
        // terminator
        FreeString(&DecimalNum, Allocator);
    }
}

b32
StringToSignedInteger(s64* Result, string& String)
{
    u32 Offset = (String.Buffer[0] == '-') ? 1 : 0;
    s64 Number = 0;

    for (u32 CharIndex = Offset; CharIndex < String.Length; ++CharIndex)
    {
        Number *= 10;
        Number += String.Buffer[CharIndex] - 48;
    }

    *Result = (Offset == 1) ? -Number : Number;

    return (true);
}

b32
StringToUnsignedInteger(u64* Out, string& String)
{
    u64 Result = 0;

    for (u32 CharIndex = 0; CharIndex < String.Length; ++CharIndex)
    {
        Result *= 10;
        Result += String.Buffer[CharIndex] - 48;
    }

    *Out = Result;

    return (true);
}

b32
StringToFloat(f64* Out, string& String)
{
    if (String.Buffer[0] == '0' && String.Length == 1)
    {
        *Out = 0;
        return (0);
    }

    f64 Result            = 0;
    u32 WholeNumberLength = 0;
    b32 Negative          = (String.Buffer[0] == '-') ? true : false;
    u32 NegativeOffset    = Negative ? 1 : 0;
    b32 HasDecimal        = false;

    { // Whole number parsing
        for (u32 WholeNumberIndex = 0; WholeNumberIndex < String.Length;
             ++WholeNumberIndex)
        {
            if (String.Buffer[WholeNumberIndex] != '.')
            {
                ++WholeNumberLength;
            }
            else
            {
                HasDecimal = true;
                break;
            }
        }

        if (WholeNumberLength > 0)
        {
            string WholeNumberString = String[NegativeOffset];
            WholeNumberString.Length = WholeNumberLength - (Negative ? 1 : 0);
            u64 WholeNumber;
            StringToUnsignedInteger(&WholeNumber, WholeNumberString);
            Result = WholeNumber;
        }
    }

    if (!HasDecimal)
    {
        *Out = (Negative) ? -Result : Result;
        return (true);
    }

    u32    DecimalLength = String.Length - WholeNumberLength - 1;
    string DecimalString = String[WholeNumberLength + 1];
    DecimalString.Length = DecimalLength;

    u64 DecimalAsWhole;
    StringToUnsignedInteger(&DecimalAsWhole, DecimalString);
    f64 Decimal = DecimalAsWhole;

    for (u32 DecimalIndex = 0; DecimalIndex < DecimalLength; ++DecimalIndex)
    {
        Decimal /= 10;
    }

    Result += Decimal;

    if (Negative)
    {
        Result *= -1;
    }

    *Out = Result;

    return (true);
}

void
FormatStringList(string* Result, const char* FormattedString, allocator* Allocator,
                 va_list Args)
{
    u32 FormattedStringLength = StringLength((char*)FormattedString);

    u32    ResultIndex = 0;
    string TypeString;
    TypeString.Length   = 4;
    TypeString.Capacity = 4;

    for (u32 FormattedStringIndex = 0; FormattedStringIndex < FormattedStringLength;)
    {
        char FormattedCurrent = FormattedString[FormattedStringIndex];

        if (FormattedCurrent == '%')
        {
            TypeString.Buffer = ((char*)FormattedString) + (FormattedStringIndex + 1);
            u32 TypeLength    = 0;

            ++FormattedStringIndex;

            if (StartsWith(TypeString, CreateString("str")))
            {
                string ArgString = va_arg(Args, string);
                string ResultAt;
                StringAt(&ResultAt, *Result, ResultIndex);
                FormatStringList(&ResultAt, ArgString.Buffer, Allocator, Args);

                ResultIndex += ResultAt.Length;
                TypeLength = 3;
            }
            else if (StartsWith(TypeString, CreateString("s8")) ||
                     StartsWith(TypeString, CreateString("s16")) ||
                     StartsWith(TypeString, CreateString("s32")))
            {
                // :var_arg_32bit
                // NOTE: va_arg does not allow signed integers smaller than 32
                // bits so it did not make sense to make their
                // own else if section if I was running the same calls on all
                // three
                s32    ArgValue = (s32)va_arg(Args, s32);
                string String;
                SignedIntegerToString(&String, ArgValue, Allocator);

                engine_memcpy(Result->Buffer + ResultIndex, String.Buffer, String.Length);

                ResultIndex += String.Length;

                if (StartsWith(TypeString, CreateString("s8")))
                {
                    TypeLength = 2;
                }
                else
                {
                    TypeLength = 3;
                }

                FreeString(&String, Allocator);
            }
            else if (StartsWith(TypeString, CreateString("u8")) ||
                     StartsWith(TypeString, CreateString("u16")) ||
                     StartsWith(TypeString, CreateString("u32")))
            {
                // :var_arg_32bit
                u32    ArgValue = va_arg(Args, u32);
                string String;
                UnsignedIntegerToString(&String, ArgValue, Allocator);

                engine_memcpy(Result->Buffer + ResultIndex, String.Buffer, String.Length);

                ResultIndex += String.Length;
                if (StartsWith(TypeString, CreateString("u8")))
                {
                    TypeLength = 2;
                }
                else
                {
                    TypeLength = 3;
                }

                FreeString(&String, Allocator);
            }
            else if (StartsWith(TypeString, CreateString("s64")))
            {
                s64    ArgValue = va_arg(Args, s64);
                string String;
                SignedIntegerToString(&String, ArgValue, Allocator);

                engine_memcpy(Result->Buffer + ResultIndex, String.Buffer, String.Length);

                ResultIndex += String.Length;
                TypeLength = 3;

                FreeString(&String, Allocator);
            }
            else if (StartsWith(TypeString, CreateString("u64")))
            {
                u64    ArgValue = va_arg(Args, u64);
                string String;
                UnsignedIntegerToString(&String, ArgValue, Allocator);

                engine_memcpy(Result->Buffer + ResultIndex, String.Buffer, String.Length);

                ResultIndex += String.Length;
                TypeLength = 3;

                FreeString(&String, Allocator);
            }
            else if (StartsWith(TypeString, CreateString("f32")) ||
                     StartsWith(TypeString, CreateString("f64")))
            {
                // :var_arg_32bit
                // NOTE: The same thing happens with a float to a double as a
                // smaller int to an int happens
                f64    ArgValue = va_arg(Args, f64);
                string String;
                FloatToString(&String, ArgValue, Allocator);

                engine_memcpy(Result->Buffer + ResultIndex, String.Buffer, String.Length);

                ResultIndex += String.Length;
                TypeLength = 3;

                FreeString(&String, Allocator);
            }
            else if (StartsWith(TypeString, CreateString("b32")))
            {
                b32   Value  = va_arg(Args, b32);
                char* String = (char*)((Value) ? "true" : "false");
                u32   Length = StringLength(String);

                engine_memcpy(Result->Buffer + ResultIndex, String, Length);

                ResultIndex += Length;
                TypeLength = 3;
            }

            if (StartsWith(TypeString, CreateString("char")))
            {
                char ArgValue = (char)va_arg(Args, int);

                Result->Buffer[ResultIndex] = ArgValue;

                ++ResultIndex;
                TypeLength = 4;
            }

            FormattedStringIndex += TypeLength;
        }
        else
        {
            Result->Buffer[ResultIndex] = FormattedCurrent;
            ++ResultIndex;
            ++FormattedStringIndex;
        }
    }

    Result->Length = ResultIndex;
}

void
FormatStringToBuffer(string* Result, const char* FormattedString, allocator* Allocator,
                     ...)
{
    va_list Args;
    va_start(Args, Allocator);

    FormatStringList(Result, FormattedString, Allocator, Args);
    Result->Buffer[Result->Length] = 0;

    va_end(Args);
}

void
FormatString(string* Result, const char* FormattedString, allocator* Allocator, ...)
{
    va_list Args;
    va_start(Args, Allocator);

    Result->Buffer   = (char*)Allocator->Allocate(2048);
    Result->Capacity = 2048;
    Result->Length   = 0;

    FormatStringList(Result, FormattedString, Allocator, Args);
    Result->Buffer[Result->Length] = 0;

    va_end(Args);
}

void
CopyString(string* Destination, string& Source, allocator* Allocator)
{
    Destination->Buffer   = (char*)Allocator->Allocate(Source.Length + 1);
    Destination->Length   = Source.Length;
    Destination->Capacity = Source.Length + 1;

    engine_memcpy(Destination->Buffer, Source.Buffer, Source.Length);
    Destination->Buffer[Destination->Length] = 0;
}

void
FreeString(string* String, allocator* Allocator)
{
    Allocator->Free(String->Buffer, String->Capacity);

    String->Buffer   = NULL;
    String->Capacity = 0;
    String->Length   = 0;
}

void
RemoveWhitespace(string* Result, string& String, allocator* Allocator)
{
    Result->Capacity = String.Length + 1;
    Result->Buffer   = (char*)Allocator->Allocate(Result->Capacity);
    u32 Pointer      = 0;

    for (u32 Index = 0; Index < String.Length; ++Index)
    {
        char CurrentChar = String.Buffer[Index];
        if (IsWhiteSpace(CurrentChar))
        {
            continue;
        }

        Result->Buffer[Pointer] = CurrentChar;
        ++Pointer;
    }

    Result->Buffer[Pointer] = 0;
    Result->Length          = Pointer;
}

split_string
SplitString(string& String, char Regex, allocator* Allocator)
{
    split_string Result;
    Result.Buffer  = NULL;
    Result.Count   = 0;
    u32 RegexCount = 1;

    for (u32 Index = 0; Index < String.Length; ++Index)
    {
        char Current = String.Buffer[Index];
        if (Current == Regex && (Index + 1) < String.Length)
        {
            ++RegexCount;
        }
    }

    if (RegexCount == 1)
    {
        return (Result);
    }

    string* Buffer      = (string*)Allocator->Allocate(sizeof(string) * RegexCount);
    u32     StartOffset = 0;

    u32 SplitCount = 0;

    u32 ResultIndex = 0;

    for (u32 Index = 0; Index < String.Length; ++Index)
    {
        char Current = String.Buffer[Index];
        if (Current == Regex || (Index + 1) == String.Length)
        {
            u32     nStringLength = Index - StartOffset + 1;
            string* NewString     = Buffer + ResultIndex;

            if ((Index + 1) < String.Length)
            {
                --nStringLength;
            }

            NewString->Buffer   = (char*)Allocator->Allocate(nStringLength + 1);
            NewString->Length   = nStringLength;
            NewString->Capacity = nStringLength + 1;

            engine_memcpy(NewString->Buffer, String[StartOffset].Buffer, nStringLength);

            NewString->Buffer[nStringLength] = 0;
            StartOffset                      = Index + 1;
            ++SplitCount;
            ++ResultIndex;
        }
    }

    Result.Buffer = Buffer;
    Result.Count  = RegexCount;

    return (Result);
}

b32
Contains(string& String, char Regex)
{
    for (u32 Index = 0; Index < String.Length; ++Index)
    {
        if (String.Buffer[Index] == Regex)
        {
            return (true);
        }
    }

    return (false);
}

b32
Contains(string& String, string& Part)
{
    if (Part.Length > String.Length)
    {
        return (false);
    }

    u64 LoopLength = String.Length - Part.Length + 1;

    for (u64 Index = 0; Index < LoopLength; ++Index)
    {
        string Current = String[Index];
        Current.Length = Part.Length;

        if (StringsEqual(Current, Part))
        {
            return (true);
        }
    }

    return (false);
}

#include <unistd.h>

b32
SubString(string* Out, string& String, u64 Start, u64 End, allocator* Allocator)
{
    if (End == 0)
    {
        End = String.Length;
    }

    u64 Length = End - Start;

    Out->Buffer   = (char*)Allocator->Allocate(Length + 1);
    Out->Length   = Length;
    Out->Capacity = Length + 1;

    for (u32 Index = 0; Index < Length; ++Index)
    {
        u64 StringIndex = (Index + Start);

        if (StringIndex >= String.Length)
        {
	    FreeString(Out, Allocator);
            return (false);
        }

        Out->Buffer[Index] = String.Buffer[StringIndex];
    }

    Out->Buffer[Length] = 0;

    return (true);
}

s64
IndexOf(string& String, char Regex)
{
    for (u64 Index = 0; Index < String.Length; ++Index)
    {
        if (String.Buffer[Index] == Regex)
        {
            return ((s64)Index);
        }
    }

    return (-1);
}

s64
IndexOf(string& String, string Value)
{
    if (Value.Length > String.Length)
    {
        return (-1);
    }

    u64 MaxIndex = String.Length - Value.Length + 1;

    for (u64 Index = 0; Index < MaxIndex; ++Index)
    {
        b32 Found = true;
        for (u32 ValueIndex = 0; ValueIndex < Value.Length; ++ValueIndex)
        {
            if (String.Buffer[Index + ValueIndex] != Value.Buffer[ValueIndex])
            {
                Found = false;
                break;
            }

            if (Found)
            {
                return ((s64)Index);
            }
        }
    }

    return (-1);
}

#undef ABS
