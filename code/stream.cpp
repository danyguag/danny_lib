#include "std/stream.h"

#include "std/memory.h"

void
CreateStream(stream* Stream, allocator* Allocator, u64 Length)
{
    Stream->Buffer       = (char*)Allocator->Allocate(Length);
    Stream->Length       = Length;
    Stream->WritePointer = 0;
    Stream->ReadPointer  = 0;
    Stream->Allocator    = Allocator;
}

void
DestroyStream(stream* Stream)
{
    if (Stream->Allocator)
    {
        Stream->Allocator->Free(Stream->Buffer, Stream->Length);
        Stream->Allocator = NULL;
    }

    Stream->Length       = 0;
    Stream->WritePointer = 0;
    Stream->ReadPointer  = 0;
}

void
SetBuffer(stream* Stream, char* Buffer, u64 Length)
{
    Stream->Buffer = Buffer;
    Stream->Length = Length;
}

void
ResizeStream(stream* Stream)
{
    u64 NewLength = Stream->Length * 2;
    char* Temp = (char*)Stream->Allocator->Allocate(NewLength);

    engine_memcpy(Temp, Stream->Buffer, Stream->Length);
    Stream->Allocator->Free(Stream->Buffer, Stream->Length);
    Stream->Buffer = Temp;
    Stream->Length = NewLength;
}

void
ResetStream(stream* Stream)
{
    engine_memset(Stream->Buffer, 0, Stream->WritePointer);
    Stream->WritePointer = 0;
    Stream->ReadPointer  = 0;
}

b32
CanFit(stream* Stream, u64 Length)
{
    return ((Stream->WritePointer + Length) <= Stream->Length);
}

b32
Write(stream* Stream, void* Memory, u64 Length)
{
    while (!CanFit(Stream, Length))
    {
        ResizeStream(Stream);
    }

    engine_memcpy(Stream->Buffer + Stream->WritePointer, Memory, Length);
    Stream->WritePointer += Length;

    return (true);
}

void*
Read(stream* Stream, u64 Length)
{
    void* Result = Stream->Buffer + Stream->ReadPointer;
    Stream->ReadPointer += Length;

    return (Result);
}
