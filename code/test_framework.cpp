#include "std/test_framework.h"

test_framework*
CreateTestFrameWork(allocator* Allocator)
{
    test_framework* TestFramework =
        (test_framework*)Allocator->Allocate(sizeof(test_framework));
    TestFramework->TestPointer      = 1;
    TestFramework->TotalFailedCount = 0;
    TestFramework->Lock             = CreateMutex(Allocator);
    TestFramework->Allocator        = Allocator;
    
    CreateArray(&TestFramework->Runners, Allocator);

    return (TestFramework);
}

static void*
TestRunner(void* Memory)
{
    test_runner*    TestRunner = (test_runner*)Memory;
    test_framework* Framework  = TestRunner->Parent;
    array<test>* Tests = &TestRunner->Tests;
    u32 TestLength = Tests->Size;

    allocator PrintAllocator;
    InitHeapAllocator(&PrintAllocator);

    for (u32 TestIndex = 0; TestIndex < TestLength; ++TestIndex)
    {
        test* Test = GetElement(Tests, TestIndex);

        test_result TestResult =
            Test->Function(Framework->Lock, TestRunner->Allocator);

        if (!TestResult.Success)
        {
            ++TestRunner->FailedCount;
            string Message;
            FormatString(&Message, "%str failed: %str\n", &PrintAllocator, Test->Name, TestResult.Error);

            Guard(Framework->Lock,
                  [&]()
                  {
                      StandardPrint(Message);
                  });

            FreeString(&Message, &PrintAllocator);
            FreeString(&TestResult.Error, &PrintAllocator);
        }
    }

    u32 SuccessCount = TestLength - TestRunner->FailedCount;

    {
        string Message;
        FormatString(&Message, "%str: %u32 Failed %u32 Passed\n", &PrintAllocator, TestRunner->System,
                     TestRunner->FailedCount, SuccessCount);

        Guard(Framework->Lock,
              [&]()
              {
                  StandardPrint(Message);
              });
        FreeString(&Message, &PrintAllocator);
    }

    return (NULL);
}

void
AddTestCollection(test_framework* TestFramework, char* System, array<test>* Tests,
                  allocator* Allocator)
{
    test_runner* Runner = NULL;

    Guard(TestFramework->Lock,
          [&]()
          {
              Runner              = Add(&TestFramework->Runners);
              Runner->System      = CreateString(System);
              Runner->Parent      = TestFramework;
              Runner->Allocator   = Allocator;
	      Runner->FailedCount = 0;
              CopyArray(&Runner->Tests, Tests, Allocator);
              ++TestFramework->TestPointer;
          });
    Runner->ThreadHandle = StartThread(TestRunner, Runner, TestFramework->Allocator);

    ClearArray(Tests);
}

void
WaitAndDestroyTestFramework(test_framework* TestFramework)
{
    array<test_runner>* TestRunner      = &TestFramework->Runners;
    u32                 RunnerWaitCount = TestRunner->Size;

    for (u32 RunnerIndex = 0; RunnerIndex < RunnerWaitCount; ++RunnerIndex)
    {
        test_runner* Runner = GetElement(TestRunner, RunnerIndex);

        JoinThread(Runner->ThreadHandle);

        DestroyThreadHandle(Runner->ThreadHandle, TestFramework->Allocator);
        FreeArray(&Runner->Tests);
    }

    FreeArray(&TestFramework->Runners);
    DestroyMutex(&TestFramework->Lock, TestFramework->Allocator);
    TestFramework->Allocator->Free(TestFramework, sizeof(test_framework));
}
