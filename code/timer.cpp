#include "std/timer.h"

#include <time.h>

#include "std/base.h"
#include "std/string.h"

s64
GetTime()
{
    struct timespec CurrentTime;
    clock_gettime(CLOCK_REALTIME, &CurrentTime);

    return (Seconds(CurrentTime.tv_sec) + CurrentTime.tv_nsec);
}

void
StartTimer(timer* Timer, s64 RunTime)
{
    struct timespec CurrentTime;
    clock_gettime(CLOCK_REALTIME, &CurrentTime);

    s64 RunningTimeSeconds = RunTime / 1000000000;
    s64 RunningTimeNano    = RunTime % 1000000000;

    if (RunningTimeNano + CurrentTime.tv_nsec >= 1000000000)
    {
        ++RunningTimeSeconds;
        RunningTimeNano += CurrentTime.tv_nsec - 1000000000;
    }

    Timer->EndTimeSeconds = CurrentTime.tv_sec + RunningTimeSeconds;
    Timer->EndTimeNano    = RunningTimeNano;
}

b32
IsTimerComplete(timer* Timer)
{
    struct timespec CurrentTime;
    clock_gettime(CLOCK_REALTIME, &CurrentTime);

    return (CurrentTime.tv_sec >= Timer->EndTimeSeconds && CurrentTime.tv_nsec >= Timer->EndTimeNano);
}

void
SyncTimer(timer* Destination, timer* Source, u64 Offset)
{
    Destination->EndTimeSeconds = Source->EndTimeSeconds;
    Destination->EndTimeNano    = Source->EndTimeNano;

    if (Offset > 0)
    {
        s64 OffsetSeconds = Offset / 1000000000;
        s64 OffsetNano    = Offset % 1000000000;

        if (OffsetNano + Destination->EndTimeNano >= 1000000000)
        {
            ++Destination->EndTimeSeconds;
            Destination->EndTimeNano += OffsetNano - 1000000000;
        }

        Destination->EndTimeSeconds += OffsetSeconds;
    }
}
