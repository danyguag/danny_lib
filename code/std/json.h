#if !defined(JSON_H)

#include "array.h"
#include "memory.h"
#include "stream.h"
#include "string.h"
#include "token.h"
#include "types.h"

/**
 * The type of just array
 */
enum json_array_type
{
    /**
     * Default value when value is type is not set
     */
    JSON_ARRAY_NONE = 0,

    /**
     * Type for json object array
     */
    JSON_ARRAY_OBJECT = 1,

    /**
     * Type for json number array
     */
    JSON_ARRAY_NUMBER = 2,

    /**
     * Type for json boolean array
     */
    JSON_ARRAY_BOOLEAN = 3,

    /**
     * Type for json string array
     */
    JSON_ARRAY_STRING = 4,
};

/**
 * Json number
 */
struct json_number
{
    /**
     * Name of the json number
     */
    string Name;

    /**
     * The number value
     */
    f64 Value;
};

/**
 * Json boolean
 */
struct json_boolean
{
    /**
     * Name of the json boolean
     */
    string Name;

    /**
     * The boolean value
     */
    b32 Value;
};

/**
 * Json string
 */
struct json_string
{
    /**
     * Name of the json string
     */
    string Name;

    /**
     * The string value
     */
    string Value;
};

struct json;

/**
 * Array of json objects, either array of numbers, booleans, strings, or json objects
 */
struct json_array
{
    /**
     * Name of the json array
     */
    string Name;

    /**
     * The type of json array defaults to JSON_ARRAY_NONE
     */
    json_array_type Type;

    union
    {
        /**
         * The array of json objects
         */
        array<json> Objects;

        /**
         * The array of numbers
         */
        array<f64> NumberValues;

        /**
         * The array of booleans
         */
        array<b32> BooleanValues;

        /**
         * The array of strings
         */
        array<string> StringValues;
    };
};

/**
 * Json object, can contain other json objects, json numbers, json booleans, json strings,
 * and json arrays
 */
struct json
{
    /**
     * Name of the json object
     */
    string Name;

    /**
     * Array of child json objects
     */
    array<json> Objects;

    /**
     * Array of json numbers
     */
    array<json_number> Numbers;

    /**
     * Array of child json objects
     */
    array<json_boolean> Booleans;

    /**
     * Array of child json objects
     */
    array<json_string> Strings;

    /**
     * Array of child json objects
     */
    array<json_array> Arrays;
};

/**
 * Add the given json obect to the parent, by default the allocator will be the heap
 * allocator.  This will only be used when the array of child json objects has not
 * been created yet
 *
 * @param Object The parent json object to add the new json object to
 * @param NewValue The child to add to the parent json object
 * @param Allocator The allocator to use to allocate any memory
 */
void AddJsonObject(json* Object, json& NewValue, allocator* Allocator);

/**
 * Add the given value to the parent, by default the allocator will be the heap
 * allocator.  This will only be used when the array of json numbers has not
 * been created yet
 *
 * @param Object The json object to add the json number to
 * @param Name The name of the json number
 * @param NewValue The number value of the json number
 * @param Allocator The allocator to use when allocating memory
 */
void AddJsonNumber(json* Object, string& Name, f64 NewValue, allocator* Allocator);

/**
 * Add the given value to the parent, by default the allocator will be the heap
 * allocator.  This will only be used when the array of json booleans has not
 * been created yet
 *
 * @param Object The json object to add the json boolean to
 * @param Name The name of the json boolean
 * @param NewValue The boolean value of the json boolean
 * @param Allocator The allocator to use when allocating memory
 */
void AddJsonBoolean(json* Object, string& Name, b32 NewValue, allocator* Allocator);

/**
 * Add the given value to the parent, by default the allocator will be the heap
 * allocator.  This will only be used when the array of json strings has not
 * been created yet
 *
 * @param Object The json object to add the json string to
 * @param Name The name of the json string
 * @param NewValue The string value of the json string
 * @param Allocator The allocator to use when allocating memory
 */
void AddJsonString(json* Object, string& Name, string NewValue, allocator* Allocator);

/**
 * Add the given value array to the parent, by default the allocator will be the
 * heap allocator.  This will only be used when the array of json object arrays  has
 * not been created yet
 *
 * @param Object The json object to add the json object array to
 * @param Name The name of the json object array
 * @param Array The array of json objects to add to the parent object
 * @param Allocator The allocator to use when allocating memory
 */
void AddJsonObjectArray(json* Object, string& Name, array<json> Array,
                        allocator* Allocator);

/**
 * Add the given value array to the parent, by default the allocator will be the
 * heap allocator.  This will only be used when the array of string arrays has not
 * been created yet
 *
 * @param Object The json object to add the string array to
 * @param Name The name of the string array
 * @param Array The array of strings to add to the parent object
 * @param Allocator The allocator to use when allocating memory
 */
void AddJsonStringArray(json* Object, string& Name, array<string> Array,
                        allocator* Allocator);

/**
 * Add the given value array to the parent, by default the allocator will be the
 * heap allocator.  This will only be used when the array of boolean arrays has not
 * been created yet
 *
 * @param Object The json object to add the boolean array to
 * @param Name The name of the boolean array
 * @param Array The array of booleans to add to the parent object
 * @param Allocator The allocator to use when allocating memory
 */
void AddJsonBooleanArray(json* Object, string& Name, array<b32> Array,
                         allocator* Allocator);

/**
 * Add the given value array to the parent, by default the allocator will be the
 * heap allocator.  This will only be used when the array of number arrays has not
 * been created yet
 *
 * @param Object The json object to add the number array to
 * @param Name The name of the number array
 * @param Array The array of numbers to add to the parent object
 * @param Allocator The allocator to use when allocating memory
 */
void AddJsonNumberArray(json* Object, string& Name, array<f64> Array,
                        allocator* Allocator);

/**
 * Gets the array of json objects from the parent json object
 *
 * @param Out A pointer the json array output
 * @param Object The parent json object to get the json object array from
 * @param Name The name of the json object array to get
 * @return Whether the array was in the json object
 */
b32 GetJsonObjectArray(array<json>* Out, json* Object, const char* Name);

/**
 * Gets the array of json strings from the parent json object
 *
 * @param Out A pointer the json array output
 * @param Object The parent json object to get the json string array from
 * @param Name The name of the string array to get
 * @return Whether the array was in the json object
 */
b32 GetJsonStringArray(array<string>* Out, json* Object, const char* Name);

/**
 * Gets the array of json booleans from the parent json object
 *
 * @param Out A pointer the json array output
 * @param Object The parent json object to get the boolean array from
 * @param Name The name of the boolean array to get
 * @return Whether the array was in the json object
 */
b32 GetJsonBooleanArray(array<b32>* Out, json* Object, const char* Name);

/**
 * Gets the array of json numbers from the parent json object
 *
 * @param Out A pointer the json array output
 * @param Object The parent json object to get the number array from
 * @param Name The name of the number array to get
 * @return Whether the array was in the json object
 */
b32 GetJsonNumberArray(array<f64>* Out, json* Object, const char* Name);

/**
 * Gets the json object from the parent object
 *
 * @param Out The json object output from the parent object
 * @param Object The parent object to get the json object from
 * @param Name The name of the json object to get
 * @return Whether the json object was found in the parent json object
 */
b32 GetJsonObject(json** Out, json* Object, const char* Name);

/**
 * Gets the number from the parent object
 *
 * @param Out The number output from the parent object
 * @param Object The parent object to get the number from
 * @param Name The name of the number to get
 * @return Whether the number was found in the parent json object
 */
b32 GetJsonNumber(f64* Out, json* Object, const char* Name);

/**
 * Gets the boolean from the parent object
 *
 * @param Out The boolean output from the parent object
 * @param Object The parent object to get the boolean from
 * @param Name The name of the boolean to get
 * @return Whether the boolean was found in the parent json object
 */
b32 GetJsonBoolean(b32* Out, json* Object, const char* Name);

/**
 * Gets the string from the parent object
 *
 * @param Out The string output from the parent object
 * @param Object The parent object to get the string from
 * @param Name The name of the string to get
 * @return Whether the string was found in the parent json object
 */
b32 GetJsonString(string* Out, json* Object, const char* Name);

/**
 * Converts a string to a json object.  Currently there is no syntax checking and comments
 * are not supported.  By default the heap allocator will be used
 *
 * @param Object The object to create
 * @param Buffer The buffer containing the json in string form
 * @param BufferLength The size of the buffer containing the json in string form
 * @param Allocator The allocator to use to allocate memory
 */
void BufferToJSON(json* Object, char* Buffer, u32 BufferLength, allocator* Allocator);

/**
 * Creates an empty json object by copying the name and zeroing everything.  By default
 * the allocator used is the heap allocator
 *
 * @param Json The json object to initialize
 * @param Name The name of the json object
 * @param Allocator The allocator to use to allocate memory
 */
void CreateEmptyJson(json* Json, string Name, allocator* Allocator);

/**
 * Simply loads the file into memory and then calls BufferToJSON on the resulting file
 * contents.  By default the heap allocator will be used
 *
 * @param Object The object to initialize
 * @param FileName The name of the file to read
 * @param Allocator The allocator to use to allocate memory
 */
b32 LoadJsonFromFile(json* Object, const char* FileName, allocator* Allocator);

/**
 * Frees all memory currently allocated in a json object
 *
 * @param Json The json object to free
 * @param Allocator The allocator to use to free all allocated memory
 */
void DestroyJson(json* Json, allocator* Allocator);

void JsonArrayToString(stream* WriteStream, json_array* Array, allocator* Allocator);
void JsonObjectToString(stream* WriteStream, json* Object, allocator* Allocator);
/**
 * Converts the json object to a string
 *
 * @param Object The object to convert into a string
 * @param WriteStream The stream to write the json object to
 * @param Allocator The allocator to use to make all allocations
 */
void JsonToString(json* Object, stream* WriteStream, allocator* Allocator);

#define JSON_H
#endif
