#if !defined(MEMORY_ALLOCATOR_H)

#include "array.h"
#include "memory.h"
#include "types.h"

/**
 * Regions inside of memory
 */
struct allocation_region
{
    /**
     * The index of the starting point of this memory region
     */
    u64 StartIndex;

    /**
     * The index of the last byte in this memory region
     */
    u64 EndIndex;
};

/**
 * This is a general purpose memory allocator that allocates a lot of memory at creation
 * and can give out chunks of it to whoever calls PushMemory
 */
struct memory_allocator
{
    /**
     * The memory buffer that everything will be stored in
     */
    char* Memory;

    /**
     * The pointer to where the next available allocation is
     */
    u64 Pointer;

    /**
     * The size of the memory
     */
    u64 AllocationLength;

    /**
     * This is how the entire library can be used with this allocator
     */
    allocator* Allocator;

    /**
     * These are all of the regions that memory can be allocated from
     * All of the free memory
     */
    array<allocation_region> AllocationRegions;
};

/**
 * Creates a new memory allocator
 *
 * @param AllocationLength The size in bytes of the large chunk of memory to allocate
 * @return A pointer to the new memory allocator
 */
memory_allocator* CreateMemoryAllocator(u64 AllocationLength);

/**
 * Frees the given memory allocator
 *
 * @param MemoryAllocator A pointer to the memory allocator to free
 */
void DestroyMemoryAllocator(memory_allocator** MemoryAllocator);

/**
 * Clears the memory allocator freeing all memory allocated from PushMemory
 *
 * @param MemoryAllocator The memory allocator to clear
 */
void ClearMemoryAllocator(memory_allocator* MemoryAllocator);

/**
 * Returns a pointer to memory of the given length
 *
 * @param MemoryAllocator The memory allocator to use to allocate memory
 * @param Length The number of bytes to allocate
 * @return A pointer to the newly allocated memory or NULL if it was not possible
 */
void* PushMemory(memory_allocator* MemoryAllocator, u64 Length);

/**
 * Frees the given memory
 *
 * @param MemoryAllocator The memory allocator to use to free the memory
 * @param Memory The memory to free
 * @param Length The length of the memory to free
 */
void FreeMemory(memory_allocator* MemoryAllocator, void* Memory, u64 Length);

/**
 * Combines all freed memory regions that are in contact with each other, for any
 * application using this system over a very long period of time or that has a very random
 * pattern of allocation this should be called when the memory allocator has tons of freed
 * memory regions
 *
 * @param MemoryAllocator The memory allocator to combine all overlapping regions
 */
void CombineAllocationRegions(memory_allocator* MemoryAllocator);

// These are only exposed for testing
s32  FindSuitableMemoryLocation(memory_allocator* MemoryAllocator, u64 Length);
void AddSorted(array<allocation_region>* Regions, allocation_region Region);
//

#define MEMORY_ALLOCATOR_H
#endif
