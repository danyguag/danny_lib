#if !defined(COMMANDLINE_PARSER_H)

#include "array.h"
#include "string.h"

enum argument_type
{
    ARGUMENT_TYPE_FLAG   = 1,
    ARGUMENT_TYPE_WORD   = 2,
    ARGUMENT_TYPE_STRING = 3,
};

struct flag
{
    string        ShortName;
    string        LongName;
    string        HelpMessage;
    argument_type Type;

    b32    Found;
    string Value;
};

struct commandline_parser
{
    array<flag> Flags;
    string      GeneralHelpMessage;
    allocator*  Allocator;
};

void  CreateCommandlineParser(commandline_parser* Parser, string GeneralHelpMessage,
                              allocator* Allocator);
void  DestroyCommandlineParser(commandline_parser* Parser, allocator* Allocator);
flag* AddFlag(commandline_parser* Parser, string ShortFlag, string LongFlag,
              argument_type Type, string HelpMessage);
flag* GetFlagByShortName(commandline_parser* Parser, const char* ShortName);
u32   ParseCommandlineArgs(commandline_parser* Parser, int ArgCount, char** Args);

#define COMMANDLINE_PARSER_H
#endif
