#if !defined(MEMORY_H)

#include "types.h"
#include <string.h>

/**
 * The definition of the allocation function.  memory_allocate is a typedef for this
 * function
 *
 * @param Length The length of the memory to allocate
 * @param UserData The userdata for the function
 * @return A pointer to the allocated memory
 */
#define MEMORY_ALLOCATE(name) void* name(u64 Length, void* UserData)
typedef MEMORY_ALLOCATE(memory_allocate);

/**
 * The definition of the de-allocation function.  memory_free is a typedef for this
 * function
 *
 * @param Memory A pointer to the memory to free
 * @param Length The length of the memory to allocate
 * @param UserData The userdata for the function
 */
#define MEMORY_FREE(name) void name(void* Memory, u64 Length, void* UserData)
typedef MEMORY_FREE(memory_free);

/**
 * This struct contains two methods, allocate and free, with user data.  This will be
 * passed into every library function that requires memory operations.
 */
struct allocator
{
    /**
     * Used for allocating memory, see the typedef and define above
     */
    memory_allocate* AllocateFunc;

    /**
     * Used for free memory allocated with the above allocation method, see the typedef
     * and define above
     */
    memory_free* FreeFunc;

    /**
     * Passed into
     */
    void* UserData;

    /**
     * This function should be called to allocate memory and not AllocateFunc
     *
     * @param Length The length of the memory allocation
     * @return A pointer to the allocated memory
     */
    inline void*
    Allocate(u64 Length)
    {
        return AllocateFunc(Length, UserData);
    }

    /**
     * This function should be called to free memory and not FreeFunc
     *
     * @param Pointer The pointer to the memory to free
     * @param Length The length of the memory to free
     */
    inline void
    Free(void* Pointer, u64 Length)
    {
        FreeFunc(Pointer, Length, UserData);
    }
};

/**
 * Initializes an allocator to the given values
 *
 * @param Allocator The allocator to initialize
 * @param AllocFunc The allocation function to use
 * @param FreeFunc The de-allocation function to use
 * @param UserData The user data to pass to the other functions
 */
inline void
CreateAllocator(allocator* Allocator, memory_allocate* AllocFunc, memory_free* FreeFunc,
                void* UserData)
{
    Allocator->AllocateFunc = AllocFunc;
    Allocator->FreeFunc     = FreeFunc;
    Allocator->UserData     = UserData;
}

/**
 * The heap allocator allocation function 
 */
inline
MEMORY_ALLOCATE(HeapMalloc)
{
    UNUSED_VARIABLE(UserData);

    char* Memory = (char*)malloc(Length);

    memset(Memory, 0, Length);

    return Memory;
}

/**
 * The heap de-allocator allocation function 
 */
inline
MEMORY_FREE(HeapFree)
{
    UNUSED_VARIABLE(UserData);
    UNUSED_VARIABLE(Length);

    free(Memory);
}

/**
 * Initializes an allocator to the heap functions
 *
 * @param Allocator The allocator to initialize
 */
inline void
InitHeapAllocator(allocator* Allocator)
{
    CreateAllocator(Allocator, HeapMalloc, HeapFree, NULL);
}

/**
 * Legacy unfortunately many places used engine_memset so it cannot be removed
 * Just a wrapper
 */
#define engine_memset(Memory, Value, Size) memset(Memory, Value, Size)

/**
 * Legacy unfortunately many places used engine_memcpy so it cannot be removed
 * Just a wrapper
 */
#define engine_memcpy(Destination, Source, Size) memcpy(Destination, Source, Size)

/**
 * Copies the passed in memory and returns the copy, by default memory callback is used
 *
 * @param Source The memory to copy
 * @param Length The number of bytes to copy
 * @param MemoryCallback The memory callback to allocate memory
 * @return The copied memory
 */
inline void*
memcpy(void* Source, u64 Length,
       allocator* Allocator)
{
    void* Destination = Allocator->Allocate(Length);
    engine_memcpy(Destination, Source, Length);
    return (Destination);
}

#define MEMORY_H
#endif
