#if !defined(TYPES_H)

#include <stdint.h>
#if defined(PLATFORM_WINDOWS)
#include <intrin.h>
#endif

#include "emmintrin.h"
#include "immintrin.h"
#include "xmmintrin.h"

/**
 * Kilobytes to bytes
 */
#define Kilobytes(Value) ((Value)*1024)

/**
 * Megabytes to bytes
 */
#define Megabytes(Value) (Kilobytes(Value) * 1024)

/**
 * Gigabytes to bytes
 */
#define Gigabytes(Value) (Megabytes(Value) * 1024)

/**
 * Terabytes to bytes
 */
#define Terabytes(Value) (Gigabytes(Value) * 1024)

/**
 * stdint typedefs, the full type is long and annoying to type out
 */
typedef uint8_t  u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t  s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

/**
 * Typedef for float and double to f and then their respective bit sizes
 */
typedef float  f32;
typedef double f64;

typedef s32 b32;

/**
 * Makes sure that NULL is nullptr and not 0
 */
#undef NULL
#define NULL nullptr

/**
 * Define for marking variables as unused also way for
 */
#define UNUSED_VARIABLE(variable) (void)variable;

/**
 * Rounds a float to an integer
 *
 * @param Float The float to round
 * @return The rounded float
 */
inline s64
RoundFloat(f64 Float)
{
    s64 Value = (s64)Float;

    if (Float - Value >= .5)
    {
        Value += 1;
    }

    return (Value);
}

/**
 * Compares the two floats and checks if its less than the Value passed in
 *
 * @param First One of the floating point values to compare
 * @param Second One of the floating point values to compare
 * @param Value The value to check if the difference is less than
 * @return Whether ABS(First - Second) <= Value
 */
inline b32
CompareFloat(f64 First, f64 Second, f64 Value)
{
    f64 Diff = First - Second;

    if (Diff < 0)
    {
        Diff *= -1;
    }

    return (Diff <= Value);
}

#define TYPES_H
#endif
