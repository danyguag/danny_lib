#if !defined(BASE_H)

#include "array.h"
#include "memory.h"
#include "stream.h"
#include "string.h"
#include "types.h"

/**
 * Linux uses integers as handles, the type is abstracted away for portability
 */
typedef int system_handle;

/**
 * Just a pointer to the memory containing the socket address
 */
typedef void* socket_address;

/**
 * A function type for thread functions
 */
#define THREAD_FUNCTION(name) void* name(void* Memory)
typedef THREAD_FUNCTION(thread_function);

/**
 * Used to abstract away the thread handle type
 */
typedef void* thread_handle;

/**
 * Used to abstract away the mutex type
 */
typedef void* mutex;

/**
 * Used to abstract away the semaphore type
 */
typedef void* semaphore;

/**
 * Used to abstract away the type of a loaded shared library
 */
typedef void* shared_library;

/**
 * The type of an io obect
 */
enum io_type
{
    /**
     * The file io type
     */
    IO_FILE,

    /**
     * The folder/directory io type
     */
    IO_FOLDER,
};

/**
 * The type of file operations a file can do once created
 */
enum file_operations
{
    /**
     * The file can only read
     */
    FILE_READ,

    /**
     * The file can only write
     */
    FILE_WRITE,

    /**
     * The file can read and write
     */
    FILE_BOTH
};

/**
 * The types of sockets that can be created
 */
enum socket_type
{
    /**
     * Default socket type should never be used
     */
    SOCKET_TYPE_NONE = 0,

    /**
     * Socket type for tcp sockets
     */
    SOCKET_TYPE_TCP = 1,

    /**
     * Socket type for tcp sockets
     */
    SOCKET_TYPE_UDP = 2,
};

/**
 * Platform file wrapper containing useful information for easy I/O operations
 */
struct file
{
    /**
     * Platform specific handle to the file
     */
    system_handle Handle;

    /**
     * The path to the file, either relative or absolute
     */
    char* FileName;

    /**
     * The length of the file in bytes
     */
    u64 Length;
};

/**
 * Struct to store a file path and the type of io object it is
 */
struct io_object
{
    /**
     * The type of io object
     */
    io_type Type;

    /**
     * The name of an io object located in whatever folder passed to SearchFolder
     */
    string Name;

    /**
     * Compares this io objects name to the passed in string
     *
     * @param String The other string to compare to Name
     * @return Whether the two strings are the same
     */
    inline b32
    operator==(string String)
    {
        return (Name == String);
    }
};

/**
 * Gets the platform specific error
 *
 * @return The error code
 */
int GetPlatformError();

/**
 * Sets the current working directory
 *
 * @param Path The directory path to try and set
 * @return Whether or not the path was changed successfully
 */
b32 SetWorkingDirectory(const char* Path);

/**
 * Gets the current working directory
 *
 * @param Result Pointer to the string to put the current path in
 * @param Allocator The allocator to allocate the string with
 */
void GetCurrentWorkingDirectory(string* Result, allocator* Allocator);

/**
 * Sleep for the given time
 *
 * @param Miliseconds The time in miliseconds to sleep
 */
void Sleep(u32 Miliseconds);

/**
 * Sleep for the given time
 *
 * @param Nanoseconds The time in nanoseconds to sleep
 */
void NanoSleep(u64 Nanoseconds);

/**
 * Gets the total amount of ram
 *
 * @return The total amount of ram in bytes
 */
u64 GetAvailableRam();

/**
 * Closes system_handle if its valid
 *
 * @param Handle A pointer to the handle to free
 */
void CloseSystemHandle(system_handle* Handle);

/**
 * Creates a file handle to the given file, by default if the file does not exist it will
 * be created
 *
 * @param FileName The path to the file, either relative or absolute
 * @param FileOperations The operations that can be done on this file once created
 * @param Create Whether or not to create the file if it does not exist
 * @return The new file struct
 */
file OpenFile(const char* FileName, file_operations FileOperations, b32 Create = true);

/**
 * Sets the given handles offset
 *
 * @param File The file to change the offset of
 * @param Offset The offset to change the file's offset to
 * @return b32 Whether the file's offset was changed
 */
b32 SetFileOffset(file File, s32 Offset);

/**
 * Writes to a file handle
 *
 * @param File The file to write to
 * @param Buffer The memory to write to a file
 * @param BufferLength The number of bytes to write from buffer
 * @return Whether BufferLength bytes were written to the file
 */
b32 WriteFile(file File, char* Buffer, u32 BufferLength);

/**
 * Reads the given file
 *
 * @param File The file to read from
 * @param Buffer The memory to put the read contents into
 * @param Length The number of bytes to read from the file
 * @return The number of bytes read or -1 if there was an error
 */
s32 ReadFile(file File, char* Buffer, s32 Length);

/**
 * Reads the entire file given, the space needed for the entire file is allocated and then
 * the memory is returned.  It is up to the user to make sure that the entire file can fit
 * into memory, by default the memory callback will be the heap callback, the number of
 * bytes read is stored in the file struct under the file::Length variable
 *
 * @param File The file to read
 * @param Allocator The allocator to allocate the memory to store the file
 * contents
 * @return The pointer to the file contents
 */
char* ReadFile(file File, allocator* Allocator);

/**
 * Checks if the given file exists
 *
 * @param FileName The file path to check for
 * @return Whether the file exists or not
 */
b32 DoesFileExist(const char* FileName);

/**
 * Deletes the given file
 */
void DeleteFile(const char* FileName);

/**
 * Gets the time of the last modification of a file
 *
 * @param FileName The file to get the modification date of
 * @return The time in seconds taken from the stat::st_mtime
 */
u64 GetFileLastModifiedTime(const char* FileName);

/**
 * Gets the length of the file
 *
 * @param FileName The file to get the length of
 * @return The length in bytes of the file
 */
u64 GetFileLength(const char* FileName);

/**
 * This wraps the mmap call, assumes map is private, and first arg addr is NULL
 *
 * @param Size The size of the mapped memory
 * @param FileOperations This is either FILE_READ, FILE_WRITE, or FILE_BOTH
 * @param The file to map
 * @param Offset The offset of the file to start the map at
 */
void* MmapFile(u64 Size, file_operations FileOperations, file File, u64 Offset);

/**
 * Wraps the munmap call
 *
 * @param MappedMemory The mapped memory to unmap
 * @param Size The size of the mapped memory to unmap
 */
b32 MunmapFile(void* MappedMemory, u64 Size);

/**
 * Makes the given directory
 *
 * @param FileName The given directory to make
 * @return Whether the directory was made or not
 */
b32 MakeDirectory(const char* FileName);

/**
 * Creates a new tcp socket
 *
 * @param Type The type of socket to create tcp or udp
 * @return The handle to the new socket
 */
system_handle OpenSocket(socket_type Type);

/**
 * Changes the given socket to non blocking
 *
 * @param Socket The socket to modify
 * @return Whether the socket could be changed to non blocking or not
 */
b32 SetNonblocking(system_handle Socket);

/**
 * Tries to start a tcp connection
 *
 * @param Socket The socket to modify
 * @param ServerAddress The ip address of the tcp server to connect to
 * @param Port The port of the tcp server to connect to
 * @return Whether the socket connected to the tcp server
 */
b32 ConnectSocket(system_handle Socket, const char* ServerAddress, u32 Port);

/**
 * Binds a socket to the specified port, note this is not needed for clients connecting to
 * a tcp server
 *
 * @param s The socket to bind
 * @param Port The port number to bind the socket to
 * @return Whether the bind operation was successful
 */
b32 BindSocket(system_handle Socket, u32 Port);

/**
 * Creates a pointer to a platform specific representation of a socket address
 *
 * @param Address The address of the socket
 * @param Port The port of the socket
 * @param Allocator The allocator to use to manage memory
 * @return The pointer to the platform specific socket address
 */
socket_address MakeServerAddress(const char* Address, u32 Port, allocator* Allocator);

/**
 * Sends data over the socket
 *
 * @param Socket The socket to send the data over
 * @param Buffer The data to send over the socket
 * @param Length The number of bytes to send over the socket
 */
s32 SendData(system_handle Socket, char* Buffer, u32 Length);

/**
 * Determines if the error code is a non blocking error
 *
 * @param Error The error code to check
 * @return Whether the error code is a non blocking error
 */
b32 IsBlockingError(s32 Error);

/**
 * Reads data from the socket
 *
 * @param Socket The socket to read from
 * @param Buffer The memory to store what was read in
 * @param Length The max number of bytes to read
 * @return The number of bytes read or -1 to indicate error
 */
s32 RecvData(system_handle Socket, char* Buffer, u32 Length);

/**
 * Reads all of the available data on the socket into the given stream
 *
 * @param Socket The socket to read from
 * @param ReadStream The stream to put the data in
 * @return The number of bytes read
 */
s32 Read(system_handle Socket, stream* ReadStream);

/**
 * Enables a socket to accept sockets
 *
 * @param Socket The socket to modify
 * @param Backlog The max number of sockets waiting to be accepted before they are denied
 * @return Whether the socket can accept sockets or not
 */
b32 SocketListen(system_handle Socket, s32 Backlog);

/**
 * Accepts a socket on the given socket
 *
 * @param Socket The socket to accept sockets from
 * @return The new socket or an invalid socket, you can tell if a socket is invalid by
 * calling IsInvalidSocket
 */
system_handle AcceptSocket(system_handle Socket);

/**
 * Checks a socket to see if its invalid
 *
 * @param Socket The socket to check for validity
 * @return Whether the socket is invalid or not
 */
b32 IsInvalidSocket(system_handle Socket);

/**
 * Starts a thread with the given thread function and user data, by default the heap
 * memory callback is used
 *
 * @param Function The function to run on the new thread
 * @param Memory The user data to pass to this function
 * @param Allocator The allocator to use when allocating the thread_handle
 * @return The valid thread handle if the thread was created or NULL if the thread could
 * not be created
 */
thread_handle StartThread(thread_function Function, void* Memory, allocator* Allocator);

/**
 * Frees a thread handle
 *
 * @param Handle The thread handle to free
 * @param Allocator The memory callback to use to free the thread handle
 */
void DestroyThreadHandle(thread_handle Handle, allocator* Allocator);

/**
 * Fetches the current thread's handle and creates a thread_handle to store it, by default
 * the heap memory callback is used
 *
 * @param Allocator The memory callback to allocate the thread_handle pointer
 * @return Posix thread handle
 */
thread_handle GetCurrentThreadHandle(allocator* Allocator);

/**
 * Waits until the given thread has finished
 *
 * @param ThreadHandle The thread handle of the thread to wait for
 */
void JoinThread(thread_handle ThreadHandle);

/**
 * Exits the current thread with the given return value
 *
 * @param Value The return value of the thread
 */
void ExitThread(void* Value);

/**
 * Changes the affinity of the given thread
 *
 * @param ThreadHandle The thread to modify
 * @param Core Which core to chagne the thread to
 * @return Whether the affinity could be changed
 */
b32 SetThreadAffinity(thread_handle ThreadHandle, u32 Core);

/**
 * Gets the given threads affinity
 *
 * @param ThreadHandle The thread to check
 * @return The core affinity of the given thread
 */
u32 GetThreadAffinity(thread_handle ThreadHandle);

/**
 * Creates a semaphore with the given initial value
 *
 * @param InitialValue The initial value of the semaphore
 * @return A pointer to a platform specific semaphore object
 */
semaphore CreateSemaphore(u32 InitialValue);

/**
 * Destroys the given semaphore object
 *
 * @param Semaphore The object to destroy
 */
void DestroySemaphore(semaphore Semaphore);

/**
 * Waits for the semaphore to signal
 *
 * @param Semaphore The semaphore to wait on
 */
void Wait(semaphore Semaphore);

/**
 * Signals the semaphore
 *
 * @param Semaphore The semaphore to signal
 */
void Signal(semaphore Semaphore);

/**
 * Creates a mutex, by default the heap memory callback is used
 *
 * @param Allocator The allocator to allocate the mutex
 * @return A pointer to a platform specific mutex object
 */
mutex CreateMutex(allocator* Allocator);

/**
 * Destroys a mutex, by default the heap memory callback is used
 *
 * @param Mutex The mutex to free
 * @param Allocator The allocator to free the mutex with
 */
void DestroyMutex(mutex* Mutex, allocator* Allocator);

/**
 * These functions still need to be abstracted away because of different platform
 * implementations, but should only be used inside this header file and platform source
 * files
 */
namespace internal_platform
{
void _LockMutex(mutex Mutex);
void _UnlockMutex(mutex Mutex);
} // namespace internal_platform
/**
 * Ensures that a mutex locks and frees allowing the caller to pass a function to run in
 * between
 *
 * @template K The template for the callback
 * @param Mutex The mutex to lock and unlock
 * @param Function The function to run while the mutex is locked
 */
template<typename K>
inline void
Guard(mutex Mutex, K&& Function)
{
    internal_platform::_LockMutex(Mutex);
    Function();
    internal_platform::_UnlockMutex(Mutex);
}

/**
 * Constantly checks for the data pointed to by Data to be equal to the ExpectedValue
 * while the given mutex is locked
 *
 * @template expected_type The type of the values being compared
 * @param Lock The mutex to lock and unlock
 * @param SleepTime The time to sleep in between checking Data and ExpectedValue
 * @param Data The pointer to the data to keep checking
 * @param ExpectedValue The value to wait for Data to be
 */
template<typename expected_type>
inline void
WaitForExpected(mutex Lock, u64 SleepTime, expected_type* Data,
                expected_type ExpectedValue)
{
    internal_platform::_LockMutex(Lock);

    while (*Data != ExpectedValue)
    {
        internal_platform::_UnlockMutex(Lock);
        NanoSleep(SleepTime);
        internal_platform::_LockMutex(Lock);
    }

    internal_platform::_UnlockMutex(Lock);
}

/**
 * Guards the given Function with the given mutex and expected a return value
 *
 * @template return_value The return type of the callback
 * @template K The template for the callback
 * @param Mutex The mutex to lock and unlock
 * @param Function The callback to call while the mutex is locked
 * @return The return value of the callback
 */
template<typename return_value, typename K>
inline return_value
GuardRet(mutex Mutex, K&& Function)
{
    internal_platform::_LockMutex(Mutex);
    return_value Value = Function();
    internal_platform::_UnlockMutex(Mutex);

    return (Value);
}

/**
 * Gets the core count of the system
 *
 * @return The number of cores on the current system
 */
u32 GetCoreCount();

/**
 * Prints out to stdout
 *
 * @param Print The string to print to stdout
 */
void StandardPrint(string Print);

/**
 * Searches the given directory
 *
 * @param Path The directory to search in
 * @param Allocator The allocator to use when allocating memory
 * @return The array containing files and folders of the given directory
 */
array<io_object> SearchFolder(const char* Path, allocator* Allocator);

#define BASE_H
#endif
