#if !defined(ARRAY_H)

#include "memory.h"
#include "string.h"
#include "types.h"

#define DEFAULT_ARRAY_SIZE 256

/**
 * Simple dynamic array wrapper
 */
template<typename T>
struct array
{
    /**
     * Initialize to default values
     */
    array()
    {
        Buffer          = NULL;
        Size            = 0;
        AllocatedLength = 0;
    }

    /**
     * Array Memory
     */
    T* Buffer;

    /**
     * Number of elements in the array
     */
    u64 Size;

    /**
     * Max number of elements in the array
     */
    u64 AllocatedLength;

    /**
     * The allocator that will be used to create, resize, and destroy the array
     */
    allocator* Allocator;

    /**
     * Gets the element at the Index
     *
     * @param Index The index to get the element at
     * @return Returns the element at the given index
     */
    inline T&
    operator[](u64 Index)
    {
        return (Buffer[Index]);
    }

    /**
     * Gets the element at the Index
     *
     * @param Index The index to get the element at
     * @return Returns the element at the given index
     */
    inline T*
    operator+(u64 Index)
    {
        return (Buffer + Index);
    }
};

// Template Declerations
template<typename T>
void CreateArray(array<T>* Array, allocator* Allocator,
                 u64 AllocationLength = DEFAULT_ARRAY_SIZE);
template<typename T>
void CopyArray(array<T>* Out, array<T>* In, allocator* Allocator);
template<typename T>
b32 Add(array<T>* Destination, array<T>* Source, u64 SourceStartOffset = 0);
//

/**
 * Copies the source string array to the destination source array, by default the heap
 * memory callback is used
 *
 * @param Destination The array to copy the source to
 * @param Source The array to copy from
 * @param Allocator The allocator to use to allocate memory for the destination
 * array
 */
void CopyArray(array<string>* Destination, array<string>* Source, allocator* Allocator);

/**
 * Adds the source string array to the destination starting at the offset of the source,
 * by default the offset to start adding from the source is 0
 *
 * @param Destination The array to add the source to
 * @param Source The source array to get elements from
 * @param SourceStartOffset The offset to start adding elements from the source array
 */
b32 Add(array<string>* Destination, array<string>* Source, u64 SourceStartOffset = 0);

/**
 * Adds the specified string
 *
 * @param Array The array to add to
 * @param New The string to add
 * @return Whether the string was added or not
 */
b32 Add(array<string>* Array, string New);

/**
 * Removes the string at the index specified
 *
 * @param Array The array to remove from
 * @param Index The index of the element to remove
 * @return Whether or not the element was removed at that index
 */
b32 Remove(array<string>* Array, u64 Index);

/**
 * Compares the two f32 arrays
 *
 * @param First One of the two arrays to compare
 * @param Second The other of the two arrays to compare
 * @return Whether the two arrays contain the same numbers
 */
b32 CompareArray(array<f32>& First, array<f32>& Second);

/**
 * Compares the two f64 arrays
 *
 * @param First One of the two arrays to compare
 * @param Second The other of the two arrays to compare
 * @return Whether the two arrays contain the same numbers
 */
b32 CompareArray(array<f64>& First, array<f64>& Second);

/**
 * Creates an array, if no memory callback is passed in the default will be the
 * heap callback, if no allocation length is passed in then the default is
 * DEFAULT_ARRAY_SIZE, if the resizable option is not passed the default is true
 *
 * @template T The storage type
 * @param Array The array to initialize
 * @param Allocator The allocator to use to initialize the buffer
 * @param AllocationLength The length to allocate an array to
 */
template<typename T>
void
CreateArray(array<T>* Array, allocator* Allocator, u64 AllocationLength)
{
    Array->AllocatedLength = AllocationLength;
    Array->Size            = 0;
    Array->Allocator       = Allocator;

    Array->Buffer = (T*)Allocator->Allocate(sizeof(T) * AllocationLength);
}

/**
 * Checks whether the array is empty
 *
 * @template T The storage type
 * @return b32 Whether the array is empty
 */
template<typename T>
inline b32
IsEmpty(array<T>* Array)
{
    return (Array->Size == 0);
}

/**
 * Checks whether the array is full
 *
 * @template T The storage type
 * @return b32 Whether the array is full
 */
template<typename T>
inline b32
IsFull(array<T>* Array)
{
    return (Array->AllocatedLength == Array->Size);
}

/**
 * Gets the element at the Index
 *
 * @template T The storage type
 * @param Array The array to get the element from
 * @param Index The index to get the element at
 * @return T Returns the element at the given index
 */
template<typename T>
inline T*
GetElement(array<T>* Array, u64 Index)
{
    return (Array->Buffer + Index);
}

/**
 * Resizes the array by doubling the allocated length
 *
 * @template T The storage type
 * @param Array The array to resize
 */
template<typename T>
void
ResizeArray(array<T>* Array)
{
    T* Temp = (T*)Array->Allocator->Allocate(Array->AllocatedLength * 2 * sizeof(T));

    engine_memcpy(Temp, Array->Buffer, Array->AllocatedLength * sizeof(T));
    Array->Allocator->Free(Array->Buffer, Array->AllocatedLength * sizeof(T));
    Array->Buffer = Temp;
    Array->AllocatedLength *= 2;
}

/**
 * Frees all of the allocated memory in the array
 *
 * @template T The storage type
 * @param Array The array to free
 */
template<typename T>
void
FreeArray(array<T>* Array)
{
    allocator* Allocator = Array->Allocator;

    if (Allocator && Array->AllocatedLength > 0)
    {
        Allocator->Free(Array->Buffer, Array->AllocatedLength * sizeof(T));
    }

    Array->Size            = 0;
    Array->AllocatedLength = 0;
}

/**
 * Copies the source array to the destination array
 *
 * @template T The storage type
 * @param Destination The array to copy to
 * @param Source The array to copy from
 * @param Allocator The allocator to allocate the destination array with
 */
template<typename T>
void
CopyArray(array<T>* Destination, array<T>* Source, allocator* Allocator)
{
    Destination->AllocatedLength = Source->AllocatedLength;
    Destination->Size            = Source->Size;
    Destination->Allocator       = Allocator;

    if (Allocator)
    {
        Destination->Buffer =
            (T*)Allocator->Allocate(sizeof(T) * Destination->AllocatedLength);
        engine_memcpy(Destination->Buffer, Source->Buffer, sizeof(T) * Source->Size);
    }
}

/**
 * Removes all of the elements in the array
 *
 * @template T The storage type
 * @param Array The array to clear
 */
template<typename T>
void
ClearArray(array<T>* Array)
{
    engine_memset(Array->Buffer, 0, sizeof(T) * Array->Size);
    Array->Size = 0;
}

/**
 * Sets all of the elements in the array to the given value
 *
 * @template T The storage type
 * @param Array The array to modify
 * @param DefaultValue The value to set every element in the array
 */
template<typename T>
void
SetArray(array<T>* Array, T DefaultValue)
{
    Array->Size      = Array->AllocatedLength;
    T* pDefaultValue = &DefaultValue;

    for (u64 Index = 0; Index < Array->Size; ++Index)
    {
        engine_memset(Array->Buffer + Index, pDefaultValue, sizeof(T));
    }
}

/**
 * Returns whether or not the elements in the arrays are the same
 *
 * @template T The storage type
 * @param Left One of the arrays to compare
 * @param Right The other array to compare
 * @return True or false depending on the comparison of the elements
 */
template<typename T>
b32
CompareArray(array<T>& Left, array<T>& Right)
{
    if (Left.Size != Right.Size)
    {
        return (false);
    }

    u64 Count = Right.Size;

    for (u64 Index = 0; Index < Count; ++Index)
    {
        if (Left[Index] != Right[Index])
        {
            return (false);
        }
    }

    return (true);
}

/**
 * Adds a new element to an array
 *
 * @template T The storage type
 * @param Array The array to modify
 * @param New The new element to add
 * @return Whether the element was added
 */
template<typename T>
b32
Add(array<T>* Array, T New)
{
    // Resize the array if necessary
    if (IsFull(Array))
    {
        ResizeArray(Array);
    }

    engine_memcpy(Array->Buffer + Array->Size, &New, sizeof(T));
    ++Array->Size;

    return (true);
}

/**
 * Adds an array to another array
 *
 * @template T The storage type
 * @param Destination The array to add the other to
 * @param Source The source array to add to the destination
 * @return Whether the array could be added
 */
template<typename T>
b32
Add(array<T>* Destination, array<T>* Source, u64 SourceStartOffset)
{
    u64 SourceCopyLength = Source->Size - SourceStartOffset;

    // Resize until the array is the right size, do not do anything if the array has a
    // large enough allocated length
    while (IsFull(Destination) ||
           Destination->Size + SourceCopyLength > Destination->AllocatedLength)
    {
        ResizeArray(Destination);
    }

    engine_memcpy(Destination->Buffer + Destination->Size,
                  Source->Buffer + SourceStartOffset, SourceCopyLength * sizeof(T));
    Destination->Size += SourceCopyLength;

    return (true);
}

/**
 * Increments the element pointer indicating a new element was added and return the
 * pointer to the location of the new element
 *
 * @template T The storage type
 * @param Array The array to modify
 * @return The pointer to the new element
 */
template<typename T>
T*
Add(array<T>* Array)
{
    // Resize the array if necessary
    if (IsFull(Array))
    {
        ResizeArray(Array);
    }

    T* Result = Array->Buffer + Array->Size;
    ++Array->Size;

    return (Result);
}

/**
 * Removes the element at the given index
 *
 * @template T The storage type
 * @param Array The array to modify
 * @param Index The index of the element to remove
 * @return Whether or not the element was removed
 */
template<typename T>
b32
Remove(array<T>* Array, u64 Index)
{
    if (Array->Size <= Index)
    {
        return (false);
    }

    engine_memcpy(Array->Buffer + Index, Array->Buffer + (Index + 1),
                  sizeof(T) * (Array->Size - Index));
    --Array->Size;

    return (true);
}

/**
 * Checks the array for the given value
 *
 * @template T The storage type
 * @param Array The array to check in
 * @param T The value to find
 * @return True or false whether the array contains this value
 */
template<typename T>
b32
Contains(array<T>* Array, T Value)
{
    u64 ArrayLength = Array->Size;

    for (u64 ArrayIndex = 0; ArrayIndex < ArrayLength; ++ArrayIndex)
    {
        if (*GetElement(Array, ArrayIndex) == Value)
        {
            return (true);
        }
    }

    return (false);
}

#define ARRAY_H
#endif
