#if !defined(TOKEN_H)

#include "memory.h"
#include "string.h"
#include "types.h"

/**
 * The type a token can be
 */
enum token_type
{
    /**
     * The default type of none
     */
    TOKEN_NONE,

    /**
     * When a character is not a number or alphabetical
     */
    TOKEN_SYMBOL,

    /**
     * When a token is a number
     */
    TOKEN_NUMBER,

    /**
     * When a token is a a single work that does not contain quotations
     *
     * Example, the first token would be a number, and the second would be TOKEN_TEXT
     *
     * 1238737 ThisisatypeofTOKEN_TEXT
     */
    TOKEN_TEXT,

    /**
     * When a token is a string in quotations
     *
     * An example of this token type would be: "This would be of this type"
     */
    TOKEN_STRING,

    /**
     * This is when the word true or false is found
     */
    TOKEN_BOOLEAN,

    /**
     * This is when there is something like -f or -c
     */
    TOKEN_FLAG
};

/**
 * Contains information regarding a single token
 */
struct token
{
    /**
     * The type of token, explained above
     */
    token_type Type;

    /**
     * The token information
     */
    union
    {
        /**
         * The symbol value
         */
        char Symbol;

        /**
         * The number value
         */
        f64 Number;

        /**
         * The text or string value
         */
        string String;

        /**
         * The boolean value
         */
        b32 Boolean;

        /**
         * The flag value
         */
        char Flag;
    };
};

/**
 * Contains information for reading new tokens
 */
struct tokenizer
{
    /**
     * The body of text to parse and turn into tokens
     */
    string Buffer;

    /**
     * The pointer of where the tokenizer has read to in the buffer
     */
    u64 Pointer;

    /**
     * Memory for the token since only one will be read at a time
     */
    token Token;
};

/**
 * Initializes a tokenizer
 *
 * @param Tokenizer The tokenizer to initialize
 * @param Buffer The body of text to use when parsing for tokens
 * @param BufferLength The length of the buffer to parse
 */
void CreateTokenizer(tokenizer* Tokenizer, char* Buffer, u64 BufferLength);

/**
 * Zeros a token and free's any memory that may have been allocated with it, by default
 * the heap memory callback is used
 *
 * @param Token The token to reset
 * @param Allocator The allocator to use to free any memory
 */
void ResetToken(token* Token, allocator* Allocator);

/**
 * Gets the next token from the tokenizer, by default the heap memory callback is used
 *
 * @param Tokenizer The tokenizer to read from
 * @param Token The token output
 * @param Allocator The allocator to use when allocating memory
 */
b32 GetNextToken(tokenizer* Tokenizer, token* Token,
                  allocator* Allocator);

/**
 * Determines if there are any more tokens in the tokenizer
 *
 * @param Tokenizer The tokenizer to check
 * @return Whether there are any tokens left in the tokenizer
 */
inline b32
HasNextToken(tokenizer& Tokenizer)
{
    return (Tokenizer.Pointer < Tokenizer.Buffer.Length);
}

#define TOKEN_H
#endif
