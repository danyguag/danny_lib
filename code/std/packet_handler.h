#if !defined(PACKET_HANDLER_H)

#include "array.h"
#include "base.h"
#include "pool.h"
#include "stream.h"

/**
 * Packet callback type
 *
 * @param Socket The socket that received the packet
 * @param ReadStream The stream containing the packet
 * @param UserData The user data passed when the packet callback was added
 */
#define PACKET_HANDLER_CALLBACK(name)                                                    \
    void name(system_handle Socket, stream* ReadStream, void* UserData)
typedef PACKET_HANDLER_CALLBACK(packet_handler_function);

/**
 * Descrbes the potential errors that could happen when reading for a packet
 */
enum packet_read_error
{
    /**
     * There was no error
     */
    PACKET_READ_NO_ERROR = 0,

    /**
     * The packet header was shorter than expected
     */
    PACKET_READ_HEADER_INVALID = 1,

    /**
     * There was no data waiting to be read from the socket
     */
    PACKET_READ_NO_PACKET = 2,

    /**
     * There was either nothing to read or there was an error when reading
     */
    PACKET_READ_INVALID_LENGTH = 3,

    /**
     * No packet exists for the packet callback that was just read in
     */
    PACKET_READ_INVALID_PACKET = 4,
};

/**
 * All information needed to search for and call a packet callback
 */
struct packet_handler_callback
{
    /**
     * The packet callback
     */
    packet_handler_function* Function;

    /**
     * The packet id of this packet callback
     */
    u32 PacketId;

    /**
     * The user data to pass to the packet callback
     */
    void* UserData;
};

struct packet_handler
{
    /**
     * All of the packet callbacks
     */
    array<packet_handler_callback> Callbacks;

    /**
     * Stream to put read data
     */
    stream ReadStream;

    /**
     * The stream to use when reading in the packet header
     */
    stream PacketHeader;
};

/**
 * Creates a new packet handler, by default the heap memory callback is used
 *
 * @param PacketHandler The packet handler to initialize
 * @param Allocator The allocator to use to allocate memory
 */
void CreatePacketHandler(packet_handler* PacketHandler, allocator* Allocator);

/**
 * Destroys the given packet handler
 *
 * @param PacketHandler The packet handler to free
 */
void DestroyPacketHandler(packet_handler* PacketHandler);

/**
 * Adds the given packet callback with the given packet id and user data to the packet
 * handler
 *
 * @param PacketHandler The packet handler to add the new packet callback to
 * @param PacketId The packet id to use to look up this new packet callback
 * @param Callback The packet callback function to call when the given packet id is read
 * @param UserData The user data to pass to the packet callback function when it is
 * invoked
 */
void AddCallback(packet_handler* PacketHandler, u32 PacketId,
                 packet_handler_function* Callback, void* UserData);

/**
 * Removes the packet callback with the given packet id
 *
 * @param PacketHandler The packet handler to remove the packet callback from
 * @param PacketId The packet id of the packet callback to remove
 */
void RemoveCallback(packet_handler* PacketHandler, u32 PacketId);

/**
 * Reads a packet from the given socket, if there is no packet it returns with
 * PACKET_READ_NO_ERROR.  If there is a packet and it has a valid packet header the rest
 * of the packet is read in and the callback called.
 *
 * @param PacketHandler The packet handler to use when finding the callback
 * @param Socket The socket to read on
 * @return The state of the read when exiting
 */
packet_read_error ReadAndProcessPacket(packet_handler* PacketHandler,
                                       system_handle   Socket);

// Only exposed for testing
packet_handler_callback* GetPacketFunctionById(array<packet_handler_callback>& Callbacks,
                                               u32                             PacketId);
//

/**
 * Writes the packet header into a stream
 *
 * @param Stream The stream to write the header into
 * @param PacketId The packet id of the packet
 * @return u64
 */
inline u64
StartPacketWrite(stream* Stream, u32 PacketId)
{
    Write<u32>(Stream, PacketId);

    u64 StartIndex = Stream->WritePointer;

    Write<u64>(Stream, (u64)0);

    return (StartIndex);
}

inline void EndPacketWrite(stream* Stream, u64 PacketStartIndex = sizeof(u32));

/**
 * Completes the packet header once the length of the packet is known, by default the
 * place to write the packet length is the sizeof u32 if the packet being written to when
 * StartPacketWrite is a stream with stuff already written in it then the result of that
 * function should be the passed in as PacketStartIndex
 *
 * @param Stream The stream containing the packet
 * @param PacketStartIndex The index where the packet length should be written
 */
inline void
EndPacketWrite(stream* Stream, u64 PacketStartIndex)
{
    u64 CurrentWritePointer = Stream->WritePointer;

    Stream->WritePointer = PacketStartIndex;
    Write<u64>(Stream, CurrentWritePointer - (sizeof(u32) + sizeof(u64)));

    Stream->WritePointer = CurrentWritePointer;
}

#define PACKET_HANDLER_H
#endif
