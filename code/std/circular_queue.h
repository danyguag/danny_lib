#if !defined(CIRCULAR_QUEUE_H)

#include "memory.h"
#include "types.h"

#define DEFAULT_QUEUE_SIZE 256

/**
 * Queue implementation that is circular
 */
template<typename T>
struct circular_queue
{
    /**
     * Default initialization of struct fields
     */
    circular_queue()
    {
        Buffer          = NULL;
        EnqueueIndex    = 0;
        DequeueIndex    = 0;
        AllocatedLength = 0;
    }

    /**
     * The memory callback used at creation, this is used to resize and free the queue
     */
    allocator* Allocator;

    /**
     * The queue storage memory
     */
    T* Buffer;

    /**
     * The max number of elements that can be stored
     */
    u32 AllocatedLength;

    /**
     * The enqueue index of the last added element
     */
    u32 EnqueueIndex;

    /**
     * The dequeue index of the last removed element
     */
    u32 DequeueIndex;

    /**
     * The number of elements in the queue
     */
    u32 EnqueuedCount;

    /**
     * The number of elemets in the queue
     *
     * @return The number of elements in the queue
     */
    inline u32
    Size()
    {
        return (EnqueuedCount);
    }

    /**
     * Whether the queue has been initialized
     */
    inline operator b32()
    {
        return (AllocatedLength > 0);
    }
};

template<typename T>
void CreateCircularQueue(circular_queue<T>* CircularQueue, allocator* Allocator,
                         u32 Size = DEFAULT_QUEUE_SIZE);
template<typename T>
void DestroyQueue(circular_queue<T>* CircularQueue);
template<typename T>
void Clear(circular_queue<T>* CircularQueue);

template<typename T>
void Enqueue(circular_queue<T>* CircularQueue, T New);
template<typename T>
T* Enqueue(circular_queue<T>* CircularQueue);

template<typename T>
b32 Dequeue(T* Out, circular_queue<T>* CircularQueue);
template<typename T>
b32 Peek(T* Out, circular_queue<T>* CircularQueue);

/**
 * Function calls only the circular queue functions should call
 */
namespace circular_queue_internal
{

/**
 * Increments an enqueue index based off of the queue
 *
 * @template T The storage type of the queue
 * @param CircularQueue The queue to use
 * @return u32 The next enqueue index for the specified queue
 */
template<typename T>
inline u32
IncrementCircularIndex(circular_queue<T>* CircularQueue, u32 Index)
{
    return ((Index + 1) % CircularQueue->AllocatedLength);
}

/**
 * Resizes the circular queue
 *
 * @template T The storage type of the queue
 * @param CircularQueue The queue to resize
 */
template<typename T>
static void
ResizeQueue(circular_queue<T>* CircularQueue)
{
    T* NewBuffer = (T*)CircularQueue->Allocator->Allocate(
        (((CircularQueue->AllocatedLength - 1) * 2) + 1) * sizeof(T));

    u32 Count = CircularQueue->Size();

    u32 DequeueIndex = CircularQueue->DequeueIndex;

    for (u32 Index = 0; Index < Count; ++Index)
    {
        NewBuffer[Index] = CircularQueue->Buffer[DequeueIndex];
        DequeueIndex     = IncrementCircularIndex(CircularQueue, DequeueIndex);
    }

    CircularQueue->DequeueIndex    = 0;
    CircularQueue->EnqueueIndex    = Count - 1;
    CircularQueue->AllocatedLength = ((CircularQueue->AllocatedLength - 1) * 2) + 1;
    CircularQueue->Allocator->Free(CircularQueue->Buffer,
                                   sizeof(T) * CircularQueue->AllocatedLength);
    CircularQueue->Buffer = NewBuffer;
}

} // namespace circular_queue_internal

/**
 * Creates a circular queue, the default memory callback is the heap memory callback
 *
 * @template T The storage type of the queue
 * @param CircularQueue The queue being created
 * @param Size The number of entries to allocate for initially
 * @param Allocator The allocator to use to allocate the buffer
 */
template<typename T>
void
CreateCircularQueue(circular_queue<T>* CircularQueue, allocator* Allocator, u32 Size)
{
    CircularQueue->Buffer          = (T*)Allocator->Allocate(sizeof(T) * (Size + 1));
    CircularQueue->AllocatedLength = Size + 1;
    CircularQueue->EnqueueIndex    = Size;
    CircularQueue->DequeueIndex    = 0;
    CircularQueue->EnqueuedCount   = 0;
    CircularQueue->Allocator       = Allocator;
}

/**
 * Frees a circular queue
 *
 * @template T The storage type of the queue
 * @param CircularQueue The queue being destroyed
 */
template<typename T>
void
DestroyQueue(circular_queue<T>* CircularQueue)
{
    CircularQueue->Allocator->Free(CircularQueue->Buffer,
                                   sizeof(T) * CircularQueue->AllocatedLength);

    CircularQueue->Buffer          = NULL;
    CircularQueue->EnqueueIndex    = 0;
    CircularQueue->DequeueIndex    = 0;
    CircularQueue->EnqueuedCount   = 0;
    CircularQueue->AllocatedLength = 0;
}

/**
 * Clears the circular queue
 *
 * @template T The storage type of the queue
 * @param CircularQueue The queue being cleared
 */
template<typename T>
void
Clear(circular_queue<T>* CircularQueue)
{
    engine_memset(CircularQueue->Buffer, 0, sizeof(T) * CircularQueue->AllocatedLength);

    CircularQueue->EnqueueIndex  = CircularQueue->AllocatedLength - 1;
    CircularQueue->DequeueIndex  = 0;
    CircularQueue->EnqueuedCount = 0;
}

/**
 * Enqueues the new element into the buffer
 *
 * @template T The storage type of the queue
 * @param CircularQueue The queue being manipulated
 * @param New The new element
 */
template<typename T>
void
Enqueue(circular_queue<T>* CircularQueue, T New)
{
    if (IsFull(CircularQueue))
    {
        circular_queue_internal::ResizeQueue(CircularQueue);
    }

    u32 EnqueueIndex = circular_queue_internal::IncrementCircularIndex(
        CircularQueue, CircularQueue->EnqueueIndex);
    CircularQueue->Buffer[EnqueueIndex] = New;
    CircularQueue->EnqueueIndex         = EnqueueIndex;
    ++CircularQueue->EnqueuedCount;
}

/**
 * Returns the pointer to the next space for an element and increments the enqueue index
 *
 * @template T The storage type of the queue
 * @param CircularQueue The queue being manipulated
 * @return The new element pointer
 */
template<typename T>
T*
Enqueue(circular_queue<T>* CircularQueue)
{
    if (IsFull(CircularQueue))
    {
        circular_queue_internal::ResizeQueue(CircularQueue);
    }

    u32 EnqueueIndex = circular_queue_internal::IncrementCircularIndex(
        CircularQueue, CircularQueue->EnqueueIndex);
    CircularQueue->EnqueueIndex = EnqueueIndex;

    ++CircularQueue->EnqueuedCount;

    return (CircularQueue->Buffer + EnqueueIndex);
}

/**
 * Dequeues the next element
 *
 * @template T The storage type of the queue
 * @param Out The first element to dequeue
 * @param CircularQueue The queue being manipulated
 * @return Whether the operation was successful
 */
template<typename T>
b32
Dequeue(T* Out, circular_queue<T>* CircularQueue)
{
    if (IsEmpty(CircularQueue))
    {
        return (false);
    }

    u32 DequeueIndex = CircularQueue->DequeueIndex;
    CircularQueue->DequeueIndex =
        circular_queue_internal::IncrementCircularIndex(CircularQueue, DequeueIndex);

    engine_memcpy(Out, CircularQueue->Buffer + DequeueIndex, sizeof(T));

    --CircularQueue->EnqueuedCount;

    return (true);
}

/**
 * Peaks the front of the queue
 *
 * @template T The storage type of the queue
 * @param Out The front of the queue
 * @param CircularQueue The queue to look into
 * @return Whether the operation was successful
 */
template<typename T>
b32
Peek(T* Out, circular_queue<T>* CircularQueue)
{
    if (IsEmpty(CircularQueue))
    {
        return (false);
    }

    engine_memcpy(Out, CircularQueue->Buffer + CircularQueue->DequeueIndex, sizeof(T));

    return (true);
}

/**
 * Checks if a queue is empty
 *
 * @template T The storage type of the queue
 * @param CircularQueue The queue to check
 * @return Whether the queue is empty
 */
template<typename T>
inline b32
IsEmpty(circular_queue<T>* CircularQueue)
{
    return (CircularQueue->Size() == 0);
}

/**
 * Checks if a queue is full
 *
 * @template T The storage type of the queue
 * @param CircularQueue The queue to check
 * @return Whether the queue is full
 */
template<typename T>
inline b32
IsFull(circular_queue<T>* CircularQueue)
{
    return (CircularQueue->Size() == (CircularQueue->AllocatedLength - 1));
}

#define CIRCULAR_QUEUE_H
#endif
