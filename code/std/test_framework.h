#if !defined(TEST_FRAMEWORK_H)

#include "array.h"
#include "base.h"
#include "memory.h"
#include "string.h"

/**
 * Test function return
 */
struct test_result
{
    /**
     * Whether the test function returned with an error
     */
    b32 Success;

    /**
     * The error string to print out if there was an error
     */
    string Error;
};

/**
 * Test function type decleration for test functions
 */
#define TEST_FUNCTION(name) test_result name(mutex WriteLock, allocator* Allocator)
typedef TEST_FUNCTION(test_function);

/**
 * Define to use to return from test function when there is no error
 */
#define TEST_RETURN_SUCCESS()                                                            \
    UNUSED_VARIABLE(WriteLock);                                                          \
    UNUSED_VARIABLE(Allocator);                                                          \
    test_result R;                                                                       \
    R.Success = true;                                                                    \
    return (R)

/**
 * The define to use when there is an error in a test function
 */
#define TEST_RETURN_FAILURE(ErrorMsg, ...)                                               \
    test_result R;                                                                       \
    R.Success = false;                                                                   \
    FormatString(&R.Error, ErrorMsg, Allocator, ##__VA_ARGS__);                          \
    return (R)

/**
 * Since test functions are ran in parrallel this is the define to use to print things out
 * to stdout instead of StandardPrint
 */
#define TestPrint(string)                                                                \
    LockMutex(WriteLock);                                                                \
    StandardPrint(CreateString(string));                                                 \
    UnlockMutex(WriteLock)

/**
 * Contains information on a single test
 */
struct test
{
    /**
     * The name of the test for printing out information
     */
    string Name;

    /**
     * The test function
     */
    test_function* Function;
};

struct test_framework;

/**
 * Gets passed to a thread to run a collection of tests
 */
struct test_runner
{
    /**
     * The name of the collection of tests
     */
    string System;

    /**
     * The thread handle of the thread that this runner is on
     */
    thread_handle ThreadHandle;

    /**
     * The test framework used to deploy this runner
     */
    test_framework* Parent;

    /**
     * The tests to run on this runner
     */
    array<test> Tests;

    /**
     * The number of tests that failed
     */
    u32 FailedCount;

    /**
     * The allocator to pass to each test
     */
    allocator* Allocator;
};

/**
 * Used to handle the running of tests
 */
struct test_framework
{
    /**
     * Since printing to stdout is not thread safe this makes sure that only one thread is
     * printing to stdout at one time
     */
    mutex Lock;

    /**
     * The total number of test collections being ran
     */
    u32 TestPointer;

    /**
     * The total number of failed tests
     */
    u32 TotalFailedCount;

    /**
     * All of the created test runners
     */
    array<test_runner> Runners;

    /**
     * The allocator to use
     */
    allocator* Allocator;
};

/**
 * Creates a new test framework ready to have tests added to it
 *
 * @return The newly created test framework
 */
test_framework* CreateTestFrameWork(allocator* Allocator);

/**
 * Waits for all added tests to be started and finished
 */
void WaitAndDestroyTestFramework(test_framework* TestFramework);

/**
 * Adds a collection of tests to the test framework, by default the heap allocator
 * is used
 *
 * @param TestFramework The test framework to add this collection of tests to
 * @param System The name of the collection of tests
 * @param Tests The collection of tests
 * @param Allocator The allocator to use in the tests
 */
void AddTestCollection(test_framework* TestFramework, char* System, array<test>* Tests,
                       allocator* Allocator);

/**
 * Adds a test to an array of tests
 *
 * @param Tests the array of tests to add to
 * @param Function The test function to add
 * @param Name The name of the test
 */
inline void
AddTestToFrameWork(array<test>* Tests, test_function* Function, char* Name)
{
    test* Test     = Add(Tests);
    Test->Function = Function;
    Test->Name     = CreateString(Name);
}

/**
 * Define to use when adding tests, this assumes the array of tests is called "Tests"
 */
#define AddTest(Function, Name) AddTestToFrameWork(Tests, Function, Name)

#define TEST_FRAMEWORK_H
#endif
