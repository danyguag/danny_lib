#if !defined(ATOMIC_H)

#include "types.h"

/**
 * Atomically adds two integers, note this can be used as an atomic fetch if the Addend is
 * 0
 *
 * @param Value The atomic integer to read from
 * @param Addend The integer to add to the atomic integer
 * @return The added value of the two integers
 */
s64 AtomicAdd(volatile s64* Value, s64 Addend);

/**
 * Fetches the integer atomically
 *
 * @param Value The atomic integer to fetch
 * @return The value of the integer
 */
#define AtomicFetch(Value) AtomicAdd(Value, 0)

#define ATOMIC_H
#endif
