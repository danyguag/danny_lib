#if !defined(POOL_H)

#include "array.h"
#include "circular_queue.h"

#define DEFAULT_MAX_ENTRY_COUNT 256

template<typename T>
struct pool
{
    array<T>            Entries;
    circular_queue<u64> RemovedIndices;
};

template<typename T>
b32 CreatePool(pool<T>* Pool, allocator* Allocator,
               u64 MaxEntryCount = (u64)DEFAULT_MAX_ENTRY_COUNT);

/**
 * Creates a pool of storage type T, by default the heap memory callback is used
 *
 * @template T The storage type of the pool
 * @param Pool The pool to initialize
 * @param MaxEntryCount The max number of entries that can be stored in the pool
 * @param Allocator The allocator to allocate to use to allocate memory
 * @return Returns the success of the operation
 */
template<typename T>
b32
CreatePool(pool<T>* Pool, u64 MaxEntryCount, allocator* Allocator)
{
    CreateArray(&Pool->Entries, Allocator, MaxEntryCount);
    CreateCircularQueue(&Pool->RemovedIndices, Allocator, MaxEntryCount);

    return (Pool->Entries.AllocatedLength == 0 && Pool->RemovedIndices);
}

/**
 * Destroys the given pool with the memory callback used at creation
 *
 * @template T The storage type of the pool
 * @param Pool The pool to destroy
 */
template<typename T>
void
DestroyPool(pool<T>* Pool)
{
    FreeArray(&Pool->Entries);
    DestroyQueue(&Pool->RemovedIndices);
}

/**
 * Gets an entry from the given pool
 *
 * @template T The storage type of the pool
 * @param Pool The pool to get the entry from
 * @return A pointer to an element from the pool
 */
template<typename T>
T*
GetNextEntry(pool<T>* Pool)
{
    T* Entry = NULL;

    if (Pool->RemovedIndices.Size() == 0)
    {
        Entry = Add(&Pool->Entries);
    }
    else
    {
        u64 FreeIndex;

        Dequeue(&FreeIndex, &Pool->RemovedIndices);
        Entry = Pool->Entries + FreeIndex;
    }

    return (Entry);
}

/**
 * Frees the element at the given index allowing it to be reused
 *
 * @template T The storage type of the pool
 * @param Pool The pool to remove the entry from
 * @param Index The index of the entry to remove
 */
template<typename T>
void
RemoveEntry(pool<T>* Pool, T* Entry)
{
    engine_memset(Entry, 0, sizeof(T));
    Enqueue(&Pool->RemovedIndices, (u64)(Entry - Pool->Entries.Buffer));
}

#define POOL_H
#endif
