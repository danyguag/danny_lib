#if !defined(STD_TIMER_H)

#include "types.h"

/**
 * Used for waiting periods of time, end time is broken up because that is how the time is
 * read from system calls
 *
 * These numbers should not be set by the user but by the function StartTimer
 */
struct timer
{
    /**
     * The time the timer was started in seconds plus the amount of time in seconds till
     * the end of the timer
     */
    s64 EndTimeSeconds = -1;

    /**
     * The number of nano seconds till the end of the timer, the remainder of time after
     * the EndTimeSeconds
     */
    s64 EndTimeNano = -1;
};

/**
 * Miliseconds to nanoseconds
 */
#define Miliseconds(Miliseconds) ((s64)(Miliseconds)*1000000)

/**
 * Seconds to nanoseconds
 */
#define Seconds(Seconds) ((s64)Miliseconds(Seconds * 1000))

/**
 * Minutes to nanoseconds
 */
#define Minutes(Minutes) ((s64)Seconds(Minutes * 60))

/**
 * Hours to nanoseconds
 */
#define Hours(Hours) ((s64)Minutes(Hours * 60))

/**
 * Days to nanoseconds
 */
#define Days(Days) ((s64)Hours(Days * 24))

/**
 * Gets the systems current time
 *
 * @return The systems current time in nanoseconds
 */
s64 GetTime();

/**
 * Starts a timer ending in RunTime nanoseconds
 */
void StartTimer(timer* Timer, s64 RunTime);

/**
 * Checks if a timer is complete
 *
 * @param Timer The timer to initialize
 * @return Whether the timer is complete
 */
b32 IsTimerComplete(timer* Timer);

/**
 * Sets the destination timer's end time to the sources end time plus the offset time, by
 * default the offset time is 0
 *
 * @param Destination The timer to initialize
 * @param Source The timer to use to initialize the destination timer
 * @param Offset The time to add to the sources time
 */
void SyncTimer(timer* Destination, timer* Source, u64 Offset = 0);

#define STD_TIMER_H
#endif
