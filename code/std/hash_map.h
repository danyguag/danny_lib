#if !defined(HASHMAP_H)

#include "array.h"
#include "memory.h"
#include "string.h"
#include "types.h"

template<typename T>
struct hash_map_pair
{
    string Key;
    T      Value;
};

template<typename T>
struct hash_map
{
    hash_map()
    {
        Buffer    = NULL;
        Size      = 0;
        Capacity  = 0;
        Allocator = NULL;
    }

    allocator* Allocator;
    array<hash_map_pair<T>>* Buffer;
    u64                      Size;
    u64                      Capacity;

    inline operator b32()
    {
        return (Capacity > 0);
    }
};

inline u64
HashFunction(string String)
{
    u64 HashValue = 0;
    u64 High;

    for (u32 Index = 0; Index < String.Length; ++Index)
    {
        HashValue = (HashValue << 4) + String.Buffer[Index];

        if ((High = (HashValue & 0xF0000000)))
        {
            HashValue = HashValue ^ (High >> 24);
        }

        HashValue = HashValue & 0x7FFFFFFF;
    }

    return (HashValue);
}

inline u64
GetIndexByKey(u64 Length, string& Key)
{
    return (HashFunction(Key) % Length);
}

template<typename T>
void CreateHashmap(hash_map<T>* Hashmap, allocator* Allocator, u64 Capacity = 512);

template<typename T>
void
CreateHashmap(hash_map<T>* Hashmap, allocator* Allocator, u64 Capacity)
{
    Hashmap->Buffer = (array<hash_map_pair<T>>*)Allocator->Allocate(
        sizeof(array<hash_map_pair<T>>) * Capacity);
    Hashmap->Capacity  = Capacity;
    Hashmap->Size      = 0;
    Hashmap->Allocator = Allocator;;
}
#include "base.h"

template<typename T>
void
DestroyHashmap(hash_map<T>* Hashmap)
{
    for (u64 Index = 0; Index < Hashmap->Capacity; ++Index)
    {
        array<hash_map_pair<T>>* Current = Hashmap->Buffer + Index;

        if (Current->AllocatedLength > 0)
        {
            u32 Count = Current->Size;

            for (u32 Index = 0; Index < Count; ++Index)
            {
                hash_map_pair<T>* Pair = GetElement(Current, Index);

                FreeString(&Pair->Key, Hashmap->Allocator);
            }

            FreeArray(Current);
        }
    }

    Hashmap->Allocator->Free(Hashmap->Buffer, sizeof(array<hash_map_pair<T>>) * Hashmap->Capacity);

    Hashmap->Buffer    = NULL;
    Hashmap->Capacity  = 0;
    Hashmap->Size      = 0;
    Hashmap->Allocator = NULL;
}

template<typename T>
T*
Add(hash_map<T>* Hashmap, string Key)
{
    u64                      Index   = GetIndexByKey(Hashmap->Capacity, Key);
    array<hash_map_pair<T>>* Entries = Hashmap->Buffer + Index;

    if (Entries->AllocatedLength == 0)
    {
        CreateArray(Entries, Hashmap->Allocator, 16);
    }

    hash_map_pair<T>* Pair = Add(Entries);

    CopyString(&Pair->Key, Key, Hashmap->Allocator);
    ++Hashmap->Size;

    return (&Pair->Value);
}

template<typename T>
T*
Get(hash_map<T>* Hashmap, string Key)
{
    if (!Hashmap || !Key)
    {
        return (NULL);
    }

    u64                      Index   = GetIndexByKey(Hashmap->Capacity, Key);
    array<hash_map_pair<T>>* Entries = Hashmap->Buffer + Index;

    if (Entries->AllocatedLength == 0)
    {
        return (NULL);
    }

    u32 EntryCount = Entries->Size;

    for (u32 EntryIndex = 0; EntryIndex < EntryCount; ++EntryIndex)
    {
        hash_map_pair<T>* Pair = GetElement(Entries, EntryIndex);

        if (StringsEqual(Pair->Key, Key))
        {
            return (&Pair->Value);
        }
    }

    return (NULL);
}

template<typename T>
b32
Remove(hash_map<T>* Hashmap, string Key)
{
    if (!Hashmap || !Key)
    {
        return (false);
    }

    u64                      Index   = GetIndexByKey(Hashmap->Capacity, Key);
    array<hash_map_pair<T>>* Entries = Hashmap->Buffer + Index;

    if (Entries->AllocatedLength == 0)
    {
        return (false);
    }

    u32 EntryCount = Entries->Size;

    for (u32 EntryIndex = 0; EntryIndex < EntryCount; ++EntryIndex)
    {
        hash_map_pair<T>* Pair = GetElement(Entries, EntryIndex);

        if (StringsEqual(Pair->Key, Key))
        {
            FreeString(&Pair->Key, Hashmap->Allocator);

            Remove(Entries, EntryIndex);
            --Hashmap->Size;

            return (true);
        }
    }

    return (false);
}

#define HASHMAP_H
#endif
