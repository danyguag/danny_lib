#if !defined(STREAM_H)

#include "memory.h"
#include "types.h"

#define DEFAULT_STREAM_ALLOCATION_SIZE Kilobytes(2)

/**
 * Easy way of writing simple types into memory
 */
struct stream
{
    /**
     * The memory used to write to
     */
    char* Buffer;

    /**
     * Length of the buffer
     */
    u64 Length;

    /**
     * The index to write to the buffer
     */
    u64 WritePointer;

    /**
     * The index to read from the buffer
     */
    u64 ReadPointer;

    /**
     * The allocator to resize the buffer with, if needed
     */
    allocator* Allocator;
};

/**
 * Initializes a stream, by default the buffer allocated will be
 * DEFAULT_STREAM_ALLOCATION_SIZE bytes long and the default memory callback used will be
 * the heap memory callback
 *
 * @param Stream The stream to initialize
 * @param Allocatoe The allocator to use to allocate memory
 * @param Length The length of the buffer to allocate
 */
void CreateStream(stream* Stream, allocator* Allocator, u64 Length = DEFAULT_STREAM_ALLOCATION_SIZE);

/**
 * Frees the buffer and reset the read and write pointers
 *
 * @param Stream The stream to deinitialize
 */
void DestroyStream(stream* Stream);

/**
 * Sets the buffer and buffer length
 *
 * @param Stream The stream to modify
 * @param Buffer The buffer to use in the stream
 * @param Length The length of the buffer
 */
void SetBuffer(stream* Stream, char* Buffer, u64 Length);

/**
 * Doubles the length of the buffer in the stream
 *
 * @param Stream The stream to resize
 */
void ResizeStream(stream* Stream);

/**
 * Zeros the buffer, and resets the read and write pointer back to zero
 */
void ResetStream(stream* Stream);

/**
 * Checks if the passed in length will fit into the stream
 *
 * @param Stream The stream to check
 * @param Length The length to see if it would fit in the stream
 * @return Whether the length will fit in the stream
 */
b32 CanFit(stream* Stream, u64 Length);

/**
 * Writes the memory into the stream
 *
 * @param Stream The stream to write to
 * @param Memory The memory to write to the stream
 * @param Length The number of bytes of the memory to write into the stream
 * @return Whether the memory was written to the stream
 */
b32 Write(stream* Stream, void* Memory, u64 Length);

/**
 * Reads the number of bytes
 *
 * @param Stream The stream to read from
 * @param Length The number of bytes to increment the read pointer by
 * @return A pointer to the stream's buffer at the read pointer index before it is
 * incremented
 */
void* Read(stream* Stream, u64 Length);

/**
 * Returns a pointer to the stream's buffer at the read pointer index similar to the read
 * call but the read pointer is not incremented
 *
 * @param Stream The stream to read from
 * @return A pointer to the stream's current read address
 */
inline char*
GetCurrentReadLocation(stream* Stream)
{
    return (Stream->Buffer + Stream->ReadPointer);
}

/**
 * Writes the type T to the stream
 *
 * @param Stream The stream write to
 * @param Value The value to write to the stream
 * @return Whether the value was written to the stream
 */
template<typename T>
b32
Write(stream* Stream, T Value)
{
    u64 Length = sizeof(T);

    while (!CanFit(Stream, Length))
    {
        ResizeStream(Stream);
    }

    engine_memcpy(Stream->Buffer + Stream->WritePointer, &Value, sizeof(T));
    Stream->WritePointer += Length;

    return (true);
}

/**
 * Reads type T from the stream
 *
 * @param Stream The stream to read from
 * @return The value returned from reading T from the stream
 */
template<typename T>
T
Read(stream* Stream)
{
    u64 Length = sizeof(T);

    T* Result = (T*)(Stream->Buffer + Stream->ReadPointer);

    Stream->ReadPointer += Length;

    return (*Result);
}

#define STREAM_H
#endif
