#if !defined(SOCKET_H)

#include "base.h"
#include "memory.h"
#include "memory_allocator.h"
#include "stream.h"

#include "packet_handler.h"

/**
 * Simple tcp socket wrapper
 */
struct tcp_client
{
    /**
     * Socket
     */
    system_handle s;

    /**
     * The packet handler to catch packets with
     */
    packet_handler PacketHandler;

    /**
     * Tells whether the client is connected and is writable
     */
    b32 IsAlive;

    /**
     * Whether or not keepalive is enabled
     */
    b32 KeepAlive;

    /**
     * The keepalive packet if keepalive is enabled
     */
    stream KeepAlivePacket;
};

/**
 * Creates a tcp client, by default the heap memory callback is used
 *
 * @param Client The client to initialize
 * @param Allocator The allocator to allocate memory with
 */
void CreateTcpClient(tcp_client* Client, allocator* Allocator);

/**
 * Frees the given tcp client
 *
 * @param Client The client to destroy
 */
void DestroyTcpClient(tcp_client* Client);

/**
 * Read, parse, and process all incoming packets
 *
 * @param Client The client to read from
 */
void ProcessAllIncoming(tcp_client* Client);

/**
 * Enables keep alive on the given client and creates the keep alive packet, by default
 * the heap memory callback is used
 *
 * @param Client The client to initialize keep alive on
 * @param PacketId The packetId to send keep alive packets with
 * @param Allocator The allocator to allocate memory with
 */
void
SetClientKeepAlive(tcp_client* Client, u32 PacketId, allocator* Allocator);

/**
 * The packet callback to handle keep alive messages from the server, only exposed for
 * testing purposes
 *
 * @param Socket The socket that recieved this data
 * @param ReadStream The packet data
 * @param UserData The user data passed in when this callback was added to the packet
 * handler
 */
void ClientKeepAlivePacketCallback(system_handle Socket, stream* ReadStream,
                                   void* UserData);

/**
 * Wrapper for the SetNonblocking call in base
 *
 * @param Client The client to enable non blocking on
 * @return Whether the operation was successful
 */
inline b32
SetSocketNonBlocking(tcp_client* Client)
{
    return (SetNonblocking(Client->s));
}

/**
 * Wrapper for the connect call in base
 *
 * @param Client The client to use to connect to given address
 * @param Address The ip address of the tcp server
 * @param Port The port of the tcp server
 * @return Whether the client connected to the tcp server
 */
inline b32
Connect(tcp_client* Client, string Address, u32 Port)
{
    Client->IsAlive = ConnectSocket(Client->s, Address.Buffer, Port);

    return (Client->IsAlive);
}

/**
 * Wrapper for SendData in base, sends the given data in the stream over the socket
 *
 * @param Client The client to use to send
 * @param WriteStream The stream with the write data
 * @return Whether all of the data was sent
 */
inline b32
Send(tcp_client* Client, stream& WriteStream)
{
    return (SendData(Client->s, WriteStream.Buffer, WriteStream.WritePointer));
}

/**
 * Wrapper for AddCallback in packet_handler
 *
 * @param Client The client to add the new packet to
 * @param PacketId The packet id to watch for this packet for
 * @param Function The packet handler function to call for the given packet id
 * @param UserData The user data to pass to the function
 */
inline void
AddPacketCallback(tcp_client* Client, u32 PacketId, packet_handler_function* Function,
                  void* UserData)
{
    AddCallback(&Client->PacketHandler, PacketId, Function, UserData);
}

#define SOCKET_H
#endif
