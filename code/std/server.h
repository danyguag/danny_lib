#if !defined(SERVER_H)

#include "array.h"
#include "base.h"
#include "packet_handler.h"
#include "stream.h"
#include "timer.h"

/**
 * Clients that connect to the server
 */
struct connected_tcp_client
{
    system_handle s;
    b32           IsAlive;
    void*         UserData;
    b32           Authenticated;
    u32           MissedAckCount;
};

/**
 * Wrapper over sockets for tcp server
 */
struct tcp_server
{
    /**
     * Server socket
     */
    system_handle s;

    /**
     * Packet handler that will handle all of the packets from clients
     */
    packet_handler PacketHandler;

    /**
     * All clients that are either connected or just disconnected and are waitting to be
     * removed
     */
    array<connected_tcp_client> Clients;

    /**
     * Whether keep alive is enabled for clients
     */
    b32 KeepAlive;

    /**
     * The timer tracking when clients should be sent a new keepalive packet
     */
    timer KeepAliveTimer;

    /**
     * How often keep alive packets are sent out
     */
    u64 CheckTime;

    /**
     * How many times a client has not responded to a keep alive packet before
     * disconnecting clients
     */
    u32 RetryCount;

    /**
     * The stream containing the keep alive packet to send to clients
     */
    stream KeepAlivePacket;
};

/**
 * Creates a tcp server on the specified port, allows ListBacklog number of clients to be
 * waiting in the queue for connection before they are dropped, by default the heap memory
 * callback is used
 *
 * @param Server The server to initialize
 * @param Port The port to start the tcp server on
 * @param ListenBacklog The max number of clients trying to connect to the socket before
 * disconnecting
 * @param Allocator The allocator to use to allocate memory
 * @return Whether the server was created
 */
b32 CreateTcpServer(tcp_server* Server, u32 Port, int ListenBacklog,
                    allocator* Allocator);

/**
 * The server to destroy, this will free all memory and destroy all sockets related to the
 * server socket
 *
 * @param Server The server to deinitialize
 */
void DestroyTcpServer(tcp_server* Server);

/**
 * Enables keep alve on the given packet id for the given time and with the given retry
 * count, by default the heap allocator is used
 *
 * @param Server The server to enable keep alive on
 * @param PacketId The packet id to use to send the keep alive packets
 * @param CheckTime The interval to send keep alive packets
 * @param RetryCount The number of unresponded keep alive packets to ignore before
 * declaring a packet dead
 * @param Allocator The allocator to use when allocating memory
 */
void SetServerKeepAlive(tcp_server* Server, u32 PacketId, u64 CheckTime,
                        u32 RetryCount, allocator* Allocator);

/**
 * Accepts a new client
 *
 * @param Server The server to accept the new client on
 * @return A pointer the newly connected client or NULL if there was no client to accept
 */
connected_tcp_client* AcceptTcpConnection(tcp_server* Server);

/**
 * Reads all packets waiting to be read and then processes them all
 *
 * @param Server The server to read all client packets
 */
void ProcessAllIncoming(tcp_server* Server);

/**
 * Adds a packet callback for client packets to be handled
 *
 * @param Server The server to add the packet callback to
 * @param PacketId The packet id of the packet callback
 * @param Function The packet callback function to invoke
 * @param UserData The user data to pass to the callback when invoked
 */
void AddPacketCallback(tcp_server* Server, u32 PacketId,
                       packet_handler_function* Function, void* UserData);

/**
 * Gets a client by its socket, mainly used in packet callback functions
 *
 * @param Server The server to get the connected client from
 * @param Socket The socket of the client to fetch
 * @return A pointer to the connected client struct or NULL if no client was found
 */
connected_tcp_client* GetClientBySocket(tcp_server* Server, system_handle Socket);

/**
 * Removes a client by its socket
 *
 * @param Server The server to remove the client from
 * @param Socket The socket of the client to remove
 */
void RemoveClientBySocket(tcp_server* Server, system_handle Socket);

/**
 * The server keep alive packet callback function, only exposed for testing
 */
void ServerKeepAlivePacketCallback(system_handle Socket, stream* ReadStream,
                                   void* UserData);

/**
 * Clears all clients that are dead, calls a callback function before removing them
 *
 * @template T The type of the user data for each connected client
 * @template K The callback template
 * @param Server The server to clear all dead client from
 * @param PerClientCallback The callback to call on each dead client
 */
template<typename T, typename K>
void
ClearDeadClients(tcp_server* Server, K&& PerClientCallback)
{
    array<connected_tcp_client>* Clients     = &Server->Clients;
    u32                          ClientCount = Clients->Size;

    for (u32 ClientIndex = 0; ClientIndex < ClientCount; ++ClientIndex)
    {
        connected_tcp_client* Client = GetElement(Clients, ClientIndex);

        if (Client->IsAlive)
        {
            continue;
        }

        PerClientCallback((T*)Client->UserData);
        CloseSystemHandle(&Client->s);
        Remove(Clients, ClientIndex);

        if (ClientCount > 1)
        {
            --ClientIndex;
            --ClientCount;
        }
    }
}

#define SERVER_H
#endif
