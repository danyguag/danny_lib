#if !defined(STRING_H)

#include <stdarg.h>

#include "memory.h"
#include "types.h"

/**
 * String representation, contains a buffer with the string data, the length of the string
 * and the capacity of the buffer
 */
struct string
{
    /**
     * The string data
     */
    char* Buffer;

    /**
     * The length of the string
     */
    u64 Length;

    /**
     * The capacity of the buffer
     */
    u64 Capacity;

    /**
     * Compares this string to another string
     *
     * @param String The other string to compare this one to
     * @return Whether this string is equal to the other string
     */
    b32 operator==(string& String);

    /**
     * Compares this string to a char pointer
     *
     * @param String The char pointer to compare to this string
     * @return Whether this string is equal to the char pointer
     */
    b32 operator==(const char* String);

    /**
     * Compares this string to another string
     *
     * @param String The other string to compare this one to
     * @return Whether this string is not equal to the other string
     */
    b32 operator!=(string& String);

    /**
     * Compares this string to a char pointer
     *
     * @param String The char pointer to compare to this string
     * @return Whether this string is not equal to the char pointer
     */
    b32 operator!=(const char* String);

    /**
     * Gets the string starting at the index
     *
     * @param Index THe offset to start the new string from this one
     * @return A new string with Length - Index characters in it
     */
    inline string
    operator[](u32 Index)
    {
        string String;
        String.Buffer   = Buffer + Index;
        String.Length   = Length - Index;
        String.Capacity = String.Length + 1;

        return (String);
    }

    /**
     * Increments the buffer by one and decrements the length
     *
     * @param Index The number of chars to increment the buffer by
     */
    inline void
    operator+=(u64 Index)
    {
        Buffer = Buffer + Index;
        Length -= Index;
    }

    /**
     * Checks whether the capacity is over 0
     *
     * @return Whether the string is alive or dead
     */
    inline operator b32()
    {
        return (Capacity > 0);
    }

    /**
     * Returns the buffer as a const char*
     *
     * @return The string data
     */
    inline operator const char*()
    {
        return ((const char*)Buffer);
    }
};

/**
 * This is the return type for SplitString
 */
struct split_string
{
    /**
     * A list of strings
     */
    string* Buffer;

    /**
     * The number of strings in the buffer
     */
    u32 Count;
};

/**
 * Initializes all the fields to their defaults, so the buffer will be set to NULL and the
 * length and capacity set to 0
 *
 * @return The empty string
 */
string CreateEmptyString();

/**
 * Initializes the string to the const char pointer passed in, the length and capacity
 * will be set to the length of const char pointer
 *
 * @param Buffer The const char pointer to initialize the string with
 * @return The initialized string
 */
string CreateString(const char* Buffer);

/**
 * Initializes the string to the given buffer and the given length, the capacity will also
 * be set to the length
 *
 * @param Buffer The buffer to use in the string
 * @param BufferLength The length and capacity of the string
 * @return The initialized string
 */
string CreateString(char* Buffer, u64 BufferLength);

/**
 * Gets the length of the c string
 *
 * @param String The c string to get the length of
 * @return The length of the c string
 */
u32 StringLength(char* String);

/**
 * Compares two strings
 *
 * @param String1 One of the strings to compare
 * @param String2 The other string to compare
 * @return Whether the two strings are equal
 */
b32 StringsEqual(string& String1, string String2);

/**
 * Checks whether the string ends with the test string
 *
 * @param String The string to check
 * @param Test The string to check at the end for
 * @return Whether String ends with Test
 */
b32 EndsWith(string& String, string Test);

/**
 * Checks whether the string starts with the test string
 *
 * @param String The string to check
 * @param Test The string to check at the start for
 * @return Whether String starts with Test
 */
b32 StartsWith(string& String, string Test);

/**
 * Converts an unsigned integer to a string, by default the heap memory callback is used
 *
 * @param Out The number as a string
 * @param Value The number to convert to a string
 * @param MemoryCallback The memory callback to use when creating the output string
 */
void UnsignedIntegerToString(string* Out, u64 Value, allocator* Allocator);

/**
 * Converts an signed integer to a string, by default the heap memory callback is used
 *
 * @param Out The number as a string
 * @param Value The number to convert to a string
 * @param MemoryCallback The memory callback to use when creating the output string
 */
void SignedIntegerToString(string* Out, s64 Value, allocator* Allocator);

/**
 * Converts an floating point number to a string, by default the heap memory callback is
 * used
 *
 * @param Out The number as a string
 * @param Value The number to convert to a string
 * @param MemoryCallback The memory callback to use when creating the output string
 */
void FloatToString(string* Out, f64 Value, allocator* Allocator);

/**
 * Converts a string to a unsigned integer
 *
 * @param Out The number as a unsigned integer
 * @param String The string number to convert to a binary representation
 * @return Whether the number was converted without error
 */
b32 StringToUnsignedInteger(u64* Out, string& String);

/**
 * Converts a string to a signed integer
 *
 * @param Out The number as a signed integer
 * @param String The string number to convert to a binary representation
 * @return Whether the number was converted without error
 */
b32 StringToSignedInteger(s64* Out, string& String);

/**
 * Converts a string to a floating pointer number
 *
 * @param Out The number as a float
 * @param String The string number to convert to a binary representation
 * @return Whether the number was converted without error
 */
b32 StringToFloat(f64* Out, string& String);

/**
 * Checks whether the given string contains a character
 *
 * @param String The string to check
 * @param Character The character to look for
 * @return Whether the string contains the character
 */
b32 Contains(string& String, char Character);

/**
 * Checks whether the given string contains the other string
 *
 * @param String The string to check
 * @param Part The string to check for
 * @return Whether the string contains the other string
 */
b32 Contains(string& String, string& Part);

/**
 * Gets the index of the character in the given string
 *
 * @param String The string to check
 * @param Character The character to look for
 * @return The index of the character or -1 if it was not found
 */
s64 IndexOf(string& String, char Character);

/**
 * Gets the index of the string in the given string
 *
 * @param String The string to check
 * @param Part The string to look for
 * @return The index of the string or -1 if it was not found
 */
s64 IndexOf(string& String, string Part);

/**
 * Formats the string using the given args and the passed in memory callback, if you want
 * to use the heap memory callback then use the second FormatStringToBuffer call that does
 * not take a memory callback, for how to format the strings please see FormatString's
 * documentation, this function assumes that Result is already allocated
 *
 * @param Result The formatted string
 * @param FormatString The string to parse and add values to
 * @param MemoryCallback The memory callback to use when allocating memory
 */
void FormatStringToBuffer(string* Result, const char* FormatString, allocator* Allocator,
                          ...);

/**
 * Allocates the result string and formats the string with the given args, format specifiers are listed below.
 *
 * Format Specifiers:
 *
 * "%str" struct string
 * "%u8"  u8
 * "%u16" u16
 * "%u32" u32
 * "%u64" u64
 * "%s8"  s8
 * "%s16" s16
 * "%s32" s32
 * "%s64" s64
 * "%f32" f32
 * "%f64" f64
 *
 * @param Result The formatted string
 * @param FormatString The string to parse and add values to
 * @param MemoryCallback The memory callback to use when allocating memory
 */
void FormatString(string* Result, const char* FormatString, allocator* Allocator, ...);

/**
 * Removes the whitespace from the given string and stores it in a new string, by default
 * the heap memory callback is used
 *
 * @param Result The result string with no whitespace
 * @param String The string to remove the whitespace from
 * @param MemoryCallback The memory callback to allocate the result string
 */
void RemoveWhitespace(string* Result, string& String, allocator* Allocator);

/**
 * Copies the source string into the destination string, by default the heap memory
 * callback is used
 *
 * @param Destination The copied string
 * @param Source The string to copy
 * @param MemoryCallback The memory callback to use when allocating the destination
 */
void CopyString(string* Destination, string& Source, allocator* Allocator);

/**
 * Frees the buffer of the given string and resets the string to default values, by
 * default the heap memory callback is used to free the string
 *
 * @param String The string to free
 * @param MemoryCallback The memory callback to free the string
 */
void FreeString(string* String, allocator* Allocator);

/**
 * Copies a section of the given string, by default the heap memory callback is used and
 * by default the end index will be the end of the string
 *
 * @param Out The copied section of the string
 * @param String The string to copy a section from
 * @param Start The start index of the string to copy from
 * @param End The end index to copy to, not inclusive, pass 0 for the end of the string
 * @param MemoryCallback The memory callback to allocate the out string
 */
b32 SubString(string* Out, string& String, u64 Start, u64 End, allocator* Allocator);

/**
 * Splits the given string by the character, by default the heap memory callback is used
 *
 * @param String The string to split
 * @param Character The character to split by
 * @param MemoryCallback The memory callback to use to allocate all the strings
 * @return The struct containing the split strings and how many there are
 */
split_string SplitString(string& String, char Character, allocator* Allocator);

/**
 * Returns if the character is a whitespace character
 *
 * @param Value The character to check
 * @return Whether the character is a whitespace
 */
inline b32
IsWhiteSpace(char Value)
{
    return (Value == ' ' || Value == '\n' || Value == '\r' || Value == '\t' ||
            Value == '\0');
}

/**
 * Returns if the character is alphabetical
 *
 * @param Value The character to check
 * @return Whether the character is alphabetical
 */
inline b32
IsAlpha(char Value)
{
    return ((Value >= 'a' && Value <= 'z') || (Value >= 'A' && Value <= 'Z'));
}

/**
 * Returns if the character is a digit
 *
 * @param Value The character to check
 * @return Whether the character is a digit
 */
inline b32
IsDigit(char Value)
{
    return (Value >= '0' && Value <= '9');
}

/**
 * Returns if the character is a symbol, right now it is anything thats not a whitespace,
 * alphabetical, or digit
 *
 * @param Value The character to check
 * @return Whether the character is a symbol
 */
inline b32
IsSymbol(char Value)
{
    return (!IsWhiteSpace(Value) && !IsAlpha(Value) && !IsDigit(Value));
}

/**
 * Gets the string at the index of the source string
 *
 * @param Destination The source string plus the index
 * @param Source The string to increment by the index
 * @param Index The offset to get the string for the destination
 */
inline void
StringAt(string* Destination, string& Source, u64 Index)
{
    Destination->Buffer   = Source.Buffer + Index;
    Destination->Length   = Source.Length - Index;
    Destination->Capacity = Source.Capacity - Index;
}

#define STRING_H
#endif
