#include "std/array.h"

b32
Add(array<string>* Array, string New)
{
    if (IsFull(Array))
    {
        ResizeArray(Array);
    }

    string* Result = Array->Buffer + Array->Size;

    if (Result == NULL)
    {
        return (false);
    }

    CopyString(Result, New, Array->Allocator);
    ++Array->Size;

    return (true);
}

b32
Add(array<string>* Destination, array<string>* Source, u64 SourceStartOffset)
{
    u32 SourceCopyLength = Source->Size - SourceStartOffset;

    // Resize the array if necessary
    if (IsFull(Destination) ||
        Destination->Size + SourceCopyLength >= Destination->AllocatedLength)
    {
        u64 NewAllocatedLength = Destination->AllocatedLength * 2;

        while (Destination->Size + SourceCopyLength <= NewAllocatedLength)
        {
            NewAllocatedLength *= 2;
        }

        string* Temp = (string*)Destination->Allocator->Allocate(NewAllocatedLength *
                                                                 sizeof(string));
        engine_memcpy(Temp, Destination->Buffer, Destination->Size * sizeof(string));
        Destination->Allocator->Free(Destination->Buffer,
                                     Destination->Size * sizeof(string));
        Destination->AllocatedLength *= 2;
    }

    for (u64 StringIndex = SourceStartOffset; StringIndex < SourceCopyLength;
         ++StringIndex)
    {
        string  SourceString   = *GetElement(Source, StringIndex);
        string* NewDestination = Destination->Buffer + Destination->Size;

        CopyString(NewDestination, SourceString, Destination->Allocator);

        ++Destination->Size;
    }

    return (true);
}

void
CopyArray(array<string>* Destination, array<string>* Source, allocator* Allocator)
{
    Destination->AllocatedLength = Source->AllocatedLength;
    Destination->Size            = Source->Size;
    Destination->Allocator       = Allocator;

    if (Allocator)
    {
        Destination->Buffer =
            (string*)Allocator->Allocate((sizeof(string) * Destination->AllocatedLength));

        u64 SourceCount = Source->Size;


        for (u64 Index = 0; Index < SourceCount; ++Index)
        {
            string* Current = GetElement(Destination, Index);
            CopyString(Current, *GetElement(Source, Index), Allocator);
        }
    }
}

b32
Remove(array<string>* Array, u64 Index)
{
    if (Index >= Array->Size)
    {
        return (false);
    }

    FreeString(Array->Buffer + Index, Array->Allocator);
    engine_memcpy(Array->Buffer + Index, Array->Buffer + (Index + 1),
                  sizeof(string) * (Array->Size - Index));
    --Array->Size;

    return (true);
}

b32
CompareArray(array<f32>& Out, array<f32>& In)
{
    if (Out.Size != In.Size)
    {
        return (false);
    }

    u64 Count = Out.Size;

    for (u64 Index = 0; Index < Count; ++Index)
    {
        if (!CompareFloat(Out[Index], In[Index], 0.00001f))
        {
            return (false);
        }
    }

    return (true);
}

b32
CompareArray(array<f64>& Out, array<f64>& In)
{
    if (Out.Size != In.Size)
    {
        return (false);
    }

    u64 Count = Out.Size;

    for (u64 Index = 0; Index < Count; ++Index)
    {
        if (!CompareFloat(Out[Index], In[Index], 0.000000001f))
        {
            return (false);
        }
    }

    return (true);
}
