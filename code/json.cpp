#include "std/json.h"

#include "std/base.h"

void ParseJsonObject(tokenizer* Tokenizer, json* Object, string& ObjectName,
                     allocator* Allocator);
void BufferToJsonArray(tokenizer* Tokenizer, json_array* Array, string& Name,
                       allocator* Allocator);
void ParseJsonObject(tokenizer* Tokenizer, json* Object, string& ObjectName,
                     allocator* Allocator);

b32
GetJsonObjectArray(array<json>* Out, json* Object, const char* Name)
{
    u32 ArrayCount = Object->Arrays.Size;

    for (u32 ArrayIndex = 0; ArrayIndex < ArrayCount; ++ArrayIndex)
    {
        json_array* Value = Object->Arrays + ArrayIndex;

        if (Value->Name == Name)
        {
            engine_memcpy((char*)Out, (char*)&Value->StringValues, sizeof(array<json>));
            return (true);
        }
    }

    return (false);
}

b32
GetJsonStringArray(array<string>* Out, json* Object, const char* Name)
{
    u32 ArrayCount = Object->Arrays.Size;

    for (u32 ArrayIndex = 0; ArrayIndex < ArrayCount; ++ArrayIndex)
    {
        json_array* Value = Object->Arrays + ArrayIndex;

        if (Value->Name == Name)
        {
            engine_memcpy(Out, &Value->StringValues, sizeof(array<string>));
            return (true);
        }
    }

    return (false);
}

b32
GetJsonBooleanArray(array<b32>* Out, json* Object, const char* Name)
{
    u32 ArrayCount = Object->Arrays.Size;

    for (u32 ArrayIndex = 0; ArrayIndex < ArrayCount; ++ArrayIndex)
    {
        json_array* Value = Object->Arrays + ArrayIndex;

        if (Value->Name == Name)
        {
            engine_memcpy(Out, &Value->BooleanValues, sizeof(array<b32>));
            return (true);
        }
    }

    return (false);
}

b32
GetJsonNumberArray(array<f64>* Out, json* Object, const char* Name)
{
    u32 ArrayCount = Object->Arrays.Size;

    for (u32 ArrayIndex = 0; ArrayIndex < ArrayCount; ++ArrayIndex)
    {
        json_array* Value = Object->Arrays + ArrayIndex;

        if (Value->Name == Name)
        {
            engine_memcpy(Out, &Value->NumberValues, sizeof(array<f64>));

            return (true);
        }
    }

    return (false);
}

b32
GetJsonObject(json** Out, json* Object, const char* Name)
{
    u32 ObjectCount = Object->Objects.Size;

    for (u32 ObjectIndex = 0; ObjectIndex < ObjectCount; ++ObjectIndex)
    {
        json* ChildObject = Object->Objects + ObjectIndex;

        if (ChildObject->Name == Name)
        {
            *Out = ChildObject;
            return (true);
        }
    }

    return (false);
}

b32
GetJsonNumber(f64* Out, json* Object, const char* Name)
{
    u32 NumberCount = Object->Numbers.Size;

    for (u32 NumberIndex = 0; NumberIndex < NumberCount; ++NumberIndex)
    {
        json_number* Value = Object->Numbers + NumberIndex;

        if (Value->Name == Name)
        {
            *Out = Value->Value;
            return (true);
        }
    }

    return (0);
}

b32
GetJsonBoolean(b32* Out, json* Object, const char* Name)
{
    u32 BooleanCount = Object->Booleans.Size;

    for (u32 BooleanIndex = 0; BooleanIndex < BooleanCount; ++BooleanIndex)
    {
        json_boolean* Value = Object->Booleans + BooleanIndex;

        if (Value->Name == Name)
        {
            *Out = Value->Value;
            return (true);
        }
    }

    return (false);
}

b32
GetJsonString(string* Out, json* Object, const char* Name)
{
    u32 StringCount = Object->Strings.Size;

    for (u32 StringIndex = 0; StringIndex < StringCount; ++StringIndex)
    {
        json_string* Value = Object->Strings + StringIndex;

        if (Value->Name == Name)
        {
            *Out = Value->Value;
            return (true);
        }
    }

    return (false);
}

void
AddJsonObject(json* Object, json& NewValue, allocator* Allocator)
{
    if (Object->Objects.AllocatedLength == 0)
    {
        CreateArray(&Object->Objects, Allocator);
    }

    Add(&Object->Objects, NewValue);
}

void
AddJsonNumber(json* Object, string& Name, f64 NewValue, allocator* Allocator)
{
    if (Object->Numbers.AllocatedLength == 0)
    {
        CreateArray(&Object->Numbers, Allocator);
    }

    json_number* Number = Add(&Object->Numbers);
    CopyString(&Number->Name, Name, Allocator);
    Number->Value = NewValue;
}

void
AddJsonBoolean(json* Object, string& Name, b32 NewValue, allocator* Allocator)
{
    if (Object->Booleans.AllocatedLength == 0)
    {
        CreateArray(&Object->Booleans, Allocator);
    }

    json_boolean* Boolean = Add(&Object->Booleans);
    CopyString(&Boolean->Name, Name, Allocator);
    Boolean->Value = NewValue;
}

void
AddJsonString(json* Object, string& Name, string NewValue,
              allocator* Allocator)
{
    if (Object->Strings.AllocatedLength == 0)
    {
        CreateArray(&Object->Strings, Allocator);
    }

    json_string* String = Add(&Object->Strings);
    CopyString(&String->Name, Name, Allocator);
    CopyString(&String->Value, NewValue, Allocator);
}

void
AddJsonObjectArray(json* Object, string& Name, array<json> Array,
                   allocator* Allocator)
{
    if (Object->Arrays.AllocatedLength == 0)
    {
        CreateArray(&Object->Arrays, Allocator);
    }

    json_array* JsonArray = Add(&Object->Arrays);
    JsonArray->Type       = JSON_ARRAY_OBJECT;

    CopyString(&JsonArray->Name, Name, Allocator);
    CopyArray(&JsonArray->Objects, &Array, Allocator);
}

void
AddJsonStringArray(json* Object, string& Name, array<string> Array,
                   allocator* Allocator)
{
    if (Object->Arrays.AllocatedLength == 0)
    {
        CreateArray(&Object->Arrays, Allocator);
    }

    json_array* JsonArray = Add(&Object->Arrays);
    JsonArray->Type       = JSON_ARRAY_STRING;

    CopyString(&JsonArray->Name, Name, Allocator);
    CopyArray(&JsonArray->StringValues, &Array, Allocator);
}

void
AddJsonBooleanArray(json* Object, string& Name, array<b32> Array,
                    allocator* Allocator)
{
    if (Object->Arrays.AllocatedLength == 0)
    {
        CreateArray(&Object->Arrays, Allocator);
    }

    json_array* JsonArray = Add(&Object->Arrays);
    JsonArray->Type       = JSON_ARRAY_BOOLEAN;

    CopyString(&JsonArray->Name, Name, Allocator);
    CopyArray(&JsonArray->BooleanValues, &Array, Allocator);
}

void
AddJsonNumberArray(json* Object, string& Name, array<f64> Array,
                   allocator* Allocator)
{
    if (Object->Arrays.AllocatedLength == 0)
    {
        CreateArray(&Object->Arrays, Allocator);
    }

    json_array* JsonArray = Add(&Object->Arrays);
    JsonArray->Type       = JSON_ARRAY_NUMBER;

    CopyString(&JsonArray->Name, Name, Allocator);
    CopyArray(&JsonArray->NumberValues, &Array, Allocator);
}

void
BufferToJsonArray(tokenizer* Tokenizer, json_array* Array, string& Name,
                  allocator* Allocator)
{
    token CurrentToken {};

    string ObjectNameString = CreateEmptyString();

    if (Name.Length > 0)
    {
        CopyString(&Array->Name, Name, Allocator);
    }

    while (true)
    {
        GetNextToken(Tokenizer, &CurrentToken, Allocator);
        switch (CurrentToken.Type)
        {
            case TOKEN_SYMBOL:
            {
                if (CurrentToken.Symbol == '{')
                {
                    if (Array->Objects.AllocatedLength == 0)
                    {
                        CreateArray<json>(&Array->Objects, Allocator);
                        Array->Type = JSON_ARRAY_OBJECT;
                    }

                    json* NewObject = Add(&Array->Objects);

                    ParseJsonObject(Tokenizer, NewObject, ObjectNameString,
                                    Allocator);
                }
                else if (CurrentToken.Symbol != ',')
                {
                    if (CurrentToken.Symbol == ']')
                    {
                        return;
                    }
                }
                else
                {
                    continue;
                }
            }
            break;
            case TOKEN_NUMBER:
            {
                if (Array->NumberValues.AllocatedLength == 0)
                {
                    CreateArray<f64>(&Array->NumberValues, Allocator);
                    Array->Type = JSON_ARRAY_NUMBER;
                }

                Add(&Array->NumberValues, CurrentToken.Number);
            }
            break;
            case TOKEN_BOOLEAN:
            {
                if (Array->BooleanValues.AllocatedLength == 0)
                {
                    CreateArray<b32>(&Array->BooleanValues, Allocator);
                    Array->Type = JSON_ARRAY_BOOLEAN;
                }

                Add(&Array->BooleanValues, CurrentToken.Boolean);
            }
            break;
            case TOKEN_STRING:
            {
                if (Array->StringValues.AllocatedLength == 0)
                {
                    CreateArray(&Array->StringValues, Allocator);
                    Array->Type = JSON_ARRAY_STRING;
                }

                Add(&Array->StringValues, CurrentToken.String);
            }
            break;
            default:
            {
            }
            break;
        }
    }
}

void
ParseJsonObject(tokenizer* Tokenizer, json* Object, string& ObjectName,
                allocator* Allocator)
{
    if (ObjectName.Length > 0)
    {
        CopyString(&Object->Name, ObjectName, Allocator);
    }

    token  CurrentToken {};
    json*  Current = Object;
    string Name    = CreateEmptyString();

    while (HasNextToken(*Tokenizer))
    {
        GetNextToken(Tokenizer, &CurrentToken, Allocator);

        switch (CurrentToken.Type)
        {
            case TOKEN_NONE:
            case TOKEN_TEXT:
            case TOKEN_FLAG:
            {
                return;
            }
            break;
            case TOKEN_SYMBOL:
            {
                char Symbol = CurrentToken.Symbol;
                if (Symbol == ',' || Symbol == ':')
                {
                    continue;
                }
                else if (Symbol == '{')
                {
                    if (Current->Objects.AllocatedLength == 0)
                    {
                        CreateArray<json>(&Current->Objects, Allocator);
                    }

                    json* NewObject = Add(&Current->Objects);
                    ParseJsonObject(Tokenizer, NewObject, Name, Allocator);
                    engine_memset(Name.Buffer, 0, Name.Length);
                    Name.Length = 0;
                    continue;
                }
                else if (Symbol == '[')
                {
                    if (Current->Arrays.AllocatedLength == 0)
                    {
                        CreateArray<json_array>(&Current->Arrays, Allocator);
                    }

                    json_array* Array = Add(&Current->Arrays);
                    BufferToJsonArray(Tokenizer, Array, Name, Allocator);
                    engine_memset(Name.Buffer, 0, Name.Length);
                    Name.Length = 0;
                    continue;
                }
                else if (Symbol == '}')
                {
                    FreeString(&Name, Allocator);

                    return;
                }
            }
            break;
            case TOKEN_NUMBER:
            {
                if (Current->Numbers.AllocatedLength == 0)
                {
                    CreateArray<json_number>(&Current->Numbers, Allocator);
                }

                json_number* Number = Add(&Current->Numbers);
                CopyString(&Number->Name, Name, Allocator);
                Number->Value = CurrentToken.Number;
                engine_memset(Name.Buffer, 0, Name.Length);
                Name.Length = 0;
            }
            break;
            case TOKEN_STRING:
            {
                if (Name.Length == 0)
                {
                    if (Name)
                    {
                        FreeString(&Name, Allocator);
                    }

                    CopyString(&Name, CurrentToken.String, Allocator);
                }
                else
                {
                    if (Current->Strings.AllocatedLength == 0)
                    {
                        CreateArray<json_string>(&Current->Strings, Allocator);
                    }

                    json_string* String = Add(&Current->Strings);
                    CopyString(&String->Name, Name, Allocator);
                    CopyString(&String->Value, CurrentToken.String, Allocator);
                    engine_memset(Name.Buffer, 0, Name.Length);
                    Name.Length = 0;
                }
            }
            break;
            case TOKEN_BOOLEAN:
            {
                if (Current->Booleans.AllocatedLength == 0)
                {
                    CreateArray<json_boolean>(&Current->Booleans, Allocator);
                }

                json_boolean* Boolean = Add(&Current->Booleans);

                Boolean->Value = CurrentToken.Boolean;
                CopyString(&Boolean->Name, Name, Allocator);
                engine_memset(Name.Buffer, 0, Name.Length);
                Name.Length = 0;
            }
            break;
        }
    }
}

void
BufferToJSON(json* Object, char* Buffer, u32 BufferLength,
             allocator* Allocator)
{
    tokenizer Tokenizer {};
    CreateTokenizer(&Tokenizer, Buffer, BufferLength);

    while (HasNextToken(Tokenizer))
    {
        token Token {};
        GetNextToken(&Tokenizer, &Token, Allocator);

        if (Token.Type == TOKEN_SYMBOL)
        {
            if (Token.Symbol == '{')
            {
                string Name = CreateEmptyString();

                ParseJsonObject(&Tokenizer, Object, Name, Allocator);
            }
        }
    }
}

void
CreateEmptyJson(json* Json, string Name, allocator* Allocator)
{
    engine_memset((char*)Json, 0, sizeof(json));

    if (Name)
    {
        CopyString(&Json->Name, Name, Allocator);
    }
    else
    {
        string RootString;
        RootString.Buffer   = (char*)"Root";
        RootString.Length   = 4;
        RootString.Capacity = 4;

        CopyString(&Json->Name, RootString, Allocator);
    }
}

void
DestroyJson(json* Json, allocator* Allocator)
{
    FreeString(&Json->Name, Allocator);

    for (u64 ObjectIndex = 0; ObjectIndex < Json->Objects.Size; ++ObjectIndex)
    {
        DestroyJson(Json->Objects + ObjectIndex, Allocator);
    }

    for (u64 NumberIndex = 0; NumberIndex < Json->Numbers.Size; ++NumberIndex)
    {
        FreeString(&(Json->Numbers + NumberIndex)->Name, Allocator);
    }

    for (u64 BooleanIndex = 0; BooleanIndex < Json->Booleans.Size; ++BooleanIndex)
    {
        FreeString(&(Json->Booleans + BooleanIndex)->Name, Allocator);
    }

    for (u64 StringIndex = 0; StringIndex < Json->Strings.Size; ++StringIndex)
    {
        json_string* String = Json->Strings + StringIndex;

        FreeString(&String->Name, Allocator);
        FreeString(&String->Value, Allocator);
    }

    for (u64 ArrayIndex = 0; ArrayIndex < Json->Arrays.Size; ++ArrayIndex)
    {
        json_array* Array = Json->Arrays + ArrayIndex;

        FreeString(&Array->Name, Allocator);

        if (Array->Type == JSON_ARRAY_NUMBER)
        {
            FreeArray(&Array->NumberValues);
        }
        else if (Array->Type == JSON_ARRAY_BOOLEAN)
        {
            FreeArray(&Array->BooleanValues);
        }
        else if (Array->Type == JSON_ARRAY_STRING)
        {
            array<string>* StringArray = &Array->StringValues;

            for (u64 StringIndex = 0; StringIndex < StringArray->Size; ++StringIndex)
            {
                FreeString(GetElement(StringArray, StringIndex), Allocator);
            }

            FreeArray(&Array->StringValues);
        }
        else if (Array->Type == JSON_ARRAY_OBJECT)
        {
            array<json>* ObjectArray = &Array->Objects;

            for (u64 ObjectIndex = 0; ObjectIndex < ObjectArray->Size; ++ObjectIndex)
            {
                DestroyJson(GetElement(ObjectArray, ObjectIndex), Allocator);
            }

            FreeArray(&Array->Objects);
        }
    }

    FreeArray(&Json->Objects);
    FreeArray(&Json->Numbers);
    FreeArray(&Json->Booleans);
    FreeArray(&Json->Strings);
    FreeArray(&Json->Arrays);
}

b32
LoadJsonFromFile(json* Object, const char* FileName, allocator* Allocator)
{
    if (!DoesFileExist(FileName))
    {
        return (false);
    }

    file File = OpenFile(FileName, FILE_READ, false);

    if (File.Handle < 0)
    {
        return (false);
    }

    char* FileContents = ReadFile(File, Allocator);

    CloseSystemHandle(&File.Handle);

    BufferToJSON(Object, FileContents, File.Length, Allocator);

    return (true);
}

void
JsonArrayToString(stream* WriteStream, json_array* Array,
                  allocator* Allocator)
{
    if (Array->Name.Length > 0)
    {
        Write(WriteStream, '\"');
        Write(WriteStream, Array->Name.Buffer, Array->Name.Length);
        Write(WriteStream, (char*)"\"=", 2);
    }

    Write(WriteStream, '[');

    switch (Array->Type)
    {
        case JSON_ARRAY_NUMBER:
        {
            array<f64> NumberValues = Array->NumberValues;
            u32        ValueCount   = NumberValues.Size;

            for (u32 ValueIndex = 0; ValueIndex < ValueCount; ++ValueIndex)
            {
                f64 Value = NumberValues[ValueIndex];

                string StringValue;
                FloatToString(&StringValue, Value, Allocator);

                Write(WriteStream, StringValue.Buffer, StringValue.Length);
                FreeString(&StringValue, Allocator);

                if (ValueIndex < ValueCount - 1)
                {
                    Write(WriteStream, ',');
                }
            }
        }
        break;
        case JSON_ARRAY_BOOLEAN:
        {
            array<b32> BooleanValues = Array->BooleanValues;
            u32        ValueCount    = BooleanValues.Size;

            for (u32 ValueIndex = 0; ValueIndex < ValueCount; ++ValueIndex)
            {
                b32 Value = BooleanValues[ValueIndex];

                char* StringValue = (char*)((Value) ? "true" : "false");

                Write(WriteStream, StringValue, (Value) ? 4 : 5);

                if (ValueIndex < ValueCount - 1)
                {
                    Write(WriteStream, ',');
                }
            }
        }
        break;
        case JSON_ARRAY_STRING:
        {
            array<string> StringValues = Array->StringValues;
            u32           ValueCount   = StringValues.Size;

            for (u32 ValueIndex = 0; ValueIndex < ValueCount; ++ValueIndex)
            {
                string* StringValue = StringValues + ValueIndex;

                Write(WriteStream, '\"');
                Write(WriteStream, StringValue->Buffer, StringValue->Length);
                Write(WriteStream, '\"');

                if (ValueIndex < ValueCount - 1)
                {
                    Write(WriteStream, ',');
                }
            }
        }
        break;
        default:
        {
        }
        break;
    }

    Write(WriteStream, ']');
}

void
JsonObjectToString(stream* WriteStream, json* Object, allocator* Allocator)
{
    if (Object->Name.Length > 0)
    {
        Write(WriteStream, '\"');
        Write(WriteStream, Object->Name.Buffer, Object->Name.Length);
        Write(WriteStream, (char*)"\":", 3);
    }

    Write(WriteStream, '{');

    array<json_string> Strings     = Object->Strings;
    u32                StringCount = Strings.Size;

    for (u32 StringIndex = 0; StringIndex < StringCount; ++StringIndex)
    {
        json_string* String = Strings + StringIndex;

        Write(WriteStream, '\"');
        Write(WriteStream, String->Name.Buffer, String->Name.Length);
        Write(WriteStream, (char*)"\":\"", 3);

        Write(WriteStream, String->Value.Buffer, String->Value.Length);
        Write(WriteStream, (char*)"\",", 2);
    }

    array<json_number> Numbers     = Object->Numbers;
    u32                NumberCount = Numbers.Size;

    for (u32 NumberIndex = 0; NumberIndex < NumberCount; ++NumberIndex)
    {
        json_number* Number = Numbers + NumberIndex;

        Write(WriteStream, '\"');
        Write(WriteStream, Number->Name.Buffer, Number->Name.Length);
        Write(WriteStream, (char*)"\":", 2);

        string NumberToString;
        FloatToString(&NumberToString, Number->Value, Allocator);

        Write(WriteStream, NumberToString.Buffer, NumberToString.Length);
        Write(WriteStream, ',');
    }

    array<json_boolean> Booleans     = Object->Booleans;
    u32                 BooleanCount = Booleans.Size;

    for (u32 BooleanIndex = 0; BooleanIndex < BooleanCount; ++BooleanIndex)
    {
        json_boolean* Boolean = Booleans + BooleanIndex;

        Write(WriteStream, '\"');
        Write(WriteStream, Boolean->Name.Buffer, Boolean->Name.Length);
        Write(WriteStream, (char*)"\":", 2);

        char* BooleanToString = (char*)(Boolean->Value ? "true" : "false");

        Write(WriteStream, (char*)BooleanToString, StringLength(BooleanToString));
        Write(WriteStream, ',');
    }

    array<json_array> Arrays     = Object->Arrays;
    u32               ArrayCount = Arrays.Size;

    for (u32 ArrayIndex = 0; ArrayIndex < ArrayCount; ++ArrayIndex)
    {
        json_array* Array = Arrays + ArrayIndex;

        JsonArrayToString(WriteStream, Array, Allocator);

        if (ArrayIndex < ArrayCount - 1)
        {
            Write(WriteStream, ',');
        }
    }

    array<json> Children   = Object->Objects;
    u32         ChildCount = Children.Size;

    for (u32 ChildIndex = 0; ChildIndex < ChildCount; ++ChildIndex)
    {
        JsonObjectToString(WriteStream, Children + ChildIndex, Allocator);
        Write(WriteStream, ',');
    }
}
