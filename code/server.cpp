#include "std/server.h"

#include "std/string.h"

// Opens a socket, binds on to the specified port, and starts listening for
// clients
b32
CreateTcpServer(tcp_server* Server, u32 Port, int ListenBacklog,
                allocator* Allocator)
{
    // Open Server Socket
    Server->s = OpenSocket(SOCKET_TYPE_TCP);

    // If the server socket is invalid return
    if (Server->s == 0)
    {
        return (false);
    }

    // Bind Server Socket
    if (!BindSocket(Server->s, Port))
    {
        return (false);
    }

    // Start Listening on the server socket
    if (!SocketListen(Server->s, ListenBacklog))
    {
        return (false);
    }

    SetNonblocking(Server->s);

    CreatePacketHandler(&Server->PacketHandler, Allocator);

    // Initialize the clients array and the ClientReadstream
    CreateArray(&Server->Clients, Allocator);
    // CreateStream(&Server->ClientReadStream, Kilobytes(2), MemoryCallback);

    Server->KeepAlive = false;

    return (true);
}

void
DestroyTcpServer(tcp_server* Server)
{
    array<connected_tcp_client> Clients     = Server->Clients;
    u32                         ClientCount = Clients.Size;

    for (u32 ClientIndex = 0; ClientIndex < ClientCount; ++ClientIndex)
    {
        connected_tcp_client* Client = Clients + ClientIndex;

        CloseSystemHandle(&Client->s);
    }

    FreeArray(&Server->Clients);

    if (Server->KeepAlive)
    {
        DestroyStream(&Server->KeepAlivePacket);
    }

    Server->KeepAlive = false;
    DestroyPacketHandler(&Server->PacketHandler);
    CloseSystemHandle(&Server->s);
}

void
SetServerKeepAlive(tcp_server* Server, u32 PacketId, u64 CheckTime, u32 RetryCount,
                   allocator* Allocator)
{
    Server->KeepAlive  = true;
    Server->CheckTime  = CheckTime;
    Server->RetryCount = RetryCount;

    CreateStream(&Server->KeepAlivePacket, Allocator, sizeof(u32) + sizeof(u64));
    Write<u32>(&Server->KeepAlivePacket, PacketId);
    Write<u64>(&Server->KeepAlivePacket, 0);

    AddPacketCallback(Server, PacketId, ServerKeepAlivePacketCallback, Server);
    StartTimer(&Server->KeepAliveTimer, CheckTime);
}

connected_tcp_client*
AcceptTcpConnection(tcp_server* Server)
{
    system_handle s = AcceptSocket(Server->s);

    if (!IsInvalidSocket(s))
    {
        connected_tcp_client* NewClient = Add(&Server->Clients);

        NewClient->s              = s;
        NewClient->IsAlive        = true;
        NewClient->MissedAckCount = 0;

        return (NewClient);
    }

    return (NULL);
}

void
ProcessAllIncoming(tcp_server* Server)
{
    array<connected_tcp_client>* Clients     = &Server->Clients;
    u32                          ClientCount = Clients->Size;
    packet_read_error            ReadError;

    b32 CheckKeepAlive = false;

    if (Server->KeepAlive && IsTimerComplete(&Server->KeepAliveTimer))
    {
        CheckKeepAlive = true;
        StartTimer(&Server->KeepAliveTimer, Server->CheckTime);
    }

    // Loop through all of the clients and read and update all of them
    for (u32 ClientIndex = 0; ClientIndex < ClientCount; ++ClientIndex)
    {
        connected_tcp_client* Client = GetElement(Clients, ClientIndex);

        if (!Client->IsAlive)
        {
            continue;
        }

        ReadError = ReadAndProcessPacket(&Server->PacketHandler, Client->s);

        while (ReadError == PACKET_READ_NO_ERROR)
        {
            ReadError = ReadAndProcessPacket(&Server->PacketHandler, Client->s);
        }

        if (CheckKeepAlive)
        {
            if (Client->MissedAckCount == Server->RetryCount)
            {
                Client->IsAlive = false;
            }
            else
            {
                SendData(Client->s, Server->KeepAlivePacket.Buffer,
                         Server->KeepAlivePacket.Length);
            }

            ++Client->MissedAckCount;
        }
    }
}

void
AddPacketCallback(tcp_server* Server, u32 PacketId, packet_handler_function* Function,
                  void* UserData)
{
    AddCallback(&Server->PacketHandler, PacketId, Function, UserData);
}

connected_tcp_client*
GetClientBySocket(tcp_server* Server, system_handle Socket)
{
    array<connected_tcp_client> Clients     = Server->Clients;
    u32                         ClientCount = Clients.Size;

    for (u32 ClientIndex = 0; ClientIndex < ClientCount; ++ClientIndex)
    {
        connected_tcp_client* Client = Clients + ClientIndex;

        if (Client->s == Socket)
        {
            return (Client);
        }
    }

    return (NULL);
}

void
RemoveClientBySocket(tcp_server* Server, system_handle Socket)
{
    array<connected_tcp_client> Clients     = Server->Clients;
    u32                         ClientCount = Clients.Size;

    b32 FoundClient  = false;
    u32 RemovalIndex = 0;

    for (u32 ClientIndex = 0; ClientIndex < ClientCount; ++ClientIndex)
    {
        connected_tcp_client* Client = Clients + ClientIndex;

        if (Client->s == Socket)
        {
            RemovalIndex = (u32)ClientIndex;
            FoundClient  = true;
            break;
        }
    }

    if (FoundClient)
    {
        CloseSystemHandle(&Socket);

        Remove(&Server->Clients, (u32)RemovalIndex);
    }
}

void
ServerKeepAlivePacketCallback(system_handle Socket, stream* ReadStream, void* UserData)
{
    UNUSED_VARIABLE(ReadStream);
    tcp_server* Server = (tcp_server*)UserData;

    connected_tcp_client* Client = GetClientBySocket(Server, Socket);

    if (Server->KeepAlive)
    {
        Client->MissedAckCount = 0;
    }
}
