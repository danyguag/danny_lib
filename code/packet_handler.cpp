#include "std/packet_handler.h"
#include "std/string.h"

void
CreatePacketHandler(packet_handler* PacketHandler, allocator* Allocator)
{
    CreateArray<packet_handler_callback>(&PacketHandler->Callbacks, Allocator);
    CreateStream(&PacketHandler->ReadStream, Allocator);
    CreateStream(&PacketHandler->PacketHeader, Allocator, sizeof(u32) + sizeof(u64));
}

void
DestroyPacketHandler(packet_handler* PacketHandler)
{
    DestroyStream(&PacketHandler->PacketHeader);
    DestroyStream(&PacketHandler->ReadStream);
    FreeArray(&PacketHandler->Callbacks);
}

void
AddCallback(packet_handler* PacketHandler, u32 PacketId,
            packet_handler_function* Function, void* UserData)
{
    packet_handler_callback Callback { Function, PacketId, UserData };

    Add<packet_handler_callback>(&PacketHandler->Callbacks, Callback);
}

void
RemoveCallback(packet_handler* PacketHandler, u32 PacketId)
{
    u32 CallbackCount = PacketHandler->Callbacks.Size;

    for (u32 CallbackIndex = 0; CallbackIndex < CallbackCount; ++CallbackIndex)
    {
        packet_handler_callback* Callback = PacketHandler->Callbacks + CallbackIndex;

        if (Callback->PacketId == PacketId)
        {
            Remove(&PacketHandler->Callbacks, CallbackIndex);

            return;
        }
    }
}

packet_handler_callback*
GetPacketFunctionById(array<packet_handler_callback>& Callbacks, u32 PacketId)
{
    u32 CallbackCount = Callbacks.Size;

    for (u32 CallbackIndex = 0; CallbackIndex < CallbackCount; ++CallbackIndex)
    {
        packet_handler_callback* Callback = Callbacks + CallbackIndex;

        if (Callback->PacketId == PacketId)
        {
            return (Callback);
        }
    }

    return (NULL);
}

packet_read_error
ReadAndProcessPacket(packet_handler* PacketHandler, system_handle Socket)
{
    ResetStream(&PacketHandler->PacketHeader);
    s32 BytesRead = RecvData(Socket, PacketHandler->PacketHeader.Buffer,
                             PacketHandler->PacketHeader.Length);

    if (BytesRead <= 0)
    {
        return (PACKET_READ_NO_PACKET);
    }

    if ((u64)BytesRead != PacketHandler->PacketHeader.Length)
    {
        return (PACKET_READ_HEADER_INVALID);
    }

    u32     PacketId   = Read<u32>(&PacketHandler->PacketHeader);
    u64     PacketSize = Read<u64>(&PacketHandler->PacketHeader);
    stream* ReadStream = &PacketHandler->ReadStream;

    packet_handler_callback* Callback =
        GetPacketFunctionById(PacketHandler->Callbacks, PacketId);

    if (Callback == NULL)
    {
        return (PACKET_READ_INVALID_PACKET);
    }

    if (PacketSize > 0)
    {

        ResetStream(ReadStream);
        BytesRead       = RecvData(Socket, ReadStream->Buffer, ReadStream->Length);
        u64 ReadPointer = (u64)BytesRead;

        while (ReadPointer < PacketHandler->ReadStream.Length)
        {
            BytesRead = RecvData(Socket, PacketHandler->ReadStream.Buffer + ReadPointer,
                                 ReadStream->Length - ReadPointer);

            if (BytesRead <= 0)
            {
                return (PACKET_READ_INVALID_LENGTH);
            }

            ReadPointer += (u64)BytesRead;
        }
    }

    if (Callback != NULL)
    {
        Callback->Function(Socket, ReadStream, Callback->UserData);
    }

    return (PACKET_READ_NO_ERROR);
}
