#include "std/memory_allocator.h"
#include "std/memory.h"

static void* AllocateMemoryCallback(u64 Length, void* UserData);
static void FreeMemoryCallback(void* Pointer, u64 Length, void* UserData);

memory_allocator*
CreateMemoryAllocator(u64 AllocationLength)
{
    allocator HeapAllocator;
    InitHeapAllocator(&HeapAllocator);
    
    memory_allocator* MemoryAllocator =
        (memory_allocator*)HeapAllocator.Allocate(sizeof(memory_allocator));

    MemoryAllocator->Pointer          = 0;
    MemoryAllocator->AllocationLength = AllocationLength;
    MemoryAllocator->Memory           = (char*)HeapAllocator.Allocate(AllocationLength);

    MemoryAllocator->Allocator = (allocator*)HeapAllocator.Allocate(sizeof(allocator));
    CreateAllocator(MemoryAllocator->Allocator, AllocateMemoryCallback,
                    FreeMemoryCallback, MemoryAllocator);
    CreateArray(&MemoryAllocator->AllocationRegions, &HeapAllocator,
                2046);

    return (MemoryAllocator);
}

void
DestroyMemoryAllocator(memory_allocator** pMemoryAllocator)
{
    allocator HeapAllocator;
    InitHeapAllocator(&HeapAllocator);
    memory_allocator* MemoryAllocator = *pMemoryAllocator;
    MemoryAllocator->AllocationRegions.Allocator = &HeapAllocator;
    
    FreeArray(&MemoryAllocator->AllocationRegions);
    free(MemoryAllocator->Allocator);
    free(MemoryAllocator->Memory);
    free(MemoryAllocator);

    *pMemoryAllocator = NULL;
}

void
CombineAllocationRegions(memory_allocator* MemoryAllocator)
{
    array<allocation_region>* AllocationRegions = &MemoryAllocator->AllocationRegions;
    u32                       RegionCount       = AllocationRegions->Size;

    for (u32 RegionIndex = 0; RegionIndex < RegionCount; ++RegionIndex)
    {
        allocation_region* CurrentRegion = GetElement(AllocationRegions, RegionIndex);

        if (RegionIndex + 1 < AllocationRegions->Size)
        {
            allocation_region* NextRegion =
                GetElement(AllocationRegions, RegionIndex + 1);

            if (CurrentRegion->EndIndex + 1 == NextRegion->StartIndex)
            {
                CurrentRegion->EndIndex = NextRegion->EndIndex;
                Remove(AllocationRegions, RegionIndex + 1);

                --RegionIndex;
                --RegionCount;
            }
        }
    }

    if (AllocationRegions->Size > 0)
    {
        allocation_region* Region = MemoryAllocator->AllocationRegions + RegionCount - 1;

        if (Region->EndIndex + 1 == MemoryAllocator->Pointer)
        {
            MemoryAllocator->Pointer = Region->StartIndex;
            Remove(&MemoryAllocator->AllocationRegions, RegionCount - 1);
        }
    }
}

s32
FindSuitableMemoryLocation(memory_allocator* MemoryAllocator, u64 Length)
{
    u32 RegionCount      = MemoryAllocator->AllocationRegions.Size;
    s32 FinalRegionIndex = -1;

    for (u32 RegionIndex = 0; RegionIndex < RegionCount; ++RegionIndex)
    {
        allocation_region* Region = MemoryAllocator->AllocationRegions + RegionIndex;

        if (Region->EndIndex - Region->StartIndex >= Length - 1)
        {
            FinalRegionIndex = RegionIndex;
            break;
        }
    }

    if (FinalRegionIndex >= 0)
    {
        allocation_region* Region =
            MemoryAllocator->AllocationRegions + (u32)FinalRegionIndex;

        s32 Pointer = Region->StartIndex;

        if (Pointer + Length == Region->EndIndex + 1)
        {
            Remove(&MemoryAllocator->AllocationRegions, (u32)FinalRegionIndex);
        }
        else
        {
            Region->StartIndex += Length;
        }

        return (Pointer);
    }

    return (-1);
}

void*
PushMemory(memory_allocator* MemoryAllocator, u64 Length)
{
    s32   Pointer = FindSuitableMemoryLocation(MemoryAllocator, Length);
    void* Result  = NULL;

    if (Pointer < 0)
    {
        if (MemoryAllocator->Pointer + Length > MemoryAllocator->AllocationLength)
        {
            return (NULL);
        }

        Result = MemoryAllocator->Memory + MemoryAllocator->Pointer;
        MemoryAllocator->Pointer += Length;
    }
    else
    {
        Result = MemoryAllocator->Memory + Pointer;
    }

    return (Result);
}

void
AddSorted(array<allocation_region>* Regions, allocation_region Region)
{
    u32 RegionCount = Regions->Size;

    for (u32 RegionIndex = 0; RegionIndex < RegionCount; ++RegionIndex)
    {
        allocation_region* CurrentRegion = GetElement(Regions, RegionIndex);

        if (CurrentRegion->StartIndex - 1 == Region.EndIndex)
        {
            CurrentRegion->StartIndex = Region.StartIndex;
            return;
        }
        else if (CurrentRegion->EndIndex + 1 == Region.StartIndex)
        {
            CurrentRegion->EndIndex = Region.EndIndex;
            return;
        }

        if (CurrentRegion->StartIndex > Region.EndIndex)
        {
            if (RegionIndex + 1 == RegionCount)
            {
                engine_memcpy(GetElement(Regions, RegionCount),
                              GetElement(Regions, RegionCount - 1),
                              sizeof(allocation_region));
                engine_memcpy(GetElement(Regions, RegionCount - 1), &Region,
                              sizeof(allocation_region));
                ++Regions->Size;
                return;
            }

            allocation_region* Next = GetElement(Regions, RegionIndex);

            if (Next->StartIndex > Region.EndIndex)
            {
                for (u32 InverseIndex = RegionCount - 1; InverseIndex > (RegionIndex - 1);
                     --InverseIndex)
                {
                    engine_memcpy(GetElement(Regions, InverseIndex + 1),
                                  GetElement(Regions, InverseIndex),
                                  sizeof(allocation_region));
                }

                *Next = Region;
                ++Regions->Size;
                return;
            }
        }
    }

    Add(Regions, Region);
}

void
FreeMemory(memory_allocator* MemoryAllocator, void* Memory, u64 Length)
{
    allocation_region Region {};
    Region.StartIndex = (char*)Memory - (char*)MemoryAllocator->Memory;
    Region.EndIndex   = Region.StartIndex + Length - 1;

    AddSorted(&MemoryAllocator->AllocationRegions, Region);

    engine_memset(Memory, 0, Length);
}

void
ClearMemoryAllocator(memory_allocator* MemoryAllocator)
{
    engine_memset(MemoryAllocator->Memory, 0, MemoryAllocator->Pointer);
    MemoryAllocator->Pointer = 0;
    ClearArray(&MemoryAllocator->AllocationRegions);
}

static void*
AllocateMemoryCallback(u64 Length, void* UserData)
{
    return (PushMemory((memory_allocator*)UserData, Length));
}

static void
FreeMemoryCallback(void* Pointer, u64 Length, void* UserData)
{
    FreeMemory((memory_allocator*)UserData, Pointer, Length);
}
