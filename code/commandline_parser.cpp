#include "std/commandline_parser.h"

#include "std/base.h"

void
CreateCommandlineParser(commandline_parser* Parser, string GeneralHelpMessage,
                        allocator* Allocator)
{
    CreateArray(&Parser->Flags, Allocator);

    Parser->GeneralHelpMessage = GeneralHelpMessage;
    Parser->Allocator          = Allocator;
}

void
DestroyCommandlineParser(commandline_parser* Parser, allocator* Allocator)
{
    u32 FlagCount = Parser->Flags.Size;

    for (u32 FlagIndex = 0; FlagIndex < FlagCount; ++FlagIndex)
    {
        flag* Flag = Parser->Flags + FlagIndex;

        if (Flag->Found)
        {
            FreeString(&Flag->Value, Allocator);
        }
    }

    FreeArray(&Parser->Flags);
}

flag*
AddFlag(commandline_parser* Parser, string ShortFlagName, string LongFlagName,
        argument_type Type, string HelpMessage)
{
    flag* Flag = Add(&Parser->Flags);

    Flag->ShortName   = ShortFlagName;
    Flag->LongName    = LongFlagName;
    Flag->HelpMessage = HelpMessage;
    Flag->Type        = Type;
    Flag->Found       = false;
    Flag->Value       = CreateEmptyString();

    return (Flag);
}

flag*
GetFlagByShortName(commandline_parser* Parser, const char* ShortName)
{
    u64    FlagCount       = Parser->Flags.Size;
    string ShortNameString = CreateString(ShortName);

    for (u64 FlagIndex = 0; FlagIndex < FlagCount; ++FlagIndex)
    {
        flag* Flag = Parser->Flags + FlagIndex;

        if (Flag->ShortName == ShortNameString)
        {
            return (Flag);
        }
    }

    return (NULL);
}

void
PrintHelpMessage(commandline_parser* Parser)
{
    u32    FlagCount = Parser->Flags.Size;
    string Message;

    StandardPrint(Parser->GeneralHelpMessage);
    StandardPrint(CreateString("\n"));

    for (u32 FlagIndex = 0; FlagIndex < FlagCount; ++FlagIndex)
    {
        flag* Flag = Parser->Flags + FlagIndex;

        FormatString(&Message, "-%str, --%str: %str\n", Parser->Allocator,
                     Flag->ShortName, Flag->LongName, Flag->HelpMessage);
        StandardPrint(Message);
        FreeString(&Message, Parser->Allocator);
    }
}

int
ParseCommandlineStringValue(flag* Flag, string ValueString, int ArgCount, char** Args,
                            allocator* Allocator)
{
    stream ValueStream;
    int    ArgPointer   = 0;
    string CurrentValue = CreateString(Args[ArgPointer]);
    u64    EndOffset    = 1;

    if (ValueString.Buffer[ValueString.Length - 1] == ValueString.Buffer[0])
    {
        ++EndOffset;
    }

    CreateStream(&ValueStream, Allocator);
    Write(&ValueStream, &ValueString.Buffer[1], ValueString.Length - EndOffset);

    if (EndOffset == 2)
    {
        string Copy;
        Copy.Buffer   = ValueStream.Buffer;
        Copy.Length   = ValueStream.WritePointer;
        Copy.Capacity = ValueStream.Length;

        CopyString(&Flag->Value, Copy, Allocator);
        DestroyStream(&ValueStream);

        return (0);
    }

    do
    {
        Write(&ValueStream, ' ');
        ++ArgPointer;

        if (ArgPointer >= ArgCount)
        {
            break;
        }

        CurrentValue = CreateString(Args[ArgPointer]);
        EndOffset    = 0;

        if (CurrentValue.Buffer[CurrentValue.Length - 1] == ValueString.Buffer[0])
        {
            ++EndOffset;
        }

        Write(&ValueStream, CurrentValue.Buffer, CurrentValue.Length - EndOffset);

    } while (EndOffset == 0);

    string Copy;
    Copy.Buffer   = ValueStream.Buffer;
    Copy.Length   = ValueStream.WritePointer;
    Copy.Capacity = ValueStream.Length;

    CopyString(&Flag->Value, Copy, Allocator);
    DestroyStream(&ValueStream);

    return (ArgPointer);
}

flag*
GetKnownFlag(array<flag>* KnownFlags, string FlagName)
{
    u32 FlagCount = KnownFlags->Size;

    for (u32 FlagIndex = 0; FlagIndex < FlagCount; ++FlagIndex)
    {
        flag* Flag = GetElement(KnownFlags, FlagIndex);

        if (Flag->ShortName == FlagName || Flag->LongName == FlagName)
        {
            return (Flag);
        }
    }

    return (NULL);
}

u32
ParseCommandlineArgs(commandline_parser* Parser, int ArgCount, char** Args)
{
    if (ArgCount == 1)
    {
        return (0);
    }
    int ArgIndex;

    for (ArgIndex = 1; ArgIndex < ArgCount; ++ArgIndex)
    {
        string Current        = CreateString(Args[ArgIndex]);
        u32    CurrentPointer = 0;

        if (Current.Buffer[CurrentPointer] == '-')
        {
            ++CurrentPointer;
            char CurrentChar = Current.Buffer[CurrentPointer];

            if (CurrentChar != '-')
            {
                if (CurrentChar == 'h')
                {
                    PrintHelpMessage(Parser);
                    exit(1);
                }

                string FlagName = CreateString(&CurrentChar, 1);
                flag*  Flag     = GetKnownFlag(&Parser->Flags, FlagName);

                if (Flag == NULL)
                {
                    string ErrorMessage;

                    FormatString(&ErrorMessage, "Unknown Flag: %str\n", Parser->Allocator,
                                 FlagName);
                    StandardPrint(ErrorMessage);
                    FreeString(&ErrorMessage, Parser->Allocator);
                    continue;
                }

                Flag->Found = true;

                if (Flag->Type == ARGUMENT_TYPE_FLAG)
                {
                    Flag->Value = CreateEmptyString();
                    continue;
                }
                else
                {
                    ++ArgIndex;
                    string ValueString = CreateString(Args[ArgIndex]);

                    if (Flag->Type == ARGUMENT_TYPE_WORD)
                    {
                        CopyString(&Flag->Value, ValueString, Parser->Allocator);
                        continue;
                    }

                    ArgIndex += ParseCommandlineStringValue(
                        Flag, ValueString, ArgCount - ArgIndex, Args + ArgIndex,
                        Parser->Allocator);
                }
            }
            else
            {
                ++CurrentPointer;
                string CurrentFlagName = Current[CurrentPointer];

                if (StringsEqual(CurrentFlagName, CreateString("help")))
                {
                    PrintHelpMessage(Parser);
                    exit(1);
                }

                s64    FlagNameEnd = IndexOf(CurrentFlagName, '=');
                string FlagName    = CreateEmptyString();

                if (FlagNameEnd == -1)
                {
                    CopyString(&FlagName, CurrentFlagName, Parser->Allocator);
                }
                else
                {
                    if (!SubString(&FlagName, CurrentFlagName, 0, FlagNameEnd,
                                   Parser->Allocator))
                    {
			continue;
                    }
                }

                flag* Flag = GetKnownFlag(&Parser->Flags, FlagName);

                if (Flag == NULL)
                {
                    string ErrorMessage;

                    FormatString(&ErrorMessage, "Unknown Flag: %str\n", Parser->Allocator,
                                 FlagName);
                    StandardPrint(ErrorMessage);
                    FreeString(&ErrorMessage, Parser->Allocator);
                    exit(1);
                }

                u64    ValueStartIndex = FlagNameEnd + 1;
                string ValueString     = CurrentFlagName[ValueStartIndex];

                Flag->Found = true;

                if (Flag->Type == ARGUMENT_TYPE_FLAG)
                {
                    Flag->Value = CreateEmptyString();
                }
                else if (Flag->Type == ARGUMENT_TYPE_WORD)
                {
                    SubString(&Flag->Value, CurrentFlagName, ValueStartIndex,
                              CurrentFlagName.Length, Parser->Allocator);
                }
		else
		{
		    // NOTE: One less because the starting string is apart of the flag name
		    // word
		    ArgIndex +=
			ParseCommandlineStringValue(Flag, ValueString, ArgCount - ArgIndex,
						    Args + ArgIndex, Parser->Allocator);
		}

		FreeString(&FlagName, Parser->Allocator);
	    }
        }
    }

    return (ArgIndex - 1);
}
