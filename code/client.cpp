#include "std/client.h"

#include "std/memory.h"
#include "std/string.h"

void
CreateTcpClient(tcp_client* Client, allocator* Allocator)
{
    Client->s       = OpenSocket(SOCKET_TYPE_TCP);
    Client->IsAlive = false;
    CreatePacketHandler(&Client->PacketHandler, Allocator);

    Client->KeepAlive = false;
}

void
DestroyTcpClient(tcp_client* Client)
{
    if (Client->KeepAlive)
    {
        DestroyStream(&Client->KeepAlivePacket);
    }

    Client->KeepAlive = false;
    Client->IsAlive   = false;

    CloseSystemHandle(&Client->s);
    DestroyPacketHandler(&Client->PacketHandler);
}

void
SetClientKeepAlive(tcp_client* Client, u32 PacketId, allocator* Allocator)
{
    Client->KeepAlive = true;
    AddPacketCallback(Client, PacketId, ClientKeepAlivePacketCallback, Client);

    CreateStream(&Client->KeepAlivePacket, Allocator, sizeof(u32) + sizeof(u64));
    Write<u32>(&Client->KeepAlivePacket, PacketId);
    Write<u64>(&Client->KeepAlivePacket, 0);
}

void
ProcessAllIncoming(tcp_client* Client)
{
    packet_read_error Error = ReadAndProcessPacket(&Client->PacketHandler, Client->s);

    while (Error == PACKET_READ_NO_ERROR)
    {
        Error = ReadAndProcessPacket(&Client->PacketHandler, Client->s);
    }
}

void
ClientKeepAlivePacketCallback(system_handle Socket, stream* ReadStream, void* UserData)
{
    UNUSED_VARIABLE(Socket);
    UNUSED_VARIABLE(ReadStream);
    tcp_client* Client = (tcp_client*)UserData;

    if (Client->KeepAlive)
    {
        Send(Client, Client->KeepAlivePacket);
    }
}
