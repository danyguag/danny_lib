#include "std/base.h"

#include <dirent.h>
#include <dlfcn.h>
#include <errno.h>
#include <fcntl.h>
#include <pthread.h>
#include <semaphore.h>
#include <sys/stat.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <netdb.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <sys/poll.h>
#include <sys/socket.h>
#include <sys/sysinfo.h>
#include <sys/types.h>
#include <sys/mman.h>

#include "std/array.h"
#include "std/memory.h"
#include "std/string.h"

void
Sleep(u32 Miliseconds)
{
    usleep(Miliseconds * 1000);
}

void
NanoSleep(u64 Nanoseconds)
{
    usleep(Nanoseconds);
}

u64
GetAvailableRam()
{
    struct sysinfo SystemInformation;

    if (sysinfo(&SystemInformation) == -1)
    {
        return (0);
    }

    return (SystemInformation.freeram * SystemInformation.mem_unit);
}

int
GetPlatformError()
{
    return (errno);
}

b32
SetWorkingDirectory(const char* Path)
{
    return (chdir(Path) == 0);
}

void
GetCurrentWorkingDirectory(string* Result, allocator* Allocator)
{
    Result->Capacity = 255;
    Result->Buffer   = (char*)Allocator->Allocate(Result->Capacity);

    if (getcwd(Result->Buffer, Result->Capacity) == NULL)
    {
        Allocator->Free(Result->Buffer, Result->Capacity);
    }

    Result->Length = StringLength(Result->Buffer);
}

void
CloseSystemHandle(system_handle* Handle)
{
    if (!Handle)
    {
        return;
    }

    close(*Handle);
    *Handle = -1;
}

file
OpenFile(const char* FileName, file_operations FileOperations, b32 CreateFile)
{
    file Result {};

    Result.Handle   = -1;
    Result.FileName = (char*)FileName;
    Result.Length   = 0;

    int Flags = 0;

    if (FileOperations == FILE_READ)
    {
        Flags = (CreateFile) ? O_RDONLY | O_CREAT : O_RDONLY;
    }

    if (FileOperations == FILE_WRITE)
    {
        Flags = (CreateFile) ? O_WRONLY | O_CREAT : O_WRONLY;
    }

    if (FileOperations == FILE_BOTH)
    {
        Flags = (CreateFile) ? O_RDWR | O_CREAT : O_RDWR;
    }

    Result.Handle = open(FileName, Flags, 0666);

    return (Result);
}

b32
SetFileOffset(file File, s32 Offset)
{
    off_t ReturnedOffset = lseek(File.Handle, Offset, SEEK_SET);

    return (ReturnedOffset == Offset);
}

b32
WriteFile(file File, char* Buffer, u32 BufferLength)
{
    ssize_t BytesWritten = write(File.Handle, Buffer, BufferLength);
    return (BytesWritten == BufferLength);
}

s32
ReadFile(file File, char* Buffer, s32 Length)
{
    return (read(File.Handle, Buffer, Length));
}

u64
GetFileLength(const char* FileName)
{
    struct stat Stats;

    if (stat(FileName, &Stats) == -1)
    {
        return (0);
    }

    return (Stats.st_size);
}

void*
MmapFile(u64 Size, file_operations FileOperations, file File, u64 Offset)
{
    int Rights = PROT_READ | PROT_WRITE;

    if (FileOperations == FILE_READ)
    {
	Rights = PROT_READ;
    }
    else if (FileOperations == FILE_WRITE)
    {
	Rights = PROT_WRITE;
    }
    
    void* Memory = mmap(NULL, Size, Rights, MAP_PRIVATE, File.Handle, Offset);

    if (Memory != 0)
    {
	return (NULL);
    }

    return (Memory);
}

b32
MunmapFile(void* MappedMemory, u64 Size)
{
    return (munmap(MappedMemory, Size) == 0);
}

b32
MakeDirectory(const char* FileName)
{
    return (mkdir(FileName, 0700) == 0);
}

char*
ReadFile(file File, allocator* Allocator)
{
    u64 FileLength = GetFileLength(File.FileName);

    if (FileLength == 0)
    {
        return (NULL);
    }

    char* Buffer = (char*)Allocator->Allocate(FileLength + 1);

    read(File.Handle, Buffer, FileLength);

    Buffer[FileLength] = 0;
    File.Length        = FileLength;

    return (Buffer);
}

system_handle
OpenSocket(socket_type Type)
{
    int SocketType = SOCK_STREAM;

    if (Type == SOCKET_TYPE_UDP)
    {
        SocketType = SOCK_DGRAM;
    }

    system_handle Socket = socket(AF_INET, SocketType, 0);

    if (Socket < 0)
    {
        return (0);
    }

    return (Socket);
}

b32
SetNonblocking(system_handle s)
{
    s32 flags = fcntl(s, F_GETFL, 0);
    flags     = O_NONBLOCK;
    return (fcntl(s, F_SETFL, flags) == 0) ? true : false;
}

socket_address
MakeServerAddress(char* Address, u32 Port, allocator* Allocator)
{
    sockaddr_in* ServerAddress = (sockaddr_in*)Allocator->Allocate(sizeof(sockaddr_in));

    ServerAddress->sin_family = AF_INET;
    ServerAddress->sin_port   = htons(Port);
    inet_pton(AF_INET, Address, &ServerAddress->sin_addr);

    return (Address);
}

b32
ConnectSocket(system_handle s, const char* Address, u32 Port)
{
    sockaddr_in ServerAddress;

    ServerAddress.sin_family = AF_INET;
    ServerAddress.sin_port   = htons(Port);

    if (inet_pton(AF_INET, Address, &ServerAddress.sin_addr) <= 0)
    {
        return (false);
    }

    if (connect(s, (sockaddr*)&ServerAddress, sizeof(ServerAddress)) < 0)
    {
        return (false);
    }

    return (true);
}

b32
BindSocket(system_handle s, u32 Port)
{
    sockaddr_in Address;

    engine_memset((char*)&Address, 0, sizeof(Address));
    Address.sin_family      = AF_INET;
    Address.sin_addr.s_addr = INADDR_ANY;
    Address.sin_port        = htons(Port);

    if (bind(s, (sockaddr*)&Address, sizeof(Address)) < 0)
    {
        return (false);
    }

    return (true);
}

s32
SendData(system_handle s, char* Buffer, u32 Length)
{
    u32 BytesSent = send(s, Buffer, Length, MSG_DONTWAIT | MSG_NOSIGNAL);

    if (BytesSent != Length)
    {
        {
            return (errno);
        }
    }

    return (0);
}

b32
IsBlockingError(s32 Error)
{
    return (Error != EWOULDBLOCK && Error != EAGAIN);
}

s32
RecvData(system_handle s, char* Buffer, u32 Length)
{
    s32 BytesRecv = recv(s, Buffer, Length, MSG_DONTWAIT);

    if (BytesRecv == -1 && (errno == EAGAIN || errno == EWOULDBLOCK))
    {
        BytesRecv = 0;
    }

    return (BytesRecv);
}

// Reads the socket until there is no data left
s32
Read(system_handle Socket, stream* ReadStream)
{
    if (Socket == 0)
    {
        return (false);
    }

    // Keep this a variable since its needed in a few different places and it
    // needs to be different than ReadStream->Length but since we need that value
    // as well another variable is needed
    u32 MaxReadLength = ReadStream->Length - ReadStream->WritePointer;
    u64 InitialWritePointer = ReadStream->WritePointer;

    s32 BytesRecved =
        RecvData(Socket, ReadStream->Buffer + ReadStream->WritePointer, MaxReadLength);

    // Return in case of error or nothing to read
    if (BytesRecved <= 0)
    {
        return (BytesRecved);
    }

    ReadStream->WritePointer += BytesRecved;
    
    // IMPORTANT: The only reason I do it like this is because of the one case
    // that the packet is exactly the length of the MaxReadLength which means the
    // function will return false which is an false negative which could
    // potentially break things down the line without anyone knowing(it is like 1
    // and a trillion chance that the packet size is the same as the allocated
    // size in the stream) b32 Result = BytesRecved > 0;

    struct pollfd Poll;
    Poll.fd     = Socket;
    Poll.events = POLLIN;

    if (poll(&Poll, 1, 0) == -1)
    {
        Poll.revents = 0;
    }

    // This loops allows the stream to get bigger until it can fit everything
    while (ReadStream->WritePointer == ReadStream->Length && Poll.revents & POLLIN)
    {
        // Calculates the new length and resizes the buffer and reads more from
        // the socket
        u32 OldLength = ReadStream->Length;
        u32 NewLength = ReadStream->Length * 2;
	char* Temp = (char*) ReadStream->Allocator->Allocate(NewLength);
	
	engine_memcpy(Temp, ReadStream->Buffer, OldLength);
	ReadStream->Allocator->Free(ReadStream->Buffer, OldLength);
	ReadStream->Buffer = Temp;
	ReadStream->Length = NewLength;
	MaxReadLength = ReadStream->Length - ReadStream->WritePointer;

        BytesRecved =
            RecvData(Socket, ReadStream->Buffer + ReadStream->WritePointer, MaxReadLength);

        // Check for error in socket
        if (BytesRecved < 0)
        {
            return (BytesRecved);
        }
	
        ReadStream->WritePointer += BytesRecved;

        if (poll(&Poll, 1, 0) == -1)
        {
            Poll.revents = 0;
        }
    }

    if (BytesRecved > 0)
    {
	return (ReadStream->WritePointer - InitialWritePointer);
    }
    
    return (BytesRecved);
}

b32
SocketListen(system_handle s, s32 Backlog)
{
    return (listen(s, Backlog) == 0);
}

system_handle
AcceptSocket(system_handle s)
{
    system_handle socket = accept(s, NULL, NULL);

    return (socket);
}

b32
IsInvalidSocket(system_handle Socket)
{
    return (Socket < 0);
}

thread_handle
StartThread(thread_function Function, void* Memory, allocator* Allocator)
{
    pthread_t* Thread = (pthread_t*)Allocator->Allocate(sizeof(pthread_t));

    if (pthread_create(Thread, NULL, Function, Memory) != 0)
    {
        Allocator->Free(Thread, sizeof(pthread_t));
        return (NULL);
    }

    return (Thread);
}

void
DestroyThreadHandle(thread_handle Handle, allocator* Allocator)
{
    Allocator->Free(Handle, sizeof(pthread_t));
}

void
JoinThread(thread_handle ThreadHandle)
{
    pthread_join(*((pthread_t*)ThreadHandle), NULL);
}

void
ExitThread(void* Value)
{
    pthread_exit(Value);
}

b32
SetThreadAffinity(thread_handle ThreadHandle, u32 Core)
{
    cpu_set_t Set;
    CPU_ZERO(&Set);
    CPU_SET(Core, &Set);

    if (pthread_setaffinity_np(*((pthread_t*)ThreadHandle), sizeof(cpu_set_t), &Set) != 0)
    {
        return (false);
    }

    return (true);
}

u32
GetThreadAffinity(thread_handle ThreadHandle)
{
    u32       Result = 0;
    cpu_set_t Set;
    CPU_ZERO(&Set);

    if (pthread_getaffinity_np(*((pthread_t*)ThreadHandle), sizeof(cpu_set_t), &Set) != 0)
    {
        --Result;
        return (Result);
    }

    u32 CoreCount = GetCoreCount();

    for (u32 CoreIndex = 0; CoreIndex < CoreCount; ++CoreIndex)
    {
        if (CPU_ISSET(CoreIndex, &Set))
        {
            return (CoreIndex);
        }
    }
    --Result;
    return (Result);
}

mutex
CreateMutex(allocator* Allocator)
{
    pthread_mutex_t* Mutex =
        (pthread_mutex_t*)Allocator->Allocate(sizeof(pthread_mutex_t));

    if (pthread_mutex_init(Mutex, NULL) != 0)
    {
        return NULL;
    }

    return (mutex)Mutex;
}

void
DestroyMutex(mutex* Mutex, allocator* Allocator)
{
    pthread_mutex_destroy((pthread_mutex_t*)*Mutex);
    Allocator->Free(*Mutex, sizeof(pthread_mutex_t));
}

void
internal_platform::_LockMutex(mutex Mutex)
{
    pthread_mutex_lock((pthread_mutex_t*)Mutex);
}

void
internal_platform::_UnlockMutex(mutex Mutex)
{
    pthread_mutex_unlock((pthread_mutex_t*)Mutex);
}

thread_handle
GetCurrentThreadHandle(allocator* Allocator)
{
    thread_handle Handle = (thread_handle)Allocator->Allocate(sizeof(thread_handle));

    *((pthread_t*)Handle) = pthread_self();

    return (Handle);
}

semaphore
CreateSemaphore(u32 InitialValue, allocator* Allocator)
{
    sem_t* Semaphore = (sem_t*)Allocator->Allocate(sizeof(sem_t));

    if (sem_init(Semaphore, 0, InitialValue) != 0)
    {
        return (NULL);
    }

    return (Semaphore);
}

void
DestroySemaphore(semaphore pSemaphore, allocator* Allocator)
{
    sem_t* Semaphore = (sem_t*)pSemaphore;

    sem_destroy(Semaphore);
    Allocator->Free((char*)Semaphore, sizeof(sem_t));
}

void
Wait(semaphore Semaphore)
{
    sem_wait((sem_t*)Semaphore);
}

void
Signal(semaphore Semaphore)
{
    sem_post((sem_t*)Semaphore);
}

u32
GetCoreCount()
{
    u32 CoreCount = (u32)get_nprocs();

    return (CoreCount);
}

#include <stdio.h>
#include <string.h>
void
StandardPrint(string OutString)
{
    fputs(OutString.Buffer, stdout);
}

array<io_object>
SearchFolder(const char* Path, allocator* Allocator)
{
    array<io_object> Objects;
    CreateArray<io_object>(&Objects, Allocator);

    DIR*    Directory = opendir(Path);
    dirent* CurrentObject;

    while ((CurrentObject = readdir(Directory)) != NULL)
    {
        string FileName = CreateString(CurrentObject->d_name);

        if (StringsEqual(FileName, CreateString(".")) ||
            StringsEqual(FileName, CreateString("..")))
        {
            continue;
        }
        io_object* Next = Add(&Objects);

        CopyString(&Next->Name, FileName, Allocator);

        if (CurrentObject->d_type == DT_DIR)
        {
            Next->Type = IO_FOLDER;
        }
        else if (CurrentObject->d_type == DT_REG)
        {
            Next->Type = IO_FILE;
        }
    }

    closedir(Directory);

    return (Objects);
}

b32
ContainsFolder(array<io_object>* Objects, char* Name)
{
    u32    ObjectLength = Objects->Size;
    string NameString   = CreateString(Name);

    for (u32 ObjectIndex = 0; ObjectIndex < ObjectLength; ++ObjectIndex)
    {
        io_object* Object = GetElement(Objects, ObjectIndex);
        if (StringsEqual(Object->Name, NameString))
        {
            return (true);
        }
    }

    return (false);
}

b32
DoesFileExist(const char* FileName)
{
    return (access(FileName, F_OK) != -1);
}

void
DeleteFile(const char* FileName)
{
    unlink(FileName);
}

u64
GetFileLastModifiedTime(char* FileName)
{
    struct stat FileData;

    if (stat(FileName, &FileData) == 0)
    {
        struct timespec LastModifiedTime = FileData.st_mtim;

        return LastModifiedTime.tv_sec + (LastModifiedTime.tv_nsec / 1000000000.0);
    }

    return 0;
}

/*#include <time.h>

u64
GetTime()
{
    long time_res;
    time(&time_res);

    return time_res;
}
*/
