function create_and_destroy_stream_test()
   local test_stream_length = 1024

   local test_stream = create_stream(test_stream_length)

   if (test_stream.buffer == nil) then
      standard_print("Failed to initialize stream buffer\n")
      return 1
   end

   if ((test_stream.read_location == nil) or (test_stream.read_location ~= 0)) then
      standard_print("Failed to initialize stream read_location\n")
      return 1
   end

   if ((test_stream.write_location == nil) or (test_stream.write_location ~= 0)) then
      standard_print("Failed to initialize stream read_location\n")
      return 1
   end

   if ((test_stream.length == nil) or (test_stream.length ~= test_stream_length)) then
      standard_print("Failed to initialize stream length\n")
      return 1
   end

   if (test_stream.memory_callback == nil) then
      standard_print("Failed to initialize stream memory_callback\n")
      return 1
   end

   test_stream:destroy()

   if (test_stream.buffer ~= nil) then
      standard_print("Failed to destroy stream buffer\n")
      return 1
   end

   if (test_stream.read_location ~= nil) then
      standard_print("Failed to destroy stream read_location\n")
      return 1
   end

   if (test_stream.write_location ~= nil) then
      standard_print("Failed to destroy stream read_location\n")
      return 1
   end

   if (test_stream.length ~= nil) then
      standard_print("Failed to destroy stream length\n")
      return 1
   end

   if (test_stream.memory_callback ~= nil) then
      standard_print("Failed to destroy stream memory_callback\n")
      return 1
   end
   
   return 0
end

function set_buffer_test()
   local test_stream = create_stream()

   test_stream.read_location = 100
   local original_pointer_length = test_stream.length
   local original_pointer = test_stream.buffer
   local different_pointer_length = test_stream.length - 100
   local different_pointer = test_stream:get_current_read_location()

   test_stream:set_buffer(different_pointer, different_pointer_length)

   if (test_stream.buffer ~= different_pointer) then
      standard_print("Failed to change to the correct stream buffer\n")
      return 1
   end
   
   if (test_stream.length ~= different_pointer_length) then
      standard_print("Failed to change to the correct stream length\n")
      return 1
   end

   test_stream:set_buffer(original_pointer, original_pointer_length)
   test_stream:destroy()
   
   return 0
end

function reset_stream_test()
   local test_stream = create_stream()

   test_stream.read_location = 100
   test_stream.write_location = 100
   
   test_stream:reset_buffer()

   if (test_stream.read_location ~= 0) then
      standard_print("Failed to reset read_location\n")
      return 1
   end
   
   if (test_stream.write_location ~= 0) then
      standard_print("Failed to reset write_location\n")
      return 1
   end

   test_stream:destroy()
   
   return 0
end

function write_and_read_signed_test()
   local test_stream = create_stream()

   test_stream:write_signed(100, 8)  -- 1 byte
   test_stream:write_signed(100, 16) -- 2 byte
   test_stream:write_signed(100, 32) -- 4 byte
   test_stream:write_signed(100, 64) -- 8 byte

   if (test_stream.write_location ~= 15) then
      standard_print("Failed to reset write_location\n")
      return 1
   end

   local value = test_stream:read_signed(8)

   if (value ~= 100) then
      standard_print("Failed to read 8 bit value\n")

      return 1
   end

   value = test_stream:read_signed(16)

   if (value ~= 100) then
      standard_print("Failed to read 16 bit value\n")

      return 1
   end

   value = test_stream:read_signed(32)

   if (value ~= 100) then
      standard_print("Failed to read 32 bit value\n")

      return 1
   end

   value = test_stream:read_signed(64)

   if (value ~= 100) then
      standard_print("Failed to read 64 bit value\n")

      return 1
   end
   
   test_stream:destroy()
   
   return 0
end

function write_and_read_unsigned_test()
   local test_stream = create_stream()

   test_stream:write_unsigned(100, 8)  -- 1 byte
   test_stream:write_unsigned(100, 16) -- 2 byte
   test_stream:write_unsigned(100, 32) -- 4 byte
   test_stream:write_unsigned(100, 64) -- 8 byte

   if (test_stream.write_location ~= 15) then
      standard_print("Failed to reset write_location\n")
      return 1
   end

   print(test_stream.read_location)
   
   local value = test_stream:read_unsigned(8)

   if (value ~= 100) then
      standard_print("Failed to read 8 bit value: ")
      standard_print(integer_to_string(value))
      standard_print("\n")

      return 1
   end

   value = test_stream:read_unsigned(16)

   if (value ~= 100) then
      standard_print("Failed to read 16 bit value: ")
      standard_print(integer_to_string(value))
      standard_print("\n")

      return 1
   end

   value = test_stream:read_unsigned(32)

   if (value ~= 100) then
      standard_print("Failed to read 32 bit value: ")
      standard_print(integer_to_string(value))
      standard_print("\n")

      return 1
   end

   value = test_stream:read_unsigned(64)

   if (value ~= 100) then
      standard_print("Failed to read 64 bit value: ")
      standard_print(integer_to_string(value))
      standard_print("\n")

      return 1
   end
   
   test_stream:destroy()
   
   return 0
end

function write_and_read_pointer_test()

   return 1
end

function run_tests()
   local failure_count = 0
   
   failure_count = failure_count + create_and_destroy_stream_test()
   failure_count = failure_count + set_buffer_test()
   failure_count = failure_count + reset_stream_test()
   failure_count = failure_count + write_and_read_signed_test()
   failure_count = failure_count + write_and_read_unsigned_test()
   failure_count = failure_count + write_and_read_pointer_test()

   standard_print("Stream Tests Failed: ")
   standard_print(integer_to_string(failure_count))
   standard_print("\n")
end

run_tests()
