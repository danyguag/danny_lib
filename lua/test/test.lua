function dump(o)
   if type(o) == 'table' then
      local s = '{ '
      for k,v in pairs(o) do
         if type(k) ~= 'number' then k = '"'..k..'"' end
         s = s .. '['..k..'] = ' .. dump(v) .. ','
      end
      return s .. '} '
   else
      return tostring(o)
   end
end





local empty_string = create_empty_string()

local string1 = create_string("This is a test buffer\n")
local string2 = create_string("This is a test buffer\n")

function test_table_modification_stream_write_location(stream_name, stream)
   local expected_stream_write_location = stream.write_location
   
   print(stream_name, ".write_location: ", stream.write_location);

   stream:write_signed(100, 32)
   expected_stream_write_location = expected_stream_write_location + 4
   print(stream_name, ".write_location: ", stream.write_location);

   stream:write_signed(100, 32)
   expected_stream_write_location = expected_stream_write_location + 4
   print(stream_name, ".write_location: ", stream.write_location);

   stream:write_pointer(stream.buffer, stream.write_location)
   expected_stream_write_location = expected_stream_write_location + 8
   print(stream_name, ".write_location: ", stream.write_location);

   if (expected_stream_write_location == stream.write_location)  then
      print("test_table_modification_stream_write_location: passed")
   else
      print("test_table_modification_stream_write_location: failed")
      print("test_table_modification_stream_write_location: ", stream_name,
	    ".expcted_write_location: ", expected_stream_write_location)
   end
   
end

function test_table_modification_stream_length(stream_name, stream)
   local expected_stream_length = stream.length
   
   print(stream_name, ".length: ", stream.length);

   stream:write_signed(100, 32)
   expected_stream_length = expected_stream_length / 2;
   print(stream_name, ".length: ", stream.length);
   if (expected_stream_length == stream.length) then
      print("test_table_modification_stream_length: passed")
   else
      print("test_table_modification_stream_length: failed")
      print("test_table_modification_stream_length: ", stream_name,
	    ".expcted_write_location: ", expected_stream_write_location)
   end

   stream:write_signed(100, 32)
   expected_stream_length = expected_stream_length / 2;
   print(stream_name, ".length: ", stream.length);
   
   stream:write_pointer(stream.buffer, stream.length)
   print(stream_name, ".length: ", stream.length);

   if (expected_stream_length == stream.length) then
      print("test_table_modification_stream_length: passed")
   else
      print("test_table_modification_stream_length: failed")
      print("test_table_modification_stream_length: ", stream_name,
	    ".expcted_write_location: ", expected_stream_write_location)
   end
end


local test_stream = create_stream()
local test_stream_2 = create_stream()

if (test_stream == nil) then
   print("The value is bad")
end

test_stream:write_pointer(test_stream.buffer, 100)

--test_table_modification_stream_write_location("test_stream", test_stream)
--test_table_modification_stream_length("test_stream_2", test_stream_2)

--[[local test_data = {}
test_data.data = 10

   test_stack(test_data, test_stream.buffer, 100)]]

