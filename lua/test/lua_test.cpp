#include <std/base.h>

#include "../danny_lua.h"

#include <tuple>

std::tuple<char*, size_t>
OpenAndReadLuaFile(const char* FileName)
{
    file LuaTestFile = OpenFile(FileName, FILE_READ, false);

    if (LuaTestFile.Handle < 0)
    {
        StandardPrint("Failed to load test.lua\n");
        exit(1);
    }

    char* LuaFileContents = ReadFile(LuaTestFile);

    CloseSystemHandle(&LuaTestFile.Handle);

    return { LuaFileContents, LuaTestFile.Length };
}

int
main(int ArgCount, char** Args)
{
    UNUSED_VARIABLE(ArgCount);
    UNUSED_VARIABLE(Args);

    lua_State* State = luaL_newstate();

    luaL_openlibs(State);
    
    LoadDannyStandardLibs(State);

    auto [LuaContents, FileSize] = OpenAndReadLuaFile("stream_test.lua");

    if (luaL_loadbuffer(State, LuaContents, FileSize, "test.lua") != LUA_OK)
    {
        StandardPrint("Failed to load lua\n");
    }

    int ReturnCode = lua_pcall(State, 0, 0, 0);

    if (ReturnCode != LUA_OK)
    {
        string ErrorMessage;
        GetStringArg(&ErrorMessage, State, -1);

        StandardPrint(ErrorMessage);

        if (ReturnCode == LUA_ERRRUN)
        {
            StandardPrint("\nRuntime error\n");
        }
        else if (ReturnCode == LUA_ERRMEM)
        {
            StandardPrint("Memory allocation error\n");
        }
        else if (ReturnCode == LUA_ERRERR)
        {
            StandardPrint("Error in message handler\n");
        }
    }

    
    lua_close(State);
}
