#include "lua_stream.h"

int LuaCreateStream(lua_State* State);
int LuaDestroyStream(lua_State* State);
int LuaGetCurrentReadLocation(lua_State* State);

int LuaStreamWritePointer(lua_State* State);
int LuaStreamWriteSigned(lua_State* State);
int LuaStreamWriteUnsigned(lua_State* State);
int LuaStreamReadPointer(lua_State* State);
int LuaStreamReadSigned(lua_State* State);
int LuaStreamReadUnsigned(lua_State* State);

int LuaStreamSetBuffer(lua_State* State);
int LuaStreamResetBuffer(lua_State* State);

b32 LoadStream(stream* Stream, lua_State* State, int Index);

int
TestStack(lua_State* State)
{
    PrintStack(State);

    PrintStack(State);

    return (0);
}

void
LoadStreamFunctions(lua_State* State)
{
    lua_register(State, "create_stream", LuaCreateStream);
    lua_register(State, "test_stack", TestStack);
}

void
PushStream(lua_State* State, stream& Stream)
{
    BeginLuaStruct(State, 6);
    PushIntegerVariable(State, "length", Stream.Length);
    PushIntegerVariable(State, "write_location", Stream.WritePointer);
    PushIntegerVariable(State, "read_location", Stream.ReadPointer);
    PushPointer(State, "memory_callback", Stream.MemoryCallback);
    PushPointer(State, "buffer", Stream.Buffer);

    PushFunctionVariable(State, "get_current_read_location", LuaGetCurrentReadLocation);
    PushFunctionVariable(State, "write_pointer", LuaStreamWritePointer);
    PushFunctionVariable(State, "write_signed", LuaStreamWriteSigned);
    PushFunctionVariable(State, "write_unsigned", LuaStreamWriteUnsigned);
    PushFunctionVariable(State, "read_pointer", LuaStreamReadPointer);
    PushFunctionVariable(State, "read_signed", LuaStreamReadSigned);
    PushFunctionVariable(State, "read_unsigned", LuaStreamReadUnsigned);
    PushFunctionVariable(State, "set_buffer", LuaStreamSetBuffer);
    PushFunctionVariable(State, "reset_buffer", LuaStreamResetBuffer);
    PushFunctionVariable(State, "destroy", LuaDestroyStream);
}

int
LuaCreateStream(lua_State* State)
{
    int ArgCount = lua_gettop(State);

    u64 StreamLength = DEFAULT_STREAM_ALLOCATION_SIZE;

    if (ArgCount == 1)
    {
        s64 ArgLength;

        if (!GetIntegerArg(&ArgLength, State, 1))
        {
            luaL_error(State, "Error getting the integer argument");
        }

        StreamLength = ArgLength;
    }

    stream Stream;
    CreateStream(&Stream, StreamLength);

    PushStream(State, Stream);

    return (1);
}

int
LuaDestroyStream(lua_State* State)
{
    stream Stream;

    if (!LoadStream(&Stream, State, 1))
    {
        luaL_error(State, "Incorrect first argument expected stream");
    }

    DestroyStream(&Stream);

    lua_pushnil(State);
    lua_setfield(State, 1, "write_location");
    lua_pushnil(State);
    lua_setfield(State, 1, "read_location");
    lua_pushnil(State);
    lua_setfield(State, 1, "length");
    lua_pushnil(State);
    lua_setfield(State, 1, "buffer");
    lua_pushnil(State);
    lua_setfield(State, 1, "memory_callback");

    return (0);
}

int
LuaGetCurrentReadLocation(lua_State* State)
{
    stream Stream;

    if (!LoadStream(&Stream, State, 1))
    {
        luaL_error(State, "Incorrect first argument expected stream");
    }

    lua_pushlightuserdata(State, Stream.GetCurrentReadLocation());

    return (1);
}

int
LuaStreamWritePointer(lua_State* State)
{
    stream Stream;
    void*  Pointer;
    s64    Length;

    if (!LoadStream(&Stream, State, 1))
    {
        luaL_error(State, "Incorrect first argument expected stream");
    }

    if (!GetPointerArg(&Pointer, State, 2))
    {
        luaL_error(State, "Incorrect second argument expected pointer");
    }

    if (!GetIntegerArg(&Length, State, 3))
    {
        luaL_error(State, "Incorrect second argument expected pointer");
    }

    lua_pop(State, 2);

    u64 OldLength = Stream.Length;
    Write(&Stream, Pointer, Length);

    if (Stream.Length != OldLength)
    {
        UpdateBufferPointer(State, Stream.Buffer);
        UpdateLength(State, Stream.Length);
    }

    UpdateWritePointer(State, Stream.WritePointer);

    return (0);
}

int
LuaStreamWriteSigned(lua_State* State)
{
    stream Stream;
    s64    FullValue;
    s64    BitCount;

    if (!LoadStream(&Stream, State, 1))
    {
        luaL_error(State, "Incorrect first argument expected stream");
    }

    if (!GetIntegerArg(&FullValue, State, 2))
    {
        luaL_error(State, "Incorrect second argument expected integer");
    }

    if (!GetIntegerArg(&BitCount, State, 3))
    {
        luaL_error(State, "Incorrect third argument expected integer");
    }

    lua_pop(State, 2);

    u64 OldLength = Stream.Length;

    if (BitCount == 8)
    {
        Write<s8>(&Stream, FullValue);
    }
    else if (BitCount == 16)
    {
        Write<s16>(&Stream, FullValue);
    }
    else if (BitCount == 32)
    {
        Write<s32>(&Stream, FullValue);
    }
    else if (BitCount == 64)
    {
        Write<s64>(&Stream, FullValue);
    }
    else
    {
        luaL_error(State, "Incorrect bit size");
    }

    if (Stream.Length != OldLength)
    {
        UpdateBufferPointer(State, Stream.Buffer);
        UpdateLength(State, Stream.Length);
    }

    UpdateWritePointer(State, Stream.WritePointer);

    UpdateLength(State, Stream.Length / 2);

    return (0);
}

int
LuaStreamWriteUnsigned(lua_State* State)
{
    stream Stream;
    s64    FullValue;
    s64    BitCount;

    if (!LoadStream(&Stream, State, 1))
    {
        luaL_error(State, "Incorrect first argument expected stream");
    }

    if (!GetIntegerArg(&FullValue, State, 2))
    {
        luaL_error(State, "Incorrect second argument expected integer");
    }

    if (!GetIntegerArg(&BitCount, State, 3))
    {
        luaL_error(State, "Incorrect third argument expected integer");
    }

    lua_pop(State, 2);

    u64 OldLength = Stream.Length;

    if (BitCount == 8)
    {
        Write<u8>(&Stream, FullValue);
    }
    else if (BitCount == 16)
    {
        Write<u16>(&Stream, FullValue);
    }
    else if (BitCount == 32)
    {
        Write<u32>(&Stream, FullValue);
    }
    else if (BitCount == 64)
    {
        Write<u64>(&Stream, FullValue);
    }
    else
    {
        luaL_error(State, "Incorrect bit size");
    }

    if (Stream.Length != OldLength)
    {
        UpdateBufferPointer(State, Stream.Buffer);
        UpdateLength(State, Stream.Length);
    }

    UpdateWritePointer(State, Stream.WritePointer);

    return (0);
}

int
LuaStreamReadPointer(lua_State* State)
{
    stream Stream;
    s64    Length;

    if (!LoadStream(&Stream, State, 1))
    {
        luaL_error(State, "Incorrect first argument expected stream");
    }

    if (!GetIntegerArg(&Length, State, 3))
    {
        luaL_error(State, "Incorrect second argument expected pointer");
    }

    lua_pop(State, 1);

    void* Pointer = Read(&Stream, Length);

    UpdateReadPointer(State, Stream.ReadPointer);

    lua_pushlightuserdata(State, Pointer);

    return (0);
}

int
LuaStreamReadSigned(lua_State* State)
{
    stream Stream;
    s64    BitCount;

    if (!LoadStream(&Stream, State, 1))
    {
        luaL_error(State, "Incorrect first argument expected stream");
    }

    if (!GetIntegerArg(&BitCount, State, 2))
    {
        luaL_error(State, "Incorrect second argument expected integer");
    }

    lua_pop(State, 1);

    s64 Value;

    if (BitCount == 8)
    {
        Value = Read<u8>(&Stream);
    }
    else if (BitCount == 16)
    {
        Value = Read<u16>(&Stream);
    }
    else if (BitCount == 32)
    {
        Value = Read<u32>(&Stream);
    }
    else if (BitCount == 64)
    {
        Value = Read<u64>(&Stream);
    }
    else
    {
        luaL_error(State, "Incorrect bit size");
    }

    UpdateReadPointer(State, Stream.ReadPointer);

    lua_pushinteger(State, Value);

    return (0);
}

int
LuaStreamReadUnsigned(lua_State* State)
{
    stream Stream;
    s64    BitCount;

    if (!LoadStream(&Stream, State, 1))
    {
        luaL_error(State, "Incorrect first argument expected stream");
    }

    if (!GetIntegerArg(&BitCount, State, 2))
    {
        luaL_error(State, "Incorrect second argument expected integer");
    }

    lua_pop(State, 1);

    s64 Value;

    if (BitCount == 8)
    {
        Value = Read<u8>(&Stream);
    }
    else if (BitCount == 16)
    {
        Value = Read<u16>(&Stream);
    }
    else if (BitCount == 32)
    {
        Value = Read<u32>(&Stream);
    }
    else if (BitCount == 64)
    {
        Value = Read<u64>(&Stream);
    }
    else
    {
        luaL_error(State, "Incorrect bit size");
    }

    UpdateReadPointer(State, Stream.ReadPointer);

    lua_pushinteger(State, Value);

    return (0);
}

int
LuaStreamSetBuffer(lua_State* State)
{
    stream Stream;
    void*  NewBuffer;
    s64    NewBufferLength;

    if (!LoadStream(&Stream, State, 1))
    {
        luaL_error(State, "set_buffer: Incorrect first argument expected stream");
    }

    if (!GetPointerArg(&NewBuffer, State, 2))
    {
        luaL_error(State, "set_buffer: Incorrect second argument expected integer");
    }

    if (!GetIntegerArg(&NewBufferLength, State, 3))
    {
        luaL_error(State, "set_buffer: Incorrect third argument expected integer");
    }

    lua_pop(State, 2);

    SetBuffer(&Stream, (char*)NewBuffer, NewBufferLength);

    UpdateBufferPointer(State, Stream.Buffer);
    UpdateLength(State, Stream.Length);

    return (0);
}

int
LuaStreamResetBuffer(lua_State* State)
{
    stream Stream;

    if (!LoadStream(&Stream, State, 1))
    {
        luaL_error(State, "set_buffer: Incorrect first argument expected stream");
    }

    ResetStream(&Stream);

    UpdateWritePointer(State, Stream.WritePointer);
    UpdateReadPointer(State, Stream.ReadPointer);

    return (0);
}

b32
LoadStream(stream* Stream, lua_State* State, int Index)
{
    int InputType = lua_type(State, Index);

    if (InputType != LUA_TTABLE)
    {
        return (false);
    }

    lua_getfield(State, Index, "length");
    lua_getfield(State, Index, "write_location");
    lua_getfield(State, Index, "read_location");
    lua_getfield(State, Index, "memory_callback");
    lua_getfield(State, Index, "buffer");

    s64 Length;
    s64 WritePointer;
    s64 ReadPointer;

    if (!GetIntegerArg(&Length, State, -5))
    {
        return (false);
    }

    if (!GetIntegerArg(&WritePointer, State, -4))
    {
        return (false);
    }

    if (!GetIntegerArg(&ReadPointer, State, -3))
    {
        return (false);
    }

    Stream->MemoryCallback = (memory_callback**)lua_touserdata(State, -2);
    Stream->Buffer         = (char*)lua_touserdata(State, -1);
    Stream->Length         = Length;
    Stream->ReadPointer    = ReadPointer;
    Stream->WritePointer   = WritePointer;

    lua_pop(State, 5);

    return (Stream->MemoryCallback || Stream->Buffer);
}
