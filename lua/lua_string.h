#if !defined(LUA_STRING_H)

#include "lua_wrapper.h"

void LoadStringFunctions(lua_State* State);

#define LUA_STRING_H
#endif
