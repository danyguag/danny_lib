#if !defined(LUA_WRAPPER_H)

#include <std/base.h>

extern "C"
{
#include <lauxlib.h>
#include <lua.h>
#include <lualib.h>
}

b32 GetFloatArg(f64* Value, lua_State* State, s32 Index);
b32 GetIntegerArg(s64* Value, lua_State* State, s32 Index);
b32 GetStringArg(string* String, lua_State* State, s32 Index);
b32 GetPointerArg(void** Pointer, lua_State* State, s32 Index);

void BeginLuaStruct(lua_State* State, u32 VariableCount);
void PushIntegerVariable(lua_State* State, const char* VariableName, s64 Value);
void PushStringVariable(lua_State* State, const char* VariableName, const char* Variable);
void PushFunctionVariable(lua_State* State, const char* VariableName, lua_CFunction Function);

void PushPointer(lua_State* State, const char* VariableName, void* Pointer);

void PrintStack(lua_State* State);

#define LUA_WRAPPER_H
#endif
