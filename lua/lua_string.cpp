#include "lua_string.h"

#include <std/string.h>

int CreateEmptyString(lua_State* State);
int CreateString(lua_State* State);
int LuaEndsWith(lua_State* State);
int LuaStartsWith(lua_State* State);
int LuaStandardPrint(lua_State* State);
int LuaFreeString(lua_State* State);
int LuaCopyString(lua_State* State);
int LuaIntegerToString(lua_State* State);
int LuaFloatToString(lua_State* State);
int LuaStringToInteger(lua_State* State);
int LuaStringToFloat(lua_State* State);
int LuaStringContains(lua_State* State);
int LuaStringIndexOf(lua_State* State);

b32 LoadString(string* String, lua_State* State, int Index);

void
PushString(lua_State* State, u64 Length, u64 Capacity, char* Buffer)
{
    BeginLuaStruct(State, 3);
    PushIntegerVariable(State, "length", Length);
    PushIntegerVariable(State, "capacity", Capacity);
    PushPointer(State, "buffer", Buffer);
}

void
LoadStringFunctions(lua_State* State)
{
    lua_register(State, "create_empty_string", CreateEmptyString);
    lua_register(State, "create_string", CreateString);

    lua_register(State, "ends_with", LuaEndsWith);
    lua_register(State, "starts_with", LuaStartsWith);

    lua_register(State, "integer_to_string", LuaIntegerToString);
    lua_register(State, "float_to_string", LuaFloatToString);
    lua_register(State, "string_to_integer", LuaStringToInteger);
    lua_register(State, "string_to_float", LuaStringToFloat);

    lua_register(State, "contains", LuaStringContains);
    lua_register(State, "index_of", LuaStringIndexOf);

    lua_register(State, "standard_print", LuaStandardPrint);
    lua_register(State, "free_string", LuaFreeString);
    lua_register(State, "copy_string", LuaCopyString);
}

int
CreateEmptyString(lua_State* State)
{
    PushString(State, 0, 0, NULL);

    return (1);
}

int
CreateString(lua_State* State)
{
    string String;

    if (!GetStringArg(&String, State, 1))
    {
        return (CreateEmptyString(State));
    }

    PushString(State, String.Length, String.Capacity, String.Buffer);

    return (1);
}

int
LuaEndsWith(lua_State* State)
{
    string First;
    string Second;

    if (!LoadString(&First, State, 1))
    {
        luaL_error(State, "Missing first argument");
    }

    if (!LoadString(&Second, State, 2))
    {
        luaL_error(State, "Missing second argument");
    }

    lua_pushboolean(State, EndsWith(First, Second));

    return (1);
}

int
LuaStartsWith(lua_State* State)
{
    string First;
    string Second;

    if (!LoadString(&First, State, 1))
    {
        luaL_error(State, "Missing first argument");
    }

    if (!LoadString(&Second, State, 2))
    {
        luaL_error(State, "Missing second argument");
    }

    lua_pushboolean(State, StartsWith(First, Second));

    return (1);
}

int
LuaStandardPrint(lua_State* State)
{
    string String;

    if (!LoadString(&String, State, 1))
    {
        luaL_error(State, "Missing string argument");
    }

    StandardPrint(String);

    return (0);
}

int
LuaFreeString(lua_State* State)
{
    string String;

    if (!LoadString(&String, State, 1))
    {
        luaL_error(State, "Missing string argument");
    }

    FreeString(String);

    return (0);
}

int
LuaCopyString(lua_State* State)
{
    string StringToCopy;

    if (!LoadString(&StringToCopy, State, 1))
    {
        luaL_error(State, "Missing string argument");
    }

    string Copy;
    CopyString(&Copy, StringToCopy);
    PushString(State, Copy.Length, Copy.Capacity, Copy.Buffer);

    return (1);
}

int
LuaIntegerToString(lua_State* State)
{
    s64 Integer;

    if (!GetIntegerArg(&Integer, State, 1))
    {
        luaL_error(State, "Missing integer argument");
    }

    string IntegerString;
    S64ToString(&IntegerString, Integer);
    PushString(State, IntegerString.Length, IntegerString.Capacity, IntegerString.Buffer);

    return (1);
}

int
LuaFloatToString(lua_State* State)
{
    f64 Float;

    if (!GetFloatArg(&Float, State, 1))
    {
        luaL_error(State, "Missing float argument");
    }

    string FloatString;
    F64ToString(&FloatString, Float);
    PushString(State, FloatString.Length, FloatString.Capacity, FloatString.Buffer);

    return (1);
}

int
LuaStringToInteger(lua_State* State)
{
    string String;

    if (!LoadString(&String, State, 1))
    {
        luaL_error(State, "Missing string argument");
    }

    s64 Number;

    if (!StringToS64(&Number, String))
    {
        luaL_error(State, "Failed to convert string to integer");
    }

    lua_pushinteger(State, Number);

    return (1);
}

int
LuaStringToFloat(lua_State* State)
{
    string String;

    if (!LoadString(&String, State, 1))
    {
        luaL_error(State, "Missing string argument");
    }

    f64 Number;

    if (!StringToF64(&Number, String))
    {
        luaL_error(State, "Failed to convert string to float");
    }

    lua_pushnumber(State, Number);

    return (1);
}

int
LuaStringContains(lua_State* State)
{
    string Whole;
    string Part;

    if (!LoadString(&Whole, State, 1))
    {
        luaL_error(State, "Missing first string argument");
    }

    if (!LoadString(&Part, State, 1))
    {
        luaL_error(State, "Missing second string argument");
    }

    lua_pushboolean(State, Contains(Whole, Part));

    return (1);
}

int
LuaStringIndexOf(lua_State* State)
{
    string Whole;
    string Part;

    if (!LoadString(&Whole, State, 1))
    {
        luaL_error(State, "Missing first string argument");
    }

    if (!LoadString(&Part, State, 1))
    {
        luaL_error(State, "Missing second string argument");
    }

    lua_pushinteger(State, IndexOf(Whole, Part));

    return (1);
}

b32
LoadString(string* String, lua_State* State, int Index)
{
    int InputType = lua_type(State, Index);

    if (InputType == LUA_TSTRING)
    {
        return (GetStringArg(String, State, Index));
    }

    if (InputType != LUA_TTABLE)
    {
        return (false);
    }

    s64 Length;
    s64 Capacity;

    lua_getfield(State, Index, "length");
    lua_getfield(State, Index, "capacity");
    lua_getfield(State, Index, "buffer");

    if (!GetIntegerArg(&Length, State, -3))
    {
        return (false);
    }

    if (!GetIntegerArg(&Capacity, State, -2))
    {
        return (false);
    }

    String->Buffer   = (char*)lua_touserdata(State, -1);
    String->Length   = Length;
    String->Capacity = Capacity;

    lua_pop(State, 3);

    return (String->Buffer ? true : false);
}
