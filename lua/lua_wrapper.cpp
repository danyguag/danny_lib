#include "lua_wrapper.h"

static void PrintItem(lua_State* State, s32 Index, b32 Wrap);

b32
GetFloatArg(f64* Value, lua_State* State, s32 Index)
{
    if (!Value)
    {
        return (false);
    }

    b32 ReturnCode;
    *Value = lua_tonumberx(State, Index, &ReturnCode);

    return (ReturnCode);
}

b32
GetIntegerArg(s64* Value, lua_State* State, s32 Index)
{
    if (!Value)
    {
        return (false);
    }

    b32 ReturnCode;
    *Value = lua_tointegerx(State, Index, &ReturnCode);

    return (ReturnCode);
}

b32
GetStringArg(string* String, lua_State* State, s32 Index)
{
    if (!String)
    {
        return (false);
    }

    size_t      Length;
    const char* Buffer = lua_tolstring(State, Index, &Length);

    *String = string((char*)Buffer, Length);

    return (Buffer ? true : false);
}

b32
GetPointerArg(void** Pointer, lua_State* State, s32 Index)
{
    if (!Pointer)
    {
        return (false);
    }

    *Pointer = lua_touserdata(State, Index);

    return (*Pointer ? true : false);
}

void
BeginLuaStruct(lua_State* State, u32 VariableCount)
{
    UNUSED_VARIABLE(VariableCount);
    lua_newtable(State);
    //    lua_createtable(State, VariableCount, VariableCount);
}

void
PushIntegerVariable(lua_State* State, const char* VariableName, s64 Value)
{
    lua_pushstring(State, VariableName);
    lua_pushinteger(State, (lua_Integer)Value);
    lua_settable(State, -3);
}

void
PushStringVariable(lua_State* State, const char* VariableName, const char* Variable)
{
    lua_pushstring(State, VariableName);
    lua_pushstring(State, Variable);
    lua_settable(State, -3);
}

void
PushFunctionVariable(lua_State* State, const char* VariableName, lua_CFunction Function)
{
    lua_pushstring(State, VariableName);
    lua_pushcclosure(State, Function, 0);
    lua_settable(State, -3);
}

void
PushPointer(lua_State* State, const char* VariableName, void* Pointer)
{
    lua_pushstring(State, VariableName);
    lua_pushlightuserdata(State, Pointer);
    lua_settable(State, -3);
}

static int
IsSeq(lua_State* State, s32 Index)
{
    lua_pushnil(State);

    s32 KeyIndex = 1;

    while (lua_next(State, Index))
    {
        lua_rawgeti(State, Index, KeyIndex);

        if (lua_isnil(State, -1))
        {
            lua_pop(State, 3);
            return 0;
        }

        lua_pop(State, 2);
        KeyIndex++;
    }

    return 1;
}

static void
PrintSeq(lua_State* State, int Index)
{
    StandardPrint(string("{"));

    for (s32 StackIndex = 1;; ++StackIndex)
    {
        lua_rawgeti(State, Index, StackIndex);

        if (lua_isnil(State, -1))
        {
            break;
        }

        if (StackIndex > 1)
        {
            StandardPrint(string(", "));
        }

        PrintItem(State, -1, false);
        lua_pop(State, 1);
    }

    lua_pop(State, 1);

    StandardPrint(string("}"));
}

static void
PrintTable(lua_State* State, s32 Index)
{
    if (Index < 0)
    {
        Index = lua_gettop(State) + Index + 1;
    }

    string Prefix = string("{");

    if (IsSeq(State, Index))
    {
        PrintSeq(State, Index);
    }
    else
    {
        lua_pushnil(State);

        while (lua_next(State, Index))
        {
            StandardPrint(Prefix);

            PrintItem(State, -2, true);
            StandardPrint(string(" = "));
            PrintItem(State, -1, false);
            lua_pop(State, 1);

            Prefix = ", ";
        }

        StandardPrint(string("}"));
    }
}

static void
GetFunctionName(string* FunctionName, lua_State* State, s32 Index)
{
    FormatString(FunctionName, "%u64", (u64)lua_topointer(State, Index));
}

static void
PrintItem(lua_State* State, s32 Index, b32 Wrap)
{
    int ItemType = lua_type(State, Index);

    switch (ItemType)
    {
        case LUA_TNIL:
        {
            string Message = (char*)"nil";
            StandardPrint(Message);

            return;
        }
        case LUA_TNUMBER:
        {
            string Message;

            FormatString(&Message, "[%f64]", lua_tonumber(State, Index));
            StandardPrint(Message);
            FreeString(Message);

            return;
        }
        case LUA_TBOOLEAN:
        {
            string BooleanName = (char*)(lua_toboolean(State, Index) ? "true" : "false");

            StandardPrint(BooleanName);

            return;
        }
        case LUA_TSTRING:
        {
            string String = (char*)lua_tostring(State, Index);

            if (Wrap)
            {
                string Message;

                FormatString(&Message, "\'%str\'", String);
                StandardPrint(Message);
                FreeString(Message);
            }
            else
            {
		StandardPrint(String);
            }
	    
            return;
        }
        case LUA_TTABLE:
        {
            PrintTable(State, Index);

            return;
        }
        case LUA_TFUNCTION:
        {
            string Message;
            string FunctionString;

            GetFunctionName(&FunctionString, State, Index);
            FormatString(&Message, "[%str]", FunctionString);
            StandardPrint(Message);
            FreeString(Message);

            return;
        }
        case LUA_TUSERDATA:
        case LUA_TLIGHTUSERDATA:
        {
            string Message = "pointer: ";

            StandardPrint(Message);

            break;
        }
        case LUA_TTHREAD:
        {
            string Message = "thread: ";

            StandardPrint(Message);

            break;
        }
        default:
        {
            string Message = "<internal_error_in_print_stack_item!>";

            StandardPrint(Message);

            return;
        }
    }

    string Message;

    FormatString(&Message, "%u64", (u64)lua_topointer(State, Index));
    StandardPrint(Message);
    FreeString(Message);
}

void
PrintStack(lua_State* State)
{
    int StackCount = lua_gettop(State);
    StandardPrint("Stack:");

    for (s32 Index = 1; Index <= StackCount; ++Index)
    {
        StandardPrint(" ");
        PrintItem(State, Index, false);
    }

    if (StackCount == 0)
    {
        StandardPrint(" <empty>");
    }

    printf("\n");
}
