#if !defined(DANNY_LUA_H)

#include "lua_wrapper.h"

void LoadDannyStandardLibs(lua_State* State);

#define DANNY_LUA_H
#endif
