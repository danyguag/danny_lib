#include "danny_lua.h"

#include "lua_stream.h"
#include "lua_string.h"

void
LoadDannyStandardLibs(lua_State* State)
{
    LoadStringFunctions(State);
    LoadStreamFunctions(State);
}
