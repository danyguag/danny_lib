#if !defined (LUA_STREAM_H)

#include "lua_wrapper.h"

void LoadStreamFunctions(lua_State* State);

inline void
UpdateWritePointer(lua_State* State, s64 WritePointer)
{
    lua_pushinteger(State, WritePointer);
    lua_setfield(State, 1, "write_location");
}

inline void
UpdateReadPointer(lua_State* State, s64 ReadPointer)
{
    lua_pushinteger(State, ReadPointer);
    lua_setfield(State, 1, "read_location");
}

inline void
UpdateLength(lua_State* State, s64 Length)
{
    lua_pushinteger(State, Length);
    lua_setfield(State, 1, "length");
}

inline void
UpdateBufferPointer(lua_State* State, void* Pointer)
{
    lua_pushlightuserdata(State, Pointer);
    lua_setfield(State, 1, "buffer");
}

#define LUA_STREAM_H
#endif
